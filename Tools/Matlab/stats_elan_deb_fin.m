%% MEMO - Stats ContextODD EEG - Elan permutation tests
% Typical mismatch analysis : dev vs. dev in condition  var_c and var_p
% separately - Each condition was delivered twice
% HERE : contrasting MMN in beginning of bloc (deb) and ends (fin)
% MNE : script ana_erp_deb_fin.py, 

% 1) Export MNe evoked files into mat files (done with script
% stats_comp_mne_elan.py in
% ContextOdd_Git/Tools/Python/EEG/ana_surprise_var_c.py
% 2) From matfiles, create Elan *.p files (done here)
% 3) using Elan bash comman lines do the stats (here and linux script )


addpath('/sps/cermep/cermep/experiments/DCM/Dycog_prog/elan_matlab');


%% Create *.p files
path_mne = '/sps/cermep/cermep/experiments/DCM/ContextOdd/Ana_MNE';
erp_folder = 'erp_2-20Hz';
elan_folder = 'stats_elan';
subj_id_list = {'00', '01','02','03','04','05','06','07','08','09',...
                '10','11','12','13','14','15','16','17','18','19',...
                '20','21','22','23','24','25','26','27','28','29',...
                '30','31','32','33','34','35'   };


mat_list = {    '2-20Hz_deb_var_c_std',  '2-20Hz_deb_var_c_dev','2-20Hz_deb_var_c_mmn' , ...
                '2-20Hz_fin_var_c_std',  '2-20Hz_fin_var_c_dev','2-20Hz_fin_var_c_mmn' , ...
                '2-20Hz_deb_var_p_std',  '2-20Hz_deb_var_p_dev','2-20Hz_deb_var_p_mmn' , ...
                '2-20Hz_fin_var_p_std',  '2-20Hz_fin_var_p_dev','2-20Hz_fin_var_p_mmn'                } ;
% we first do a first subject to get ElecDat indices ...
i_su = 1; subj = ['sub-' subj_id_list{i_su}];
i_mat = 1;
f_mat = fullfile(path_mne,  subj,  erp_folder, elan_folder,  [subj '.' mat_list{i_mat} '.mat']);
a = load(f_mat); % gives d = data (sens*samples), times= vector of time bins, sfreq=sample freq and sens = channel names
nr_sens = size(a.d,1);
f_elecdat = '/pbs/throng/cermep/bin/Elan/elec/elec.dat';
[ind, x, y, name] = textread(f_elecdat, '%d %f %f  %s');
elecdat_ind_REF =zeros(nr_sens,1);
for i_se =1:nr_sens
    ch_name = strtrim(a.sens(i_se,:)); 
    [u] = find(contains(name, ch_name));
    for i=1:length(u)
        
        if isequal(ch_name, name{u(i)})
            ind_ch = u(i);
            elecdat_ind_REF(i_se) = ind(ind_ch);
            break
            
        end
    end
    disp([ch_name ':   ' name{u(i)} '--->'  num2str(elecdat_ind_REF(i_se))]);
end


% Now we apply to all

for i_su =1:numel(subj_id_list)
    subj = ['sub-' subj_id_list{i_su}];    
    for i_mat = 1:numel(mat_list)
        %load data
        f_mat = fullfile(path_mne,  subj,  erp_folder, elan_folder,  [subj '.' mat_list{i_mat} '.mat']);
        a = load(f_mat); % gives d = data (sens*samples), times= vector of time bins, sfreq=sample freq and sens = channel names
        nr_sens = size(a.d,1);
        nr_samp = size(a.d,2);
        % create ep file
        hdr1_eventcode=123;
        hdr2_nbeventaver=123;
        % ----- sample rate
        Fech=a.sfreq;
        % ----- number of pre-sttim samples
        h = find(a.times==0)
        Nprestim=h-1 ; 
        % -----  save ! 
        f_ep = fullfile(path_mne,  subj,  erp_folder, elan_folder,  [subj '.' mat_list{i_mat} '.p']);        
        mat2ep (f_ep, hdr1_eventcode, Fech, Nprestim, elecdat_ind_REF, hdr2_nbeventaver,a.d');
    end 
end

% and to the group
subj = 'group36';
for i_mat = 1:numel(mat_list)
    %load data
    f_mat = fullfile(path_mne,  subj,  erp_folder, elan_folder,  [subj '.' mat_list{i_mat} '.mat']);
    a = load(f_mat); % gives d = data (sens*samples), times= vector of time bins, sfreq=sample freq and sens = channel names
    nr_sens = size(a.d,1);
    nr_samp = size(a.d,2);
    % create ep file
    hdr1_eventcode=123;
    hdr2_nbeventaver=123;
    % ----- sample rate
    Fech=a.sfreq;
    % ----- number of pre-sttim samples
    h = find(a.times==0)
    Nprestim=h-1 ;
    % -----  save !
    f_ep = fullfile(path_mne,  subj,  erp_folder, elan_folder,  [subj '.' mat_list{i_mat} '.p']);
    mat2ep (f_ep, hdr1_eventcode, Fech, Nprestim, elecdat_ind_REF, hdr2_nbeventaver,a.d');
end



%% do the stats ! ---> epranddiff --->  write epranddiff file
% linux
% script:/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Scripts/qsub_epranddiff,memo_stats_comp_mne_elan
% 

% write epranddiff file
subj = 'group36';
subj_id_list = {'00', '01','02','03','04','05','06','07','08','09',...
                '10','11','12','13','14','15','16','17','18','19',...
                '20','21','22','23','24','25','26','27','28','29',...
                '30','31','32','33','34','35'   };
nr_subj = numel(subj_id_list);

% DEV vs. STD ,   -----------------------------------
list = { 'deb_var_c', 'fin_var_c', 'deb_var_p', 'fin_var_p'};

for i =1:numel(list)
    contrast =  {['2-20Hz_' list{i} '_dev'],['2-20Hz_' list{i} '_std']} ;
    contrast_name = ['2-20Hz_' list{i} '_dev_VS_std'];
    
    f_epranddiff = fullfile(path_mne,  subj,  erp_folder, elan_folder,  ['epranddiff_' contrast_name '.par']);
    fid = fopen(f_epranddiff, 'w');
    fprintf(fid, '1 2 %d\n', nr_subj);
    for i_cond = 1:numel(contrast)
        for i_su = 1:nr_subj
            f_ep = fullfile(path_mne, ['sub-' subj_id_list{i_su}],  erp_folder, elan_folder,  ['sub-' subj_id_list{i_su} '.' contrast{i_cond} '.p']);
            fprintf(fid, 'fact1 \t cond%d \t  suj%d \t %s\n', i_cond, i_su, f_ep);
        end
    end
    fclose(fid);
end


% CONTRASTS -------------------------------------
% contrasting b1 and b2 : separately in (dev, mm, std), and in (var_p,
% var_c)
list_stim = { 'std', 'dev', 'mmn'};
list_cond = { 'var_c', 'var_p'};

for i =1:numel(list_stim)
    for i_cond = 1:numel(list_cond)
        contrast =  {['2-20Hz_deb_' list_cond{i_cond} '_' list_stim{i}],['2-20Hz_fin_' list_cond{i_cond} '_' list_stim{i}] } ;
        contrast_name = ['2-20Hz_deb_VS_fin_' list_cond{i_cond} '_' list_stim{i}];
        
        f_epranddiff = fullfile(path_mne,  subj,  erp_folder, elan_folder,  ['epranddiff_' contrast_name '.par']);
        fid = fopen(f_epranddiff, 'w');
        fprintf(fid, '1 2 %d\n', nr_subj);
        for i_cond = 1:numel(contrast)
            for i_su = 1:nr_subj
                f_ep = fullfile(path_mne, ['sub-' subj_id_list{i_su}],  erp_folder, elan_folder,  ['sub-' subj_id_list{i_su} '.' contrast{i_cond} '.p']);
                fprintf(fid, 'fact1 \t cond%d \t  suj%d \t %s\n', i_cond, i_su, f_ep);
            end
        end
        fclose(fid);
    end
    
end



%% Plots stat maps
addpath('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Matlab');
cd('/sps/cermep/cermep/experiments/DCM/ContextOdd/Ana_MNE/group36/erp_2-20Hz/stats_elan')
gain = 1000;
Vmax = 0.01;
Dur_mini = 1;

% ---------  mismatch in every condition
liste={  '2-20Hz_var_c_dev_VS_std_th001_100000_0-410' ...
        '2-20Hz_var_c_c+_dev_VS_std_th001_100000_410' ...
        '2-20Hz_var_c_c-_dev_VS_std_th001_100000_410' };
for i=1: numel(liste)
    
    ficp=[liste{i} '.rand.mask.p.p']; %mask <=> multiple testing correction, minimum consecutive samples
    codd_view_pFile_image_clean(ficp,gain,Vmax,Dur_mini)
end


% contrastes
pref = '2-20Hz_deb_VS_fin_var_p'; % var_c
stim = {'std', 'dev', 'mmn'};
woi={'-80_0' '0_80', '95_200', '230_290'};

% go!
clear liste; cpt=1   ; 
for s=1:numel(stim)
    for w= 1:numel(woi)
        liste{cpt} = [ pref '_' stim{s} '_th001_100000_' woi{w} ];
        cpt=cpt+1;
    end
end
for i=1: numel(liste)
    ficp=[liste{i} '.rand.mask.p.p']; %mask <=> multiple testing correction, minimum consecutive samples
    try
        codd_view_pFile_image_clean(ficp,gain,Vmax,Dur_mini)
        
    end
end

