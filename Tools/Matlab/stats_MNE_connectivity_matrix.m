%% Try to compute a connectivity matrix for MNE spatio-temporal clusterin
% informed by biophysical gain matrix (instead of euclidian or geodesic
% distances between surface electrodes)

%% Principle
% Load BEM measured for 20 subjects during my phd (openmeeg)
% Select relevant electrodes (ContextOdd 32 sensors / MMNDCM 64 electrodes)
% Compute the  L*L' for each subject, take the absolute value, 
% average across subjects
% define a threshold to keep like 4-5 neighbourgs ...

%% Paths
addpath('/sps/cermep/cermep/experiments/DCM/Dycog_prog/elan_matlab');
path_mne = '/sps/cermep/cermep/experiments/DCM/ContextOdd/Ana_MNE';
erp_folder = 'erp_2-20Hz';
elan_folder = 'stats_elan';

%% MNE sensors
% We take a file exported into mat (see script stats_elan*.mat)
% Get sensor names, and order
subj = 'sub-00' ;
f_mat = fullfile(path_mne,  subj,  erp_folder, elan_folder,  [subj '.2-20Hz_var_p_p-_std.mat']);
a = load(f_mat); % gives d = data (sens*samples), times= vector of time bins, sfreq=sample freq and sens = channel names
nr_sens = size(a.d,1);
mne_sens = a.sens; % ch_name = strtrim(a.sens(i_se,:)); 

for i=1:nr_sens
    disp([num2str(i) '  ' mne_sens(i,:)]);
end

%% SPM BEM
% we take original OpenMeeg BEM computed dureing my phd (MMNDCM)
% we take raw BEM (no re-referencing matoids or avg)
% these files were initially computed in recontruction_2-45, and imported
% here from my external disk backup
% (I've checked, fine - )
%
% since Oz was not recorded during my PhD , I replace it here by Iz
% (approximation that should not corrupt neighboring computaions)

dirmat='/sps/cermep/cermep/experiments/DCM/manip1/outils/matlab';
cd([dirmat '/mysetpath'] );
setDCMpath_fusedDCM_spm12  % spm12 version for SPM, with toolbox/dcm_meeg from spm12 and adapted for fused DCM
spm('defaults','eeg');
addpath('/sps/cermep/cermep/experiments/DCM/manip1/outils/matlab/mmndcm_ana_DCM');


tabsuj={ 'pil01' 'suj01' 'suj02'  'suj05' 'suj06'  'suj08' 'suj09'   'suj11'  'suj13' 'suj14'  'suj16' 'suj17' 'suj18' 'suj19' 'suj20' 'suj21' 'suj22' 'suj23' 'suj24' 'suj26'};
% I remove pil01, subj1,2,5,6 because they don(t have the same nr of
% sensors :
tabsuj={   'suj08' 'suj09'   'suj11'  'suj13' 'suj14'  'suj16' 'suj17' 'suj18' 'suj19' 'suj20' 'suj21' 'suj22' 'suj23' 'suj24' 'suj26'};
dirbem = '/sps/cermep/cermep/experiments/DCM/ContextOdd/OpenMeeg'
cond = 'RFRI_2-45' ;

LLi_all = zeros(nr_sens,nr_sens);
for i=1:numel(tabsuj)
    disp(tabsuj{i});
    bem_file = [ dirbem '/SPMgainmatrix_ucccc' tabsuj{i} '_' cond '_mmn_meg_1.mat'    ];
    f = load(bem_file); % G = gain matrix, label = channel names
    idx = zeros(nr_sens,1);
    for i_se = 1:nr_sens
        ch_name = strtrim(mne_sens(i_se,:));
        if isequal(ch_name, 'Oz')
            [a, b] = ismember({'Iz'}, f.label)    ;       
            idx(i_se) = b;
            disp([ch_name '   ' num2str(b) '   ' f.label{b}]); 
        else            
            [a, b] = ismember({ch_name}, f.label)  ;         
            idx(i_se) = b;
            disp([ch_name '   ' num2str(b) '   ' f.label{b}]);            
        end        
        
    end
    Li = f.G(idx,:);
    LLi = Li*Li';
    LLi_all = LLi_all + LLi;
 
end
LLi_all = LLi_all / numel(tabsuj); % avf across subjeccts of L.L'
figure; imagesc(LLi_all); colorbar;


%% Threshold LL'
L = LLi_all;


% is LLi_all(i,i) the maximum value for sensor ci?
% nr_sens = nr_sens;
% for i=1:nr_sens
%     [a, b] = max(L(i,:));
%     disp(num2str(b));
% end

Lnorm = abs(L);
for i=1:nr_sens
    [vmax, smax] = max(Lnorm(i,:));
    nL = Lnorm(i,:)./vmax;
    Lnorm(i,:) = nL;
    
end
figure; imagesc(Lnorm)
colorbar  


thresh = 0.85;
h=find(Lnorm < thresh);
Lnorm_th = Lnorm.*0+1;
Lnorm_th(h)=0;
figure; imagesc(Lnorm_th)
colorbar 




connectivity = Lnorm_th;
save( '/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Reference_Files/Connectivity_15subj_th85_lefttop=Fp1.mat', 'connectivity')

% connectivity=zeros(nr_sens, nr_sens);
% for i=1:nr_sens
%     connectivity(i,:) = Lnorm_th(nr_sens-i+1,:);
% end
% figure; imagesc(connectivity)
% colorbar 
% %connectivity = Lnorm_th;
% save( '/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Reference_Files/Connectivity_15subj_th85_leftbottom=Fp1.mat', 'connectivity')


%% Problem ! MNE requires symetry ...
sym_conn = connectivity + connectivity';
sym_conn(logical(eye(size(sym_conn))))=1;
figure; imagesc(sym_conn)
colorbar 
connectivity = sym_conn;
save( '/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Reference_Files/Connectivity_15subj_th85_sym.mat', 'connectivity')


%% grenier
% i=3;
% d_file = [dirsrc  '/uccccc' tabsuj{i} '_' cond '_refmasto_mmn_meg.mat'    ];
% D = spm_eeg_load(d_file);    %  ucccccsuj08_RFRI_2-45_mmn_meg.mat
% f_gain = D.inv{1}.gainmat;
% l{1} = load([dirsrc '/' f_gain]); % G = gain matrix, label = channel names
% bem_file = [ dirbem '/SPMgainmatrix_ucccc' tabsuj{i} '_' cond '_mmn_meg_1.mat'    ];
% l{2} = load(bem_file);
% bem_file = [ dirbem '/SPMgainmatrix_ucc' tabsuj{i} '_RFPF_2-45_mmn_meg_1.mat'    ];
% l{3}= load(bem_file);
% for k=1:3
%     ll = l{k};
%     G = ll.G;
%     label = ll.label;
%     idx = zeros(nr_sens,1);
%     for i_se = 1:nr_sens
%         ch_name = strtrim(mne_sens(i_se,:));
%         if isequal(ch_name, 'Oz')
%             [a, b] = ismember({'Iz'}, label)    ;
%             idx(i_se) = b;
%             disp([ch_name '   ' num2str(b) '   ' label{b}]);
%         else
%             [a, b] = ismember({ch_name}, label)  ;
%             idx(i_se) = b;
%             disp([ch_name '   ' num2str(b) '   ' label{b}]);
%         end
%         
%     end
%     L{k} = G(idx,:);
% end
% G = L{1};
% plot(G([17,22], :)')
% figure;
% G = L{3};
% plot(G([17,22], :)')