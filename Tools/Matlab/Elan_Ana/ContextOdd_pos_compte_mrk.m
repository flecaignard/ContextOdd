function [codemrk, nbmrk, nbmrkok, pcok]=ContextOdd_pos_compte_mrk(ficpos)




%disp(['.............. ' ficpos '..............']);

pos=load(ficpos);
code=pos(:,2);
rej=pos(:,3);

codemrk=[];
nbmrk=[];
nbmrkrej=[];
pcok=[];

cpt=0;
while length(code)>0
    
    cpt=cpt+1;
    c=code(1);
    h=find(code==c);    
    hh=find(rej(h)==8);
   
    codemrk(cpt)=c;
    nbmrk(cpt)=length(h);
    nbmrkok(cpt)=length(h)-length(hh);
    pcok(cpt)=floor(100*(length(h)-length(hh))/length(h));
    
    
    code(h)=[];
    rej(h)=[];
    
    if isempty(code)
        break
    end
end