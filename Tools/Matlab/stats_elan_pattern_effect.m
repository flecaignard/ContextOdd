%% MEMO - Stats ContextODD EEG - Elan permutation tests
% Typical mismatch analysis : dev vs. dev in condition  var_c and var_p
% separately - Each condition was delivered twice
% HERE : we contrast the two ways of grouping chunks together: either tight (low fluctuations) or loose (high ones)
% we focus on chunks c4 and c6, being common to these two categories
% contexts var_p and var_c have been pooled together
% script python ... ana_erp_pattern_effect.py

% 1) Export MNe evoked files into mat files (done with script
% stats_comp_mne_elan.py in
% ContextOdd_Git/Tools/Python/EEG/ana_surprise_var_c.py
% 2) From matfiles, create Elan *.p files (done here)
% 3) using Elan bash comman lines do the stats (here and linux script )


addpath('/sps/cermep/cermep/experiments/DCM/Dycog_prog/elan_matlab');


%% Create *.p files
path_mne = '/sps/cermep/cermep/experiments/DCM/ContextOdd/Ana_MNE';
erp_folder = 'erp_2-20Hz';
baseline =0;
if baseline==1
    elan_folder ='stats_elan_baseline'; %
    dot_extension = '_baseline200';
else
    elan_folder = 'stats_elan'; %
    dot_extension = [];
end
subj_id_list = {'00', '01','02','03','04','05','06','07','08','09',...
                '10','11','12','13','14','15','16','17','18','19',...
                '20','21','22','23','24','25','26','27','28','29',...
                '30','31','32','33','34','35'   };

pref = '2-20Hz' ; % '2-20Hz';
mat_list = {    [ pref '_tight_46_std'], [ pref '_tight_46_dev'],    [ pref '_tight_46_mmn'], ...
                [ pref '_loose_46_std'], [ pref '_loose_46_dev'],    [ pref '_loose_46_mmn'], ...
    }
                                        
% we first do a first subject to get ElecDat indices ...
i_su = 1; subj = ['sub-' subj_id_list{i_su}];
i_mat = 1;
f_mat = fullfile(path_mne,  subj,  erp_folder, elan_folder,  [subj '.' mat_list{i_mat} dot_extension '.mat']);
a = load(f_mat); % gives d = data (sens*samples), times= vector of time bins, sfreq=sample freq and sens = channel names
nr_sens = size(a.d,1);
f_elecdat = '/pbs/throng/cermep/bin/Elan/elec/elec.dat';
[ind, x, y, name] = textread(f_elecdat, '%d %f %f  %s');
elecdat_ind_REF =zeros(nr_sens,1);
for i_se =1:nr_sens
    ch_name = strtrim(a.sens(i_se,:)); 
    [u] = find(contains(name, ch_name));
    for i=1:length(u)
        
        if isequal(ch_name, name{u(i)})
            ind_ch = u(i);
            elecdat_ind_REF(i_se) = ind(ind_ch);
            break
            
        end
    end
    disp([ch_name ':   ' name{u(i)} '--->'  num2str(elecdat_ind_REF(i_se))]);
end


% Now we apply to all

for i_su =1:numel(subj_id_list)
    subj = ['sub-' subj_id_list{i_su}];    
    for i_mat = 1:numel(mat_list)
        %load data
        f_mat = fullfile(path_mne,  subj,  erp_folder, elan_folder,  [subj '.' mat_list{i_mat} dot_extension '.mat']);
        a = load(f_mat); % gives d = data (sens*samples), times= vector of time bins, sfreq=sample freq and sens = channel names
        nr_sens = size(a.d,1);
        nr_samp = size(a.d,2);
        % create ep file
        hdr1_eventcode=123;
        hdr2_nbeventaver=123;
        % ----- sample rate
        Fech=a.sfreq;
        % ----- number of pre-sttim samples
        h = find(a.times==0)
        Nprestim=h-1 ; 
        % -----  save ! 
        f_ep = fullfile(path_mne,  subj,  erp_folder, elan_folder,  [subj '.' mat_list{i_mat} dot_extension '.p']);        
        mat2ep (f_ep, hdr1_eventcode, Fech, Nprestim, elecdat_ind_REF, hdr2_nbeventaver,a.d');
    end 
end

% and to the group
subj = 'group36';
for i_mat = 1:numel(mat_list)
    %load data
    f_mat = fullfile(path_mne,  subj,  erp_folder, elan_folder,  [subj '.' mat_list{i_mat} dot_extension '.mat']);
    a = load(f_mat); % gives d = data (sens*samples), times= vector of time bins, sfreq=sample freq and sens = channel names
    nr_sens = size(a.d,1);
    nr_samp = size(a.d,2);
    % create ep file
    hdr1_eventcode=123;
    hdr2_nbeventaver=123;
    % ----- sample rate
    Fech=a.sfreq;
    % ----- number of pre-sttim samples
    h = find(a.times==0)
    Nprestim=h-1 ;
    % -----  save !
    f_ep = fullfile(path_mne,  subj,  erp_folder, elan_folder,  [subj '.' mat_list{i_mat}  dot_extension '.p']);
    mat2ep (f_ep, hdr1_eventcode, Fech, Nprestim, elecdat_ind_REF, hdr2_nbeventaver,a.d');
end



%% do the stats ! ---> epranddiff --->  write epranddiff file
% linux
% script:/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Scripts/qsub_epranddiff,memo_stats_comp_mne_elan
% % write epranddiff file
path_mne = '/sps/cermep/cermep/experiments/DCM/ContextOdd/Ana_MNE';
erp_folder = 'erp_2-20Hz';
baseline =0;
if baseline==1
    elan_folder ='stats_elan_baseline'; %
    dot_extension = '_baseline200';
else
    elan_folder = 'stats_elan'; %
    dot_extension = [];
end
subj = 'group36';
subj_id_list = {'00', '01','02','03','04','05','06','07','08','09',...
                '10','11','12','13','14','15','16','17','18','19',...
                '20','21','22','23','24','25','26','27','28','29',...
                '30','31','32','33','34','35'   };
nr_subj = numel(subj_id_list);
pref = '2-20Hz';


% DEV vs. STD ,   -----------------------------------
list_cond={ 'tight_46', 'loose_46'};
pref = '2-20Hz'; 
for i_co = 1:numel(list_cond)
    
    
    contrast =  {[ pref '_' list_cond{i_co} '_dev'], [pref '_' list_cond{i_co} '_std']} ;
    contrast_name = [pref '_' list_cond{i_co} '_dev_VS_std'];
    
    f_epranddiff = fullfile(path_mne,  subj,  erp_folder, elan_folder,  ['epranddiff' dot_extension '_' contrast_name '.par']);
    fid = fopen(f_epranddiff, 'w');
    fprintf(fid, '1 2 %d\n', nr_subj);
    for i_cont = 1:numel(contrast)
        for i_su = 1:nr_subj
            f_ep = fullfile(path_mne, ['sub-' subj_id_list{i_su}],  erp_folder, elan_folder,  ['sub-' subj_id_list{i_su} '.' contrast{i_cont}  dot_extension '.p']);
            fprintf(fid, 'fact1 \t cond%d \t  suj%d \t %s\n', i_cont, i_su, f_ep);
        end
    end
    fclose(fid);
    
end


% CONTRASTS (compare MMN)-------------------------------------
% 

list_stim = { 'std', 'dev', 'mmn'};
list_cond = { 'tight_46_VS_loose_46'};
list_contrast = { {'tight_46', 'loose_46'}};

for i =1:numel(list_stim)
    for i_cond = 1:numel(list_cond)
        contrast_name = [ pref '_' list_cond{i_cond} '_' list_stim{i}];
        
        f_epranddiff = fullfile(path_mne,  subj,  erp_folder, elan_folder,  ['epranddiff' dot_extension '_' contrast_name '.par']);
        fid = fopen(f_epranddiff, 'w');
        fprintf(fid, '1 2 %d\n', nr_subj);
        contrast = list_contrast{i_cond};
        for i_cont = 1:numel(contrast)
            for i_su = 1:nr_subj
                f_ep = fullfile(path_mne, ['sub-' subj_id_list{i_su}],  erp_folder, elan_folder,  ['sub-' subj_id_list{i_su} '.' pref '_' contrast{i_cont} '_' list_stim{i} dot_extension '.p']);
                fprintf(fid, 'fact1 \t cond%d \t  suj%d \t %s\n', i_cont, i_su, f_ep);
            end
        end
        fclose(fid);
    end
    
end



%% Plots stat maps
addpath('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Matlab');
baseline =0;
if baseline==1
    elan_folder ='stats_elan_baseline'; %
    dot_extension = '_baseline200';
else
    elan_folder = 'stats_elan'; %
    dot_extension = [];
end


cd(['/sps/cermep/cermep/experiments/DCM/ContextOdd/Ana_MNE/group36/erp_2-20Hz/' elan_folder ] )
gain = 1000;
Vmax = 0.01;
Dur_mini = 1;

pref = '2-20Hz';
suff = 'th001_100000';

% ----------- Plot emergence --------------------
woi={'410'};
cond ={  'tight_46', 'loose_46' };
        
for i_woi =1:numel(woi)
    for i_cond=1: numel(cond)
        try
            ficp=[ pref '_' cond{i_cond} '_dev_VS_std_' suff '_' woi{i_woi} '.rand.mask.p.p']; %mask <=> multiple testing correction, minimum consecutive samples
            codd_view_pFile_image_clean(ficp,gain,Vmax,Dur_mini)
        end
    end
end




% ----------- Plot contrasts --------------------
woi={'410', '-80_0','0_80','95_200','230_290' };
contrast = 'tight_46_VS_loose_46';
stim ={  'std', 'dev', 'mmn' };
        
for i_woi =1:numel(woi)
    for i_stim=2%1: numel(stim)
        try
            ficp=[ pref '_' contrast '_'  stim{i_stim} '_' suff '_' woi{i_woi} '.rand.mask.p.p']; %mask <=> multiple testing correction, minimum consecutive samples
            codd_view_pFile_image_clean(ficp,gain,Vmax,Dur_mini)
        end
    end
end


% ----------- Plot contrasts --------One figure with all wois--------

filedir = ['/sps/cermep/cermep/experiments/DCM/ContextOdd/Ana_MNE/group36/erp_2-20Hz/stats_elan' ] ;
contrast = 'tight_46_VS_loose_46';
stim ={  'std', 'dev', 'mmn' };
        

for i_stim=1: numel(stim)
   
        basename = [ '2-20Hz_' contrast '_' stim{i_stim} ];
        codd_view_pFile_image_Frontiers(filedir, basename)
   
end


%% Print for POster HBM / Rome
addpath('/sps/cermep/cermep/experiments/DCM/Dycog_prog/elan_matlab');
addpath('/sps/cermep/cermep/experiments/DCM/Figures_Articles_Matlab');
% plot std, dev, mmn
filedir = ['/sps/cermep/cermep/experiments/DCM/ContextOdd/Ana_MNE/group36/erp_2-20Hz/stats_elan' ] ;
dirFIG = '/sps/cermep/cermep/experiments/DCM/ContextOdd/Figures'
codd_eeg_PosterRome_hbm_ERP (filedir,dirFIG,  'group36','Cz', 'contrast_pattern', 'pattern_contrast_Cz.svg')


