%____-------------------------------------------------------------____%
%____-------------------------------------------------------------____%
%
% Analyses of Pilote1
% EEG data + Simulation on sound sequences
%
%____-------------------------------------------------------------____%
%____-------------------------------------------------------------____%

% feb. 2018

dirvba='/sps/cermep/cermep/experiments/DCM/VBA/VBA-toolbox-master';
dirlm='/sps/cermep/cermep/experiments/DCM/manip2/tests_design';
dirseq='/sps/cermep/cermep/experiments/DCM/manip2/SeqPresentation';
dirmat='/sps/cermep/cermep/experiments/DCM/manip2/ana_matlab';

addpath(genpath(dirvba))
addpath(dirlm)
addpath(dirseq)
addpath(dirmat)



%% Import sound sequences - Pilote1
% these files have been created with CreateNewDesignSeq_FL.m, see memo_seqPres.m
SubjName = 'Pilote1';
SubjDir = [ dirseq '/' SubjName ];
A = load( [ SubjDir '/' SubjName '_ContextOdd_Sequence.mat']) ; % gives S1, S2, T1, T2, Con1, Con2 (see CreateNewDesignSeq_FL.m in SeqPresentation )
R = load( [ SubjDir '/' SubjName '_ContextOdd_SequenceReverse.mat']) ; % gives S1, S2, T1, T2, Con1, Con2 (see SeqPresentation files)
% con = [ chunk r p ]

% bloc order
exp  =load([ dirseq '/' SubjName '/' SubjName '_ContextOdd_Expe.mat']); % gives exp.Expe (1 and 2 for cond. var_r and var_p, resp.) and exp.Reverse (1 and 2 for no reverse, and reverse, resp.)

% View bloc sequences
% four blocs in the same plot

ContextOdd_view_toneseq(A,R,exp);


%% Sort events
% Pilote1 : a single continuous file
% code 16 separates blocs

% say we have a vector of 16-0-1 codes from the acquisition
% we will use a 5-digit code
% d1-d2-d3-d4-d5
% d1 = condition: 1/2
% d2 = chunk: 2/../8
% d3 = r: 1/2 (r-,r+) <=> chunks 4:6 vs. [2:4 6:8]
% d4 = p:1/2/3 (p-,p,p+) <=> 2:4 vs. 4:6 vs. 6:8
% d5 = tone category: standard preceding a deviant=1, deviant=2
dirseq='/sps/cermep/cermep/experiments/DCM/manip2/SeqPresentation';
SubjName = 'Pilote1';
SubjDir = [ dirseq '/' SubjName ];
Nbloc = 4;

[ EvMrk_Bloc ] = ContextOdd_sort_events(dirseq, SubjName, Nbloc) % recode directly from the matlab sound sequence
File_EvMrk = [ SubjDir '/' SubjName '_ContextOdd_EventMrk.mat']
save(File_EvMrk, 'EvMrk_Bloc' ) ; 

% alt: using BrainAmp mark file
% InputSeq = from pos file or brainamp mrk ?
% [ EvMrk_Bloc ] = ContextOdd_sort_events(dirseq, SubjName, Nbloc, InputSeq) % recode  from the acquisition code sequence (continuous, with 16-start codes)

a = load(File_EvMrk)

%% Data matrix (case EEG, real)
% build up Data_Bloc a cell array of N=4 blocs
% each cell is a data matrix Ntrials * Ns (epochs are concatenated across sensors: 
% trial1: [ sens1_sample1 .... sens1_sampleN sens2_sample1 ... sens2_sampleN...]
% Ns = Nsensors * Nsamples

%% Data matrix (case LM data, simulations)
% build up Data_Bloc a cell array of N=4 blocs
% each cell is a data matrix Ntrials * Ns (one value per sensors, there may
% be different sensors corresponding to different learning quantities)

% learning models, Bernoulli/Ostwald
% Here we consider Ns = 6 corresponding to 6 values of tau
% tau in [ 5 10 15 20 50 70 ]



dirseq='/sps/cermep/cermep/experiments/DCM/manip2/SeqPresentation';
Nbloc = 4;

SubjName = 'Pilote1';
SubjDir = [ dirseq '/' SubjName ];
A = load( [ SubjDir '/' SubjName '_ContextOdd_Sequence.mat']) ; % gives S1, S2, T1, T2, Con1, Con2 (see CreateNewDesignSeq_FL.m in SeqPresentation )
R = load( [ SubjDir '/' SubjName '_ContextOdd_SequenceReverse.mat']) ; % gives S1, S2, T1, T2, Con1, Con2 (see SeqPresentation files)
exp  =load([ dirseq '/' SubjName '/' SubjName '_ContextOdd_Expe.mat']); % gives exp.Expe (1 and 2 for cond. var_r and var_p, resp.) and exp.Reverse (1 and 2 for no reverse, and reverse, resp.)

Prob_Val =  [ 1/4   1/6 1/8]; % p-, p, p+

tau = [5 10 15 20 50 100];
flag_plot = { 'mu' 'BS'};
flag_plot = { };

clear Uall Proball Yall xall Rall;
for i_t = 1:numel(tau)
    for i_b = 1:Nbloc
        
        [  Cond, U ]= ContextOdd_find_cond(exp.Expe(i_b), exp.Reverse(i_b), A, R);% Cond = [ chunk r p ]
        U = U-1; % from (1/2 to 0/1 sequences);
        Prob = U*0;
        for k=1:3
            h=find(Cond(:,3)==k);
            Prob(h) = Prob_Val(k);
        end
        Rvar = Cond(:,2); % useless, for plot below
        
        [Y,x] = TestingOstwald_ModelInversion(Prob', U', tau(i_t), flag_plot);
        Uall{i_b, i_t} = U;
        Proball{i_b, i_t} = Prob;
        Yall{i_b, i_t} = Y;
        xall{i_b, i_t} = x;
        Rall{i_b, i_t} = Rvar;
    end
end

% plot BS and mu across tau values

col = { 'c' 'g' 'b' 'm' 'r'  'k'} ;

for i_b = 1:Nbloc
    figure; set(gcf,'color','white');
    
    for i_t = 1:numel(tau)
    
        Prob = Proball{i_b, i_t}; U = Uall{i_b, i_t}; Rvar = Rall{i_b, i_t};
        x = xall{i_b, i_t}; Y = Yall{i_b, i_t};
        
        mu = x(3,:) ./ (x(3,:) + x(4,:));
        
        subplot(2,1,1);     
        plot(mu,[ col{i_t} '.']);hold on
        plot(Prob,[ col{i_t} '-']);
        plot(Rvar./10 ,[ col{i_t} '-.']);
        title([ ' bloc ' num2str(i_b) ]);
        ylabel('mu estimate');
        subplot(2,1,2);
        h = find(U==1);
        plot(h,U(h),[ col{i_t} 'o']); hold on
        plot(Y,  [ col{i_t} '*']);
        ylabel('BS');
    end
    
end


% Reshape into Data_Bloc for constrasts
% cell of [Ntrial * Nsensors]
% Nsensors = BS for every tau
% Yall = {blocs * tau}
dirseq='/sps/cermep/cermep/experiments/DCM/manip2/SeqPresentation';
Nbloc = 4;
SubjName = 'Pilote1';
SubjDir = [ dirseq '/' SubjName ];


clear Data_Bloc; % cell of [Ntrial * Nsensors] matrices ( Nsensors = Nsensors *Nsamples in EEG data)
for i_b = 1:Nbloc
        
    D = [] ;
    for i_t = 1:numel(tau)
        D = [D Yall{i_b, i_t}'];
    end
    Data_Bloc{i_b} = D;
    
end
nt = length(Data_Bloc{1}); % nr of trials
Reject_Bloc = {zeros(nt,1) zeros(nt,1) zeros(nt,1) zeros(nt,1)}; % one vector per bloc

File_Data = [ SubjDir '/' SubjName '_ContextOdd_SimuData.mat']
save(File_Data, 'Data_Bloc','Reject_Bloc');



%% Compute contrast
% available contrasts
% 'local_r_46'  :   contrast MMN(r+) vs. MMN(r-) on chunk 4 and 6  (predictability effect)
% 'local_p_all' :   contrast MMN(p+) vs. MMN(p) vs. MMN(p-) using every chunk , (2-3-4) vs. (4-5-6) vs. (6-7-8)
% 'local_p_46'  :   contrast MMN(p high) vs. MMN(p low) vs. MMN(p-)on chunks 4
%                   and 6, MMN(high,6) + MMN(high,4) - MMN(low,6) -MMN(low,4)
% 'global_rp_46':   contrast the contextual effect (var_p vs. var_r) on
%                   chunks 4 and 6: local_r_46 vs. local_p_46




dirseq='/sps/cermep/cermep/experiments/DCM/manip2/SeqPresentation';
SubjName = 'Pilote1';
SubjDir = [ dirseq '/' SubjName ];

mrk = load([ dirseq '/' SubjName  '/' SubjName '_ContextOdd_EventMrk.mat'] ); % gives EvMrk_Bloc;
EvMrk_Bloc = mrk.EvMrk_Bloc; clear mrk;

d = load([ SubjDir '/' SubjName '_ContextOdd_SimuData.mat']); % gives Data_Bloc, reject_Bloc

% contrast 1 'local_r_46'  :   contrast MMN(r+) vs. MMN(r-) on chunk 4 and 6  (predictability effect)
ContrastName = 'local_r_46'; % 'local_r_46';
[ DataAvg ] = ContextOdd_contrast(dirseq, SubjName, d.Data_Bloc, d.Reject_Bloc, EvMrk_Bloc, ContrastName);

% #13 = mmn_r-, #14 = mmn_r+, #17 = r+ - r- = #14 - #13
figure; set(gcf,'color','white');
plot(DataAvg(13,:), 'r*'); % red for low uncertainty, r -
hold on
plot(DataAvg(14,:), 'g*'); % green for high uncertainty, r +
plot(DataAvg(17,:), 'm*'); % 
xticklabels({'5' '10' '15' '20' '50' '100'}); ylabel('BS');xlabel('tau');
title(ContrastName, 'Interpreter', 'none')



% contrast 2a  'local_p_all' :   contrast MMN(p+) vs. MMN(p) vs. MMN(p-) using every chunk , (2-3-4) vs. (4-5-6) vs. (6-7-8)
ContrastName = 'local_p_all'; % 'local_r_46';
clear DataAvg
[ DataAvg ] = ContextOdd_contrast(dirseq, SubjName, d.Data_Bloc, d.Reject_Bloc, EvMrk_Bloc, ContrastName)

% #25 = mmn_p-, #26 = mmn_p, #27 = mmn_p+
figure; set(gcf,'color','white');
plot(DataAvg(25,:), 'g*'); % p-, low uncertainty chunk (2,3,4) (high dev probability)
hold on
plot(DataAvg(26,:), 'b*'); % 
plot(DataAvg(27,:), 'r*'); % green for high uncertainty, p +
xticklabels({'5' '10' '15' '20' '50' '100'}); ylabel('BS');xlabel('tau');
title(ContrastName, 'Interpreter', 'none')



% contrast 2b 'local_p_46'  :   contrast MMN(p high) vs. MMN(p low) on chunks 4 and 6 
ContrastName = 'local_p_46'; % 'local_r_46';
clear DataAvg
[ DataAvg ] = ContextOdd_contrast(dirseq, SubjName, d.Data_Bloc, d.Reject_Bloc, EvMrk_Bloc, ContrastName)
% #11 = mmn_low, #14 = mmn_high, #17 = high - low
figure; set(gcf,'color','white');
plot(DataAvg(11,:), 'g*'); % low uncertainty:  chunk 4 in (2 3 4) and 6 in (4 5 6)
hold on
plot(DataAvg(14,:), 'r*'); %  % high uncertainty:  chunk 4 in (4 5 6) and 6 in (6 7 8)
plot(DataAvg(17,:), 'm*'); % high - low
xticklabels({'5' '10' '15' '20' '50' '100'}); ylabel('BS');xlabel('tau');
title(ContrastName, 'Interpreter', 'none')




%  contrast 3 'global_rp_46':   interaction contrast the contextual effect (var_p vs. var_r) on chunks 4 and 6: local_r_46 vs. local_p_46
ContrastName = 'global_rp_46'; % 'local_r_46';
clear DataAvg
[ DataAvg ] = ContextOdd_contrast(dirseq, SubjName, d.Data_Bloc, d.Reject_Bloc, EvMrk_Bloc, ContrastName)

% #21 = mmn_low_r, #22 = mmn_high_r, #27 = mmn_low_p, #28 mmn_high_p, #31 (high-low)_r - (high-low-_p
figure; set(gcf,'color','white');
plot(DataAvg(21,:), 'g*-'); hold on% low uncertainty:  mmn_low_r
plot(DataAvg(27,:), 'go-'); %  low uncertainty: mmn_low_p
plot(DataAvg(22,:), 'r*-'); %  high  uncertainty: mmn_high_r
plot(DataAvg(28,:), 'ro-'); % high  uncertainty:  mmn_high_p
plot(DataAvg(31,:), 'mp'); % 

xticklabels({'5' '10' '15' '20' '50' '100'}); ylabel('BS');xlabel('tau');
title(ContrastName, 'Interpreter', 'none')





% save output in ep Elan files
direeg = '/sps/cermep/cermep/experiments/DCM/manip2/EEG_Lyon';
Fech = 1000;
Nsens = 32;
Pref_Elan_OutFile = [ direeg '/' SubjName '_2-20Hz' ];
EP.PrefOutFile = Pref_Elan_OutFile ;
EP.Fech = Fech;
EP.Nsens = Nsens ;
EP.Init_ep = 'tobedefined.p';
    
[ DataAvg ] = ContextOdd_contrast(dirseq, SubjName, Data_Bloc, Reject_Bloc, EvMrk_Bloc, ContrastName, EP)






