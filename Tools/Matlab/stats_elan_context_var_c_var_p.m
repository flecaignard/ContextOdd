%% MEMO - Stats ContextODD EEG - Elan permutation tests
% Typical mismatch analysis : context var_p VS var_c
% 
% we compare MMN(c-, p0) in var_c with MMN(c-, p0) in var_p

% *.p files have been created in stats_elan_surprise_var_p.m and *var_c.m


addpath('/sps/cermep/cermep/experiments/DCM/Dycog_prog/elan_matlab');


%% Create *.p files
path_mne = '/sps/cermep/cermep/experiments/DCM/ContextOdd/Ana_MNE';
erp_folder = 'erp_2-20Hz';
elan_folder = 'stats_elan';
subj_id_list = {'00', '01','02','03','04','05','06','07','08','09',...
                '10','11','12','13','14','15','16','17','18','19',...
                '20','21','22','23','24','25','26','27','28','29',...
                '30','31','32','33','34','35'   };




%% do the stats ! ---> epranddiff --->  write epranddiff file
% linux
% script:/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Scripts/qsub_epranddiff,memo_stats_comp_mne_elan
% 

% write epranddiff file
subj = 'group36';
subj_id_list = {'00', '01','02','03','04','05','06','07','08','09',...
                '10','11','12','13','14','15','16','17','18','19',...
                '20','21','22','23','24','25','26','27','28','29',...
                '30','31','32','33','34','35'   };
nr_subj = numel(subj_id_list);




% STD , p VS c -----------------------------------
contrast =  {'2-20Hz_var_c_c-_std','2-20Hz_var_p_p0_std' } ; 
contrast_name = '2-20Hz_var_c_c-_VS_var_p_p0_std';

f_epranddiff = fullfile(path_mne,  subj,  erp_folder, elan_folder,  ['epranddiff_' contrast_name '.par']);
fid = fopen(f_epranddiff, 'w');
fprintf(fid, '1 2 %d\n', nr_subj); 
for i_cond = 1:numel(contrast)
    for i_su = 1:nr_subj
        f_ep = fullfile(path_mne, ['sub-' subj_id_list{i_su}],  erp_folder, elan_folder,  ['sub-' subj_id_list{i_su} '.' contrast{i_cond} '.p']);
        fprintf(fid, 'fact1 \t cond%d \t  suj%d \t %s\n', i_cond, i_su, f_ep); 
    end
end
fclose(fid);

% DEV , p VS c -----------------------------------
contrast =  {'2-20Hz_var_c_c-_dev','2-20Hz_var_p_p0_dev' } ; 
contrast_name = '2-20Hz_var_c_c-_VS_var_p_p0_dev';

f_epranddiff = fullfile(path_mne,  subj,  erp_folder, elan_folder,  ['epranddiff_' contrast_name '.par']);
fid = fopen(f_epranddiff, 'w');
fprintf(fid, '1 2 %d\n', nr_subj); 
for i_cond = 1:numel(contrast)
    for i_su = 1:nr_subj
        f_ep = fullfile(path_mne, ['sub-' subj_id_list{i_su}],  erp_folder, elan_folder,  ['sub-' subj_id_list{i_su} '.' contrast{i_cond} '.p']);
        fprintf(fid, 'fact1 \t cond%d \t  suj%d \t %s\n', i_cond, i_su, f_ep); 
    end
end
fclose(fid);


% MMN , p VS c -----------------------------------
contrast =  {'2-20Hz_var_c_c-_mmn','2-20Hz_var_p_p0_mmn' } ; 
contrast_name = '2-20Hz_var_c_c-_VS_var_p_p0_mmn';

f_epranddiff = fullfile(path_mne,  subj,  erp_folder, elan_folder,  ['epranddiff_' contrast_name '.par']);
fid = fopen(f_epranddiff, 'w');
fprintf(fid, '1 2 %d\n', nr_subj); 
for i_cond = 1:numel(contrast)
    for i_su = 1:nr_subj
        f_ep = fullfile(path_mne, ['sub-' subj_id_list{i_su}],  erp_folder, elan_folder,  ['sub-' subj_id_list{i_su} '.' contrast{i_cond} '.p']);
        fprintf(fid, 'fact1 \t cond%d \t  suj%d \t %s\n', i_cond, i_su, f_ep); 
    end
end
fclose(fid);

%% Plot stats maps
% same as article Frontiers2015
% RFvsPF_premmn_0.01_100000_0-210.group20.meg_2-20.rand.mask.p.p
% addpath('/sps/cermep/cermep/experiments/DCM/manip1/outils/matlab/mmndcm_view');
addpath('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Matlab');

liste ={  '2-20Hz_var_c_dev_VS_std_th001_100000_0-410'...
        '2-20Hz_var_p_dev_VS_std_th001_100000_0-410'};
    
liste={  '2-20Hz_var_c_VS_var_p_dev_th001_100000_0-410' ...
        '2-20Hz_var_c_VS_var_p_mmn_th001_100000_0-410' ...
        '2-20Hz_var_c_VS_var_p_std_th001_100000_0-410' };
liste={'2-20Hz_var_c_VS_var_p_std_th001_100000_230_290'};    
cd('/sps/cermep/cermep/experiments/DCM/ContextOdd/Ana_MNE/group36/erp_2-20Hz/stats_elan')
gain = 1000;
Vmax = 0.01;
Dur_mini = 1;
for i=1: numel(liste)
    
    ficp=[liste{i} '.rand.mask.p.p']; %mask <=> multiple testing correction, minimum consecutive samples
    codd_view_pFile_image_clean(ficp,gain,Vmax,Dur_mini)
end


%% group average (debug)
addpath('/sps/cermep/cermep/experiments/DCM/FL_matlab_prog/Elan_tools_FL/'); % fl_epavg.m

path_mne = '/sps/cermep/cermep/experiments/DCM/ContextOdd/Ana_MNE';
erp_folder = 'erp_2-20Hz';
elan_folder = 'stats_elan';
subj_id_list = {'00', '01','02','03','04','05','06','07','08','09',...
                '10','11','12','13','14','15','16','17','18','19',...
                '20','21','22','23','24','25','26','27','28','29',...
                '30','31','32','33','34','35'   };

nr_subj = numel(subj_id_list);

listoune = [];
contrast =  {'2-20Hz_var_p_std','2-20Hz_var_p_dev','2-20Hz_var_p_mmn' } ; 
for i_cond = 1:numel(contrast)
    for i_su = 1:nr_subj
        f_ep = fullfile(path_mne, ['sub-' subj_id_list{i_su}],  erp_folder, elan_folder,  ['sub-' subj_id_list{i_su} '.' contrast{i_cond} '.p']);
        listoune{i_su} = f_ep;  
    end
    epfilename = fullfile(path_mne,  'group36',  erp_folder, elan_folder, [ 'group36.elan_' contrast{i_cond}  '.p']);
    fl_epavg(listoune, listoune{1}, epfilename);

end

