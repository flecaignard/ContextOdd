%% Tests with HGF and ContextOdd sequences
% Sept. 2019

%% Import HGF Toolbow
addpath(genpath('/sps/cermep/cermep/experiments/DCM/ContextOdd/toolboxes/tapas'));

%% Load ContextOdd sequence
% recap order session: eSu09 1  11 2 22 3 21 4 12 % from file /sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Experimental_Design/StimSeq/recap_stimseq_group.txt

dir_seq = '/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Experimental_Design/StimSeq';
subj = 'sub-09';
dir_seq_subj = [dir_seq '/' subj ];
file_expe = [dir_seq_subj '/' subj '_ContextOdd_Expe.mat' ];
file_seq = [dir_seq_subj '/' subj '_ContextOdd_Sequence.mat' ]; 
file_seq_rev = [dir_seq_subj '/' subj '_ContextOdd_SequenceReverse.mat' ];

% manual inspection to get context V and P sequences
e = load(file_expe); % 1 = var_V, 2 = var_P  
s = load(file_seq);

u_V = s.T1 - 1; % 1=std, 2 = dev => moves to 0=std, 1=dev
u_P = s.T2 - 1; % 1=std, 2 = dev => moves to 0=std, 1=dev

nr_inputs = length(u_V);

% plots to check
figure;
plot(s.S1, 'r*-'); % context V
figure;
plot(s.S2, 'g*-');% context P



%% Simulations
% see tapas_hgf_binary
% input parameter = prc_pvec = [ mu_x1_0 mu_x2_0 mu_x3_0 sig_x1_0 sig_x2_0 sig_x3_0 rho_1_0 rho_2_0 rho_3_0 ka_2 ka_3 om_1 om_2 th_3]
%
% 
% in tapas_hgf_binary:
% th_3 -> exp(-th_3)
% om_2 -> exp(ka_2 *mu_3_k + om_2)


% we want kappa to be equal to 1 at level 2 and 3
% om_2 = 0 to have mu_2_k ~ N(mu_2_k-1, exp(mu_3_k))

inputs = u_P; % 
prc_model = 'tapas_hgf_binary';
prc_pvec = [NaN 0 1 NaN 1 1 NaN 0 0 1 1 NaN 0 -6];% [ mu_x1_0 mu_x2_0 mu_x3_0 sig_x1_0 sig_x2_0 sig_x3_0 rho_1_0 rho_2_0 rho_3_0 ka_2 ka_3 om_1 om_2 th_3]
prc_pvec = [NaN 0 1 NaN 1 1 NaN 0 0 1 1 NaN -2.5 -20]; % default TApas
obs_model = 'tapas_unitsq_sgm';  %we take the unit square sigmoid model
obs_pvec = 5 ; % _unitsq_sgm_, with parameter $\zeta=5$%'
random_seed = 13245; %an optional seed for the random number generator
sim = tapas_simModel(inputs, prc_model, prc_pvec, obs_model, obs_pvec, random_seed);

tapas_hgf_binary_plotTraj(sim)




                 
%% tests

inputs = round(rand(1,100))'; % sequence of 1/0, random
prc_model = 'tapas_hgf_binary';
prc_pvec = [NaN 0 1 NaN 1 1 NaN 0 0 1 1 NaN 0 -6];% [ mu_x1_0 mu_x2_0 mu_x3_0 sig_x1_0 sig_x2_0 sig_x3_0 rho_1_0 rho_2_0 rho_3_0 ka_2 ka_3 om_1 om_2 th_3]
obs_model = 'tapas_unitsq_sgm';  %we take the unit square sigmoid model
obs_pvec = 5 ; % _unitsq_sgm_, with parameter $\zeta=5$%'
random_seed = 13245; %an optional seed for the random number generator
sim = tapas_simModel(inputs, prc_model, prc_pvec, obs_model, obs_pvec, random_seed);

tapas_hgf_binary_plotTraj(sim)

