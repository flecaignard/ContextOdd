%% Tests with HGF
% Sept. 2019

%% Import HGF Toolbow
addpath(genpath('/sps/cermep/cermep/experiments/DCM/ContextOdd/toolboxes/tapas'));

%% Try to simulate
% comments from the doc (hgf_demo.m)
% Next, we simulate an agent's responses using the simModel function. To do 
% that, we simply choose values for $\omega$. Here, we take $\omega_2=-2.5$ and 
% $\omega_3=-6$. But in addition to the perceptual model _hgf_binary_, we now 
% need a response model. Here, we take the unit square sigmoid model, _unitsq_sgm_, 
% with parameter $\zeta=5$. The last argument is an optional seed for the random 
% number generator.


inputs = round(rand(1,100))'; % sequence of 1/0, random
prc_model = 'tapas_hgf_binary';
prc_pvec = [NaN 0 1 NaN 1 1 NaN 0 0 1 1 NaN -2.5 -6];% [ mu_x1_0 mu_x2_0 mu_x3_0 sig_x1_0 sig_x2_0 sig_x3_0 rho_1_0 rho_2_0 rho_3_0 ka_2 ka_3 om_1 om_2 th_3]
obs_model = 'tapas_unitsq_sgm';  %we take the unit square sigmoid model
obs_pvec = 5 ; % _unitsq_sgm_, with parameter $\zeta=5$%'
random_seed = 13245; %an optional seed for the random number generator
sim = tapas_simModel(inputs, prc_model, prc_pvec, obs_model, obs_pvec, random_seed);

tapas_hgf_binary_plotTraj(sim)


%% Recover parameter values from simulated responses
% We can now try to recover the parameters we put into the simulation ($\omega_2=-2.5$ 
% and $\omega_3=-6$) using fitModel.

est = tapas_fitModel(sim.y,...
                     sim.u,...
                     'tapas_hgf_binary_config',...
                     'tapas_unitsq_sgm_config',...
                     'tapas_quasinewton_optim_config');


                 
%% tests
% in tapas_hgf_binary:
% th_3 -> exp(-th_3)
% om_2 -> exp(ka_2 *mu_3_k + om_2)


% we want kappa to be equal to 1 at level 2 and 3
% om_2 = 0 to have mu_2_k ~ N(mu_2_k-1, exp(mu_3_k))

inputs = round(rand(1,100))'; % sequence of 1/0, random
prc_model = 'tapas_hgf_binary';
prc_pvec = [NaN 0 1 NaN 1 1 NaN 0 0 1 1 NaN 0 -6];% [ mu_x1_0 mu_x2_0 mu_x3_0 sig_x1_0 sig_x2_0 sig_x3_0 rho_1_0 rho_2_0 rho_3_0 ka_2 ka_3 om_1 om_2 th_3]
prc_pvec = [NaN 0 1 NaN 1 1 NaN 0 0 1 1 NaN -2.5 -6]; % default TApas

obs_model = 'tapas_unitsq_sgm';  %we take the unit square sigmoid model
obs_pvec = 5 ; % _unitsq_sgm_, with parameter $\zeta=5$%'
random_seed = 13245; %an optional seed for the random number generator
sim = tapas_simModel(inputs, prc_model, prc_pvec, obs_model, obs_pvec, random_seed);

tapas_hgf_binary_plotTraj(sim)

