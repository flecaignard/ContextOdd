function mmndcm_view_pFile_image_clean(ficp,gain,Vmax,Dur_mini)

figure;
%ficp=fichier p 

%gain = 1000 pour l'eeg (V => uV)
%       par exemple - diff pour les fT

%Vmax= echelle maxi

[HEADER1, HEADER2, DATA, CHANNELS] = ep2mat(ficp);



Tech=HEADER2.s_Sampling_Period;
Nsample_pre=HEADER2.s_Nb_Sample_PreStim;


Ncapt=HEADER2.s_Nb_Channels;
switch Ncapt
    case 32
        mod='EEG';
    case 276
        mod='MEG';
end


Nsample=HEADER2.s_Nb_Sample_per_Channel;


Tmin=-Nsample_pre*Tech;
Tmax=(Nsample-(Nsample_pre+1))*Tech;

t=Tmin:Tech:Tmax  ;
s=1:Nsample;

%% Conversion duration => Nb samples


NSdur=round(Dur_mini/Tech)

%%%%%%%%% CLUSTER de capteurs %%%%%%%%%%%%
switch mod
    case 'EEG'
%         LC1=[1 3 4 7 16 25 8 17 26 9 18 27];
%         RC1=[2 6 5 15 24 33 14 23 32 13 22 31];
%         
%         LC2=[34 35 45 36 46 37 47];
%         RC2=[44 43 53 42 52 41 51];
%         
%         
%         CC3=[10 11 12 19 20 21 28 29 30 38 39 40 48 49 50];
%         
%         OC4=[55 56 57 54 60 61 58 59 63 62];
%         
%         ordre=[LC1 RC1 CC3 LC2 RC2 OC4 64]';
        ordre=1:32;
        
        
    case 'MEG'
        ordre=1:276;
end

%ordre=1:64;
%ordre=1:276;
DATAo=DATA(:,ordre);
CHANNELS_labelo=CHANNELS.v_Label;
CHANNELS_labelo=CHANNELS_labelo(:,ordre);

%% Cleaning
%significant window must be bigger than NSdur consecutive
%samples

DATAc=DATAo;

for i=1:Ncapt
    vect=DATAc(:,i);
    hc=find(vect(1:Nsample)<=Vmax);
    while ~isempty(hc)
        j=1;
        if hc(1) > (Nsample-NSdur+1)
            vect([hc(1:end)])=100;
            hc(1:end)=[];
        else
            
            while (hc(1)+j) < Nsample
                
                if (vect(hc(1)+j) > Vmax)
                    if j < NSdur
                        vect([hc(1):hc(1)+j])=100;
                        hc(1:1+j-1)=[];
                    else
                        
                        if j> length(hc)
                            hc(1:end)=[];
                        else
                            hc(1:1+j-1)=[];
                        end
                        
                    end
                    break
                else
                    j=j+1;
                    if (hc(1)+j) >= Nsample
                        hc(1:end)=[];
                        break
                    end
                end
            end
        end
    end
    DATAc(:,i)=vect;
    clear vect;
end



%%%%%%%%% colormap %%%%%%%

% [m n]=flatreadpalette('/sps/cermep/cermep/experiments/DCM/manip1/outils/matlab/color13_std.dat');
% colormap(m/65535);
% 
% DATAc=DATAc*gain;
% h=imagesc(DATAc');
% 
% max(max(abs(DATA)))
% %
% cmax=Vmax;
% caxis([-cmax cmax]);
% 
% slot= [-Vmax:(Vmax/6):Vmax];
% l=length(slot);
% for i=1:l
%     c{i}=num2str(slot(i));
%     %c{i}=num2str(round(slot(i)));
% end
% 
% clb=colorbar('YLim',[-Vmax Vmax] ,'YTick',slot , 'YTickLabel',c,'CLim', [-Vmax Vmax]);
% get(clb,'CLim');
% 

hinf=find(DATAc<=Vmax);
hsup=find(DATAc>Vmax);
DATAc(hinf)=0;
DATAc(hsup)=1;
h=imagesc(DATAc');




tutu=[0 0 0;  1 1 1]; %noir/blanc
colormap(tutu)

%%%%%%%%% axis %%%%%%%
set(gca,'YTick',[1:Ncapt]);
set(gca,'YTickLabel', CHANNELS_labelo);

tw=0.05; %pas de 50ms
set(gca,'XTick',[1:1000*tw/Tech:Nsample]);

xlab=Tmin:tw*1000:Tmax;
l=length(xlab);
for i=1:l
    c{i}=num2str(round(xlab(i)));
end
set(gca,'XTickLabel', c);


title(ficp,'interpreter','none')

