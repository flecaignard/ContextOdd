function codd_view_pFile_image_Frontiers(filedir, basename)

% 2-20Hz_chunk_short_VS_long_mmn_th05_100000_230_290.rand.mask.p.p 
Ncapt = 32;
ordre = 1:32;
Ne=32;

file_pre001 = [filedir '/' basename '_th001_100000_-145_-40.rand.mask.p.p'  ];
if ~isfile(file_pre001)
    disp(['File ' file_pre001 ' Not Found - Break ']);
    return
end


file_early001 = [filedir '/' basename '_th001_100000_0_80.rand.mask.p.p'  ];
if ~isfile(file_early001)
    disp(['File ' file_early001 ' Not Found - Break ']);
    return
end

file_mmn001 = [filedir '/' basename '_th001_100000_95_200.rand.mask.p.p'  ];
if ~isfile(file_mmn001)
    disp(['File ' file_mmn001 ' Not Found - Break ']);
    return
end

file_p3a001 = [filedir '/' basename '_th001_100000_230_290.rand.mask.p.p'  ];
if ~isfile(file_p3a001)
    disp(['File ' file_pre001 ' Not Found - Break ']);
    return
end



TH=0.05;

figure;
mod='EEG';
        
%% fic prestim
filestat = file_pre001;
[HEADER1, HEADER2, DATA, CHANNELS] = ep2mat(filestat);
Tech=HEADER2.s_Sampling_Period;
Nsample_pre=HEADER2.s_Nb_Sample_PreStim;
Nsample=HEADER2.s_Nb_Sample_per_Channel;
Tmin=-Nsample_pre*Tech;
Tmax=(Nsample-(Nsample_pre+1))*Tech;

t=Tmin:Tech:Tmax  ;
s=1:Nsample;
CHANNELS_labelo=CHANNELS.v_Label;
CHANNELS_labelo=CHANNELS_labelo(:,ordre);


DATApre001=DATA(:,ordre);
bool_pre001=DATApre001<=TH;



bool_pre = bool_pre001.*1  ; 
t = bool_pre>1;
bool_pre(t)=1;
%% early
filestat = file_early001;
[HEADER1, HEADER2, DATA, CHANNELS] = ep2mat(filestat);
bool_early001=DATA(:,ordre)<=TH;



bool_early = bool_early001.*1 ; 
t = bool_early>1;
bool_early(t)=1;
%% mmn
filestat = file_mmn001;
[HEADER1, HEADER2, DATA, CHANNELS] = ep2mat(filestat);
bool_mmn001=DATA(:,ordre)<=TH;


bool_mmn = bool_mmn001.*1  ; 
t = bool_mmn>1;
bool_mmn(t)=1;

%% p3a
filestat = file_p3a001;
[HEADER1, HEADER2, DATA, CHANNELS] = ep2mat(filestat);
bool_p3a001=DATA(:,ordre)<=TH;


bool_p3a = bool_p3a001.*1  ; 
t = bool_p3a>1;
bool_p3a(t)=1;



%% somme
% for i=1:size(DATA0,1)
%     for j=1:size(DATA0,2)
%         
%     DATAfinal(i,j)=min([DATA0(i,j) DATA1(i,j) DATA2(i,j) DATA3(i,j)]);
%     end
% end
DATAfinal = bool_pre + bool_early + bool_mmn + bool_p3a ;

%%%%%%%%% colormap %%%%%%%
DATAfinal=DATAfinal(:,1:Ne);
h=imagesc(DATAfinal');
tutu=[0 0 0; 0.65 0.65 0.65; 1 1 1];
tutu=[1 1 1;  0.65 0.65 0.65; 0 0 0];
colormap(tutu)
%%%%%%%%% axis %%%%%%%
set(gca,'YTick',[1:Ncapt-1]);
set(gca,'YTickLabel', CHANNELS_labelo(:,1:Ne));

tw=0.05; %pas de 50ms
set(gca,'XTick',[1:1000*tw/Tech:Nsample]);

xlab=Tmin:tw*1000:Tmax;
l=length(xlab);
for i=1:l
    c{i}=num2str(round(xlab(i)));
end
set(gca,'XTickLabel', c);
set(gca,'color', 'white')

 title(basename ,'interpreter','none')
cond='stats_RFvsPF_unfiltered';
% plot2svg(sprintf('%s.svg',cond));
set(gcf, 'units', 'pixels', 'position', [100 0 400 800]);








