"""
===============================================
BIG title
===============================================

What does this do?
 DEscribe the: Outputs = *.fif, *.fif

Raw data may have been formatted as Bids prior this step (not mandatory but advised)

# Authors: Françoise Lecaignard & Pauline Duret
# Created on XXX
"""



###############################################################################
import os
import sys
sys.path.append('/sps/crnl/users/pduret/ContextOdd/ContextOdd_Git/Tools/Python/Utils')
sys.path.append('/sps/crnl/users/pduret/ContextOdd/ContextOdd_Git/Tools/Python/MEG')

#import stuff you need
from mne.io import read_events


############################################################################################
## Paths for the present analysis.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################

# MEG data -  path to the BIDS-acquisition data (raw data after BIDS - Step00 )
dir_raw_bids='/sps/crnl/users/pduret/ContextOdd/ContextOdd_Data/Raw_Data/MEG_BIDS'
# MEG data -  path to the data, analyzed with MNE  ( onwards Step01 )
dir_mne='/sps/crnl/users/pduret/ContextOdd/ContextOdd_Data/mMNE'


################################################################################
# Input Parameters 
################################################################################

# subj_id_list -----------------------

# test
#subj_id_list = ['mASD03','mTYP07'] #
# apply to all
from fun_create_subj_list import create_subj_list
subj_id_list = create_subj_list(dir_raw_bids)

################################################################################
# Define Constant Variables
################################################################################
  
# Plot raw data
plot=False
################################################################################
# Let's start!
################################################################################

#
# For each subject, preprocessings datafile  are in dir_mne/sub-XX/run-X/preproc/.
for i_su, runs in subj_id_list.items():
    print(f'######### Now treating {i_su} ##########')
    for run  in runs:
        print(f'---run {run}---')
        # create folders
        subj_path = os.path.join(dir_mne, f'{i_su}' , f'run-{run}' , 'preproc')
        if not os.path.exists(subj_path):
            os.makedirs(subj_path)
        else:
            print(f'Already done: subject {i_su}, run {run}.')
            continue
    
        # Do Stuff
        
