#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
===============================================
FUNCTIONS for CONTEXTODD MEG - STEP 1 analysis
===============================================
=> 01: Import CTF data into MNE

@author: Francoise Lecaignard & Pauline Duret
"""


#############################################################################################
## Import packages          -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
# import argparse               # add input arguments

import os                       # path.join for file names
import matplotlib.pyplot as plt # from the MNE examples
import warnings

from mne import find_events, write_events, read_events
from mne.io import read_raw_ctf, read_raw_fif
from mne.viz import plot_events

print(__doc__)

from mne import get_config, set_config
type(get_config('MNE_BROWSE_RAW_SIZE'))
set_config('MNE_BROWSE_RAW_SIZE', '20.0,10.0')

warnings.filterwarnings("ignore",category=DeprecationWarning)


#####################################################################################################################
#############################################     FUNCTIONS      ####################################################
#####################################################################################################################


def codd_meg_import_raw_all(subj, run, f_raw_in, f_raw_out, f_eve_out, nr_evt, plot=True ):
    
    # Import raw data: creates *.raw.fif
    codd_meg_import_raw_data(f_raw_in, f_raw_out, f_eve_out) 
    
    # Import raw events: creates *.raw-eve.fif
    raw = read_raw_fif(f_raw_out, preload=True)
    events = codd_meg_import_raw_event(raw, f_eve_out)
    # Control nr_evt is fine
    if len(events) != nr_evt:
        print(f'--------------> TROUBLE WITH {subj}, run {run}: {len(events)} events found instead of {nr_evt}\n')
    else:
        print(f'--------------> YOUPI WITH {subj}, run {run}')
    ### PLOT
    if plot:
        codd_meg_import_view(f_raw_out, f_eve_out, subj)

 


def codd_meg_import_raw_data(f_raw_in, f_raw_out, f_eve_out):
    
    if os.path.isdir(f_raw_in) :
        print('Found raw data file: ok')
    else:
        print('Raw data file not found- abandon', f_raw_in)
        return
    if os.path.isfile(f_raw_out):
        print('MNE raw file already exists- abandon', f_raw_out)
        return
    else:
        raw = read_raw_ctf(f_raw_in, preload=True )
        raw.save(f_raw_out, overwrite=True)
        print('-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-')
        print('Save Raw Data : Done')
        return raw



def codd_meg_import_raw_event(raw, f_eve_out):
    # extract events from raw.fif 
    events = find_events(raw, shortest_event=2, stim_channel='UPPT001', output='onset')
    write_events(f_eve_out,events)
    print('Save Raw Events : Done')
    return events
   
def codd_meg_import_view(f_raw, f_eve, subj):
    raw = read_raw_fif(f_raw, preload=True)
    events =read_events(f_eve)
    dict_color_event = {1: 'black', 2: 'magenta', 32: 'red'}
    scal = None #dict(eeg=10e-5)
    # plot sensors (top view)
    raw.plot_sensors(show_names=False)
    # plot meg data with events
    raw.plot(events=events,  event_color = dict_color_event, n_channels=raw.info['nchan'],remove_dc = True,  duration=30, highpass = None, scalings=scal) #, show_options=False)
    # plot events
    event_id_raw = {'std': 1, 'dev': 2,  'stop': 32} #'start': 16,
    color_id_raw = {1: 'blue', 2: 'green',  32: 'c'} # apparently, no start event in MEG...   16: 'red',
    plot_events(events, color=color_id_raw, event_id=event_id_raw)
    plt.title(' {} : raw events'.format(subj))
    plt.show()


#############################################################################################
## -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.    TEST and DEBUG     --.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
if __name__== '__main__':
    
    #############################################################################################
    ## Init Paths              -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
    #############################################################################################
    
    # mac_fl
    #dir_mne = '/Users/francoise/Documents/Projets/ContextOdd/Data/EEG/MNE_DevData/MNE_Data'
    #dir_raw_bids = '/Users/francoise/Documents/Projets/ContextOdd/ContextOdd_Git/Experimental_Design/StimSeq' # path to the StimSeq folder
    #dir_eventcodes = '/Users/francoise/Documents/Projets/ContextOdd/ContextOdd_Git/Experimental_Design/StimSeq' # path to the StimSeq folder
    #dir_ref = '/Users/francoise/Documents/Projets/ContextOdd/ContextOdd_Git/Reference_Files'
    
    # in2p3
    dir_mne='/sps/inter/crnl/users/pduret/ContextOdd/ContextOdd_Data/eMNE'
    dir_raw_bids = '/sps/inter/crnl/users/pduret/ContextOdd...'
    dir_eventcodes='/sps/inter/crnl/users/pduret/ContextOdd/ContextOdd_Git/Experimental_Design/StimSeq' # path to the StimSeq folder
    dir_ref='/sps/inter/crnl/users/pduret/ContextOdd/ContextOdd_Git/Reference_Files'
    

    #############################################################################################
    ## Init Variables              -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
    #############################################################################################

    # Apply to all  ---------------------------------------------------------------------------
#    for subj_code, subj_id in subj_list.items():
#        codd_eeg_import_raw_all(dir_raw_bids, dir_mne, subjlist_file, subj_code, nr_evt, plot=True )
        
    ## DEBUG Step 1 ########################################################################
#    subj_code='EDDAR'
#    montage = read_montage('standard_1020')
#    nr_evt = 6068 # should be the case : 4 sessions of 1517 evts (start - 1515 sounds - end)
#    codd_eeg_import_raw_all(dir_raw_bids, dir_mne, montage, subjlist_file, subj_code, nr_evt, plot=True )
#    
