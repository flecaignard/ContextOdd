#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
===============================================
Step 02 - Recode events
===============================================

Original event codings rests on 3 event types: Standard:1, Deviant:2, End:32
Here we adopt a five-digit coding to account for every stimulus categories
Input: *.raw-eve.fif
Output: *.5d.raw-eve.fif


Created on Thu March 7 17:57:17 2019
@author: pauline.duret@inserm.fr, Pauline Duret
5 digits naming system: Françoise Lecaignard
"""
###############################################################################
# Two comments:
#
#  1) There are two conditions: 
#           var_p = variable probability of deviants, that varies in {1/8, 1/6, 1/4}, under a constant chunk size variability (low)
#           var_c = variable chunk sizes, that vary in {low, high}, under a constant deviant probability (p=1/6)
#
# Each conditions was delivered twice, to reverse standard/deviant physical attributes (freq= 500 or 550 Hz)
# Subjects thus received 4 sessions, recorded in the same file (continous acquisition)
# Session order was balanced across subjects
#
#
#  2) Original event codings rests on 3 event types:  Standard:1, Deviant:2, End:32
# These are stored in *.raw-eve.fif
# Here we adopt a five-digit coding to account for every stimulus categories
# The mapping between acquisition coding and five-digit one is made using text files initially created with Matlab (at the time of the experimental design)
# For each session, mapping text file has two columns (1st= {1 and 2 } original, 2nd= {5-digit codes}  
# Five-digit coding is as follows: d1-d2-d3-d4-d5
# d1:     condition 
#               1: var_c, 2: var_p
# d2:     chunk size, in 2:8
# d3:     chunk size variability
#               1:  low (e.g: 4-5-6 for p=1/6) <=> c-
#               2:  high (e.g: 2-3-4-6-7-8 for p=1/6) <=> c+
# d4:     deviant probability
#               1:  p=1/4    <=> p+
#               2:  p=1/6    <=> p0
#               3:  p=1/8    <=> p-
# d5:     stimulus oddball category
#               1:  standard preceding a deviant    <=> std
#               2:  deviant    <=> dev
#               3:  other standards, not preceding a deviant    <=> std_oth
#


###############################################################################
import os
import sys      
sys.path.append('/sps/crnl/users/pduret/ContextOdd/ContextOdd_Git/Tools/Python/MEG')

from fun_02_COdd_meg_events import  codd_rename_events

################################################################################
# Input Parameters 
################################################################################

# Paths ----------------------------
dir_mne = '/sps/crnl/users/pduret/ContextOdd/ContextOdd_Data/mMNE'
################################################################################
# Define Constant Variables
################################################################################

# Total number of events 
# each session: 1512 relevant tones + 3 extras at the end +end = 1517
# 4 sessions of 1516 evts 

#Stimulus coding (five-digit format)
##event_id_codd={'var_c/c4/c-/p0/std': 14121, 'var_c/c4/c-/p0/dev': 14122,'var_c/c4/c-/p0/std_oth': 14123,\
#              'var_c/c5/c-/p0/std': 15121, 'var_c/c5/c-/p0/dev': 15122,'var_c/c5/c-/p0/std_oth': 15123,\
#              'var_c/c6/c-/p0/std': 16121, 'var_c/c6/c-/p0/dev': 16122,'var_c/c6/c-/p0/std_oth': 16123,\
#              'var_c/c2/c+/p0/std': 12221, 'var_c/c2/c+/p0/dev': 12222,'var_c/c2/c+/p0/std_oth': 12223,\
#              'var_c/c3/c+/p0/std': 13221, 'var_c/c3/c+/p0/dev': 13222,'var_c/c3/c+/p0/std_oth': 13223,\
#              'var_c/c4/c+/p0/std': 14221, 'var_c/c4/c+/p0/dev': 14222,'var_c/c4/c+/p0/std_oth': 14223,\
#              'var_c/c6/c+/p0/std': 16221, 'var_c/c6/c+/p0/dev': 16222,'var_c/c6/c+/p0/std_oth': 16223,\
#              'var_c/c7/c+/p0/std': 17221, 'var_c/c7/c+/p0/dev': 17222,'var_c/c7/c+/p0/std_oth': 17223,\
#              'var_c/c8/c+/p0/std': 18221, 'var_c/c8/c+/p0/dev': 18222,'var_c/c8/c+/p0/std_oth': 18223,\
#              'var_p/c2/c-/p-/std': 22111, 'var_p/c2/c-/p-/dev': 22112,'var_p/c2/c-/p-/std_oth': 22113,\
#              'var_p/c3/c-/p-/std': 23111, 'var_p/c3/c-/p-/dev': 23112,'var_p/c3/c-/p-/std_oth': 23113,\
#              'var_p/c4/c-/p-/std': 24111, 'var_p/c4/c-/p-/dev': 24112,'var_p/c4/c-/p-/std_oth': 24113,\
#              'var_p/c4/c-/p0/std': 24121, 'var_p/c4/c-/p0/dev': 24122,'var_p/c4/c-/p0/std_oth': 24123,\
#              'var_p/c5/c-/p0/std': 25121, 'var_p/c5/c-/p0/dev': 25122,'var_p/c5/c-/p0/std_oth': 25123,\
#              'var_p/c6/c-/p0/std': 26121, 'var_p/c6/c-/p0/dev': 26122,'var_p/c6/c-/p0/std_oth': 26123,\
#              'var_p/c6/c-/p-/std': 26131, 'var_p/c6/c-/p-/dev': 26132,'var_p/c6/c-/p-/std_oth': 26133,\
#              'var_p/c7/c-/p-/std': 27131, 'var_p/c7/c-/p-/dev': 27132,'var_p/c7/c-/p-/std_oth': 27133,\
#              'var_p/c8/c-/p-/std': 28131, 'var_p/c8/c-/p-/dev': 28132,'var_p/c8/c-/p-/std_oth': 28133                  }

# Plot raw data
plot=True

################################################################################
# Let's start!
################################################################################

#
# subj_list -----------------------
# test on 2 subjects (1 ASD, 1 TYP)
subj_list = ['mASD03','mTYP07']
runs = ['1','2','3','4']
# applyto all
# subj_list = 

for i_su in subj_list:
    subj  = f'sub-{i_su}'
    for run in runs:
        run = f'run-{run}'
        dir_imne  = os.path.join(dir_mne,f'{subj}',f'{run}','preproc')
        codd_rename_events(dir_imne, subj, run, plot=plot)
