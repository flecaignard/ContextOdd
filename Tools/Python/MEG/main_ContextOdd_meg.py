#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
===============================================
CONTEXTODD MEG - ANALYSIS HISTORY
===============================================
Created on Mon Mar  4 17:40:42 2019

@author: pduret & flecaignard
"""

#############################################################################################
## THIS SCRIPT IS DEDICATED TO THE ANALYSIS OF CONTEXTODD MEG 
##
## STEP0: CONVERT DATA INTO BIDS FORMAT
## STEP1: IMPORT DATA INTO MNE
## STEP2: RENAME EVENTS
## STEP3: DOWNSAMPLE
## STEP4: JUMPS
## STEP5:BAD SENSORS USING PSD
## STEP6 ICA 
##
##
#############################################################################################



#############################################################################################
## Import packages          -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################

import warnings
import sys      # path to my own functions
sys.path.append('/sps/crnl/users/pduret/ContextOdd/ContextOdd_Git/Tools/Python/MEG')


warnings.filterwarnings("ignore",category=DeprecationWarning)
print(__doc__)



############################################################################################
## Paths for the present analysis.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################

# MEG data - path to the acquisition data (raw data before BIDS )
dir_raw_acq='/sps/crnl/users/pduret/ContextOdd/ContextOdd_Data/Raw_Data/MEG'
# MEG data -  path to the BIDS-acquisition data (raw data after BIDS - Step00 )
dir_raw_bids='/sps/crnl/users/pduret/ContextOdd/ContextOdd_Data/Raw_Data/MEG_BIDS'
# MEG data -  path to the data, analyzed with MNE  ( onwards Step01 )
dir_mne='/sps/crnl/users/pduret/ContextOdd/ContextOdd_Data/mMNE'
# STIM data - path to the Matlab folder containing stimulus sequences (before BIDS )
dir_stim_acq='/sps/crnl/users/pduret/ContextOdd/StimSeq'

#############################################################################################
## SUBJECTS - .-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################


#############################################################################################
## STEP00 - BIDS .-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
# This steps converts raw data in native format into Bids

import step_00_COdd_meg_bidsifier
step_00_COdd_meg_bidsifier 

#############################################################################################
## STEP01 -  IMPORT INTO MNE.-.-.(Outputs = *.raw.fif, *.raw-eve.fif) .-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
# This steps imports raw data  into MNE
# Output files: *.raw.fif, *.raw-eve.fif

import step_01_COdd_meg_import_into_mne
step_01_COdd_meg_import_into_mne

#############################################################################################
## STEP02 -  RECODE EVENTS .-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
# Original event codings rests on 4 event types: Start:16, Standard:1, Deviant:2, End:32
# These are stored in *.raw-eve.fif
# Here we adopt a five-digit coding to account for all conditions
# Output = *.5d.raw-eve.fif

# we have 4 "non-regular" subjects: 4,16,19,31 : fixed

step_02_COdd_eeg_recode_events

#############################################################################################
## STEP03 -  DOWNSAMPLE .-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
# Reduce data by downsampling from 1000Hz to 500Z. Events occurence are adjjusted.
# OUtput files: *.preproc.raw.fif, *.preproc.5d-eve.fif

step_03_COdd_eeg_downsample

#############################################################################################
## STEP04 - EPOCHS    .-.-.-.-.-.-.-.-.
#############################################################################################
# Epochs of -200 + 410 ms (continuous)
# Peak-to-peak amplitude rejection (500uV) to remove non-physiological noise (EEG system), and very strong muscle artifacts
# Identification of bad sensors
# Data are epoched, but stay unfiltered
# Creates :  sub-XX/preproc/sub-XX.preproc-epo.fif
step_04_COdd_eeg_epochs_raw


#############################################################################################
## STEP05 - FILTERING and EPOCHING    .-.-.-.-.-.-.-.-.
#############################################################################################
# Raw data are filtered : notch (powerline) and bandpass 1-40 Hz
# Epochs are then created, using the step04 trial rejection
# Bad sensors are reported but not dropped nor interpolated (further steps below)
#
# Creates :  sub-XX/sub-XX.preproc_1-40.raw.fif
#            sub-XX/sub-XX.preproc_1-40Hz-epo.fif

step_05_COdd_eeg_filter


#############################################################################################
## STEP06 -ICA on EPOCHS    .-.-.-.-.-.-.-.-.
#############################################################################################
