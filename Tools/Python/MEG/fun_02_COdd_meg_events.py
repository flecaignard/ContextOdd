#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
===============================================
FUNCTIONS for CONTEXTODD - STEP 2 analysis
===============================================
=> 02: Event recoding


Created on Tue Feb 12 13:43:16 2019

@author: Pauline Duret

"""


#############################################################################################
## Import packages          -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
# import argparse               # add input arguments

import numpy as np
import os                       # path.join for file names
import matplotlib.pyplot as plt # from the MNE examples
import warnings
from statistics import mean

print(__doc__)

from mne import get_config, set_config
type(get_config('MNE_BROWSE_RAW_SIZE'))
set_config('MNE_BROWSE_RAW_SIZE', '20.0,10.0')


from mne import write_events, read_events
from mne.viz import plot_events

warnings.filterwarnings("ignore",category=DeprecationWarning)
warnings.filterwarnings("ignore",category=RuntimeWarning)


#####################################################################################################################
#############################################     FUNCTIONS      ####################################################
#####################################################################################################################

        
           
def codd_rename_events(dir_mne, subj_id, run, plot=True):
  
    print(f'Treating subject {subj_id}, run {run}..............................')
    
    # Retrieve the events from mne and concentrate on the codes
    events = read_events(os.path.join(dir_mne, subj_id, run,'preproc', f'{subj_id}_task-ContextOdd_{run}_meg.raw-eve.fif'))
    events_codes  = [event[2] for event in events]
    
    # In MEG, we are not sure to have a start (16) or stop code (32), just in case, we remove it:
    if events_codes[0] == 16:
        flag_start = events_codes.pop(0)
    if events_codes[-1] == 32:
        flag_stop = events_codes.pop(-1)
    if not len(events_codes)==1515:
        print(f'WARNING: run {run} of subject {subj_id} has {len(events_codes)} events instead of 1515')
        
    # find the deviant ids and the corresponding chunks size
    dev_id = [i for i, code in enumerate(events_codes) if code == 2]
    chunks = [dev_id[0]] + [ dev_id[i] - dev_id[i - 1] - 1 for i in range(len(dev_id)) if i >= 1 ]
    
    # initiate the new coding of events
    new_codes = [[0,0,0,0,3] for i in range(len(events_codes ))]
    condition = 'condition_var_p'
    
    for i in dev_id:
        new_codes[i][4] = 2 # code for deviants
        new_codes[i- 1][4] = 1 # code for std preceeding deviants (default = 3)
    
    for start in range(len(chunks))[::9]: 
    # The stim sequence can be divided into 28 blocks containing each 3 cycles of 3 chunks
    # The block is a unit where current deviant probability and variability can be retrieved
    
        block = chunks[start:start + 9]
        block_ids = dev_id[start:start+ 9]
        dev_proba = mean(block)
        if dev_proba == 3:
            p_code = 1
        elif 4 < dev_proba < 6:
            p_code = 2
        elif dev_proba == 7:
            p_code = 3
        else:
            print(f'Error: Block found with {dev_proba} deviant probability')
            #return
        
        if 2 in block and 8 in block:
            condition = 'condition_var_c'
            var_code = 2
        else:
            var_code = 1
        
        # let's fill up the code list for all sounds present in the current block
        for chunk, block_id in zip(block, block_ids):
            for code in new_codes[block_id - chunk : block_id + 1] :
                code [1] = chunk
                code [2] = var_code
                code [3] = p_code
            
    for code in new_codes: # let's fill up the condition (var_p or var_c)
        if condition == 'condition_var_p':
            code[0] = 2
        else:
            code[0] = 1
    
    # copy and save the events in a new file
    events_new = np.copy(events)
    for code_space, new_code in zip(events_new, new_codes):
        if code_space[2] == 16:
            continue
        elif code_space[2] == 32:
            continue
        else:
            code_space[2] = int(''.join(str(i) for i in new_code))
        
        
    print(f'{subj_id}, {run} was {condition}')    
    print('Writing new event file...')
    f_events_new = os.path.join(dir_mne,  f'{subj_id}.5d-eve.fif')
    write_events(f_events_new, events_new)
    print('                -                       ')
    
    ### PLOT
    if plot:
        plot_events(events_new,  show = False)
        plt.title(f' {subj_id} , {run}: renamed events')
        plt.show()
    
    