#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
===============================================
Step 03 - Remove jumps
===============================================

Remove jumps (high and rapid peak-to-peak variation)
These could corrupt filtering afterwards (mostly in MEG)
Such artifcats are of non-physiological origin (EEG system)
Outputs: 
    - annotation files where bad segments have been defined
    - when stated, corrected raw data at the latency of the jumps

Created on Mon Feb 11 17:57:17 2019

@authors: Françoise Lecaignard, Pauline Duret
"""



###############################################################################
#### Import Built-in and mne functions
import os
import numpy as np
import sys
from mne.io import read_raw_fif 
from mne import pick_types

#### Add your own path to Utils and specific functions
sys.path.append('/sps/crnl/users/pduret/ContextOdd/ContextOdd_Git/Tools/Python/MEG')
sys.path.append('/sps/crnl/users/pduret/ContextOdd/ContextOdd_Git/Tools/Python/Utils')

#### Import your functions
from fun_jumps import  raw_jump_detect

#### mne Settings
from mne import get_config, set_config
type(get_config('MNE_BROWSE_RAW_SIZE'))
set_config('MNE_BROWSE_RAW_SIZE', '20.0,10.0')

################################################################################
# Input Parameters 
################################################################################
# MEG data -  path to the BIDS-acquisition data (raw data after BIDS - Step00 )
dir_raw_bids='/sps/crnl/users/pduret/ContextOdd/ContextOdd_Data/Raw_Data/MEG_BIDS'
# Paths to data analysis ----------------------------
dir_mne = '/sps/crnl/users/pduret/ContextOdd/ContextOdd_Data/mMNE'

################################################################################
# Define Constant Variables
################################################################################
# In EEG, these jumps are hard to detect because not too rapid wrt muscles.
# A typical example : sub-29 at latency 3627.02 -> 3627.08: it lasts 60 ms , and p2p>450 uV
# but at 3618.65, there is a muscle artifcat that is more rapid
# So the following threshold is such that it also removes muscle, but this is fine because we don' want these data
#
# other example: sub-05, t=2408.36

jump_thresh =35e-12  #    jump_thresh= threshol in volts
jump_dur = 100e-3      #    jump_dur: jump duration in seconds
jump_tw = 2           #    jump_tw:  bad segment time-window (in sec). Ex tw=2 means that we remove 2 seconds around each jumps (using MNE annotations)

# filter data to detect jumps (?)
lf, hf = None, None

from fun_create_subj_list import create_subj_list
subj_id_list = create_subj_list(dir_raw_bids)

################################################################################
# Let's start!
################################################################################

# subj_id_list -----------------------
subj_id_list = {'sub-mTYP01':[1,2,3,4]}
#runs = ['1','2','3','4']
#
for i_su, runs in subj_id_list.items():
    print(f'######### Now treating {i_su} ##########')
    for run  in runs:
        print(f'---run {run}---')
        # create folders
        subj_path = os.path.join(dir_mne, f'{i_su}' , f'run-{run}' , 'preproc')
        #load data
        f_raw = os.path.join(subj_path, f'{i_su}_task-ContextOdd_run-{run}_meg.raw.fif')  
        raw = read_raw_fif(f_raw, preload=False)
        
        #do stuff 
        picks = pick_types(raw.info, meg='mag', exclude='bads')
        
        raw_jump_detect(raw, picks, jump_thresh, jump_dur, jump_tw, remove_jumps = False,
                    plot = False, save = False, f_jump_out = None, f_raw_out = None)


################################################################################
# TESTS
################################################################################
        
