"""
=================================
BIDS conversion for the ContextOdd studies
=================================

Here, we show how to do BIDS conversion for group studies.
The data is available here: https://openfmri.org/dataset/ds000117/

References
----------

[1] Wakeman, Daniel G., and Richard N. Henson.
"A multi-subject, multi-modal human neuroimaging dataset."
Scientific data, 2 (2015): 150001.

"""
# Adapted from:
# Authors: Mainak Jas <mainak.jas@telecom-paristech.fr>
#          Teon Brooks <teon.brooks@gmail.com>
# By Pauline Duret <pauline.duret@inserm.fr>

# License: BSD (3-clause)

###############################################################################
# Let us import ``mne_bids``

import os.path as op
import os
import re
import mne
from collections import defaultdict
from mne_bids import write_raw_bids
from mne_bids.utils import print_dir_tree
from mne import find_events


###############################################################################
# And fetch the data. List all the subjects in your Rawdata directory.

files = os.listdir(data_path)
subject_ids = dict()
runs = defaultdict(list)

# we list the files in the directory and we list the runs for each subject
for file in files:
    subject_name = re.match( r'(.*)_MMNASD(.*)_[0-9]{2}\.ds', file)
    if subject_name:
        subject_ids[subject_name[1]] = (f'{subject_name[1]}_MMNASD{subject_name[2]}')
        runs[subject_name[1]].append(file[-4]) 

###############################################################################
#
# Define event_ids.

event_id = {'Start': 16,  'End': 32, 'Std': 1,
           'Dev': 2}

###############################################################################
# Let us loop over the subjects and create BIDS-compatible folder

for subject, long_name, in subject_ids.items() :
    for run in runs[subject]:
        
        raw_fname = op.join(data_path, f'{long_name}_0{run}.ds')
        raw = mne.io.read_raw_ctf(raw_fname)
        
        if run in ['1','6']:
            bids_basename = (f'sub-{subject}_task-ContextOdd_run-RS{run}')
            write_raw_bids(raw, bids_basename, output_path, events_data = None, 
                       event_id=event_id, overwrite=True)

            
        else:
            events = find_events(raw, shortest_event=2, stim_channel='UPPT001', output='onset')
            bids_basename = (f'sub-{subject}_task-ContextOdd_run-{int(run) - 1}')
            write_raw_bids(raw, bids_basename, output_path, events_data = events, 
                       event_id=event_id, overwrite=True)

###############################################################################
# Now let's see the structure of the BIDS folder we created.
print_dir_tree(output_path)
print_dir_tree(data_path)