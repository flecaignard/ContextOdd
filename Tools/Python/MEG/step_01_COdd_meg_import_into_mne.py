"""
===============================================
Import ContextOdd raw MEG data into MNE
===============================================

This script converts CTF raw into MNE 
Outputs = *.raw.fif, *.raw-eve.fif

Raw data may have been formatted as Bids prior this step (not mandatory but advised)

"""

# Authors: Françoise Lecaignard & Pauline Duret
# Created on Tue 05 March 2019

###############################################################################
import os
import sys
sys.path.append('/sps/crnl/users/pduret/ContextOdd/ContextOdd_Git/Tools/Python/Utils')
sys.path.append('/sps/crnl/users/pduret/ContextOdd/ContextOdd_Git/Tools/Python/MEG')

#import mne_bids 
from mne_bids.utils import print_dir_tree

from fun_01_COdd_meg_import import codd_meg_import_raw_all
from fun_create_subj_list import create_subj_list

############################################################################################
## Paths for the present analysis.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################

# MEG data -  path to the BIDS-acquisition data (raw data after BIDS - Step00 )
dir_raw_bids='/sps/crnl/users/pduret/ContextOdd/ContextOdd_Data/Raw_Data/MEG_BIDS'
# MEG data -  path to the data, analyzed with MNE  ( onwards Step01 )
dir_mne='/sps/crnl/users/pduret/ContextOdd/ContextOdd_Data/mMNE'


################################################################################
# Input Parameters 
################################################################################

# subj_id_list -----------------------

# test
#subj_id_list = ['mASD03','mTYP07'] #
# apply to all
subj_id_list = create_subj_list(dir_raw_bids)

################################################################################
# Define Constant Variables
################################################################################
  
# Total number of events 
nr_evt = 1516 # 4 sessions of 1517 evts (start - 1515 sounds - end)
# apparently in MEG we only have the end, to confirm...
# Plot raw data
plot=False
################################################################################
# Let's start!
################################################################################

#
# For each subject, preprocessings datafile  are in dir_mne/sub-XX/preproc/.
for i_su, runs in subj_id_list.items:
    print(f'######### Now loading {i_su} ##########')
    for run  in runs:
        print(f'---run {run}---')
        # create folders
        subj_path = os.path.join(dir_mne, f'{i_su}' , f'run-{run}' , 'preproc')
        if not os.path.exists(subj_path):
            os.makedirs(subj_path)
        else:
            print(f'Already done: subject {i_su}, run {run}.')
            continue
    
        # Input raw filename  (BIDS format)
        bids_basename = f'{i_su}_task-ContextOdd_run-{run}_meg'
        f_raw_in = os.path.join(dir_raw_bids, f'{i_su}' ,'meg', bids_basename + '.ds')
    
        # Output filenames
        f_raw_out = os.path.join(subj_path, bids_basename + '.raw.fif')
        f_eve_out = os.path.join(subj_path, bids_basename + '.raw-eve.fif')
    
    # Import data and events => creates *.raw.fif, *.raw-eve.fif
        codd_meg_import_raw_all(f'{i_su}', run, f_raw_in, f_raw_out, f_eve_out, nr_evt, plot=plot )
        print_dir_tree(subj_path)