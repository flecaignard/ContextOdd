#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 19 16:28:59 2018

@author: flecaignard
"""

#############################################################################################
## -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.         TIPS         -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
#
    
    
# plot a single epoch
epochs[6].plot()    

# declare a random array
#a=np.random.randint(0,10, size=(4, 15))
#amp_range = np.ptp(a,axis=0)

## chanel indexing
inst.ch_names.index(k)



############################################################################################
# Script version - Get input arguments     -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
############################################################################################

import argparse


parser = argparse.ArgumentParser(description='This script is meant to downsample original data (Fs_orig = 1024 Hz => 512 Hz)')
parser.add_argument('dir_mne', type=str, help='Path to MNE data files folder')
parser.add_argument('subj_id', type=str, help='Subject ID (eSu01, ...)')

args = parser.parse_args()
print(args.subj_id)
dir_mne = args.dir_mne
subj_id = args.subj_id