#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 15 20:48:29 2018

@author: francoise
"""
#############################################################################################
## THIS RUN_SCRIPT IS DEDICATED TO PREPROCESSING DATA 
## STEP1: IMPORT DATA
## STEP2: RENAME EVENTS
## STEP3: DOWNSAMPLE
## STEP4: NOTCH POWERLINE ARTIFACT
## STEP5:BAD SENSORS USING PSD
## STEP6 ICA 
##
##
## IT GOES WITH funct_COdd_eeg_preproc.py
## AND ALSO COdd_eeg_run_MISC.py  to manipulate, view, plot specific things
## ADVICE: COdd_eeg_run_MISC.py should be executed in a second Spyder Iconsole)
#############################################################################################

#
#
# COdd_eeg_run_preproc.py is associated to COdd_eeg_preproc.py
# Each function in *run* accounts for individual cases (missing data, ICA component id, ...)
# Main function in *run* (at the end of the file) is meant to proceed to each step one after the other , and comments are meant to keep a history of what has be done for each subject
#
#############################################################################################
## Import packages          -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
# import argparse               # add input arguments

import numpy as np             # convention d'import
import os                       # path.join for file names
import matplotlib.pyplot as plt # from the MNE examples
import csv
import warnings
import argparse

from mne import Epochs, pick_types, find_events, write_events, read_events
from mne.channels import read_layout, Montage, read_montage
from mne.io import concatenate_raws, read_raw_brainvision, read_raw_fif, find_edf_events
from mne.datasets import eegbci
from mne.decoding import CSP
from mne.viz import plot_events
from mne.preprocessing import read_ica



print(__doc__)


from mne import get_config, set_config
type(get_config('MNE_BROWSE_RAW_SIZE'))
set_config('MNE_BROWSE_RAW_SIZE', '20.0,10.0')

import sys      # path to my own functions
sys.path.append('/Users/francoise/Documents/Projets/ContextOdd/ContextOdd_Git/Tools/Python/EEG')

from COdd_eeg_preproc import codd_eeg_view_raw, codd_eeg_import_raw_all,  codd_eeg_get_session_order
from COdd_eeg_preproc import codd_eeg_rename_events, codd_eeg_rename_events_eSu04, codd_eeg_rename_events_eSu16, codd_eeg_rename_events_eSu19
from COdd_eeg_preproc import codd_eeg_downsample_data_events, codd_eeg_downsample_raw_events, codd_eeg_notch_powerline
from COdd_eeg_preproc import codd_eeg_plot_psd, codd_eeg_psd_save_bad, codd_eeg_psd_erase_bad, codd_eeg_filter
from COdd_eeg_preproc import codd_eeg_ica_fit, codd_eeg_ica_view, codd_eeg_ica_save, codd_eeg_ica_view_corr, codd_eeg_ica_plot_blink_evoked
from COdd_eeg_preproc import codd_eeg_make_epochs, codd_eeg_explore_epoching_threshold, codd_eeg_find_threshold

from fun_eeg_misc import raw_jump_detect

warnings.filterwarnings("ignore",category=DeprecationWarning)




#############################################################################################
## Step1 - Import raw data  into MNE (output = *.raw.fif) -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
def run_codd_eeg_import_raw_all(dir_rawdata, dir_mne, subjlist_file, subj_code, plot=True ):
    
    # Import montage 10-20
    montage = read_montage('standard_1020')
    
    # Total number of events 
    nr_evt = 6068 # 4 sessions of 1517 evts (start - 1515 sounds - end)
    
    # Import data and events => creates *.raw.fif, *.raw-eve.fif
    codd_eeg_import_raw_all(dir_rawdata, dir_mne, montage, subjlist_file, subj_code, nr_evt, plot=plot )
 

#############################################################################################
## Step2 - Recode events  (output = *.renamed.raw-eve.fif) -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
# from a continuous file made of 4 sessions (codes: 16, 1, 2, 32 for start, std, dev and stop, resp.)
# rename every std and dev of each session according to a 5-digit code (see notes about this)
# using for each session, a text file with two columns (1st= {1 and 2 } original, 2nd= {5-digit codes}  that had been created with Matlab

def run_codd_eeg_rename_events(dir_eventcodes, dir_mne, subjlist_file,  subj_id,  plot=True):

    session_order = codd_eeg_get_session_order(subjlist_file, subj_id)
    nr_event = 6068
    
    if subj_id == 'eSu04': # beginning of bloc2 is missing
        codd_eeg_rename_events_eSu04(dir_eventcodes, dir_mne, session_order , nr_event, plot = plot)
        
    elif subj_id == 'eSu16': # faux-depart 
        codd_eeg_rename_events_eSu16(dir_eventcodes, dir_mne, session_order , nr_event, plot = plot)
        
    elif subj_id == 'eSu19': # received sequences of eSu18, faux-depart at bloc1
        codd_eeg_rename_events_eSu19(dir_eventcodes, dir_mne, session_order , nr_event, plot = plot)

    elif subj_id == 'eSu31': # received sequences of eSu30
        codd_eeg_rename_events(dir_eventcodes, dir_mne,  subj_id, 'eSu30', session_order , nr_event, plot=plot)
    else:
        codd_eeg_rename_events(dir_eventcodes, dir_mne,  subj_id, subj_id, session_order , nr_event, plot=plot)


#############################################################################################
## Step3 - Downsample (output = *.preproc.raw.fif, *.preproc.renamed.raw-eve.fif) -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
def run_codd_eeg_downsample(dir_mne,  subj_id,  plot=False):
    #this function downsample raw data and renamed events
    new_freq = 500
    codd_eeg_downsample_data_events(dir_mne, subj_id, new_freq, plot=plot )  

def run_codd_eeg_downsample_raw_events(dir_mne, subj_id, plot=False):
    # this function downsample raw events (codes 1 and 2)
    new_freq=500
    codd_eeg_downsample_raw_events(dir_mne, subj_id, new_freq, plot=plot )
#############################################################################################
## Step4 - Nocth (output = *.preproc_notch.fif) -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
def run_codd_eeg_notch(dir_mne,  subj_id,  plot=False):
    pref_in = 'preproc'
    pref_out = 'preproc50'
    power_freq_array = np.arange(50, 201, 50)
    codd_eeg_notch_powerline(dir_mne, subj_id, pref_in, pref_out, power_freq_array, plot=plot )


    
    
#############################################################################################
## Step5 - Identify bad sensors with PSD                 -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
# using notch-filtered data
# identify and set as bad sensors that show large muscle and residual powerline artifacts
# intermediate bad SNR: see whether can be kept in ERPs (not event-locked) without degrading the trial rejection
# weird artefact like barbelé : see if ICA can handle it
    
def run_codd_eeg_psd_identify(dir_mne,  subj_id):
    pref = 'preproc50'
    tmin, tmax = 420, 480 # we look at 60 sec of signal in the middle of each bloc (7th min)
    fmin, fmax = 1, 250  # look at frequencies between 2 and 300Hz
    n_fft = 2048 
    codd_eeg_plot_psd(dir_mne, subj_id, pref, tmin, tmax, fmin, fmax, n_fft )    
    
# look at plots to id bad sensors
# close all using: plt.close('all')
# and fill in the bad-sensor dictionary below:
#initiliazing this dict with empty values: 'eSuXX': ['not performed yet'],	\   
# this is useful to enter the loop in the main script (below, step5)    
badsensor_list = {'eSu00': ['T7', 'TP9'],	\
                  'eSu01': [],	\
                  'eSu02': [],	\
                  'eSu03': [],	\
                  'eSu04': ['T7'],	\
                  'eSu05': [],	\
                  'eSu06': [],	\
                  'eSu07': [],	\
                  'eSu08': [],	\
                  'eSu09': ['T7'],	\
                  'eSu10': [],	\
                  'eSu11': ['PO9'],	\
                  'eSu12': [],	\
                  'eSu13': [],	\
                  'eSu14': ['P3'],	\
                  'eSu15': ['T8'],	\
                  'eSu16': [],	\
                  'eSu17': [],	\
                  'eSu18': [''],	\
                  'eSu19': ['T7', 'T8'],	\
                  'eSu20': [],	\
                  'eSu21': [],	\
                  'eSu22': [],	\
                  'eSu23': [],	\
                  'eSu24': [],	\
                  'eSu25': [],	\
                  'eSu26': [],	\
                  'eSu27': [],	\
                  'eSu28': [],	\
                  'eSu29': [],	\
                  'eSu30': ['T8'],	\
                  'eSu31': [],	\
                  'eSu32': [],	\
                  'eSu33': [],	\
                  'eSu34': [],	\
                  'eSu35': [],	\
                  	}
def run_codd_eeg_psd_save_bad(dir_mne,  subj_id,badsensor_list):
    badsensor = badsensor_list[subj_id]
    if  badsensor:
        pref = 'preproc50'
        codd_eeg_psd_save_bad(dir_mne, subj_id, pref, badsensor)
        
    else:
        print('No bad sensors for this subject')
        
#############################################################################################
## Step6 - ICA for blink, saccade artifacts             -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
# a 1Hz-highpass is applied first    
    
def run_codd_eeg_ica_fit_epoch(dir_mne, subj_id):

    pref = 'preproc50_1Hz'
    ica_n_comp = 0.99
    epoch_tmin = -0.2
    epoch_tmax = 0.4
    pref_ica = '{}_epoch'.format( pref)
    rej_thresh = 300e-6 # as measured (manually) with a peak-to-peak analysis in run_MISC
    decim = 2
    if not os.path.isfile(os.path.join(dir_mne, '{}.{}.raw.fif'.format(subj_id, pref))):
        pref_in = 'preproc50'
        l_freq, h_freq =  1, None
        codd_eeg_filter(dir_mne, subj_id, pref_in, pref, l_freq, h_freq)

    codd_eeg_ica_fit(dir_mne, subj_id, pref,  ica_n_comp, 'epoch', decim, rej_thresh, epoch_tmin, epoch_tmax,  pref_ica)
    
def run_codd_eeg_ica_fit_conti(dir_mne, subj_id):

    pref = 'preproc50_1Hz'
    ica_n_comp = 0.99
    pref_ica = '{}_conti'.format( pref)
    rej_thresh = 300e-6 # as measured (manually) with a peak-to-peak analysis in run_MISC
    decim = 2
    if not os.path.isfile(os.path.join(dir_mne, '{}.{}.raw.fif'.format(subj_id, pref))):
        pref_in = 'preproc50'
        l_freq, h_freq =  1, None
        codd_eeg_filter(dir_mne, subj_id, pref_in, pref, l_freq, h_freq)

    codd_eeg_ica_fit(dir_mne, subj_id, pref,  ica_n_comp, 'continuous', decim,  rej_thresh, pref_ica)

        
def run_codd_eeg_ica_select(dir_mne, subj_id, mode):
    # mode = 'epoch' or 'conti'
    pref = 'preproc50_1Hz'
    pref_ica = '{}_{}'.format( pref, mode)
    codd_eeg_ica_view(dir_mne, subj_id, pref, pref_ica)

#below: selected components for each subject
# initialize dict with 'eSuXX': ['to-be-done'],	\
ica_comp_list = {'eSu00': [0, 3],	\
                  'eSu01': [1, 4],	\
                  'eSu02': [1],	\
                  'eSu03': [1],	\
                  'eSu04': [1],	\
                  'eSu05': [0],	\
                  'eSu06': [1],	\
                  'eSu07': [1,2],	\
                  'eSu08': [0, 1],	\
                  'eSu09': [1],	\
                  'eSu10': [0,2],	\
                  'eSu11': [0, 3],	\
                  'eSu12': [1,4],	\
                  'eSu13': [1,3],	\
                  'eSu14': [0],	\
                  'eSu15': [1,4],	\
                  'eSu16': [0,4],	\
                  'eSu17': [0],	\
                  'eSu18': [2],	\
                  'eSu19': [2],	\
                  'eSu20': [1,2],	\
                  'eSu21': [1, 4],	\
                  'eSu22': [1],	\
                  'eSu23': [0,3],	\
                  'eSu24': [1,2],	\
                  'eSu25': [1,3],	\
                  'eSu26': [0,2],	\
                  'eSu27': [1,5],	\
                  'eSu28': [1,4],	\
                  'eSu29': [1],	\
                  'eSu30': [1,2],	\
                  'eSu31': [1,5],	\
                  'eSu32': [0,3],	\
                  'eSu33': [1,2],	\
                  'eSu34': [1,2],	\
                  'eSu35': [1,6],	\
                  	}  
# subjects for whom sacc compo is questionable:
# 11: see if (0,3) is ok, otherwise (0)
# 00: in paricular at t=2000 sec
#01, 27, 25
## subjects with weird compo
## 03 : very few blinks
##, 08 : compo01 mirrored 00 ?
## su22
def run_codd_eeg_ica_save(dir_mne, subj_id,  ica_comp_list):
    pref = 'preproc50_1Hz'
    pref_ica = '{}_epoch'.format( pref)
    ica_sel = ica_comp_list[subj_id]
    if ica_sel == ['to-be-done']:
        print(ica_sel, subj_id)
    else:
        codd_eeg_ica_save(dir_mne, subj_id, pref_ica,  ica_sel)

def run_codd_eeg_ica_save_alt(dir_mne, subj_id,  ica_sel): #testing combination of components for instance
    pref = 'preproc50_1Hz'
    pref_ica = '{}_epoch_alt'.format( pref)
    codd_eeg_ica_save(dir_mne, subj_id, pref_ica,  ica_sel)
  
def run_codd_eeg_ica_view_corr(dir_mne, subj_id, plot_eeg=[], view_start=0, view_stop=0, alt=False):
    # plot raw data before and after ICA correction
    # optional: plot a few sensors (indicated in plot_eeg, using label names), in a time interval (in seconds) using view_start and view_stop
    # alt = True allows to plot another ica correction stored in a *epoch_alt.ica.fif file
    
    pref = 'preproc50_1Hz'
    if alt==True:
         pref_ica = '{}_epoch_alt'.format( pref) #testing combination of components for instance
    else:
         pref_ica = '{}_epoch'.format( pref)
         
         
    f_ica = os.path.join(dir_mne, '{}.{}-ica.fif'.format(subj_id, pref_ica))
    ica=read_ica(f_ica)
    print(ica.exclude)
    
    f_raw = os.path.join(dir_mne, '{}.{}.raw.fif'.format(subj_id, pref))
    raw = read_raw_fif(f_raw, preload=True)
    
    codd_eeg_ica_view_corr(dir_mne, subj_id, raw, ica, plot_eeg, view_start, view_stop)
    codd_eeg_ica_plot_blink_evoked(subj_id, raw, ica,  888, plot_raw=True)
    #codd_eeg_ica_plot_blink_evoked(subj_id, raw, ica,  888, plot_raw=True, thresh=100e-6, hf=80)

#############################################################################################
## Step7- Remove bad segments like jumps and non-physiological artefacts     .-.-.-.-.-.-.-.-.
#############################################################################################
# we do this now, maybe it should have been better to do this before the ICA (?)
# but these jumps (example eSu29, at t=3627 sec) looks well after ICA correction (no interference?)
    
def run_codd_eeg_rm_jumps(dir_mne, subj_id, lf=1, hf=40):
    raw = read_raw_fif(os.path.join(dir_mne, '{}.preproc50_1Hz.raw.fif'.format(subj_id)), preload=True)
    ica=read_ica(os.path.join(dir_mne, '{}.{}-ica.fif'.format(subj_id, 'preproc50_1Hz_epoch')))
    raw = ica.apply(raw)
#    raw.filter(lf, hf)
    picks = pick_types(raw.info, eeg=True, stim=False,exclude=())
    
    jump_thresh = 250e-6
    jump_dur = 40e-3
    jump_tw = 2 # we remove 2 seconds around each jumps (using MNE annotations)
    f_jump_out = os.path.join(dir_mne, '{}.{}-annot.fif'.format(subj_id, 'preproc50_1-40Hz'))
    f_raw_out = os.path.join(dir_mne, '{}.{}.raw.fif'.format(subj_id, 'preproc50_1Hz_annot'))
    raw_jump_detect(raw, picks, jump_thresh, jump_dur, jump_tw,save=True, f_jump_out =f_jump_out,f_raw_out=f_raw_out)
  

#############################################################################################
## Step8- Epoching on 1-40 Hz data         -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
# we interpolate bad channels    

    
def run_codd_eeg_epoching(dir_mne, subj_id, event_id, event_fname, rej_thresh, exclude_eeg=[]):
    lf, hf = 1, 40 #Hz
#    raw = read_raw_fif(os.path.join(dir_mne, '{}.preproc50_1Hz_annot.raw.fif'.format(subj_id)), preload=False)
    raw = read_raw_fif(os.path.join(dir_mne, '{}.preproc50_1Hz.raw.fif'.format(subj_id)), preload=False) # HBM emergency ;-)
    ica=read_ica(os.path.join(dir_mne, '{}.{}-ica.fif'.format(subj_id, 'preproc50_1Hz_epoch')))
    events=read_events(os.path.join(dir_mne, '{}.{}-eve.fif'.format(subj_id, event_fname)))
    f_epoch_out=os.path.join(dir_mne, '{}.preproc50_{}-{}Hz-epo.fif'.format(subj_id, lf, hf))
    codd_eeg_make_epochs(subj_id,raw,ica, events,event_id,rej_thresh, f_epoch_out, exclude_eeg=exclude_eeg, lf=lf, hf=hf )
    
    


def run_codd_eeg_epoching_explore_threshold(subj_id, event_id, event_fname,exclude_eeg=[]):
    lf, hf = 1, 40 #Hz
    raw = read_raw_fif(os.path.join(dir_mne, '{}.preproc50_1Hz_annot.raw.fif'.format(subj_id)), preload=False)
    ica=read_ica(os.path.join(dir_mne, '{}.{}-ica.fif'.format(subj_id, 'preproc50_1Hz_epoch')))
    events=read_events(os.path.join(dir_mne, '{}.{}-eve.fif'.format(subj_id, event_fname)))
    thresh_channel = 10.0
    codd_eeg_explore_epoching_threshold(subj_id,raw,ica, events,event_id,  thresh_channel, exclude_eeg=exclude_eeg, lf=lf, hf=hf )

def run_codd_eeg_epoching_find_threshold(subj_id, event_id, event_fname,exclude_eeg=[]):
    lf, hf = 1, 40 #Hz
    raw = read_raw_fif(os.path.join(dir_mne, '{}.preproc50_1Hz_annot.raw.fif'.format(subj_id)), preload=False)
    ica=read_ica(os.path.join(dir_mne, '{}.{}-ica.fif'.format(subj_id, 'preproc50_1Hz_epoch')))
    events=read_events(os.path.join(dir_mne, '{}.{}-eve.fif'.format(subj_id, event_fname)))
    thresh_channel = 10.0
    thresh_min,  thresh_max = 50e-6, 150e-6
    codd_eeg_find_threshold(subj_id,raw,ica, events,event_id, exclude_eeg=exclude_eeg, lf=lf, hf=hf,thresh_min=thresh_min, thresh_max=thresh_max, thresh_channel=thresh_channel)
    

#############################################################################################
## -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.     MAIN         -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
if __name__== '__main__':
    
    #############################################################################################
    ## Init Paths              -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
    #############################################################################################
    
    # mac_fl
    #dir_mne = '/Users/francoise/Documents/Projets/ContextOdd/Data/EEG/MNE_DevData/MNE_Data'
    #dir_rawdata = '/Users/francoise/Documents/Projets/ContextOdd/ContextOdd_Git/Experimental_Design/StimSeq' # path to the StimSeq folder
    #dir_eventcodes = '/Users/francoise/Documents/Projets/ContextOdd/ContextOdd_Git/Experimental_Design/StimSeq' # path to the StimSeq folder
    #dir_ref = '/Users/francoise/Documents/Projets/ContextOdd/ContextOdd_Git/Reference_Files'
    
    # in2p3
    dir_mne='/sps/inter/crnl/users/pduret/ContextOdd/ContextOdd_Data/eMNE'
    dir_rawdata = '/sps/inter/crnl/users/pduret/ContextOdd...'
    dir_eventcodes='/sps/inter/crnl/users/pduret/ContextOdd/ContextOdd_Git/Experimental_Design/StimSeq' # path to the StimSeq folder
    dir_ref='/sps/inter/crnl/users/pduret/ContextOdd/ContextOdd_Git/Reference_Files'
    


    #############################################################################################
    ## Init Variables              -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
    #############################################################################################
    subjlist_file = '{}/List_Participants_Import_Data.txt'.format(dir_ref)
    
    subj_list = {'NAVMA':	'eSu00',	 'POSRI': 'eSu01',	'BOULA':	'eSu02',	 'BREME': 'eSu03',	'VENAN':	'eSu04',\
                 'BIGOP':	'eSu05',	 'LANJU': 'eSu06',	'LOQJE':	'eSu07',	 'PICJO': 'eSu08',	'EDDAR':	'eSu09',	\
                 'ADOBE':	'eSu10',	 'THIMA': 'eSu11',  'BOUAU':'eSu12', 'MASLO': 'eSu13',  'ANTRA':'eSu14',\
                 'DURMA': 'eSu15', 'BERAM': 'eSu16',  'ALLMA':'eSu17', 'VIAMA': 'eSu18',  'BARTH':'eSu19',	\
                 'COULE': 'eSu20', 'CHMMI': 'eSu21',  'MIGAN':'eSu22',	 'PEGAR': 'eSu23',	'MERAR':'eSu24',	\
                 'SANLU': 'eSu25',	 'ESTYO': 'eSu26',	'FERMA':'eSu27',	 'ADANI': 'eSu28',	'COTCL':'eSu29',	\
                 'ANDNI': 'eSu30',	 'SOUMA': 'eSu31',	'KOCPI':'eSu32',	 'TALCA': 'eSu33',	'MERAM':'eSu34',	\
                 'LEDGA':'eSu35'	}

    
    event_id_codd={'R/c4/R+/Pn/std_pre': 14121, 'R/c4/R+/Pn/dev': 14122,'R/c4/R+/Pn/std_oth': 14123,\
                  'R/c5/R+/Pn/std_pre': 15121, 'R/c5/R+/Pn/dev': 15122,'R/c5/R+/Pn/std_oth': 15123,\
                  'R/c6/R+/Pn/std_pre': 16121, 'R/c6/R+/Pn/dev': 16122,'R/c6/R+/Pn/std_oth': 16123,\
                  'R/c2/R-/Pn/std_pre': 12221, 'R/c2/R-/Pn/dev': 12222,'R/c2/R-/Pn/std_oth': 12223,\
                  'R/c3/R-/Pn/std_pre': 13221, 'R/c3/R-/Pn/dev': 13222,'R/c3/R-/Pn/std_oth': 13223,\
                  'R/c4/R-/Pn/std_pre': 14221, 'R/c4/R-/Pn/dev': 14222,'R/c4/R-/Pn/std_oth': 14223,\
                  'R/c6/R-/Pn/std_pre': 16221, 'R/c6/R-/Pn/dev': 16222,'R/c6/R-/Pn/std_oth': 16223,\
                  'R/c7/R-/Pn/std_pre': 17221, 'R/c7/R-/Pn/dev': 17222,'R/c7/R-/Pn/std_oth': 17223,\
                  'R/c8/R-/Pn/std_pre': 18221, 'R/c8/R-/Pn/dev': 18222,'R/c8/R-/Pn/std_oth': 18223,\
                  'P/c2/R+/P-/std_pre': 22111, 'P/c2/R+/P-/dev': 22112,'P/c2/R+/P-/std_oth': 22113,\
                  'P/c3/R+/P-/std_pre': 23111, 'P/c3/R+/P-/dev': 23112,'P/c3/R+/P-/std_oth': 23113,\
                  'P/c4/R+/P-/std_pre': 24111, 'P/c4/R+/P-/dev': 24112,'P/c4/R+/P-/std_oth': 24113,\
                  'P/c4/R+/Pn/std_pre': 24121, 'P/c4/R+/Pn/dev': 24122,'P/c4/R+/Pn/std_oth': 24123,\
                  'P/c5/R+/Pn/std_pre': 25121, 'P/c5/R+/Pn/dev': 25122,'P/c5/R+/Pn/std_oth': 25123,\
                  'P/c6/R+/Pn/std_pre': 26121, 'P/c6/R+/Pn/dev': 26122,'P/c6/R+/Pn/std_oth': 26123,\
                  'P/c6/R+/P+/std_pre': 26131, 'P/c6/R+/P+/dev': 26132,'P/c6/R+/P+/std_oth': 26133,\
                  'P/c7/R+/P+/std_pre': 27131, 'P/c7/R+/P+/dev': 27132,'P/c7/R+/P+/std_oth': 27133,\
                  'P/c8/R+/P+/std_pre': 28131, 'P/c8/R+/P+/dev': 28132,'P/c8/R+/P+/std_oth': 28133                  }
    
    
    event_id_acq={'std': 1,    'dev':2             }
  
   

    
    ###........................ STEP 1: Import Data and Events for all subjects
#    for subj_code, subj_id in subj_list.items():
#        run_codd_eeg_import_raw_all(dir_rawdata, dir_mne, subjlist_file, subj_code, plot=True )
        
    ###........................  STEP 2: Rename ebents for all subjects
#    for subj_code, subj_id in subj_list.items():
#        run_codd_eeg_rename_events(dir_eventcodes, dir_mne, subjlist_file,  subj_id,  plot=True)

    ###........................  STEP 3: Downsample data and events to 500 Hz 
#    for subj_code, subj_id in subj_list.items():
#        #run_codd_eeg_downsample(dir_mne, subj_id)
#        run_codd_eeg_downsample_raw_events(dir_mne, subj_id)
        
    ###........................  STEP 4: Notch filter => *.preproc_notch.raw.fif
#    for subj_code, subj_id in subj_list.items():
#        run_codd_eeg_notch(dir_mne, subj_id) 

    ###........................  STEP5: PSD plost to identify bad sensors
#    subj_id = 'eSu35'
#    if badsensor_list[subj_id] == ['not performed yet'] :
#        run_codd_eeg_psd_identify(dir_mne,  subj_id) # => any sensors with high 50 Hz? and harmonics? 
#        codd_eeg_view_raw(dir_mne, subj_id, 'preproc50', 1, 40) # => any still-bad sensors  within this bandwidth?
#    # fill in dictionnary badsensor_list (STEP4 section, above)
#    else:
#        run_codd_eeg_psd_save_bad(dir_mne, subj_id, badsensor_list)
#        codd_eeg_view_raw(dir_mne, subj_id, 'preproc50')
##    
##    # hum, erase to start again
##    for subj_code, subj_id in subj_list.items():
##        codd_eeg_psd_erase_bad(dir_mne, subj_id)
#        
    ##........................  STEP6:  ICA for blink, saccade artifacts
    
    ## step0: find a threshold for selecting epochs in ICA fit
    ## this can be done using COdd_eeg_run_MISC, in another Console to see data and adjust threshold, time interval ...
    ## codd_eeg_get_raw_peak2peak, run_codd_eeg__get_nr_accepted_events
    ## => 300 uV seems fine across subjects => adjust run_codd_eeg_ica_fit accordingly
    
    ##step1: compute ica (and highpass if needed)
    # test with one subject:
    #run_codd_eeg_ica_fit_epoch(dir_mne, 'eSu00')
    
#    for subj_code, subj_id in subj_list.items():
#        run_codd_eeg_ica_fit_conti(dir_mne, subj_id)
    

    ##step2: view components => select the blink one and fill in the above dictionnary    
#    for subj_id in ['eSu28']:
#        run_codd_eeg_ica_select(dir_mne, subj_id, 'epoch')

##    ##step3: save ICA and view correction
    # same thresholf for every subject
    #but eSu02: thresh=100uV, hf=40 Hz
    # eSu6: thresh=100 uV, hf=80
#    subj_id = 'eSu28'
#    plot_eeg = [] #['Fp1', 'Fp2', 'Cz']
#    view_start, view_stop = 0.0, 0.0 #2000.0, 2010.0
#    run_codd_eeg_ica_save(dir_mne, subj_id,  ica_comp_list)
#    run_codd_eeg_ica_view_corr(dir_mne, subj_id, plot_eeg, view_start, view_stop)

##   step4, if needed: test new selection of components
#    subj_id = 'eSu00'
#    plot_eeg = ['Fp1', 'Fp2', 'Cz']
#    view_start, view_stop = 2000.0, 2010.0
#    run_codd_eeg_ica_save_alt(dir_mne, subj_id,  [7])
#    run_codd_eeg_ica_view_corr(dir_mne, subj_id, plot_eeg, view_start, view_stop, alt=True)
    
    
    
    ## ........................ STEP7 : REMOVE JUMPS 

#    dir_mne='/sps/inter/crnl/users/pduret/ContextOdd/ContextOdd_Data/eMNE'
#    subj_id='eSu03'
#    run_codd_eeg_rm_jumps(dir_mne, subj_id)
#    
    ##........................  STEP8:  EPOCHING
    # list of subjects that are kind of ready (HBM emergeny !!!)
    subj_list = {'NAVMA':	'eSu00',	 'POSRI': 'eSu01',	'BOULA':	'eSu02',	 	'VENAN':	'eSu04',\
                 'BIGOP':	'eSu05',	 'LANJU': 'eSu06',	'LOQJE':	'eSu07',	 'PICJO': 'eSu08',	'EDDAR':	'eSu09',	\
                 'ADOBE':	'eSu10',	 'THIMA': 'eSu11',  'BOUAU':'eSu12', 'MASLO': 'eSu13',  'ANTRA':'eSu14',\
                 'DURMA': 'eSu15', 'BERAM': 'eSu16',  'ALLMA':'eSu17', 'VIAMA': 'eSu18',  'BARTH':'eSu19',	\
                 'COULE': 'eSu20', 'CHMMI': 'eSu21',  	 'PEGAR': 'eSu23',	'MERAR':'eSu24',	\
                 'SANLU': 'eSu25',	 'ESTYO': 'eSu26',	'FERMA':'eSu27',	 	'COTCL':'eSu29',	\
                 'ANDNI': 'eSu30',	 'SOUMA': 'eSu31',	'KOCPI':'eSu32',	 'TALCA': 'eSu33',	\
                 'LEDGA':'eSu35'	}

    epoching_exclude_eeg = {'eSu00': [],	\
                  'eSu01': [],	\
                  'eSu02': [],	\
                  'eSu03': ['Oz', 'Pz'],	\
                  'eSu04': ['T8'],	\
                  'eSu05': [],	\
                  'eSu06': [],	\
                  'eSu07': ['O1'],	\
                  'eSu08': [],	\
                  'eSu09': [],	\
                  'eSu10': [],	\
                  'eSu11': [],	\
                  'eSu12': [],	\
                  'eSu13': ['PO10'],	\
                  'eSu14': [],	\
                  'eSu15': [],	\
                  'eSu16': ['Oz'],	\
                  'eSu17': [],	\
                  'eSu18': [],	\
                  'eSu19': ['TP10'],	\
                  'eSu20': [],	\
                  'eSu21': [],	\
                  'eSu22': [],	\
                  'eSu23': ['CP1', 'Oz'],	\
                  'eSu24': [],	\
                  'eSu25': ['Oz', 'TP9', 'T8'],	\
                  'eSu26': [],	\
                  'eSu27': [],	\
                  'eSu28': [],	\
                  'eSu29': [],	\
                  'eSu30': [],	\
                  'eSu31': [],	\
                  'eSu32': ['Oz'],	\
                  'eSu33': [],	\
                  'eSu34': ['T8', 'T7'],	\
                  'eSu35': [],	\
                  	}       
    indiv_thresh = {'eSu00': 90e-6,	\
                  'eSu01': 90e-6,	\
                  'eSu02': 100e-6,	\
                  'eSu03': [],	\
                  'eSu04': 100e-6,	\
                  'eSu05': 100e-6,	\
                  'eSu06': 100e-6,	\
                  'eSu07': 90e-6,	\
                  'eSu08': 90e-6,	\
                  'eSu09': 90e-6,	\
                  'eSu10': 90e-6,	\
                  'eSu11': 90e-6,	\
                  'eSu12': 90e-6,	\
                  'eSu13': 100e-6,	\
                  'eSu14': 90e-6,	\
                  'eSu15': 90e-6,	\
                  'eSu16': 100e-6,	\
                  'eSu17': 90e-6,	\
                  'eSu18': 90e-6,	\
                  'eSu19': 90e-6,	\
                  'eSu20': 90e-6,	\
                  'eSu21': 90e-6,	\
                  'eSu22': [],	\
                  'eSu23': 90e-6,	\
                  'eSu24': 90e-6,	\
                  'eSu25': 90e-6,	\
                  'eSu26': 90e-6,	\
                  'eSu27': 90e-6,	\
                  'eSu28': [],	\
                  'eSu29': 100e-6,	\
                  'eSu30': 90e-6,	\
                  'eSu31': 90e-6,	\
                  'eSu32': 90e-6,	\
                  'eSu33': 90e-6,	\
                  'eSu34': [],	\
                  'eSu35': 90e-6,	\
                  	}   
    for subj_code, subj_id in subj_list.items():
        event_id, event_fname = event_id_codd, 'preproc_renamed' #event_id_acq, 'preproc_raw' # event_id_codd, 'preproc_renamed'
        rej_thresh =  indiv_thresh[subj_id]
        exclude_eeg=epoching_exclude_eeg[subj_id] # to remove sensors from  above-threshold decision
        run_codd_eeg_epoching(dir_mne, subj_id, event_id, event_fname, rej_thresh, exclude_eeg=exclude_eeg)

    
#    subj_id='eSu11'
#    event_id, event_fname = event_id_codd, 'preproc_renamed' #event_id_acq, 'preproc_raw' # event_id_codd, 'preproc_renamed'
#    rej_thresh =  90e-6
#    exclude_eeg=epoching_exclude_eeg[subj_id] # to remove sensors from  above-threshold decision
#    run_codd_eeg_epoching(dir_mne, subj_id, event_id, event_fname, rej_thresh, exclude_eeg=exclude_eeg)

#    for subj_id in ['eSu16', 'eSu17', 'eSu18', 'eSu19', 'eSu20', 'eSu21', 'eSu22', 'eSu23', 'eSu24', 'eSu25']:
#    for subj_id in ['eSu26', 'eSu27', 'eSu28', 'eSu29', 'eSu30', 'eSu31', 'eSu32', 'eSu33', 'eSu34', 'eSu35']:
#        run_codd_eeg_epoching_explore_threshold(subj_id, event_id, event_fname, exclude_eeg=epoching_exclude_eeg[subj_id])
##    run_codd_eeg_epoching_find_threshold(subj_id, event_id, event_fname, exclude_eeg=epoching_exclude_eeg[subj_id])



