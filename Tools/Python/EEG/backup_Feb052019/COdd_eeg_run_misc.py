#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 15 20:48:29 2018

@author: francoise
"""
#############################################################################################
## THIS RUN SCRIPT IS DEDICATED TO VIEW, PLOT AND OTHER SIMPLE FUNCTIONS THAT COULD BE OF USE
## WHEN ANALYSZING DATA , WHILE NOT BEING A PROCESSING STEP PER SE
##
## IT GOES WITH funct_COdd_eeg_MISC.py
#############################################################################################

#
#############################################################################################
## Import packages          -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
# import argparse               # add input arguments

import numpy as np             # convention d'import
import os                       # path.join for file names
import matplotlib.pyplot as plt # from the MNE examples
import csv
import warnings
import argparse

from mne import Epochs, pick_types, find_events, write_events, read_events
from mne.channels import read_layout, Montage, read_montage
from mne.io import concatenate_raws, read_raw_brainvision, read_raw_fif, find_edf_events
from mne.datasets import eegbci
from mne.decoding import CSP
from mne.viz import plot_events
from mne.preprocessing import read_ica



print(__doc__)


from mne import get_config, set_config
type(get_config('MNE_BROWSE_RAW_SIZE'))
set_config('MNE_BROWSE_RAW_SIZE', '20.0,10.0')

import sys      # path to my own functions
sys.path.append('/Users/francoise/Documents/Projets/ContextOdd/ContextOdd_Git/Tools/Python/EEG')


from fun_eeg_misc import peak_to_peak_raw

warnings.filterwarnings("ignore",category=DeprecationWarning)


#############################################################################################
## Init Paths              -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################

# mac_fl
#dir_mne = '/Users/francoise/Documents/Projets/ContextOdd/Data/EEG/MNE_DevData/MNE_Data'
#dir_rawdata = '/Users/francoise/Documents/Projets/ContextOdd/ContextOdd_Git/Experimental_Design/StimSeq' # path to the StimSeq folder
#dir_eventcodes = '/Users/francoise/Documents/Projets/ContextOdd/ContextOdd_Git/Experimental_Design/StimSeq' # path to the StimSeq folder
#dir_ref = '/Users/francoise/Documents/Projets/ContextOdd/ContextOdd_Git/Reference_Files'

# in2p3
dir_mne='/sps/inter/crnl/users/pduret/ContextOdd/ContextOdd_Data/eMNE'
dir_rawdata = '/sps/inter/crnl/users/pduret/ContextOdd...'
dir_eventcodes='/sps/inter/crnl/users/pduret/ContextOdd/ContextOdd_Git/Experimental_Design/StimSeq' # path to the StimSeq folder
dir_ref='/sps/inter/crnl/users/pduret/ContextOdd/ContextOdd_Git/Reference_Files'

#############################################################################################
## RUN FUNCT           -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################

def run_codd_eeg_get_nr_accepted_events(dir_mne, subj_id, thresh):
    pref = 'preproc50_1Hz'
    f_raw = os.path.join(dir_mne, '{}.{}.raw.fif'.format(subj_id, pref))
    raw = read_raw_fif(f_raw, preload=True)
    picks = pick_types(raw.info, eeg=True, stim=False, exclude='bads')
    
    f_eve = os.path.join(dir_mne, '{}.preproc_renamed-eve.fif'.format(subj_id))
    events =read_events(f_eve)

    epoch_tmin, epoch_tmax = -0.2, 0.4
    print("---------------- ", subj_id, "  -  ", thresh*1e6, " uV -   ------------------" )
    epochs = Epochs(raw, events, event_id=None, tmin = epoch_tmin, tmax = epoch_tmax, picks=picks,reject=dict(eeg=thresh) , preload=True, verbose=False)
    print("Nr of accepted events = ", len(epochs.events))
   
    
#############################################################################################
## -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.     MAIN         -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
if __name__== '__main__':
    
    
   ###...........MISC .............Get raw data values .......................; 
#    subj_id, pref = 'eSu11', 'preproc50_1Hz'
#    f_raw = os.path.join(dir_mne, '{}.{}.raw.fif'.format(subj_id, pref))
#    raw = read_raw_fif(f_raw, preload=True)
#    lat =  522.75# latency in sec
#    pre, post= -0.5, 0.5 # time interval around lat, in sec
#    sens=raw.pick_types(eeg=True, exclude='bads')
#    #peak_to_peak_raw(raw, lat, pre, post, sens, thresh=None, plot=True)
#    peak_to_peak_raw(raw.filter(1,10), lat, pre, post, sens, thresh=None, plot=True)
    
    
    ### peak2peak on ica-corr raw data
    subj_id, pref = 'eSu02', 'preproc50_1Hz'
    f_raw = os.path.join(dir_mne, '{}.{}.raw.fif'.format(subj_id, pref))
    ica=read_ica(os.path.join(dir_mne, '{}.{}-ica.fif'.format(subj_id, 'preproc50_1Hz_epoch')))
    
    raw = read_raw_fif(f_raw, preload=True)
    raw = ica.apply(raw)
    raw.filter(1,40).interpolate_bads()
    raw.info['bads']

    lat =  1757.35# latency in sec
    pre, post= -0.2, 0.4 # time interval around lat, in sec
    sens=raw.pick_types(eeg=True, exclude='bads')
    peak_to_peak_raw(raw, lat, pre, post, sens, thresh=None, plot=True)
    

    
    ### ........VIEW raw data .................................................
#    subj_id, pref = 'eSu03', 'preproc50_1Hz'
#    codd_eeg_view_raw(dir_mne, subj_id, pref,highpass = None, lowpass = None )
#    
    ###.........Get the number of accepetd events for a specific threshold .............
#    thresh=300e-6    
##    subj_id = 'eSu00'    
##    run_codd_eeg__get_nr_accepted_events(dir_mne, subj_id, thresh)
#    
#    for subj_code, subj_id in subj_list.items():
#        run_codd_eeg__get_nr_accepted_events(dir_mne, subj_id, thresh)
