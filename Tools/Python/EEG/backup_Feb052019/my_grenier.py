#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Oct  2 17:46:56 2018

@author: francoise
"""

## manipulate lists of data

#############################################################################################
## Subject List (class list) -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
codd_eeg_print_step_label("Recode events -  {}".format(subj_id)) 

with open(subjlist_file, 'r') as f:
    reader = csv.reader(f,delimiter = '\t')
    print (reader)
    
    subjlist_datecode = []  # of the form 20180412_VENAN
    subjlist_id = []        #of the form eSuXX
    subjlist_eegsuff = []   #of the form BIGOP__0001 (eeg file suffixe, *.eeg, *.vmrk, *.vhdr)
    subjlist_sess_order_real = []   # should be [1, 2, 3, 4] if  bloc order was correctly applied, but could be [4, 1, 2, 3] if first session delivered bloc 4 due to Presentation issue for instance

    
    for row in reader:
        print(row[0])
        subjlist_datecode.append(row[0]) 
        subjlist_id.append(row[1]) 
        subjlist_eegsuff.append(row[2]) 
        subjlist_sess_order_real.append(np.array([row[3],row[4],row[5],row[6] ])) 
 
# control
for i_su, suj in enumerate(subjlist_sess_order_real):  
    print(i_su, subjlist_datecode[i_su], suj) 
     


