#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 15 20:48:29 2018

@author: francoise
"""

## Attempts to re-organize preprocessings scripts (A. Corneylie)


#############################################################################################
## Import packages          -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
# import argparse               # add input arguments

import numpy as np             # convention d'import
import os                       # path.join for file names
import matplotlib.pyplot as plt # from the MNE examples
import csv
import warnings
import argparse

from mne import Epochs, pick_types, find_events, write_events, read_events
from mne.channels import read_layout, Montage, read_montage
from mne.io import concatenate_raws, read_raw_brainvision, read_raw_fif, find_edf_events
from mne.datasets import eegbci
from mne.decoding import CSP
from mne.viz import plot_events, plot_ica_components, plot_ica_overlay, plot_evoked_topo
from mne.preprocessing import ICA, read_ica, create_eog_epochs, find_eog_events


print(__doc__)

from mne import get_config, set_config
type(get_config('MNE_BROWSE_RAW_SIZE'))
set_config('MNE_BROWSE_RAW_SIZE', '20.0,10.0')




import sys      # path to my own functions
sys.path.append('/Users/francoise/Documents/Projets/ContextOdd/ContextOdd_Git/Tools/Python/EEG')


warnings.filterwarnings("ignore",category=DeprecationWarning)



from fun_eeg_misc import print_step_label, peak_to_peak_raw, plot_good_and_bad_epochs, count_events
from fun_eeg_misc import  epoch_rejection_adaptive_threshold, epoch_rejection_explore_threshold



#####################################################################################################################
#####################################################################################################################
#####################################################################################################################
## Step1 - Import raw data  into MNE (output = *.raw.fif, *.raw-eve.fif) -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#####################################################################################################################
#####################################################################################################################
#####################################################################################################################
def codd_eeg_import_raw_all(dir_rawdata, dir_mne, montage, subjlist_file, subj_code, nr_evt, plot=True ):
    
    # Get acquisition info
    subj_id, datecode, eegsuff = codd_eeg_get_subject_acq_info_list(subjlist_file, subj_code)
    # Import raw data: creates *.raw.fif
    codd_eeg_import_raw_data(dir_rawdata, dir_mne, datecode, eegsuff, subj_id, montage ) 
    # Import raw events: creates *.raw-eve.fif
    f_raw = os.path.join(dir_mne, '{}.raw.fif'.format(subj_id))
    raw = read_raw_fif(f_raw, preload=True)
    events = codd_eeg_import_raw_event(dir_mne, subj_id, raw)
    # Control nr_evt is fine
    if len(events) != nr_evt:
        print('--------------> TROUBLE WITH {}: {} events found instead of {}\n'.format(subj_id, len(events), nr_evt))
    else:
        print('--------------> YOUPI WITH {}\n'.format(subj_id))
    ### PLOT
    if plot:
        codd_eeg_import_view(dir_mne, subj_id)

 
def codd_eeg_get_subject_acq_info_list(subjlist_file, subj_code):
    with open(subjlist_file, 'r') as f:
        reader = csv.reader(f,delimiter = '\t')
        for row in reader:
            if row[0] == subj_code:
                print(row[0])
                print('\n')
                subj_id = row[1]
                datecode = row[2]
                eegsuff = row[3]            
                break
            
    return subj_id, datecode, eegsuff

def codd_eeg_import_raw_data(dir_rawdata, dir_mne, datecode, eegsuff, subj_id, montage ):
    
    f_in = os.path.join(dir_rawdata,datecode, '{}.vhdr'.format(eegsuff))
    f_out = os.path.join(dir_mne, '{}.raw.fif'.format(subj_id))
    if os.path.isfile(f_in) :
        print('Found raw data file: ok')
    else:
        print('Raw data file not found- abandon', f_in)
        return
    if os.path.isfile(f_out):
        print('MNE raw file already exists- abandon', f_out)
        return
    else:
        raw = read_raw_brainvision(f_in, preload=True )
        raw.set_montage(montage, set_dig=True, verbose=None)
        raw.save(f_out, overwrite=True)
        print('-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-')
        print('Save Raw Data : Done')
        return raw



def codd_eeg_import_raw_event(dir_mne, subj_id, raw):
    # extract events from raw.fif 
    f_eve_out = os.path.join(dir_mne, '{}.raw-eve.fif'.format(subj_id))
    events = find_events(raw, shortest_event=2, stim_channel='STI 014', output='onset')
    write_events(f_eve_out,events)
    print('Save Raw Events : Done')
    return events
   
def codd_eeg_import_view(dir_mne, subj_id):
    f_raw = os.path.join(dir_mne, '{}.raw.fif'.format(subj_id))
    raw = read_raw_fif(f_raw, preload=True)
    f_eve = os.path.join(dir_mne, '{}.raw-eve.fif'.format(subj_id))
    events =read_events(f_eve)
    dict_color_event = {16: 'red', 1: 'black', 2: 'magenta', 32: 'red'}
    scal = dict(eeg=10e-5)
    # plot sensors (top view)
    raw.plot_sensors(show_names=True)
    # plot eeg data with events
    raw.plot(events=events,  event_color = dict_color_event, n_channels=raw.info['nchan'],remove_dc = True,  duration=30, highpass = None, scalings=scal) #, show_options=False)
    # plot events
    event_id_raw = {'std': 1, 'dev': 2, 'start': 16, 'stop': 32}
    color_id_raw = {1: 'blue', 2: 'green', 16: 'red', 32: 'c'}
    plot_events(events, color=color_id_raw, event_id=event_id_raw)
    plt.title(' {} : raw events'.format(subj_id))
    plt.show()


def codd_eeg_view_raw(dir_mne, subj_id, pref,highpass = None, lowpass = None ):
    # pref : subj_id.pref.raw.fif
    # defaut is no filter
    f_raw = os.path.join(dir_mne, '{}.{}.raw.fif'.format(subj_id, pref))
    raw = read_raw_fif(f_raw, preload=False)
    scal = dict(eeg=10e-5)
    raw.plot(n_channels=raw.info['nchan'],remove_dc = True,  duration=30, highpass = highpass, lowpass = lowpass,  scalings=scal) #, show_options=False)
    plt.show()



#####################################################################################################################
#####################################################################################################################
#####################################################################################################################
## Step2 - Recode events using a 5-digit format  (output = *.renamed.raw-eve.fif) -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#####################################################################################################################
#####################################################################################################################
#####################################################################################################################
 
def codd_eeg_get_session_order(subjlist_file, subj_id):
    # bloc order may have been delivered incorrectly due to Presentation issue (eg: bloc 4 was delivered first)
    with open(subjlist_file, 'r') as f:
        reader = csv.reader(f,delimiter = '\t')
        # print('youpi')
           
        for row in reader:
            # print(row)
            if row[1] == subj_id:
                print(row[1])
                print('\n')
                session_order = np.array([row[4],row[5],row[6],row[7] ])
                break
    return session_order
           
def codd_eeg_rename_events_loop(dir_eventcodes,subj_id_design,i_b, session_order, events, events_new,ind_e_bloc, ind_s_bloc ):
    # rename events within each bloc (subfunction to codd_eeg_rename_events)
    print('                -                       ')
    f_code = os.path.join(dir_eventcodes,subj_id_design, '{}_ContextOdd_Code_Bloc{}.txt'.format(subj_id_design,session_order[i_b]))
    print('{},  bloc{}----->  {}'.format(subj_id_design, i_b+1,  '{}_ContextOdd_Code_Bloc{}.txt'.format(subj_id_design,session_order[i_b]) ))
    with open(f_code, 'r') as f:
        reader = csv.reader(f,delimiter = '\t')
        nr_row = sum(1 for row in reader) # once read, object reader no longer exists (csv.reader stuff)
        print('Matlab Design, Bloc #{}-----> {} events found '.format(i_b, nr_row))
    with open(f_code, 'r') as f:
        reader = csv.reader(f,delimiter = '\t')
    
        if((ind_e_bloc - ind_s_bloc- 1) == nr_row):
            print('youpi, number of events in this bloc matches raw-eve.fif')
        else:
            print('Problem with bloc {}:not the good number of events'.format(i_b+1))
    
        cnt=1        
        for row in reader: #object reader=csv.reader()
            #print('bloc{},stim #{}, {}/{} - codebloc, acq: {}, {} '.format(i_b+1,cnt,ind_s[i_b]+cnt+1, events.shape[0], row[0], events[ind_s[i_b]+cnt,2] ))
            if(float(row[0]) == events[ind_s_bloc+cnt,2]):
                events_new[ind_s_bloc+cnt,2] = row[1]
                cnt = cnt+1
            else:
                print('Bloc {}: Mismatch btw codes  in Bloc.txt and raw-eve.fif -> abandon'.format(i_b+1) ) 
                print('bloc{},stim #{}, {}/{} - codebloc, acq: {}, {} '.format(i_b+1,cnt,ind_s_bloc+cnt+1, events.shape[0], row[0], events[ind_s_bloc+cnt,2] ))
                break 
            
def codd_eeg_rename_events(dir_eventcodes, dir_mne,  subj_id, subj_id_design, session_order , nr_event, plot = True):
    # from a continuous file made of 4 sessions (codes: 16, 1, 2, 32 for start, std, dev and stop, resp.)
    # rename every std and dev of each session according to a 5-digit code (see notes about this)
    # for each session, refers to a text file with two columns (1st= {1 and 2 } original, 2nd= {5-digit codes}  originally created with Matlab
    # subj_id = eSu00 for instance
    # subj_id_design = subj_id unless a participant received sequences of another (mistaken manipulation of Presentation *.sce files)
    code_start = 16
    code_end = 32
    f_events = os.path.join(dir_mne,  '{}.raw-eve.fif'.format(subj_id))
    events =read_events(f_events)
    events_new = np.copy(events)
    
    ind_s = np.where(events[:,2]==code_start)[0] # session boundaries within the continuous recording
    ind_e = np.where(events[:,2]==code_end)[0] # session boundaries within the continuous recording
    for i_b in range(len(ind_s)) :
        print(i_b)
        print('Raw-eve data, Bloc #{}-----> {} events in between [start; end] (expected {})'.format(i_b, ind_e[i_b] - ind_s[i_b]- 1, nr_event/4 -2) )# should be equal to nr_evt 

    for i_b in range(len(session_order)):
        ind_e_bloc = ind_e[i_b]
        ind_s_bloc = ind_s[i_b]
        codd_eeg_rename_events_loop(dir_eventcodes,subj_id_design,i_b, session_order,events,  events_new,ind_e_bloc, ind_s_bloc )

            
    print('                -                       ')
    print('Writing new event file...')
    f_events_new = os.path.join(dir_mne,  '{}.renamed-eve.fif'.format(subj_id))
    write_events(f_events_new,events_new)
    
    ### PLOT
    if plot:
        plot_events(events_new,  show = False)
        plt.title(' {} : renamed events'.format(subj_id))
        plt.show()
    

    
    
def codd_eeg_rename_events_eSu04(dir_eventcodes, dir_mne, session_order , nr_event, plot = True):
    # from a continuous file made of 4 sessions (codes: 16, 1, 2, 32 for start, std, dev and stop, resp.)
    # rename every std and dev of each session according to a 5-digit code (see notes about this)
    # for each session, refers to a text file with two columns (1st= {1 and 2 } original, 2nd= {5-digit codes}  originally created with Matlab
    # subj_id = eSu00 for instance
    # subj_id_design = subj_id unless a participant received sequences of another (mistaken manipulation of Presentation *.sce files)
    subj_id = 'eSu04'
    code_start = 16
    code_end = 32
    f_events = os.path.join(dir_mne,  '{}.raw-eve.fif'.format(subj_id))
    events =read_events(f_events)
    events_new = np.copy(events)
    
    ind_s = np.where(events[:,2]==code_start)[0] # session boundaries within the continuous recording
    ind_e = np.where(events[:,2]==code_end)[0] # session boundaries within the continuous recording
    for i_b in range(len(ind_s)) :
        print(i_b)
        print('Raw-eve data, Bloc #{}-----> {} events in between [start; end] (expected {})'.format(i_b, ind_e[i_b] - ind_s[i_b]- 1, nr_event/4 -2) )# should be equal to nr_evt 

    ### bloc 1; normal
    i_b = 0
    ind_e_bloc = ind_e[i_b]
    ind_s_bloc = ind_s[i_b]
    codd_eeg_rename_events_loop(dir_eventcodes,subj_id,i_b, session_order, events, events_new, ind_e_bloc, ind_s_bloc )

    ### bloc 2 => account for missing start, and missing first events
    i_b = 1
    f_code = os.path.join(dir_eventcodes,subj_id, '{}_ContextOdd_Code_Bloc{}.txt'.format(subj_id, session_order[i_b]))
    print('                -                       ')
    print('{}, bloc{}-----> {}'.format(subj_id, i_b+1,  f_code ))
    with open(f_code, 'r') as f:
        reader = csv.reader(f,delimiter = '\t')
        nr_row = sum(1 for row in reader) # once read, object reader no longer exists (csv.reader stuff)
        
    nr_ev2 = ind_e[1] - ind_e[0] - 1 # 1507 => we missed 9 events: start and 8 sounds
    offset = nr_row - nr_ev2
    with open(f_code, 'r') as f:
        reader = csv.reader(f,delimiter = '\t')
        cnt=1  
        dummy = 0      
        for row in reader: #object reader=csv.reader()- how to skip the first 7 rows???
            if(dummy==offset):
                # print('stim #{}, {}/{} - codebloc, acq: {}, {} '.format(cnt,ind_e[i_b-1]+cnt+1, events.shape[0], row[0], events[ind_e[i_b-1]+cnt,2] ))
                if(float(row[0]) == events[ind_e[i_b-1]+cnt,2]):
                    events_new[ind_e[i_b-1]+cnt,2] = row[1]
                    cnt = cnt+1
                else:
                    print('Bloc {}: Mismatch btw codes  in Bloc.txt and raw-eve.fif -> abandon'.format(i_b+1) ) 
                    break
            else:
                dummy = dummy+1
    ### bloc 3 and 4 : use ind_e normally, and ind_s[i_b-1] instaed of i_b
    for i_b in [2, 3]:
        ind_e_bloc = ind_e[i_b]
        ind_s_bloc = ind_s[i_b-1]
        codd_eeg_rename_events_loop(dir_eventcodes,subj_id,i_b, session_order, events, events_new,ind_e_bloc, ind_s_bloc )

    ### store eveything
    print('                -                       ')
    print('Writing new event file...')
    f_events_new = os.path.join(dir_mne,  '{}.renamed-eve.fif'.format(subj_id))
    write_events(f_events_new,events_new)
    
    ### PLOT
    if plot:
        plot_events(events_new,  show = False)
        plt.title(' {} : renamed events'.format(subj_id))
        plt.show()


def codd_eeg_rename_events_eSu16(dir_eventcodes, dir_mne, session_order , nr_event, plot = True):
    # from a continuous file made of 4 sessions (codes: 16, 1, 2, 32 for start, std, dev and stop, resp.)
    # rename every std and dev of each session according to a 5-digit code (see notes about this)
    # for each session, refers to a text file with two columns (1st= {1 and 2 } original, 2nd= {5-digit codes}  originally created with Matlab
    # subj_id = eSu00 for instance
    # subj_id_design = subj_id unless a participant received sequences of another (mistaken manipulation of Presentation *.sce files)
    subj_id = 'eSu16'
    code_start = 16
    code_end = 32
    f_events = os.path.join(dir_mne,  '{}.raw-eve.fif'.format(subj_id))
    events =read_events(f_events)
    events_new = np.copy(events)
    
    ind_s = np.where(events[:,2]==code_start)[0] # session boundaries within the continuous recording
    ind_e = np.where(events[:,2]==code_end)[0] # session boundaries within the continuous recording
    ind_s=np.delete(ind_s,0) #### issue #1: faux-depart

    ### bloc 1; normal
    i_b = 0
    ind_e_bloc = ind_e[i_b]
    ind_s_bloc = ind_s[i_b]
    codd_eeg_rename_events_loop(dir_eventcodes,subj_id,i_b, session_order, events, events_new, ind_e_bloc, ind_s_bloc )

    ### fix issue #2, bloc2
    i_b = 1
    f_code = os.path.join(dir_eventcodes,subj_id, '{}_ContextOdd_Code_Bloc{}.txt'.format(subj_id, session_order[i_b]))
    print('{}-----> bloc {}:{}'.format(subj_id, i_b+1,  f_code ))
    with open(f_code, 'r') as f:
        reader = csv.reader(f,delimiter = '\t')
        nr_row = sum(1 for row in reader) # once read, object reader no longer exists (csv.reader stuff)
        print('Nr of events found in bloc.txt: {}'.format(nr_row))
    nr_bad =  ind_e[i_b] - ind_s[i_b]- 1 - nr_row # manually
    cnt=1+nr_bad    
    with open(f_code, 'r') as f:
        reader = csv.reader(f,delimiter = '\t')
        for row in reader: #object reader=csv.reader()
            if(float(row[0]) == events[ind_s[i_b]+cnt,2]):
                events_new[ind_s[i_b]+cnt,2] = row[1]
                cnt = cnt+1
            else:
                print('Bloc {}: Mismatch btw codes  in Bloc.txt and raw-eve.fif -> abandon'.format(i_b+1) ) 
                break

    ### bloc 3 and 4 : normal
    for i_b in [2, 3]:
        ind_e_bloc = ind_e[i_b]
        ind_s_bloc = ind_s[i_b]
        codd_eeg_rename_events_loop(dir_eventcodes,subj_id,i_b, session_order, events, events_new,ind_e_bloc, ind_s_bloc )

    ### store eveything
    print('                -                       ')
    print('Writing new event file...')
    f_events_new = os.path.join(dir_mne,  '{}.renamed-eve.fif'.format(subj_id))
    write_events(f_events_new,events_new)
    
    ### PLOT
    if plot:
        plot_events(events_new,  show = False)
        plt.title(' {} : renamed events'.format(subj_id))
        plt.show()

def codd_eeg_rename_events_eSu19(dir_eventcodes, dir_mne, session_order , nr_event, plot = True):
    #faux depart, and received sounds of eSu18
    subj_id = 'eSu19'
    subj_id_design = 'eSu18'
    code_start = 16
    code_end = 32
    f_events = os.path.join(dir_mne,  '{}.raw-eve.fif'.format(subj_id))
    events =read_events(f_events)
    events_new = np.copy(events)
    
    ind_s = np.where(events[:,2]==code_start)[0] # session boundaries within the continuous recording
    ind_e = np.where(events[:,2]==code_end)[0] # session boundaries within the continuous recording
    ind_s=np.delete(ind_s,0) #### issue #1: faux-depart

    for i_b in range(len(session_order)):
        ind_e_bloc = ind_e[i_b]
        ind_s_bloc = ind_s[i_b]
        codd_eeg_rename_events_loop(dir_eventcodes,subj_id_design,i_b, session_order,events,  events_new,ind_e_bloc, ind_s_bloc )

            
    print('                -                       ')
    print('Writing new event file...')
    f_events_new = os.path.join(dir_mne,  '{}.renamed-eve.fif'.format(subj_id))
    write_events(f_events_new,events_new)
    
    ### PLOT
    if plot:
        plot_events(events_new,  show = False)
        plt.title(' {} : renamed events'.format(subj_id))
        plt.show()
    
   
#####################################################################################################################
#####################################################################################################################
#####################################################################################################################
## Step 3: Downsampling to 500 Hz (1000/2)  : data AND events             -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-
#####################################################################################################################
#####################################################################################################################
#####################################################################################################################
def codd_eeg_downsample_data_events(dir_mne, subj_id, new_freq, plot=True ):
    # downsample data and events (1024 -> 512 Hz)
    
    f_raw = os.path.join(dir_mne, '{}.raw.fif'.format(subj_id))
    f_eve = os.path.join(dir_mne, '{}.renamed-eve.fif'.format(subj_id))
    f_new_raw = os.path.join(dir_mne, '{}.preproc.raw.fif'.format(subj_id))
    f_new_eve = os.path.join(dir_mne, '{}.preproc_renamed-eve.fif'.format(subj_id))

    print_step_label('Load raw data and renamed-eve events {}.....'.format(subj_id))
    raw = read_raw_fif(f_raw, preload=True)
    events =read_events(f_eve)


    print_step_label('Downsampling {}.....'.format(subj_id))    
    new_raw, new_events = raw.resample(new_freq, npad='auto', events=events)
    
    print_step_label('Save {}.....'.format(subj_id))
    new_raw.save(fname=f_new_raw,overwrite=True)
    write_events(f_new_eve, new_events)
    
    print_step_label('Done {}.....'.format(subj_id))
    
    if plot:
        scal = dict(eeg=10e-5)
        raw.plot(events=new_events,   n_channels=new_raw.info['nchan'],remove_dc = True,  duration=30, highpass = None, scalings=scal) #, show_options=False)

def codd_eeg_downsample_raw_events(dir_mne, subj_id, new_freq, plot=True ):
    # downsample raw  events (codes 1, 2)
    # (1000 -> 500 Hz)
    
    f_raw = os.path.join(dir_mne, '{}.raw.fif'.format(subj_id))
    f_eve = os.path.join(dir_mne, '{}.raw-eve.fif'.format(subj_id))
    f_new_eve = os.path.join(dir_mne, '{}.preproc_raw-eve.fif'.format(subj_id))

    raw = read_raw_fif(f_raw, preload=True)
    events =read_events(f_eve)


    print_step_label('Downsampling events {}.....'.format(subj_id))    
    new_raw, new_events = raw.resample(new_freq, npad='auto', events=events)
    
    print_step_label('Save events {}.....'.format(subj_id))
    write_events(f_new_eve, new_events)
    
    print_step_label('Done {}.....'.format(subj_id))
    
    if plot:
        raw = read_raw_fif(os.path.join(dir_mne, '{}.preproc.raw.fif'.format(subj_id)), preload=True)
        events =read_events(f_new_eve)
        scal = dict(eeg=10e-5)
        raw.plot(events=events,   n_channels=new_raw.info['nchan'],remove_dc = True,  duration=30, highpass = None, scalings=scal) #, show_options=False)

#####################################################################################################################
## Step 4: Filtering powerline artifact (50 Hz)             -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-
#####################################################################################################################
#####################################################################################################################
#####################################################################################################################
def codd_eeg_notch_powerline(dir_mne, subj_id, pref_in, pref_out, power_freq_array, plot=True ):
    # apply a notch filter to remove powerline artifacts
    # powerline frequency and harmonics
    # power_freq_array = np.arange(50, 201, 50) or (60, 241, 60)
    
    f_raw = os.path.join(dir_mne, '{}.{}.raw.fif'.format(subj_id, pref_in))
    f_new_raw = os.path.join(dir_mne, '{}.{}.raw.fif'.format(subj_id, pref_out))
 
    print_step_label('Load raw data  {}.....'.format(subj_id))
    raw = read_raw_fif(f_raw, preload=True)
   
    print_step_label('Filtering {}.....'.format(subj_id))    
    new_raw =   raw.notch_filter(power_freq_array, filter_length='auto', phase='zero')
  
    
    print_step_label('Save {}.....'.format(subj_id))
    new_raw.save(fname=f_new_raw,overwrite=True)
    
    print_step_label('Done {}.....'.format(subj_id))
    
    if plot:
        scal = dict(eeg=10e-5)
        raw.plot(n_channels=new_raw.info['nchan'],remove_dc = True,  duration=30, highpass = None, scalings=scal) #, show_options=False)
    
#####################################################################################################################
#####################################################################################################################
#####################################################################################################################
## Step 5: Plot PSD to odentify bad sensors                             -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-
#####################################################################################################################
#####################################################################################################################
#####################################################################################################################

def  codd_eeg_plot_psd(dir_mne, subj_id, pref, tmin, tmax, fmin, fmax, n_fft):
    # plot for each session, the psd compute over intervam [tmin, tmax], in bandwidth [fmin, fmax]
    
    f_raw = os.path.join(dir_mne, '{}.{}.raw.fif'.format(subj_id, pref))
    f_eve = os.path.join(dir_mne, '{}.preproc_renamed-eve.fif'.format(subj_id))
    raw = read_raw_fif(f_raw, preload=False)
    events =read_events(f_eve)

    # raw data
    scal = dict(eeg=10e-5)
    raw.plot(events=events,   n_channels=raw.info['nchan'],remove_dc = True,  duration=30, highpass = None, scalings=scal) #, show_options=False)
   
    # get start session event
    ind_s = np.where(events[:,2]==16)[0] # session boundaries within the continuous recording
    if len(ind_s)>4:
        ind_s=np.delete(ind_s,0) #### faux-departs issues
        print('faux-depart is accounted')
    
    # for each session
    for i_b in  range(len(ind_s)):
        print('   ')
        print(events[ind_s[i_b],0],events[ind_s[i_b],1], events[ind_s[i_b],2])
        tmin_bloc = int(events[ind_s[i_b],0]/raw.info['sfreq']) + tmin
        tmax_bloc = int(events[ind_s[i_b],0]/raw.info['sfreq']) + tmax
        print(tmin_bloc, tmax_bloc)
        #Setup for reading the raw data (to save memory, crop before loading)
        raw = read_raw_fif(f_raw).crop(tmin_bloc, tmax_bloc).load_data()
        raw.plot_psd(tmin=0, tmax=60, fmin=fmin, fmax=fmax, n_fft=n_fft,
             n_jobs=1, proj=False,  color=(0, 0, 1),  show=True, average=False)
        raw.plot_psd_topo(tmin=0, tmax=60, fmin=fmin, fmax=fmax, n_fft=n_fft, dB=True)
    
def codd_eeg_psd_save_bad(dir_mne, subj_id, pref, badsensors):
    f_raw = os.path.join(dir_mne, '{}.{}.raw.fif'.format(subj_id, pref))
    raw = read_raw_fif(f_raw, preload=True)
    raw.info['bads'] += badsensors  # bads + 2 more
    raw.info['bads'] = list(set(raw.info['bads'])) # supp doublons
    raw.save(f_raw, overwrite=True)
    raw = read_raw_fif(f_raw, preload=True)
    print(raw.info['bads'])

def codd_eeg_psd_erase_bad(dir_mne, subj_id, pref):
    f_raw = os.path.join(dir_mne, '{}.{}.raw.fif'.format(subj_id, pref))
    raw = read_raw_fif(f_raw, preload=True)
    raw.info['bads'] = []  
    raw.save(f_raw, overwrite=True)
    raw = read_raw_fif(f_raw, preload=True)
    print(raw.info['bads'])

#####################################################################################################################
#####################################################################################################################
#####################################################################################################################
## Step 6: ICA to remove blink , saccade artifact   
# prior: 1hz highpass (advised)                         -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-
#####################################################################################################################
#####################################################################################################################
#####################################################################################################################
def codd_eeg_filter(dir_mne, subj_id, pref_in, pref_out, l_freq, h_freq):
        f_raw = os.path.join(dir_mne, '{}.{}.raw.fif'.format(subj_id, pref_in))
        raw = read_raw_fif(f_raw, preload=True)
        print_step_label('Filtering {}.....'.format(subj_id))    
        raw.filter(l_freq, h_freq,  l_trans_bandwidth='auto',filter_length='auto', phase='zero', fir_window='hann', fir_design='firwin')
        f_raw_out = os.path.join(dir_mne, '{}.{}.raw.fif'.format(subj_id, pref_out))
        print_step_label('Save {}.....'.format(subj_id))
        raw.save(f_raw_out, overwrite=True)


def  codd_eeg_ica_fit(dir_mne, subj_id, pref, ica_n_comp, ica_mode, decim, rej_thresh, epoch_tmin=None, epoch_tmax=None, pref_ica=[]):
    
    f_raw = os.path.join(dir_mne, '{}.{}.raw.fif'.format(subj_id, pref))
    raw = read_raw_fif(f_raw, preload=True)
    picks = pick_types(raw.info, eeg=True, stim=False, exclude='bads')
    
    f_eve = os.path.join(dir_mne, '{}.preproc_renamed-eve.fif'.format(subj_id))
    events =read_events(f_eve)
    
    
    ica = ICA(n_components=ica_n_comp, method='fastica', random_state = 123)
    
    if ica_mode == 'epoch':
        print_step_label('Fit ICA on epochs---  {}.....'.format(subj_id)) 
        epochs = Epochs(raw, events, event_id=None, tmin = epoch_tmin, tmax = epoch_tmax, picks=picks,reject=dict(eeg=rej_thresh) , preload=True, verbose=False)
        print("Nr of correct epochs = ", len(epochs.events))
        epochs.drop_bad()
        ica.fit(epochs,  decim=decim)
    elif ica_mode == 'continuous':
        ica.fit(raw, picks=picks, reject=dict(eeg=rej_thresh),decim=decim)
    
    print('  Fit %d components (explaining at least %0.1f%% of the variance)' % (ica.n_components_, 100 * ica_n_comp))
  
    print_step_label('Save ica---  {}.....'.format(subj_id)) 
    f_ica = os.path.join(dir_mne, '{}.{}-ica.fif'.format(subj_id, pref_ica))
    ica.save(f_ica)
        
def  codd_eeg_ica_view(dir_mne, subj_id, pref_raw, pref_ica):
    print_step_label('View Components---  {}.....'.format(subj_id)) 
    f_ica = os.path.join(dir_mne, '{}.{}-ica.fif'.format(subj_id, pref_ica))
    ica=read_ica(f_ica)
    #    ica.plot_properties(epochs, picks=ecg_inds)
    ica.plot_components(title=subj_id)
#    plot_ica_components(ica, picks=None, ch_type='eeg',  sensors=True, colorbar=True, title=subj_id)
    
    f_raw = os.path.join(dir_mne, '{}.{}.raw.fif'.format(subj_id, pref_raw))
    raw = read_raw_fif(f_raw, preload=False)
    #ica.plot_properties(raw)
    ica.plot_sources(raw)
#    f_eve = os.path.join(dir_mne, '{}.preproc_renamed-eve.fif'.format(subj_id))
#    events =read_events(f_eve)
#    epochs = Epochs(raw, events, event_id=None, tmin = epoch_tmin, tmax = epoch_tmax)


def codd_eeg_ica_save(dir_mne, subj_id,  pref_ica,  ica_sel):
    f_ica = os.path.join(dir_mne, '{}.{}-ica.fif'.format(subj_id, pref_ica))
    ica=read_ica(f_ica)
    ica.exclude=ica_sel
    ica.save(f_ica)
    print(ica.exclude)
    return ica


    
#    f_raw = os.path.join(dir_mne, '{}.{}.raw.fif'.format(subj_id, pref_raw))
#    f_raw_ica = os.path.join(dir_mne, '{}.{}_ica_corr.raw.fif'.format(subj_id, pref_raw))
#    
#    
#    raw = read_raw_fif(f_raw, preload=True)
#    raw_corr = raw.copy()
#    raw_corr = ica.apply(raw_corr)
#    raw_corr.save(f_raw_ica, overwrite=True)

def codd_eeg_ica_view_corr(dir_mne, subj_id, raw, ica, plot_eeg=[], view_start=0, view_stop=0):
    scal = dict(eeg=10e-5)
    raw.plot(n_channels=raw.info['nchan'],remove_dc = True,  duration=30, highpass = None, scalings=scal) #, show_options=False)
    
    raw_corr = raw.copy()
    raw_corr = ica.apply(raw_corr)
    raw_corr.plot(n_channels=raw.info['nchan'],remove_dc = True,  duration=30, highpass = None, scalings=scal) #, show_options=False)

    if plot_eeg:
        for sens in plot_eeg:
            ind_eeg = raw.ch_names.index(sens)
            print(ind_eeg, sens)
            plot_ica_overlay(ica,raw, picks=ind_eeg, start=view_start, stop=view_stop, title='{}, {}'.format(subj_id, sens))


def codd_eeg_ica_plot_blink_evoked(subj_id, raw, ica,  blink_id,  plot_raw=False, thresh=110e-6, lf=1, hf=10):
    blink_events = find_eog_events(raw, ch_name='Fp1', event_id=blink_id,thresh=thresh, l_freq=lf, h_freq=hf)
    print('{}----> found {} blinks'.format(subj_id, len(blink_events)))
    if plot_raw:
        scal = dict(eeg=10e-5)
        raw.plot(events=blink_events,   n_channels=raw.info['nchan'],remove_dc = True,  duration=30, highpass = None, scalings=scal) #, show_options=False)

    
    blink_epochs = Epochs(raw, events=blink_events, event_id=blink_id, picks=None, tmin=-0.5,tmax=0.5,  reject=dict(eeg=300e-6))
    blink_epochs.average().plot_topo(ylim=dict(eeg=[-150, 150]), title='{}, {} blinks.'.format(subj_id, len(blink_epochs.selection)))
    print('----------> pre-ICA: keeping {} blink_events'.format(len(blink_epochs.selection) ))
    raw_corr = raw.copy()
    raw_corr = ica.apply(raw_corr)
    
    blink_epochs = Epochs(raw_corr, events=blink_events[blink_epochs.selection,:], event_id=blink_id,picks=None,  tmin=-0.5,tmax=0.5, reject=None)
    blink_epochs.average().plot_topo(ylim=dict(eeg=[-150, 150]), title='{}, {} compo.'.format(subj_id, len(ica.exclude)))
    print('----------> post-ICA: keeping {} blink_events'.format(len(blink_epochs.selection) ))
    
#####################################################################################################################
#####################################################################################################################
#####################################################################################################################
## Step 7: Epoching  
# on 1-120 Hz data
# interpolate bad channels                        -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-
#####################################################################################################################
#####################################################################################################################
#####################################################################################################################
def codd_eeg_make_epochs(subj_id,raw,ica, events,event_id,rej_thresh, f_epoch_out, exclude_eeg=[], tmin=-0.2, tmax=0.4, lf=1, hf=80):
    raw.load_data()
    raw = ica.apply(raw)
    raw.filter(lf, hf)
    raw.interpolate_bads()
    picks = pick_types(raw.info, eeg=True, stim=True,exclude=())
    reject=dict(eeg=rej_thresh)
    print_step_label('Epoching---  {}.....({} uV)'.format(subj_id, rej_thresh*1e6))  
    
    if exclude_eeg:
        raw.info['bads']=exclude_eeg
        print('excluded sensors for rejection: {}'.format(exclude_eeg))
    
    epochs_params=dict(events=events, event_id=event_id, 
                     tmin=tmin, tmax=tmax, 
                     reject=reject, picks=picks, 
                     baseline=None, verbose=False)      
    epochs = Epochs(raw, **epochs_params)
    
    epochs.load_data()
    print('  Dropped %0.1f%% of epochs' % (epochs.drop_log_stats(),))
#    epochs.plot_drop_log()
  
    # compute erps quickly
#    erp=epochs.average().apply_baseline((None, 0))
#    plot_evoked_topo(erp)
#    erp.plot_topomap(times='interactive')
     

    epochs.save(f_epoch_out)
#    plot_good_and_bad_epochs(raw, events, epochs, rm_useless=[16, 32])
    count_events(epochs.events, [14122, 14222, 16122, 16222, 24112, 24122,  26122, 26132 ], events_ref=events)
    


def codd_eeg_explore_epoching_threshold(subj_id,raw,ica, events,event_id, thresh_channel, exclude_eeg=[], tmin=-0.2, tmax=0.4, lf=1, hf=40):
    raw.load_data()
    raw = ica.apply(raw)
    raw.filter(lf, hf)
    raw.interpolate_bads()
    picks = pick_types(raw.info, eeg=True, stim=True,exclude=())

    if exclude_eeg:
        raw.info['bads']=exclude_eeg
        print('excluded sensors for rejection: {}'.format(exclude_eeg))
 
    threshold_range=np.arange(150, 40,-10)*1e-6
    print_step_label('Threshold rejection ---  {}'.format(subj_id))  
    epoch_rejection_explore_threshold(raw, events, event_id, picks, tmin, tmax, threshold_range=threshold_range)
  
    

     
def codd_eeg_find_threshold(subj_id,raw,ica, events,event_id,  exclude_eeg=[], tmin=-0.2, tmax=0.4, lf=1, hf=40,thresh_min=50e-6, thresh_max=150e-6, thresh_channel=10.0):
    raw.load_data()
    raw = ica.apply(raw)
    raw.filter(lf, hf)
    raw.interpolate_bads()
    picks = pick_types(raw.info, eeg=True, stim=True,exclude=())

    if exclude_eeg:
        raw.info['bads']=exclude_eeg
        print('excluded sensors for rejection: {}'.format(exclude_eeg))
 
    epoch_rejection_adaptive_threshold(raw, events, event_id, picks, tmin, tmax, thresh_min=thresh_min, thresh_max=thresh_max, thresh_channel=thresh_channel)
    
#############################################################################################
## -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.    TEST and DEBUG     --.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
if __name__== '__main__':
    
    #############################################################################################
    ## Init Paths              -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
    #############################################################################################
    
    # mac_fl
    #dir_mne = '/Users/francoise/Documents/Projets/ContextOdd/Data/EEG/MNE_DevData/MNE_Data'
    #dir_rawdata = '/Users/francoise/Documents/Projets/ContextOdd/ContextOdd_Git/Experimental_Design/StimSeq' # path to the StimSeq folder
    #dir_eventcodes = '/Users/francoise/Documents/Projets/ContextOdd/ContextOdd_Git/Experimental_Design/StimSeq' # path to the StimSeq folder
    #dir_ref = '/Users/francoise/Documents/Projets/ContextOdd/ContextOdd_Git/Reference_Files'
    
    # in2p3
    dir_mne='/sps/inter/crnl/users/pduret/ContextOdd/ContextOdd_Data/eMNE'
    dir_rawdata = '/sps/inter/crnl/users/pduret/ContextOdd...'
    dir_eventcodes='/sps/inter/crnl/users/pduret/ContextOdd/ContextOdd_Git/Experimental_Design/StimSeq' # path to the StimSeq folder
    dir_ref='/sps/inter/crnl/users/pduret/ContextOdd/ContextOdd_Git/Reference_Files'
    

    #############################################################################################
    ## Init Variables              -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
    #############################################################################################
    subjlist_file = '{}/List_Participants_Import_Data.txt'.format(dir_ref)
    
    subj_list = {'NAVMA':	'eSu00',	 'POSRI': 'eSu01',	'BOULA':	'eSu02',	 'BREME': 'eSu03',	'VENAN':	'eSu04',\
                 'BIGOP':	'eSu05',	 'LANJU': 'eSu06',	'LOQJE':	'eSu07',	 'PICJO': 'eSu08',	'EDDAR':	'eSu09',	\
                 'ADOBE':	'eSu10',	 'THIMA': 'eSu11',  'BOUAU':'eSu12', 'MASLO': 'eSu13',  'ANTRA':'eSu14',\
                 'DURMA': 'eSu15', 'BERAM': 'eSu16',  'ALLMA':'eSu17', 'VIAMA': 'eSu18',  'BARTH':'eSu19',	\
                 'COULE': 'eSu20', 'CHMMI': 'eSu21',  'MIGAN':'eSu22',	 'PEGAR': 'eSu23',	'MERAR':'eSu24',	\
                 'SANLU': 'eSu25',	 'ESTYO': 'eSu26',	'FERMA':'eSu27',	 'ADANI': 'eSu28',	'COTCL':'eSu29',	\
                 'ANDNI': 'eSu30',	 'SOUMA': 'eSu31',	'KOCPI':'eSu32',	 'TALCA': 'eSu33',	'MERAM':'eSu34',	\
                 'LEDGA':'eSu35'	}
    

    event_id_codd={'R/c4/R+/Pn/std_pre': 14121, 'R/c4/R+/Pn/dev': 14122,'R/c4/R+/Pn/std_oth': 14123,\
                  'R/c5/R+/Pn/std_pre': 15121, 'R/c5/R+/Pn/dev': 15122,'R/c5/R+/Pn/std_oth': 15123,\
                  'R/c6/R+/Pn/std_pre': 16121, 'R/c6/R+/Pn/dev': 16122,'R/c6/R+/Pn/std_oth': 16123,\
                  'R/c2/R-/Pn/std_pre': 12221, 'R/c2/R-/Pn/dev': 12222,'R/c2/R-/Pn/std_oth': 12223,\
                  'R/c3/R-/Pn/std_pre': 13221, 'R/c3/R-/Pn/dev': 13222,'R/c3/R-/Pn/std_oth': 13223,\
                  'R/c4/R-/Pn/std_pre': 14221, 'R/c4/R-/Pn/dev': 14222,'R/c4/R-/Pn/std_oth': 14223,\
                  'R/c6/R-/Pn/std_pre': 16221, 'R/c6/R-/Pn/dev': 16222,'R/c6/R-/Pn/std_oth': 16223,\
                  'R/c7/R-/Pn/std_pre': 17221, 'R/c7/R-/Pn/dev': 17222,'R/c7/R-/Pn/std_oth': 17223,\
                  'R/c8/R-/Pn/std_pre': 18221, 'R/c8/R-/Pn/dev': 18222,'R/c8/R-/Pn/std_oth': 18223,\
                  'P/c2/R+/P-/std_pre': 22111, 'P/c2/R+/P-/dev': 22112,'P/c2/R+/P-/std_oth': 22113,\
                  'P/c3/R+/P-/std_pre': 23111, 'P/c3/R+/P-/dev': 23112,'P/c3/R+/P-/std_oth': 23113,\
                  'P/c4/R+/P-/std_pre': 24111, 'P/c4/R+/P-/dev': 24112,'P/c4/R+/P-/std_oth': 24113,\
                  'P/c4/R+/Pn/std_pre': 24121, 'P/c4/R+/Pn/dev': 24122,'P/c4/R+/Pn/std_oth': 24123,\
                  'P/c5/R+/Pn/std_pre': 25121, 'P/c5/R+/Pn/dev': 25122,'P/c5/R+/Pn/std_oth': 25123,\
                  'P/c6/R+/Pn/std_pre': 26121, 'P/c6/R+/Pn/dev': 26122,'P/c6/R+/Pn/std_oth': 26123,\
                  'P/c6/R+/P+/std_pre': 26131, 'P/c6/R+/P+/dev': 26132,'P/c6/R+/P+/std_oth': 26133,\
                  'P/c7/R+/P+/std_pre': 27131, 'P/c7/R+/P+/dev': 27132,'P/c7/R+/P+/std_oth': 27133,\
                  'P/c8/R+/P+/std_pre': 28131, 'P/c8/R+/P+/dev': 28132,'P/c8/R+/P+/std_oth': 28133                  }
    
    
    event_id_acq={'std': 1,    'dev':2             }
  
    # Apply to all  ---------------------------------------------------------------------------
#    for subj_code, subj_id in subj_list.items():
#        codd_eeg_import_raw_all(dir_rawdata, dir_mne, subjlist_file, subj_code, nr_evt, plot=True )
        
    ## DEBUG Step 1 ########################################################################
#    subj_code='EDDAR'
#    montage = read_montage('standard_1020')
#    nr_evt = 6068 # should be the case : 4 sessions of 1517 evts (start - 1515 sounds - end)
#    codd_eeg_import_raw_all(dir_rawdata, dir_mne, montage, subjlist_file, subj_code, nr_evt, plot=True )
#    
    ### DEBUG Step 2 ########################################################################
#    nr_event = 6068
##    subj_id = 'eSu00'
##    subj_id_design = 'eSu00'
##    session_order = codd_eeg_get_session_order(subjlist_file, subj_id)
##    codd_eeg_rename_events(dir_eventcodes, dir_mne,  subj_id, subj_id_design, session_order , nr_event, plot=True)
##    
##    subj_id = 'eSu04'
##    codd_eeg_rename_events_eSu04(dir_eventcodes, dir_mne, session_order , nr_event, plot = True)
##    subj_id = 'eSu16'
##    codd_eeg_rename_events_eSu16(dir_eventcodes, dir_mne, session_order , nr_event, plot = True)
#    subj_id = 'eSu18'
#    codd_eeg_rename_events_eSu19(dir_eventcodes, dir_mne, session_order , nr_event, plot = True)
    
#    subj_id, event_fname = 'eSu11', 'preproc_renamed'
#    events=read_events(os.path.join(dir_mne, '{}.{}-eve.fif'.format(subj_id, event_fname)))
#    plot_events(events)
    # each bloc = 1512 relevant sounds + 3 extra sounds at the end, + start + end = 1517 events
    
    
    ### DEBUG Step 3 ########################################################################
#    subj_id = 'eSu00'
#    new_freq = 500
#    #codd_eeg_downsample_data_events(dir_mne, subj_id, new_freq, plot=True )
#    codd_eeg_downsample_raw_events(dir_mne, subj_id, new_freq, plot=True )
    
    ### DEBUG Step5 #########################################################################
#    tmin, tmax = 420, 480 # we look at 60 sec of signal in the middle of each bloc (7th min)
#    fmin, fmax = 1, 250  # look at frequencies between 2 and 300Hz
#    n_fft = 2048 
#    subj_id = 'eSu00'
#    pref = 'preproc_notch'
#    codd_eeg_plot_psd(dir_mne, subj_id, pref, tmin, tmax, fmin, fmax, n_fft, plot=True )
#    #plt.close('all')
    
    ### DEBUG Step6 #########################################################################
    # ICA
#    subj_id = 'eSu00'
#    pref_raw = 'preproc50_1Hz'
#    pref_ica = '{}_epoch'.format( pref_raw)
#    ica_n_comp = 0.99
#    epoch_tmin = -0.2
#    epoch_tmax = 0.4
#    rej_thresh = 150e-6
#    decim = None
#    codd_eeg_ica_fit(dir_mne, subj_id, pref_raw,  ica_n_comp, decim,epoch_tmin, epoch_tmax,  rej_thresh, pref_ica)
#    epochs.plot(scalings=dict(eeg=150e-6), n_epochs=5)
    
    # print the number of blinks automatically found for each subject
#    for subj_code, subj_id in subj_list.items():
#        raw = read_raw_fif(os.path.join(dir_mne, '{}.preproc50_1Hz.raw.fif'.format(subj_id)), preload=False)
#        blink_events = find_eog_events(raw, ch_name='Fp1', event_id=888,thresh=110e-6, l_freq=1, h_freq=10)
#        print('{}----> found {} blinks'.format(subj_id, len(blink_events)))
     
#    subj_id='eSu33'
#    lf, hf=1, 10
#    raw = read_raw_fif(os.path.join(dir_mne, '{}.preproc50_1Hz.raw.fif'.format(subj_id)), preload=True)
#    blink_events = find_eog_events(raw, ch_name='Fp1', event_id=888,thresh=100e-6, l_freq=lf, h_freq=hf)
#    print('{}----> found {} blinks'.format(subj_id, len(blink_events)))
#    scal = dict(eeg=10e-5)
#    raw.filter(lf, hf).plot(events=blink_events,   n_channels=raw.info['nchan'],remove_dc = True,  duration=30, scalings=scal) #, show_options=False)

    ### DEBUG Step7 #########################################################################
    # epoching
    subj_id='eSu11'
    event_id, event_fname = event_id_codd, 'preproc_renamed' #event_id_acq, 'preproc_raw' # event_id_codd, 'preproc_renamed'
    lf, hf = 1, 40 #Hz
    raw = read_raw_fif(os.path.join(dir_mne, '{}.preproc50_1Hz.raw.fif'.format(subj_id)), preload=False)
    ica=read_ica(os.path.join(dir_mne, '{}.{}-ica.fif'.format(subj_id, 'preproc50_1Hz_epoch')))
    events=read_events(os.path.join(dir_mne, '{}.{}-eve.fif'.format(subj_id, event_fname)))
    f_epoch_out=os.path.join(dir_mne, '{}.preproc50_{}-{}Hz-epo.fif'.format(subj_id, lf, hf))
    rej_thresh=100e-6
    codd_eeg_make_epochs(subj_id,raw,ica, events,event_id,rej_thresh, f_epoch_out, exclude_eeg=[], lf=lf, hf=hf )
 