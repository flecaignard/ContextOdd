#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 15 20:48:29 2018

@author: francoise
"""
#############################################################################################
## THIS RUN_SCRIPT IS DEDICATED TO PREPROCESSING DATA 
## STEP1: IMPORT DATA
## STEP2: RENAME EVENTS
## STEP3: DOWNSAMPLE
## STEP4: NOTCH POWERLINE ARTIFACT
## STEP5:BAD SENSORS USING PSD
## STEP6 ICA 
##
##
## IT GOES WITH funct_COdd_eeg_preproc.py
## AND ALSO COdd_eeg_run_MISC.py  to manipulate, view, plot specific things
## ADVICE: COdd_eeg_run_MISC.py should be executed in a second Spyder Iconsole)
#############################################################################################

#
#
# COdd_eeg_run_preproc.py is associated to COdd_eeg_preproc.py
# Each function in *run* accounts for individual cases (missing data, ICA component id, ...)
# Main function in *run* (at the end of the file) is meant to proceed to each step one after the other , and comments are meant to keep a history of what has be done for each subject
#
#############################################################################################
## Import packages          -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
# import argparse               # add input arguments

import numpy as np             # convention d'import
import os                       # path.join for file names
import matplotlib.pyplot as plt # from the MNE examples
import csv
import warnings
import argparse

from mne import Epochs, pick_types, find_events, write_events, read_events
from mne import read_epochs, write_evokeds, read_evokeds

from mne.channels import read_layout, Montage, read_montage
from mne.io import concatenate_raws, read_raw_brainvision, read_raw_fif, find_edf_events
from mne.datasets import eegbci
from mne.decoding import CSP
from mne.viz import plot_events, plot_evoked_topo
from mne.preprocessing import read_ica



print(__doc__)


from mne import get_config, set_config
type(get_config('MNE_BROWSE_RAW_SIZE'))
set_config('MNE_BROWSE_RAW_SIZE', '20.0,10.0')

import sys      # path to my own functions
sys.path.append('/Users/francoise/Documents/Projets/ContextOdd/ContextOdd_Git/Tools/Python/EEG')

from COdd_eeg_erp import  codd_eeg_erp_epochs, codd_eeg_evokeds_indiv_mmn, codd_eeg_evokeds_group, codd_eeg_evokeds_indiv_context, codd_eeg_evokeds_indiv_pred
from COdd_eeg_erp import  codd_eeg_evokeds_indiv_context2

warnings.filterwarnings("ignore",category=DeprecationWarning)

#############################################################################################
## Step1 - Create 2-20 Hz epochs (no rejection, made in preproc, 1-40 Hz)  -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
def run_codd_eeg_erp_epochs(dir_mne, subj_id, event_id):
    lf, hf = 2,20
    raw = read_raw_fif(os.path.join(dir_mne, '{}.preproc50_1Hz.raw.fif'.format(subj_id)), preload=False)
    ica = read_ica(os.path.join(dir_mne, '{}.{}-ica.fif'.format(subj_id, 'preproc50_1Hz_epoch')))
    epochs_rej = read_epochs(os.path.join(dir_mne, '{}.preproc50_{}-{}Hz-epo.fif'.format(subj_id, 1, 40)))
    f_epoch_out = os.path.join(dir_mne, '{}.erp_{}-{}Hz-epo.fif'.format(subj_id, lf, hf))
    codd_eeg_erp_epochs(subj_id,raw,ica, epochs_rej,event_id, f_epoch_out,  lf=lf, hf=hf)

#############################################################################################
## Step1 - Compute individual ERPs (std, dev) -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
def run_codd_eeg_evokeds_indiv_mmn(dir_mne, subj_id):
    
    epochs = read_epochs(os.path.join(dir_mne, '{}.erp_{}-{}Hz-epo.fif'.format(subj_id, 2, 20)))
    codd_eeg_evokeds_indiv_mmn(dir_mne, subj_id,epochs, plot=False)

def run_codd_eeg_evokeds_indiv_context(dir_mne, subj_id):
    
    epochs = read_epochs(os.path.join(dir_mne, '{}.erp_{}-{}Hz-epo.fif'.format(subj_id, 2, 20)))
    codd_eeg_evokeds_indiv_context2(dir_mne, subj_id,epochs,plot=False)

def run_codd_eeg_evokeds_indiv_pred(dir_mne, subj_id):
    
    epochs = read_epochs(os.path.join(dir_mne, '{}.erp_{}-{}Hz-epo.fif'.format(subj_id, 2, 20)))
    codd_eeg_evokeds_indiv_pred(dir_mne, subj_id,epochs,plot=False)


#############################################################################################
## -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.     MAIN         -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
if __name__== '__main__':
    
    #############################################################################################
    ## Init Paths              -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
    #############################################################################################
    
    # mac_fl
    #dir_mne = '/Users/francoise/Documents/Projets/ContextOdd/Data/EEG/MNE_DevData/MNE_Data'
    #dir_rawdata = '/Users/francoise/Documents/Projets/ContextOdd/ContextOdd_Git/Experimental_Design/StimSeq' # path to the StimSeq folder
    #dir_eventcodes = '/Users/francoise/Documents/Projets/ContextOdd/ContextOdd_Git/Experimental_Design/StimSeq' # path to the StimSeq folder
    #dir_ref = '/Users/francoise/Documents/Projets/ContextOdd/ContextOdd_Git/Reference_Files'
    
    # in2p3
    dir_mne='/sps/inter/crnl/users/pduret/ContextOdd/ContextOdd_Data/eMNE'
    


    #############################################################################################
    ## Init Variables              -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
    #############################################################################################
    
    subj_list = {'NAVMA':	'eSu00',	 'POSRI': 'eSu01',	'BOULA':	'eSu02',	 'BREME': 'eSu03',	'VENAN':	'eSu04',\
                 'BIGOP':	'eSu05',	 'LANJU': 'eSu06',	'LOQJE':	'eSu07',	 'PICJO': 'eSu08',	'EDDAR':	'eSu09',	\
                 'ADOBE':	'eSu10',	 'THIMA': 'eSu11',  'BOUAU':'eSu12', 'MASLO': 'eSu13',  'ANTRA':'eSu14',\
                 'DURMA': 'eSu15', 'BERAM': 'eSu16',  'ALLMA':'eSu17', 'VIAMA': 'eSu18',  'BARTH':'eSu19',	\
                 'COULE': 'eSu20', 'CHMMI': 'eSu21',  'MIGAN':'eSu22',	 'PEGAR': 'eSu23',	'MERAR':'eSu24',	\
                 'SANLU': 'eSu25',	 'ESTYO': 'eSu26',	'FERMA':'eSu27',	 'ADANI': 'eSu28',	'COTCL':'eSu29',	\
                 'ANDNI': 'eSu30',	 'SOUMA': 'eSu31',	'KOCPI':'eSu32',	 'TALCA': 'eSu33',	'MERAM':'eSu34',	\
                 'LEDGA':'eSu35'	}

    
    event_id_codd={'R/c4/R+/Pn/std_pre': 14121, 'R/c4/R+/Pn/dev': 14122,'R/c4/R+/Pn/std_oth': 14123,\
                  'R/c5/R+/Pn/std_pre': 15121, 'R/c5/R+/Pn/dev': 15122,'R/c5/R+/Pn/std_oth': 15123,\
                  'R/c6/R+/Pn/std_pre': 16121, 'R/c6/R+/Pn/dev': 16122,'R/c6/R+/Pn/std_oth': 16123,\
                  'R/c2/R-/Pn/std_pre': 12221, 'R/c2/R-/Pn/dev': 12222,'R/c2/R-/Pn/std_oth': 12223,\
                  'R/c3/R-/Pn/std_pre': 13221, 'R/c3/R-/Pn/dev': 13222,'R/c3/R-/Pn/std_oth': 13223,\
                  'R/c4/R-/Pn/std_pre': 14221, 'R/c4/R-/Pn/dev': 14222,'R/c4/R-/Pn/std_oth': 14223,\
                  'R/c6/R-/Pn/std_pre': 16221, 'R/c6/R-/Pn/dev': 16222,'R/c6/R-/Pn/std_oth': 16223,\
                  'R/c7/R-/Pn/std_pre': 17221, 'R/c7/R-/Pn/dev': 17222,'R/c7/R-/Pn/std_oth': 17223,\
                  'R/c8/R-/Pn/std_pre': 18221, 'R/c8/R-/Pn/dev': 18222,'R/c8/R-/Pn/std_oth': 18223,\
                  'P/c2/R+/P-/std_pre': 22111, 'P/c2/R+/P-/dev': 22112,'P/c2/R+/P-/std_oth': 22113,\
                  'P/c3/R+/P-/std_pre': 23111, 'P/c3/R+/P-/dev': 23112,'P/c3/R+/P-/std_oth': 23113,\
                  'P/c4/R+/P-/std_pre': 24111, 'P/c4/R+/P-/dev': 24112,'P/c4/R+/P-/std_oth': 24113,\
                  'P/c4/R+/Pn/std_pre': 24121, 'P/c4/R+/Pn/dev': 24122,'P/c4/R+/Pn/std_oth': 24123,\
                  'P/c5/R+/Pn/std_pre': 25121, 'P/c5/R+/Pn/dev': 25122,'P/c5/R+/Pn/std_oth': 25123,\
                  'P/c6/R+/Pn/std_pre': 26121, 'P/c6/R+/Pn/dev': 26122,'P/c6/R+/Pn/std_oth': 26123,\
                  'P/c6/R+/P+/std_pre': 26131, 'P/c6/R+/P+/dev': 26132,'P/c6/R+/P+/std_oth': 26133,\
                  'P/c7/R+/P+/std_pre': 27131, 'P/c7/R+/P+/dev': 27132,'P/c7/R+/P+/std_oth': 27133,\
                  'P/c8/R+/P+/std_pre': 28131, 'P/c8/R+/P+/dev': 28132,'P/c8/R+/P+/std_oth': 28133                  }
    

    
    ###........................ STEP 1: Compute individual ERPs (std, dev)
#    subj_list = {'NAVMA':	'eSu00',	 'POSRI': 'eSu01',	'BOULA':	'eSu02',	 	'VENAN':	'eSu04',\
#                 'BIGOP':	'eSu05',	 'LANJU': 'eSu06',	'LOQJE':	'eSu07',	 'PICJO': 'eSu08',	'EDDAR':	'eSu09',	\
#                 'ADOBE':	'eSu10',	 'THIMA': 'eSu11',  'BOUAU':'eSu12', 'MASLO': 'eSu13',  'ANTRA':'eSu14',\
#                 'DURMA': 'eSu15', 'BERAM': 'eSu16',  'ALLMA':'eSu17', 'VIAMA': 'eSu18',  'BARTH':'eSu19',	\
#                 'COULE': 'eSu20', 'CHMMI': 'eSu21',  	 'PEGAR': 'eSu23',	'MERAR':'eSu24',	\
#                 'SANLU': 'eSu25',	 'ESTYO': 'eSu26',	'FERMA':'eSu27',	 	'COTCL':'eSu29',	\
#                 'ANDNI': 'eSu30',	 'SOUMA': 'eSu31',	'KOCPI':'eSu32',	 'TALCA': 'eSu33',	\
#                 'LEDGA':'eSu35'	}
#
#    for subj_code, subj_id in subj_list.items():
#        run_codd_eeg_erp_epochs(dir_mne, subj_id, event_id_codd)
        
    ###........................ STEP2: Compute indiv ERPs (std, dev)
    subj_list = {'NAVMA':	'eSu00',	 'POSRI': 'eSu01',	'BOULA':	'eSu02',	 	'VENAN':	'eSu04',\
                 'BIGOP':	'eSu05',	 'LANJU': 'eSu06',	'LOQJE':	'eSu07',	 'PICJO': 'eSu08',	'EDDAR':	'eSu09',	\
                 'ADOBE':	'eSu10',	 'THIMA': 'eSu11',  'BOUAU':'eSu12', 'MASLO': 'eSu13',  'ANTRA':'eSu14',\
                 'DURMA': 'eSu15', 'BERAM': 'eSu16',  'ALLMA':'eSu17', 'VIAMA': 'eSu18',  'BARTH':'eSu19',	\
                 'COULE': 'eSu20', 'CHMMI': 'eSu21',  	 'PEGAR': 'eSu23',	'MERAR':'eSu24',	\
                 'SANLU': 'eSu25',	 'ESTYO': 'eSu26',	'FERMA':'eSu27',	 	'COTCL':'eSu29',	\
                 'ANDNI': 'eSu30',	 'SOUMA': 'eSu31',	'KOCPI':'eSu32',	 'TALCA': 'eSu33',	\
                 'LEDGA':'eSu35'	}

#    for subj_code, subj_id in subj_list.items():  
#        run_codd_eeg_evokeds_indiv_mmn(dir_mne, subj_id)
        
#    code_contrast = 'mmn'
#    nr_cond = 3
#    codd_eeg_evokeds_group(dir_mne, code_contrast, nr_cond, subj_list)
    
    
    
#    for subj_code, subj_id in subj_list.items():  
#        run_codd_eeg_evokeds_indiv_context(dir_mne, subj_id)
#    code_contrast = 'context2'
#    nr_cond = 6
#    codd_eeg_evokeds_group(dir_mne, code_contrast, nr_cond, subj_list, ['std46_R', 'std46_P', 'dev46_R', 'dev46_P', 'mmn46_R', 'mmn46_P'])
    
    
#    for subj_code, subj_id in subj_list.items():  
#        run_codd_eeg_evokeds_indiv_pred(dir_mne, subj_id)
    code_contrast = 'pred'
    nr_cond = 6
    codd_eeg_evokeds_group(dir_mne, code_contrast, nr_cond, subj_list, ['std46_R+', 'std46_R-', 'dev46_R+', 'dev46_R-', 'mmn46_R+', 'mmn46_R-'])

    f_evo_group = os.path.join(dir_mne, 'group.{}_{}-{}Hz-ave.fif'.format(code_contrast, 2,20))
    G = read_evokeds(f_evo_group)
    #std
    plot_evoked_topo([G[0],G[1] ])
    #dev
    plot_evoked_topo([G[2],G[3] ])
#   
    #mmn
    plot_evoked_topo([G[4],G[5] ])
    plot_evoked_topo([G[0],G[2], G[4] ])
    plot_evoked_topo([G[1],G[3], G[5] ])
#  
#   