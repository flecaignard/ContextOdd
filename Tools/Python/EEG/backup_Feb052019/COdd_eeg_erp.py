#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 15 20:48:29 2018

@author: francoise
"""

## Attempts to re-organize preprocessings scripts (A. Corneylie)


#############################################################################################
## Import packages          -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
# import argparse               # add input arguments

import numpy as np             # convention d'import
import os                       # path.join for file names
import matplotlib.pyplot as plt # from the MNE examples
import csv
import warnings
import argparse

from mne import Epochs, pick_types, find_events, write_events, read_events, read_epochs 
from mne import write_evokeds, combine_evoked, read_evokeds
from mne.channels import read_layout, Montage, read_montage
from mne.io import concatenate_raws, read_raw_brainvision, read_raw_fif, find_edf_events
from mne.datasets import eegbci
from mne.decoding import CSP
from mne.viz import plot_events, plot_ica_components, plot_ica_overlay, plot_evoked_topo
from mne.preprocessing import ICA, read_ica, create_eog_epochs, find_eog_events


print(__doc__)

from mne import get_config, set_config
type(get_config('MNE_BROWSE_RAW_SIZE'))
set_config('MNE_BROWSE_RAW_SIZE', '20.0,10.0')




import sys      # path to my own functions
sys.path.append('/Users/francoise/Documents/Projets/ContextOdd/ContextOdd_Git/Tools/Python/EEG')


warnings.filterwarnings("ignore",category=DeprecationWarning)



from fun_eeg_misc import print_step_label, peak_to_peak_raw, plot_good_and_bad_epochs, count_events




#####################################################################################################################
#####################################################################################################################
#####################################################################################################################
## Step1 - Compute 2-20 Hz epochs with accepted events (on 1-40 Hz)     -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#####################################################################################################################
#####################################################################################################################
#####################################################################################################################

def codd_eeg_erp_epochs(subj_id,raw,ica, epochs_rej,event_id, f_epoch_out, tmin=-0.2, tmax=0.4, lf=2, hf=20):
    raw.load_data()
    raw = ica.apply(raw)
    raw.filter(lf, hf)
    raw.interpolate_bads()
    events=epochs_rej.events
    picks = pick_types(raw.info, eeg=True, stim=True,exclude=())
    print_step_label('Epoching---  {}.....(No rejection - {}-{} Hz)'.format(subj_id, lf, hf))  
    
    
    epochs_params=dict(events=events, event_id=event_id, 
                     tmin=tmin, tmax=tmax, 
                     reject=None, picks=picks, 
                     baseline=None, verbose=False)      
    epochs_erp = Epochs(raw, **epochs_params)
    epochs_erp.load_data()
#    print(len(epochs))
    
    
    #epochs.drop_bad(reject=reject)
    print('  Dropped %0.1f%% of epochs' % (epochs_erp.drop_log_stats(),))
  
    # compute erps quickly
    erp=epochs_erp.average().apply_baseline((None, 0))
#    plot_evoked_topo(erp)
    epochs_erp.save(f_epoch_out)
    print(len(epochs_erp.events))
    print(epochs_erp.events)
    
 
#####################################################################################################################
#####################################################################################################################
#####################################################################################################################
## Step2 - Compute Evoked responses (std, dev) individual level           -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#####################################################################################################################
#####################################################################################################################
#####################################################################################################################

def codd_eeg_evokeds_indiv_mmn(dir_mne, subj_id,epochs,  plot = True):
    
    print_step_label('MMN - {}'.format(subj_id))
    erp_list = ['std_pre', 'dev']
    f_evoked_out = os.path.join(dir_mne, '{}.mmn_{}-{}Hz-ave.fif'.format(subj_id, 2,20))
    
    evokeds = [epochs[name].average().apply_baseline((None, 0)) for name in erp_list]
    diff = combine_evoked([evokeds[1], -evokeds[0]], weights='equal')
    diff.apply_baseline((None, 0))
    evokeds.append(diff)
    
    A = diff.get_data()
    print(A)
    if plot:
        plot_evoked_topo(evokeds)
        
    write_evokeds(f_evoked_out, evokeds)
#    dev_erp=epochs['dev'].average()    
#    std_erp.filter(2,20).apply_baseline((None, 0)).plot_topomap(times='interactive')
#    dev_erp.filter(2,20).apply_baseline((None, 0)).plot_topomap(times='interactive')
#

def codd_eeg_evokeds_indiv_context(dir_mne, subj_id,epochs,  plot=True):
    #we pool 4- and 6- events in R-cond vs P-cond, having p(dev)=pn (2), and variance=low (1)
    # std: 14121, 16121 vs. 24121, 26121
    # dev: 14122, 16122 vs. 24122, 26122
    #erp_ist = [ std4R std6R std4P std6P dev4R dev6R dev4P dev4R]
    erp_list = ['R/c4/R+/Pn/std_pre', 'R/c6/R+/Pn/std_pre', 'P/c4/R+/Pn/std_pre', 'P/c6/R+/Pn/std_pre',
                'R/c4/R+/Pn/dev', 'R/c6/R+/Pn/dev', 'P/c4/R+/Pn/dev', 'P/c6/R+/Pn/dev']
    
    print_step_label('CONTEXT - {}'.format(subj_id))
    
    f_evoked_out = os.path.join(dir_mne, '{}.context_{}-{}Hz-ave.fif'.format(subj_id, 2,20))
    
    evokeds = [epochs[name].average().apply_baseline((None, 0)) for name in erp_list]
    
      
    #mmn4R
    diff = combine_evoked([-evokeds[0], evokeds[4]], weights='equal')
    evokeds.append(diff)
    #mmn6R
    diff = combine_evoked([-evokeds[1], evokeds[5]], weights='equal')
    evokeds.append(diff)
    #mmn4P
    diff = combine_evoked([-evokeds[2], evokeds[6]], weights='equal')
    evokeds.append(diff)
    #mmn6P
    diff = combine_evoked([-evokeds[3], evokeds[7]], weights='equal')
    evokeds.append(diff)
    
    ##pool 4c and 6c
    pool_evokeds = [list() for _ in range(6)] # (std, dev, mmn) * (R, P) = [ std_R, std_P, dev_R, dev_P, mmn_R, mmn_P]
    
   
    A = combine_evoked([evokeds[0], evokeds[1]], weights='equal')
    A.comment = 'std46_R'
    pool_evokeds[0] = A
    
    A = combine_evoked([evokeds[2], evokeds[3]], weights='equal') #
    A.comment = 'std46_P'
    pool_evokeds[1] = A
    
    A = combine_evoked([evokeds[4], evokeds[5]], weights='equal')
    A.comment = 'dev46_R'
    pool_evokeds[2] = A
    
    A = combine_evoked([evokeds[6], evokeds[7]], weights='equal')
    A.comment = 'dev46_P'
    pool_evokeds[3] = A
    
    A = combine_evoked([evokeds[8], evokeds[9]], weights='equal')
    A.comment = 'mmn46_R'
    pool_evokeds[4] = A
    
    A = combine_evoked([evokeds[10], evokeds[11]], weights='equal') 
    A.comment = 'mmn46_P'
    pool_evokeds[5] = A

# A = combine_evoked([evokeds[0], evokeds[1]], weights='equal')
#    A.comment = 'std46_R'
#    pool_evokeds[0] = A
#    pool_evokeds[1] = combine_evoked([evokeds[2], evokeds[3]], weights='equal') #std_P
#    pool_evokeds[2] = combine_evoked([evokeds[4], evokeds[5]], weights='equal') #dev_R
#    pool_evokeds[3] = combine_evoked([evokeds[6], evokeds[7]], weights='equal') #dev_P
#    pool_evokeds[4] = combine_evoked([evokeds[8], evokeds[9]], weights='equal') #mmn_R
#    pool_evokeds[5] = combine_evoked([evokeds[10], evokeds[11]], weights='equal') #mmn_P


    if plot:
        plot_evoked_topo(pool_evokeds)
        
    write_evokeds(f_evoked_out, pool_evokeds)

def codd_eeg_evokeds_indiv_context2(dir_mne, subj_id,epochs,  plot=True):
    #we pool all 4- and 6- events in R-cond vs P-cond
    
    #erp_ist = [ std4R std6R std4P std6P dev4R dev6R dev4P dev4R]
    erp_list = ['R/c4/std_pre', 'R/c6/std_pre', 'P/c4/std_pre', 'P/c6/std_pre',
                'R/c4/dev', 'R/c6/dev', 'P/c4/dev', 'P/c6/dev']
    
    print_step_label('CONTEXT - {}'.format(subj_id))
    
    f_evoked_out = os.path.join(dir_mne, '{}.context2_{}-{}Hz-ave.fif'.format(subj_id, 2,20))
    
    evokeds = [epochs[name].average().apply_baseline((None, 0)) for name in erp_list]
    
      
    #mmn4R
    diff = combine_evoked([-evokeds[0], evokeds[4]], weights='equal')
    evokeds.append(diff)
    #mmn6R
    diff = combine_evoked([-evokeds[1], evokeds[5]], weights='equal')
    evokeds.append(diff)
    #mmn4P
    diff = combine_evoked([-evokeds[2], evokeds[6]], weights='equal')
    evokeds.append(diff)
    #mmn6P
    diff = combine_evoked([-evokeds[3], evokeds[7]], weights='equal')
    evokeds.append(diff)
    
    ##pool 4c and 6c
    pool_evokeds = [list() for _ in range(6)] # (std, dev, mmn) * (R, P) = [ std_R, std_P, dev_R, dev_P, mmn_R, mmn_P]
    
   
    A = combine_evoked([evokeds[0], evokeds[1]], weights='equal')
    A.comment = 'std46_R'
    pool_evokeds[0] = A
    
    A = combine_evoked([evokeds[2], evokeds[3]], weights='equal') #
    A.comment = 'std46_P'
    pool_evokeds[1] = A
    
    A = combine_evoked([evokeds[4], evokeds[5]], weights='equal')
    A.comment = 'dev46_R'
    pool_evokeds[2] = A
    
    A = combine_evoked([evokeds[6], evokeds[7]], weights='equal')
    A.comment = 'dev46_P'
    pool_evokeds[3] = A
    
    A = combine_evoked([evokeds[8], evokeds[9]], weights='equal')
    A.comment = 'mmn46_R'
    pool_evokeds[4] = A
    
    A = combine_evoked([evokeds[10], evokeds[11]], weights='equal') 
    A.comment = 'mmn46_P'
    pool_evokeds[5] = A

# A = combine_evoked([evokeds[0], evokeds[1]], weights='equal')
#    A.comment = 'std46_R'
#    pool_evokeds[0] = A
#    pool_evokeds[1] = combine_evoked([evokeds[2], evokeds[3]], weights='equal') #std_P
#    pool_evokeds[2] = combine_evoked([evokeds[4], evokeds[5]], weights='equal') #dev_R
#    pool_evokeds[3] = combine_evoked([evokeds[6], evokeds[7]], weights='equal') #dev_P
#    pool_evokeds[4] = combine_evoked([evokeds[8], evokeds[9]], weights='equal') #mmn_R
#    pool_evokeds[5] = combine_evoked([evokeds[10], evokeds[11]], weights='equal') #mmn_P


    if plot:
        plot_evoked_topo(pool_evokeds)
        
    write_evokeds(f_evoked_out, pool_evokeds)


def codd_eeg_evokeds_indiv_pred(dir_mne, subj_id,epochs,  plot=True):
    #we pool 4- and 6- events in R-cond vs P-cond, having p(dev)=pn (2), and variance=low (1)
    # std: 14121, 16121 vs. 14221, 16221
    # dev: 14122, 16122 vs. 14222, 16222
    #erp_ist = [ std4R std6R std4P std6P dev4R dev6R dev4P dev4R]
    erp_list = ['R/c4/R+/Pn/std_pre', 'R/c6/R+/Pn/std_pre', 'R/c4/R-/Pn/std_pre', 'R/c6/R-/Pn/std_pre',
                'R/c4/R+/Pn/dev', 'R/c6/R+/Pn/dev', 'R/c4/R-/Pn/dev', 'R/c6/R-/Pn/dev']
    
    print_step_label('PRED - {}'.format(subj_id))
    
    f_evoked_out = os.path.join(dir_mne, '{}.pred_{}-{}Hz-ave.fif'.format(subj_id, 2,20))
    
    evokeds = [epochs[name].average().apply_baseline((None, 0)) for name in erp_list]
    
      
    #mmn4R+
    diff = combine_evoked([-evokeds[0], evokeds[4]], weights='equal')
    evokeds.append(diff)
    #mmn6R+
    diff = combine_evoked([-evokeds[1], evokeds[5]], weights='equal')
    evokeds.append(diff)
    #mmn4R-
    diff = combine_evoked([-evokeds[2], evokeds[6]], weights='equal')
    evokeds.append(diff)
    #mmn6R-
    diff = combine_evoked([-evokeds[3], evokeds[7]], weights='equal')
    evokeds.append(diff)
    
    ##pool 4c and 6c
    pool_evokeds = [list() for _ in range(6)] # (std, dev, mmn) * (R, P) = [ std_R, std_P, dev_R, dev_P, mmn_R, mmn_P]
    
   
    A = combine_evoked([evokeds[0], evokeds[1]], weights='equal')
    A.comment = 'std46_R+'
    pool_evokeds[0] = A
    
    A = combine_evoked([evokeds[2], evokeds[3]], weights='equal') #
    A.comment = 'std46_R-'
    pool_evokeds[1] = A
    
    A = combine_evoked([evokeds[4], evokeds[5]], weights='equal')
    A.comment = 'dev46_R+'
    pool_evokeds[2] = A
    
    A = combine_evoked([evokeds[6], evokeds[7]], weights='equal')
    A.comment = 'dev46_R-'
    pool_evokeds[3] = A
    
    A = combine_evoked([evokeds[8], evokeds[9]], weights='equal')
    A.comment = 'mmn46_R+'
    pool_evokeds[4] = A
    
    A = combine_evoked([evokeds[10], evokeds[11]], weights='equal') 
    A.comment = 'mmn46_R-'
    pool_evokeds[5] = A


    if plot:
        plot_evoked_topo(pool_evokeds)
        
    write_evokeds(f_evoked_out, pool_evokeds)
 
#   event_id_codd={'R/c4/R+/Pn/std_pre': 14121, 'R/c4/R+/Pn/dev': 14122,'R/c4/R+/Pn/std_oth': 14123,\
#                  'R/c5/R+/Pn/std_pre': 15121, 'R/c5/R+/Pn/dev': 15122,'R/c5/R+/Pn/std_oth': 15123,\
#                  'R/c6/R+/Pn/std_pre': 16121, 'R/c6/R+/Pn/dev': 16122,'R/c6/R+/Pn/std_oth': 16123,\
#                  'R/c2/R-/Pn/std_pre': 12221, 'R/c2/R-/Pn/dev': 12222,'R/c2/R-/Pn/std_oth': 12223,\
#                  'R/c3/R-/Pn/std_pre': 13221, 'R/c3/R-/Pn/dev': 13222,'R/c3/R-/Pn/std_oth': 13223,\
#                  'R/c4/R-/Pn/std_pre': 14221, 'R/c4/R-/Pn/dev': 14222,'R/c4/R-/Pn/std_oth': 14223,\
#                  'R/c6/R-/Pn/std_pre': 16221, 'R/c6/R-/Pn/dev': 16222,'R/c6/R-/Pn/std_oth': 16223,\
#                  'R/c7/R-/Pn/std_pre': 17221, 'R/c7/R-/Pn/dev': 17222,'R/c7/R-/Pn/std_oth': 17223,\
#                  'R/c8/R-/Pn/std_pre': 18221, 'R/c8/R-/Pn/dev': 18222,'R/c8/R-/Pn/std_oth': 18223,\
#                  'P/c2/R+/P-/std_pre': 22111, 'P/c2/R+/P-/dev': 22112,'P/c2/R+/P-/std_oth': 22113,\
#                  'P/c3/R+/P-/std_pre': 23111, 'P/c3/R+/P-/dev': 23112,'P/c3/R+/P-/std_oth': 23113,\
#                  'P/c4/R+/P-/std_pre': 24111, 'P/c4/R+/P-/dev': 24112,'P/c4/R+/P-/std_oth': 24113,\
#                  'P/c4/R+/Pn/std_pre': 24121, 'P/c4/R+/Pn/dev': 24122,'P/c4/R+/Pn/std_oth': 24123,\
#                  'P/c5/R+/Pn/std_pre': 25121, 'P/c5/R+/Pn/dev': 25122,'P/c5/R+/Pn/std_oth': 25123,\
#                  'P/c6/R+/Pn/std_pre': 26121, 'P/c6/R+/Pn/dev': 26122,'P/c6/R+/Pn/std_oth': 26123,\
#                  'P/c6/R+/P+/std_pre': 26131, 'P/c6/R+/P+/dev': 26132,'P/c6/R+/P+/std_oth': 26133,\
#                  'P/c7/R+/P+/std_pre': 27131, 'P/c7/R+/P+/dev': 27132,'P/c7/R+/P+/std_oth': 27133,\
#                  'P/c8/R+/P+/std_pre': 28131, 'P/c8/R+/P+/dev': 28132,'P/c8/R+/P+/std_oth': 28133                  }
         
#####################################################################################################################
#####################################################################################################################
#####################################################################################################################
## Step3 - Combine Evoked responses (std, dev) individual level           -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#####################################################################################################################
#####################################################################################################################
#####################################################################################################################
def codd_eeg_evokeds_group(dir_mne, code_contrast, nr_cond, subj_list, erp_label):
    all_evokeds = [list() for _ in range(nr_cond)]  # Container for all the categories
    
    for subj_code, subj_id in subj_list.items():
        print(subj_id)
        f_evo = os.path.join(dir_mne, '{}.{}_{}-{}Hz-ave.fif'.format(subj_id,code_contrast, 2,20))
        evokeds = read_evokeds(f_evo)
        
        assert len(evokeds) == len(all_evokeds)
        for idx, evoked in enumerate(evokeds):
            all_evokeds[idx].append(evoked)  # Insert to the container


    for idx, evokeds in enumerate(all_evokeds):
        A = combine_evoked(evokeds, 'equal')  # Combine subjects
        A.comment = erp_label[idx]
        all_evokeds[idx] = A

    f_evo_group = os.path.join(dir_mne, 'group.{}_{}-{}Hz-ave.fif'.format(code_contrast, 2,20))
    write_evokeds(f_evo_group,all_evokeds)
    plot_evoked_topo(all_evokeds)
    
    
## We need an evoked object to plot the image to be masked
#evoked = mne.combine_evoked([long_words.average(), -short_words.average()],
#                            weights='equal')  # calculate difference wave
#time_unit = dict(time_unit="s")
#evoked.plot_joint(title="Long vs. short words", ts_args=time_unit,
#                  topomap_args=time_unit)  # show difference wave
#
## Create ROIs by checking channel labels
#selections = make_1020_channel_selections(evoked.info, midline="12z")
#
## Visualize the results
#fig, axes = plt.subplots(nrows=3, figsize=(8, 8))
#axes = {sel: ax for sel, ax in zip(selections, axes.ravel())}
#evoked.plot_image(axes=axes, group_by=selections, colorbar=False, show=False,
#                  mask=significant_points, show_names="all", titles=None,
#                  **time_unit)
#plt.colorbar(axes["Left"].images[-1], ax=list(axes.values()), shrink=.3,
#             label="uV")
#
#plt.show()
#####################################################################################################################
#####################################################################################################################
#####################################################################################################################
## Step4 - Group Average           -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#####################################################################################################################
#####################################################################################################################
#####################################################################################################################
       
#############################################################################################
## -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.    TEST and DEBUG     --.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
if __name__== '__main__':
    
    #############################################################################################
    ## Init Paths              -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
    #############################################################################################
    
    # mac_fl
    #dir_mne = '/Users/francoise/Documents/Projets/ContextOdd/Data/EEG/MNE_DevData/MNE_Data'
    #dir_rawdata = '/Users/francoise/Documents/Projets/ContextOdd/ContextOdd_Git/Experimental_Design/StimSeq' # path to the StimSeq folder
    #dir_eventcodes = '/Users/francoise/Documents/Projets/ContextOdd/ContextOdd_Git/Experimental_Design/StimSeq' # path to the StimSeq folder
    #dir_ref = '/Users/francoise/Documents/Projets/ContextOdd/ContextOdd_Git/Reference_Files'
    
    # in2p3
    dir_mne='/sps/inter/crnl/users/pduret/ContextOdd/ContextOdd_Data/eMNE'
    

    #############################################################################################
    ## Init Variables              -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
    #############################################################################################
    
    subj_list = {'NAVMA':	'eSu00',	 'POSRI': 'eSu01',	'BOULA':	'eSu02',	 'BREME': 'eSu03',	'VENAN':	'eSu04',\
                 'BIGOP':	'eSu05',	 'LANJU': 'eSu06',	'LOQJE':	'eSu07',	 'PICJO': 'eSu08',	'EDDAR':	'eSu09',	\
                 'ADOBE':	'eSu10',	 'THIMA': 'eSu11',  'BOUAU':'eSu12', 'MASLO': 'eSu13',  'ANTRA':'eSu14',\
                 'DURMA': 'eSu15', 'BERAM': 'eSu16',  'ALLMA':'eSu17', 'VIAMA': 'eSu18',  'BARTH':'eSu19',	\
                 'COULE': 'eSu20', 'CHMMI': 'eSu21',  'MIGAN':'eSu22',	 'PEGAR': 'eSu23',	'MERAR':'eSu24',	\
                 'SANLU': 'eSu25',	 'ESTYO': 'eSu26',	'FERMA':'eSu27',	 'ADANI': 'eSu28',	'COTCL':'eSu29',	\
                 'ANDNI': 'eSu30',	 'SOUMA': 'eSu31',	'KOCPI':'eSu32',	 'TALCA': 'eSu33',	'MERAM':'eSu34',	\
                 'LEDGA':'eSu35'	}
    

    event_id={'R/c4/R+/Pn/std_pre': 14121, 'R/c4/R+/Pn/dev': 14122,'R/c4/R+/Pn/std_oth': 14123,\
                  'R/c5/R+/Pn/std_pre': 15121, 'R/c5/R+/Pn/dev': 15122,'R/c5/R+/Pn/std_oth': 15123,\
                  'R/c6/R+/Pn/std_pre': 16121, 'R/c6/R+/Pn/dev': 16122,'R/c6/R+/Pn/std_oth': 16123,\
                  'R/c2/R-/Pn/std_pre': 12221, 'R/c2/R-/Pn/dev': 12222,'R/c2/R-/Pn/std_oth': 12223,\
                  'R/c3/R-/Pn/std_pre': 13221, 'R/c3/R-/Pn/dev': 13222,'R/c3/R-/Pn/std_oth': 13223,\
                  'R/c4/R-/Pn/std_pre': 14221, 'R/c4/R-/Pn/dev': 14222,'R/c4/R-/Pn/std_oth': 14223,\
                  'R/c6/R-/Pn/std_pre': 16221, 'R/c6/R-/Pn/dev': 16222,'R/c6/R-/Pn/std_oth': 16223,\
                  'R/c7/R-/Pn/std_pre': 17221, 'R/c7/R-/Pn/dev': 17222,'R/c7/R-/Pn/std_oth': 17223,\
                  'R/c8/R-/Pn/std_pre': 18221, 'R/c8/R-/Pn/dev': 18222,'R/c8/R-/Pn/std_oth': 18223,\
                  'P/c2/R+/P-/std_pre': 22111, 'P/c2/R+/P-/dev': 22112,'P/c2/R+/P-/std_oth': 22113,\
                  'P/c3/R+/P-/std_pre': 23111, 'P/c3/R+/P-/dev': 23112,'P/c3/R+/P-/std_oth': 23113,\
                  'P/c4/R+/P-/std_pre': 24111, 'P/c4/R+/P-/dev': 24112,'P/c4/R+/P-/std_oth': 24113,\
                  'P/c4/R+/Pn/std_pre': 24121, 'P/c4/R+/Pn/dev': 24122,'P/c4/R+/Pn/std_oth': 24123,\
                  'P/c5/R+/Pn/std_pre': 25121, 'P/c5/R+/Pn/dev': 25122,'P/c5/R+/Pn/std_oth': 25123,\
                  'P/c6/R+/Pn/std_pre': 26121, 'P/c6/R+/Pn/dev': 26122,'P/c6/R+/Pn/std_oth': 26123,\
                  'P/c6/R+/P+/std_pre': 26131, 'P/c6/R+/P+/dev': 26132,'P/c6/R+/P+/std_oth': 26133,\
                  'P/c7/R+/P+/std_pre': 27131, 'P/c7/R+/P+/dev': 27132,'P/c7/R+/P+/std_oth': 27133,\
                  'P/c8/R+/P+/std_pre': 28131, 'P/c8/R+/P+/dev': 28132,'P/c8/R+/P+/std_oth': 28133                  }
    
    
  
    # Apply to all  ---------------------------------------------------------------------------
#    for subj_code, subj_id in subj_list.items():
#        codd_eeg_import_raw_all(dir_rawdata, dir_mne, subjlist_file, subj_code, nr_evt, plot=True )
        
### DEBUG Step1 #########################################################################
    # create 2-20 Hz epochs
#    lf, hf = 2,20
#    subj_id = 'eSu00'
#    raw = read_raw_fif(os.path.join(dir_mne, '{}.preproc50_1Hz.raw.fif'.format(subj_id)), preload=False)
#    ica = read_ica(os.path.join(dir_mne, '{}.{}-ica.fif'.format(subj_id, 'preproc50_1Hz_epoch')))
#    epochs_rej = read_epochs(os.path.join(dir_mne, '{}.preproc50_{}-{}Hz-epo.fif'.format(subj_id, 1, 40)))
#    f_epoch_out = os.path.join(dir_mne, '{}.erp_{}-{}Hz-epo.fif'.format(subj_id, lf, hf))
#    codd_eeg_erp_epochs(subj_id,raw,ica, epochs_rej,event_id, f_epoch_out,  lf=lf, hf=hf)
##    
  ### DEBUG Step2 #########################################################################
    # compute individual erps (std, dev)
    subj_id = 'eSu00'
    epochs = read_epochs(os.path.join(dir_mne, '{}.erp_{}-{}Hz-epo.fif'.format(subj_id, 2, 20)))
    codd_eeg_evokeds_indiv_mmn(dir_mne, subj_id,epochs)
    
#    erp_list=[ 'R/dev', 'P/dev']
#    subj_id = 'eSu00'
#    lf, hf = 2,20
#    epochs = read_epochs(os.path.join(dir_mne, '{}.erp_{}-{}Hz-epo.fif'.format(subj_id, 2, 20)))
#    f_evoked_out = os.path.join(dir_mne, '{}.prems_{}-{}Hz-ave.fif'.format(subj_id, lf, hf))
#    codd_eeg_evokeds_indiv(subj_id,epochs, erp_list, f_evoked_out)
#    
    ### DEBUG Step3 GROUP #########################################################################
#    code_contrast = 'mmn'
#    nr_cond = 3
#    subj_list = {'NAVMA':	'eSu00',	 'POSRI': 'eSu01',	'BOULA':	'eSu02',	 	'VENAN':	'eSu04',\
#                 'BIGOP':	'eSu05',	 'LANJU': 'eSu06',	'LOQJE':	'eSu07',	 'PICJO': 'eSu08',	'EDDAR':	'eSu09',	\
#                 'ADOBE':	'eSu10',	 'THIMA': 'eSu11',  'BOUAU':'eSu12', 'MASLO': 'eSu13',  'ANTRA':'eSu14',\
#                 'DURMA': 'eSu15', 'BERAM': 'eSu16',  'ALLMA':'eSu17', 'VIAMA': 'eSu18',  'BARTH':'eSu19',	\
#                 'COULE': 'eSu20', 'CHMMI': 'eSu21',  	 'PEGAR': 'eSu23',	'MERAR':'eSu24',	\
#                 'SANLU': 'eSu25',	 'ESTYO': 'eSu26',	'FERMA':'eSu27',	 	'COTCL':'eSu29',	\
#                 'ANDNI': 'eSu30',	 'SOUMA': 'eSu31',	'KOCPI':'eSu32',	 'TALCA': 'eSu33',	\
#                 'LEDGA':'eSu35'	}
#
#    codd_eeg_evokeds_group(code_contrast, nr_cond, subj_list)