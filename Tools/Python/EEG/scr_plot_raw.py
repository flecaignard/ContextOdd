#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
==================================================================================
UTIL SCRIPT - Screen continuous data
==================================================================================

Loop over subjects
View Continuous data, and optionnally events

@author: Françoise Lecaignard, francoise.lecaignard@inserm.fr

Created on Mon Apr  1 10:21:46 2019

"""



###############################################################################
import numpy as np             # convention d'import
import os
import matplotlib.pyplot as plt # from the MNE examples
import sys      # path to my own functions
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Python/EEG')

from mne.io import read_raw_fif
from mne import get_config, set_config
type(get_config('MNE_BROWSE_RAW_SIZE'))
set_config('MNE_BROWSE_RAW_SIZE', '20.0,10.0')
import pickle
from mne import pick_types

from fun_util_eeg import peak_to_peak_raw,find_event_at_a_latency
################################################################################
# Define Input Parameters -  Constant Variables
################################################################################

# Paths ----------------------------
dir_mne = '/sps/cermep/cermep/experiments/DCM/ContextOdd/Ana_MNE'


# ----------- data bandwidth
lf, hf = 1,40
folder = 'preproc'


# prefix for continuous raw file
pref_raw = f'preproc_{lf}-{hf}Hz'

# subset of channels
pick_chan=[]
pick_chan = ['Oz', 'TP9', 'TP10', 'PO10' ,'PO9', 'O1', 'O2']
pick_chan = ['FC2','T7', 'TP9', 'TP10', 'T8' ,'PO9', 'F8', 'O1', 'Oz', 'O2', 'PO10', 'Pz', 'CP1']

# Plot Good / bad events? --------------------------
flag_events = False
pref_events = 'clean_good_bad_events'

# adapted to detect bad sensors
scalings=dict(eeg=10e-4)
duration=60
################################################################################
# Let's start!
################################################################################



subj_id_list = np.arange(34,35)
for i_su in subj_id_list:
    subj='sub-{:02}'.format(i_su) 
    dir_imne_raw = os.path.join(dir_mne, subj, folder) #individual mne path
    f_raw = os.path.join(dir_imne_raw, f'{subj}.{pref_raw}.raw.fif') # raw continuous data: downsampled,  filtered   and ica-corrected
    raw = read_raw_fif(f_raw, preload=True)
    if pick_chan:
        rawmini = raw.pick_channels(pick_chan)
    else:
        rawmini = raw
    if flag_events:
        f_events_pkl = os.path.join(dir_imne_raw, f'{subj}.{pref_events}.pkl') ### created in step_08 summarize
        with  open(f_events_pkl, 'rb') as input:
            events = pickle.load(input)
            rawmini.plot(events=events,  n_channels=rawmini.info['nchan'],remove_dc = True,  duration=duration, highpass = None, scalings=scalings) #, show_options=False)
    else:
        rawmini.plot(n_channels=rawmini.info['nchan'],remove_dc = True,  duration=duration, highpass = None, scalings=scalings) #, show_options=False)


    
