#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
====================================================================
Compute STD-STD-DEV ERPs analysis 
====================================================================
This analysis aims at having a clear view of sound post-processing as well as anticipatory processings 
First investigations indeed revealed the complexity of choosing baseline period due to slow drift pre-stim, that was not related to eyes artifacts
Besides, looking at TF (quickly) some participants had 20 Hz activity, others 30 Hz ...

So, we work here in the 1-40 Hz bandwith , and compute three-sucessive-epochs
We don't use baseline correction
For  this, we need to redo the epoching to get successive good events (STD-STD-DEV)
We take selected events from Preprocessing epoching (preproc / sub-XX.clean_1-40Hz-epo.fif)
And from this subset we redo the rejection with thresholds used in preprocessings

Output =====> sub-XX/erp_1-40Hz/sub-XX.STD-STD-DEV_1-40Hz-epo.fif
       =====> sub-XX/erp_1-40Hz/sub-XX.STD-STD-DEV_1-40Hz_var_p_dev-ave.fif
       =====> sub-XX/erp_1-40Hz/sub-XX.STD-STD-DEV_1-40Hz_var_c_dev-ave.fif

    @author: Françoise Lecaignard, francoise.lecaignard@inserm.fr
"""


###############################################################################
import numpy as np             # convention d'import
import os                       # path.join for file names
import matplotlib.pyplot as plt # from the MNE examples

import sys
# path to my own functions
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Python/EEG')



from mne import Epochs, pick_types, read_events, read_epochs
from mne import write_evokeds, read_evokeds
from mne import get_config, set_config
type(get_config('MNE_BROWSE_RAW_SIZE'))
set_config('MNE_BROWSE_RAW_SIZE', '20.0,10.0')
from mne.preprocessing import read_ica

from mne.io import read_raw_fif

#from fun_NN_template import  ...
from fun_util_epoch import import_epochs
from fun_plot_erp import evokeds_plot
from fun_util_erp import evokeds_group_average

################################################################################
# Input Parameters 
################################################################################

# Paths ---------------------------- ---------------------------- ----------------------------
dir_mne = '/sps/cermep/cermep/experiments/DCM/ContextOdd/Ana_MNE'

erp_folder = 'erp_1-40Hz'
raw_folder = 'preproc'

# Epoching - three consecutive events S-S-D   ---------------------------- ---------------------
# ------from step08 - preprocs

pref_continuous = 'preproc_1-40Hz_ica' 
pref_epo_in = 'clean_1-40Hz' #-epo.fif, cleaned epochs from preprocs

exclude_eeg = { 'sub-00': ['T7', 'TP9'],	 'sub-01': [],	'sub-02':['TP10', 'CP5', 'P4'],	 'sub-03': ['Oz'],	'sub-04': ['T7', 'T8'],\
             'sub-05': [],	 'sub-06': [],	'sub-07':[],	 'sub-08': ['O1', 'O2'],	'sub-09': ['T7', 'T8'],\
             'sub-10': [],    'sub-11': ['PO9'],   'sub-12':[],  'sub-13': ['T7', 'PO10'],    'sub-14': ['P3'],\
             'sub-15': ['T8'],    'sub-16': ['Oz'],   'sub-17':['PO9'],  'sub-18': [],    'sub-19': ['T7', 'T8', 'TP10'],\
             'sub-20': [],    'sub-21': [],   'sub-22':['P3'],	 'sub-23': ['CP1'],    'sub-24': [],\
             'sub-25': ['Oz', 'T8', 'TP9'],    'sub-26': [],   'sub-27':[],	 'sub-28': [],    'sub-29': ['CP5'],\
             'sub-30': ['T8', 'PO10'],    'sub-31': [],   'sub-32':['Oz'],	 'sub-33': ['T8'],    'sub-34': ['T8', 'T7'],\
             'sub-35': []	}


threshold_eeg = { 'sub-00': 90e-6,	 'sub-01': 90e-6,	'sub-02':100e-6,	 'sub-03': 100e-6,	'sub-04': 100e-6,\
         'sub-05': 95e-6,	 'sub-06': 100e-6,	'sub-07':95e-6,	 'sub-08': 90e-6,	'sub-09': 90e-6,\
         'sub-10': 90e-6,    'sub-11': 90e-6,   'sub-12':85e-6,  'sub-13': 95e-6,    'sub-14': 90e-6,\
         'sub-15': 90e-6,    'sub-16': 100e-6,   'sub-17':80e-6,  'sub-18': 95e-6,    'sub-19': 80e-6,\
         'sub-20': 95e-6,    'sub-21': 85e-6,   'sub-22':100e-6,	 'sub-23': 95e-6,    'sub-24': 70e-6,\
         'sub-25': 85e-6,    'sub-26': 90e-6,   'sub-27':90e-6,	 'sub-28': 90e-6,    'sub-29': 95e-6,\
         'sub-30': 95e-6,    'sub-31': 90e-6,   'sub-32':90e-6,	 'sub-33': 85e-6,    'sub-34': 95e-6,\
         'sub-35': 90e-6	}

# epochs bounds
tmin, tmax = -1.42, 0.61 # 200 - 610 - 610 - 400, relative to deviants

# output epochs
pref_epo_out = 'STD-STD-DEV_1-40Hz'


# events for epochs

event_id = {'var_c/c4/c-/p0/std': 14121, 'var_c/c4/c-/p0/dev': 14122,'var_c/c4/c-/p0/std_oth': 14123,\
              'var_c/c5/c-/p0/std': 15121, 'var_c/c5/c-/p0/dev': 15122,'var_c/c5/c-/p0/std_oth': 15123,\
              'var_c/c6/c-/p0/std': 16121, 'var_c/c6/c-/p0/dev': 16122,'var_c/c6/c-/p0/std_oth': 16123,\
              'var_c/c2/c+/p0/std': 12221, 'var_c/c2/c+/p0/dev': 12222,'var_c/c2/c+/p0/std_oth': 12223,\
              'var_c/c3/c+/p0/std': 13221, 'var_c/c3/c+/p0/dev': 13222,'var_c/c3/c+/p0/std_oth': 13223,\
              'var_c/c4/c+/p0/std': 14221, 'var_c/c4/c+/p0/dev': 14222,'var_c/c4/c+/p0/std_oth': 14223,\
              'var_c/c6/c+/p0/std': 16221, 'var_c/c6/c+/p0/dev': 16222,'var_c/c6/c+/p0/std_oth': 16223,\
              'var_c/c7/c+/p0/std': 17221, 'var_c/c7/c+/p0/dev': 17222,'var_c/c7/c+/p0/std_oth': 17223,\
              'var_c/c8/c+/p0/std': 18221, 'var_c/c8/c+/p0/dev': 18222,'var_c/c8/c+/p0/std_oth': 18223,\
              'var_p/c2/c-/p+/std': 22111, 'var_p/c2/c-/p+/dev': 22112,'var_p/c2/c-/p+/std_oth': 22113,\
              'var_p/c3/c-/p+/std': 23111, 'var_p/c3/c-/p+/dev': 23112,'var_p/c3/c-/p+/std_oth': 23113,\
              'var_p/c4/c-/p+/std': 24111, 'var_p/c4/c-/p+/dev': 24112,'var_p/c4/c-/p+/std_oth': 24113,\
              'var_p/c4/c-/p0/std': 24121, 'var_p/c4/c-/p0/dev': 24122,'var_p/c4/c-/p0/std_oth': 24123,\
              'var_p/c5/c-/p0/std': 25121, 'var_p/c5/c-/p0/dev': 25122,'var_p/c5/c-/p0/std_oth': 25123,\
              'var_p/c6/c-/p0/std': 26121, 'var_p/c6/c-/p0/dev': 26122,'var_p/c6/c-/p0/std_oth': 26123,\
              'var_p/c6/c-/p-/std': 26131, 'var_p/c6/c-/p-/dev': 26132,'var_p/c6/c-/p-/std_oth': 26133,\
              'var_p/c7/c-/p-/std': 27131, 'var_p/c7/c-/p-/dev': 27132,'var_p/c7/c-/p-/std_oth': 27133,\
              'var_p/c8/c-/p-/std': 28131, 'var_p/c8/c-/p-/dev': 28132,'var_p/c8/c-/p-/std_oth': 28133                  }

# Evoked responses   ---------------------------- ---------------------

erp_list = ['var_p/dev','var_c/dev' ] 
pref_erp = pref_epo_out # ===> sub-00.STD-STD-DEV_1-40Hz_var_p_dev-ave.fif 
subj_group_avg = np.arange(0,36) # 0 to 35, to include sub-00 to sub-35
group_name = 'group36'


################################################################################
# Let's start!
################################################################################


################################################################################
# Compute epochs
################################################################################


# subj_id_list -----------------------
# applyto all
subj_id_list = np.arange(0,36) # 0 to 35, to include sub-00 to sub-35


for i_su in subj_id_list:
    
    subj='sub-{:02}'.format(i_su) 
    dir_imne_preproc = os.path.join(dir_mne, subj, raw_folder) #individual mne path
    dir_imne_erp = os.path.join(dir_mne, subj, erp_folder) #individual mne path
    
    f_epochs_out=os.path.join(dir_imne_erp, '{}.{}-epo.fif'.format(subj, pref_epo_out))
    
    if os.path.exists(f_epochs_out):
        print(f'------------------------------------------- {subj}: file {pref_epo_out} already exists')
    else:
        #### 1 ----- Load clean continuous 
        f_raw_cont = os.path.join(dir_imne_preproc, f'{subj}.{pref_continuous}.raw.fif') # 
        raw_cont = read_raw_fif(f_raw_cont, preload=True)
        
        #### 2 ----- Load clean single epochs 
        f_epochs_in = os.path.join(dir_imne_preproc, '{}.{}-epo.fif'.format(subj, pref_epo_in))
        epochs_in = read_epochs(f_epochs_in)    
        
        #### 3 ----- Epoching using epochs_in selected events 
        
        reject={'eeg': threshold_eeg[subj]}
        picks = pick_types(epochs_in.info, meg=False, eeg=True, stim=False,  eog=False, exclude=exclude_eeg[subj])
    
        epochs_params=dict(events=epochs_in.events,event_id=event_id,
                     tmin=tmin, tmax=tmax, 
                     reject=reject, picks=picks, detrend=None,
                     baseline=None, verbose=False)      
        epochs_rej = Epochs(raw_cont, **epochs_params) # this removes bad sensors, I d'ont want this
        epochs_rej.drop_bad()
        
        picks = pick_types(epochs_in.info, meg=False, eeg=True, stim=False,  eog=False, exclude=())
        epochs_out = import_epochs(subj, raw_cont, epochs_rej,  picks,  event_id, tmin, tmax,  plot=False)
        
    #    epochs_out.info['bads']=exclude_eeg[subj]
    
        
        #### 4 ----  We save
        if not os.path.exists(dir_imne_erp):
            os.makedirs(dir_imne_erp)
      
        f_epochs_out=os.path.join(dir_imne_erp, '{}.{}-epo.fif'.format(subj, pref_epo_out))
        epochs_out.save(f_epochs_out)   
            
   
################################################################################
# Compute individual ERPs
################################################################################

#subj_id_list = np.arange(0,36) # 0 to 35, to include sub-00 to sub-35
#baseline = None
#plot_indiv = False
#neg = True
#
#for i_su in subj_id_list:
#    
#    subj='sub-{:02}'.format(i_su) 
#    dir_imne_erp = os.path.join(dir_mne, subj, erp_folder) #individual mne path
#  
#    #### ---- Load epochs 
#    f_epochs=os.path.join(dir_imne_erp, '{}.{}-epo.fif'.format(subj, pref_epo_out))
#    epochs = read_epochs(f_epochs) 
#    epochs.info['bads']=exclude_eeg[subj]
#    epochs.interpolate_bads()
#        
#    evokeds = [epochs[name].average().apply_baseline(baseline) for name in erp_list]
#    for i_ev, erp_name in enumerate(erp_list):
#        evokeds[i_ev].comment = '{}, {}'.format(subj, erp_name)
#        new_name = erp_name.replace('/', '_')
#        print(new_name)
#        if pref_erp:
#            new_name = f'{pref_erp}_{new_name}'
#        f_out = os.path.join(dir_imne_erp, '{}.{}-ave.fif'.format(subj, new_name))
#        write_evokeds(f_out, evokeds[i_ev])
#          
#    if plot_indiv:
#        evokeds_plot(evokeds, plot_type='traces', vline = [-1.22, -0.61, 0], neg=neg)
#


#################################################################################
## Compute group-average ERPs
#################################################################################
#overwrite_group = True
#
#for erp_name in erp_list:
#    pref_evoked = erp_name.replace('/', '_')
#    if pref_erp:
#        pref_evoked = f'{pref_erp}_{pref_evoked}'
#    if overwrite_group:
#            group_list, group_avg = evokeds_group_average(dir_mne, erp_folder, subj_group_avg , group_name,  pref_evoked,  save= True)
#    else:
#            group_list, group_avg = evokeds_group_average(dir_mne, erp_folder, subj_group_avg, group_name , pref_evoked,  save= False)
#

#################################################################################
## Plot group-average ERPs
#################################################################################
plot_type= [ 'traces']
plot_list =  [f'{pref_erp}_var_c_dev', f'{pref_erp}_var_p_dev' ]
col = ['tab:orange', 'tab:purple' ] #, 'tab:red']
evokeds=list()
for name in plot_list:
    dir_imne_erp = os.path.join(dir_mne, group_name, erp_folder)
    f_evo = os.path.join(dir_imne_erp, '{}.{}-ave.fif'.format(group_name, name))
    evokeds.append(read_evokeds(f_evo, condition=0))
evokeds_plot(evokeds, plot_type, neg=True,vline=[ 0], colors=col)
    