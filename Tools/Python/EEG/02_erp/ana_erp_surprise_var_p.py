#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
====================================================================
Compute typical MMN analysis in Condition var_p => contrast p+ and p- 
====================================================================
This analysis is a typical deviant-minus-standard analysis

We work here in the 2-20 Hz bandwith 
We don't use baseline correction
Output  =====> sub-XX/erp_2-20Hz/sub-XX.2-20Hz_var_p_p+_std-ave.fif
        =====> sub-XX/erp_2-20Hz/sub-XX.2-20Hz_var_p_p+_dev-ave.fif
        =====> sub-XX/erp_2-20Hz/sub-XX.2-20Hz_var_p_p+_mmn-ave.fif

        (and same for p=moins, p=mean)
        =====>        group data in group36/erp_2-20Hz
        =====>        group36.2-20Hz_var_c_A=dev_VS_B=std_permut10000.pkl
        =====>        group36.2-20Hz_var_p_A=dev_VS_B=std_permut10000.pkl
       
       
    @author: Françoise Lecaignard, francoise.lecaignard@inserm.fr
"""


###############################################################################
import numpy as np             # convention d'import
import os                       # path.join for file names
import matplotlib.pyplot as plt # from the MNE examples
import pickle
import sys
# path to my own functions
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Python/EEG')
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Python/EEG/02_erp')



from mne import Epochs, pick_types, read_events, read_epochs, find_layout
from mne import write_evokeds, read_evokeds
from mne import get_config, set_config
type(get_config('MNE_BROWSE_RAW_SIZE'))
set_config('MNE_BROWSE_RAW_SIZE', '20.0,10.0')
from mne.preprocessing import read_ica
from mne.viz import plot_topomap,plot_compare_evokeds, tight_layout

from mne.io import read_raw_fif

#from fun_NN_template import  ...
from fun_util_epoch import import_epochs
from fun_plot_erp import evokeds_plot, evokeds_plot_all_group
from fun_util_erp import evokeds_group_average,  evokeds_indiv_diff
from fun_util_eeg import print_step_label
from fun_stats_erp import view_significant_spatiotemp_clusters
from fun_codd_stats_erp import codd_spatiotemp_clusters_1samp

################################################################################
# Input Parameters 
################################################################################

# Paths ---------------------------- ---------------------------- ----------------------------
dir_mne = '/sps/cermep/cermep/experiments/DCM/ContextOdd/Ana_MNE'

erp_folder = 'erp_2-20Hz'
raw_folder = 'preproc'


# epochs 2-20 Hz  ---------------------------- ---------------------

pref_epo = 'clean_2-20Hz' #-epo.fif, cleaned epochs from preprocs


# epochs bounds
tmin, tmax = -0.2, 0.41 

# events for epochs

event_id = {'var_c/c4/c-/p0/std': 14121, 'var_c/c4/c-/p0/dev': 14122,'var_c/c4/c-/p0/std_oth': 14123,\
              'var_c/c5/c-/p0/std': 15121, 'var_c/c5/c-/p0/dev': 15122,'var_c/c5/c-/p0/std_oth': 15123,\
              'var_c/c6/c-/p0/std': 16121, 'var_c/c6/c-/p0/dev': 16122,'var_c/c6/c-/p0/std_oth': 16123,\
              'var_c/c2/c+/p0/std': 12221, 'var_c/c2/c+/p0/dev': 12222,'var_c/c2/c+/p0/std_oth': 12223,\
              'var_c/c3/c+/p0/std': 13221, 'var_c/c3/c+/p0/dev': 13222,'var_c/c3/c+/p0/std_oth': 13223,\
              'var_c/c4/c+/p0/std': 14221, 'var_c/c4/c+/p0/dev': 14222,'var_c/c4/c+/p0/std_oth': 14223,\
              'var_c/c6/c+/p0/std': 16221, 'var_c/c6/c+/p0/dev': 16222,'var_c/c6/c+/p0/std_oth': 16223,\
              'var_c/c7/c+/p0/std': 17221, 'var_c/c7/c+/p0/dev': 17222,'var_c/c7/c+/p0/std_oth': 17223,\
              'var_c/c8/c+/p0/std': 18221, 'var_c/c8/c+/p0/dev': 18222,'var_c/c8/c+/p0/std_oth': 18223,\
              'var_p/c2/c-/p+/std': 22111, 'var_p/c2/c-/p+/dev': 22112,'var_p/c2/c-/p+/std_oth': 22113,\
              'var_p/c3/c-/p+/std': 23111, 'var_p/c3/c-/p+/dev': 23112,'var_p/c3/c-/p+/std_oth': 23113,\
              'var_p/c4/c-/p+/std': 24111, 'var_p/c4/c-/p+/dev': 24112,'var_p/c4/c-/p+/std_oth': 24113,\
              'var_p/c4/c-/p0/std': 24121, 'var_p/c4/c-/p0/dev': 24122,'var_p/c4/c-/p0/std_oth': 24123,\
              'var_p/c5/c-/p0/std': 25121, 'var_p/c5/c-/p0/dev': 25122,'var_p/c5/c-/p0/std_oth': 25123,\
              'var_p/c6/c-/p0/std': 26121, 'var_p/c6/c-/p0/dev': 26122,'var_p/c6/c-/p0/std_oth': 26123,\
              'var_p/c6/c-/p-/std': 26131, 'var_p/c6/c-/p-/dev': 26132,'var_p/c6/c-/p-/std_oth': 26133,\
              'var_p/c7/c-/p-/std': 27131, 'var_p/c7/c-/p-/dev': 27132,'var_p/c7/c-/p-/std_oth': 27133,\
              'var_p/c8/c-/p-/std': 28131, 'var_p/c8/c-/p-/dev': 28132,'var_p/c8/c-/p-/std_oth': 28133                  }


# Evoked responses   ---------------------------- ---------------------

exclude_eeg = { 'sub-00': ['T7', 'TP9'],	 'sub-01': [],	'sub-02':['TP10', 'CP5', 'P4'],	 'sub-03': ['Oz'],	'sub-04': ['T7', 'T8'],\
             'sub-05': [],	 'sub-06': [],	'sub-07':[],	 'sub-08': ['O1', 'O2'],	'sub-09': ['T7', 'T8'],\
             'sub-10': [],    'sub-11': ['PO9'],   'sub-12':[],  'sub-13': ['T7', 'PO10'],    'sub-14': ['P3'],\
             'sub-15': ['T8'],    'sub-16': ['Oz'],   'sub-17':['PO9'],  'sub-18': [],    'sub-19': ['T7', 'T8', 'TP10'],\
             'sub-20': [],    'sub-21': [],   'sub-22':['P3'],	 'sub-23': ['CP1'],    'sub-24': [],\
             'sub-25': ['Oz', 'T8', 'TP9'],    'sub-26': [],   'sub-27':[],	 'sub-28': [],    'sub-29': ['CP5'],\
             'sub-30': ['T8', 'PO10'],    'sub-31': [],   'sub-32':['Oz'],	 'sub-33': ['T8'],    'sub-34': ['T8', 'T7'],\
             'sub-35': []	}

# compute ERPs for std, dev and compute difference too:
erp_compute_dict = {    'var_p/p-/mmn': ['var_p/p-/dev','var_p/p-/std'], # dev first, std second because we do : (1) dev -  (2) std 
                        'var_p/p0/mmn': ['var_p/p0/dev','var_p/p0/std'],
                        'var_p/p+/mmn': ['var_p/p+/dev','var_p/p+/std' ] }

pref_erp = '2-20Hz'  # ===> sub-00.2-20Hz_var_p_p-_dev-ave.fif 

# Plot options   ---------------------------- ---------------------

color_dict={'2-20Hz_var_p_p-_std':'xkcd:green',\
            '2-20Hz_var_p_p-_dev':'xkcd:violet blue',\
            '2-20Hz_var_p_p-_mmn':'xkcd:wine',\
            '2-20Hz_var_p_p0_std':'xkcd:sea blue',\
            '2-20Hz_var_p_p0_dev':'xkcd:purple',\
            '2-20Hz_var_p_p0_mmn':'xkcd:red',\
            '2-20Hz_var_p_p+_std':'xkcd:vivid green',\
            '2-20Hz_var_p_p+_dev':'xkcd:vivid purple',\
            '2-20Hz_var_p_p+_mmn':'xkcd:orange red' ,\
            '2-20Hz_var_p_std':'xkcd:black',\
            '2-20Hz_var_p_dev':'xkcd:black',\
            '2-20Hz_var_p_mmn':'xkcd:black' ,\
            }

################################################################################
# Let's start!
################################################################################


#%% 
#################################################################################
## Compute individual ERPs (evoked object)
#################################################################################

subj_id_list = np.arange(0,36) # 0 to 35, to include sub-00 to sub-35
baseline = None

overwrite_ind = True

for i_su in subj_id_list:
    
    subj='sub-{:02}'.format(i_su) 
    dir_imne_erp = os.path.join(dir_mne, subj, erp_folder) #individual mne path
    dir_imne = os.path.join(dir_mne, subj, 'preproc') #individual mne path
    
    
    if overwrite_ind: # we  compute individual ERPs (std, dev ad difference in var_p, var_c)  
    
    #### ---- Load clean epochs (2-20 Hz)
        f_epochs=os.path.join(dir_imne, '{}.{}-epo.fif'.format(subj, pref_epo))
        epochs = read_epochs(f_epochs) 
        epochs.info['bads']=exclude_eeg[subj]
        epochs.interpolate_bads()
        
        for key, value   in erp_compute_dict.items():
            #### Compute std, dev and mmn ERPs, and save *-ave.fif ----
            # erp_list = [dev, std ] # 1 - 2 <=> dev - std 
            output_name = key
            erp_list = value.copy() # this is beacuse of the erp_list.append below, otherwise it adds the append thing in the dictionary too (???)
            print('-----------------')
            print(output_name, erp_list)
            print('-----------------')
            evokeds = evokeds_indiv_diff(dir_imne_erp, subj,epochs, baseline, erp_list, output_name,  neg = False, plot = None)
            erp_list.append(output_name)
            pref_evo_list=list()
            for idx, erp_name  in enumerate (erp_list):
                new_name = erp_name.replace('/', '_')
                print(new_name)
                if pref_erp:
                    new_name = f'{pref_erp}_{new_name}'
                f_out = os.path.join(dir_imne_erp, '{}.{}-ave.fif'.format(subj, new_name))
                write_evokeds(f_out, evokeds[idx])
                pref_evo_list.append(new_name)
        

  


#%%
##################################################################################
### Compute group-average ERPs
##################################################################################
subj_group_avg = np.arange(0,36) # 0 to 35, to include sub-00 to sub-35
group_name = 'group36'

overwrite_group = True
erp_list = [ 'var_p/p-/dev','var_p/p-/std', 'var_p/p-/mmn',
            'var_p/p+/dev','var_p/p0/std', 'var_p/p0/mmn',
            'var_p/p0/dev','var_p/p+/std', 'var_p/p+/mmn' ]
for erp_name in erp_list:
    pref_evoked = erp_name.replace('/', '_')
    if pref_erp:
        pref_evoked = f'{pref_erp}_{pref_evoked}'
    if overwrite_group:
            group_list, group_avg = evokeds_group_average(dir_mne, erp_folder, subj_group_avg , group_name,  pref_evoked,  save= True)
    else:
            group_list, group_avg = evokeds_group_average(dir_mne, erp_folder, subj_group_avg, group_name , pref_evoked,  save= False)

 
#%%
##################################################################################
### Plot group-average ERPs
##################################################################################
group_name = 'group36'
plot_type= [ 'traces']



plot_list =  [ '2-20Hz_var_p_p-_dev','2-20Hz_var_p_p-_std', '2-20Hz_var_p_p-_mmn'] 
#plot_list =  [ '2-20Hz_var_p_p+_dev','2-20Hz_var_p_p0_std', '2-20Hz_var_p_p0_mmn']
#plot_list =  [ '2-20Hz_var_p_p0_dev','2-20Hz_var_p_p+_std', '2-20Hz_var_p_p+_mmn' ]
plot_list =  [ '2-20Hz_var_p_p-_std', '2-20Hz_var_p_p+_std'] 
plot_list =  [ '2-20Hz_var_p_p-_dev', '2-20Hz_var_p_p+_dev'] 
#plot_list =  [ '2-20Hz_var_p_p-_mmn', '2-20Hz_var_p_p+_mmn'] 
#plot_list =  [ '2-20Hz_var_p_p-_mmn', '2-20Hz_var_p_p0_mmn', '2-20Hz_var_p_p+_mmn', '2-20Hz_var_p_mmn'] 
#plot_list =  [ '2-20Hz_var_p_p-_std', '2-20Hz_var_p_p0_std', '2-20Hz_var_p_p+_std'] #, '2-20Hz_var_p_std'] 
#plot_list =  [ '2-20Hz_var_p_p-_dev',  '2-20Hz_var_p_p0_dev', '2-20Hz_var_p_p+_dev', '2-20Hz_var_p_dev'] 

colors = []
evokeds=list()
for name in plot_list:
    dir_imne_erp = os.path.join(dir_mne, group_name, erp_folder)
    f_evo = os.path.join(dir_imne_erp, '{}.{}-ave.fif'.format(group_name, name))
    evokeds.append(read_evokeds(f_evo, condition=0))
    colors.append(color_dict[name])
evokeds_plot(evokeds, plot_type, neg=True,vline=[ 0], colors=colors)
 


#%%
##################################################################################
### Export Elan, stats
##################################################################################
import scipy.io as sio 
elan_folder = 'stats_elan' # within erp_folder

export_list = [ '2-20Hz_var_p_p-_std', '2-20Hz_var_p_p0_std', '2-20Hz_var_p_p+_std',
                '2-20Hz_var_p_p-_dev', '2-20Hz_var_p_p0_dev', '2-20Hz_var_p_p+_dev',
                '2-20Hz_var_p_p-_mmn', '2-20Hz_var_p_p0_mmn', '2-20Hz_var_p_p+_mmn',
               ] 

pref_erp = '2-20Hz'  # ===> sub-00.2-20Hz_var_p_dev-ave.fif 

# Export *-ave.fif in *.mat files


subj_id_list = np.arange(0,36) # 0 to 35, to include sub-00 to sub-35


for i_su in subj_id_list:
    subj='sub-{:02}'.format(i_su) 
    dir_imne = os.path.join(dir_mne, subj, erp_folder) #individual mne path
    dir_ielan = os.path.join(dir_imne, elan_folder) 
    if not os.path.exists(dir_ielan):
        os.makedirs(dir_ielan)

    for name in export_list:    
        f_evo = os.path.join(dir_imne, '{}.{}-ave.fif'.format(subj, name))
        evoked=read_evokeds(f_evo, condition=0)
        matinfo = {'d':evoked.data, \
                   'sens': evoked.ch_names, \
                   'times': evoked.times, \
                   'sfreq': evoked.info['sfreq']  }
        f_mat = os.path.join(dir_ielan, '{}.{}.mat'.format(subj, name))
        sio.savemat(f_mat, matinfo)
       
        
# export group avg
subj='group36'
dir_imne = os.path.join(dir_mne, subj, erp_folder) #individual mne path
dir_ielan = os.path.join(dir_imne, elan_folder) 
if not os.path.exists(dir_ielan):
    os.makedirs(dir_ielan)

for name in export_list:    
    f_evo = os.path.join(dir_imne, '{}.{}-ave.fif'.format(subj, name))
    evoked=read_evokeds(f_evo, condition=0)
    matinfo = {'d':evoked.data, \
               'sens': evoked.ch_names, \
               'times': evoked.times, \
               'sfreq': evoked.info['sfreq']  }
    f_mat = os.path.join(dir_ielan, '{}.{}.mat'.format(subj, name))
    sio.savemat(f_mat, matinfo)        


  