#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
====================================================================
Compute typical MMN analysis analysis 
====================================================================
This analysis is a typical deviant-minus-standard analysis

We work here in the 2-200 Hz bandwith 
We test with and without  baseline correction
For  this, we need to filter raw cleaned data (1-40Hz -> 2-20Hz) and redo the epoching 
We take selected events from Preprocessing epoching (preproc / sub-XX.clean_1-40Hz-epo.fif) and do no rejection

Output =====> sub-XX/preproc/sub-XX..preproc_2-20Hz_ica.raw.fif (cleaned 2-20Hz data)
       =====> sub-XX/preproc/sub-XX.clean_2-20Hz-epo.fif (cleaned epochs)
       =====> sub-XX/erp_2-20Hz/sub-XX.2-20Hz_var_c_std-ave.fif
       =====> sub-XX/erp_2-20Hz/sub-XX.2-20Hz_var_c_dev-ave.fif
       =====> sub-XX/erp_2-20Hz/sub-XX.2-20Hz_var_c_mmn-ave.fif
       (and same for var_p)
       =====>        group data in group36/erp_2-20Hz
       =====>        group36.2-20Hz_var_c_A=dev_VS_B=std_permut10000.pkl
       =====>        group36.2-20Hz_var_p_A=dev_VS_B=std_permut10000.pkl
    
    @author: Françoise Lecaignard, francoise.lecaignard@inserm.fr
"""


###############################################################################
import numpy as np             # convention d'import
import os                       # path.join for file names
import matplotlib.pyplot as plt # from the MNE examples
import pickle
import sys
# path to my own functions
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Python/EEG')
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Python/EEG/02_erp')



from mne import Epochs, pick_types, read_events, read_epochs, find_layout
from mne import write_evokeds, read_evokeds
from mne import get_config, set_config
type(get_config('MNE_BROWSE_RAW_SIZE'))
set_config('MNE_BROWSE_RAW_SIZE', '20.0,10.0')
from mne.preprocessing import read_ica
from mne.viz import plot_topomap,plot_compare_evokeds, tight_layout

from mne.io import read_raw_fif

#from fun_NN_template import  ...
from fun_util_epoch import import_epochs
from fun_plot_erp import evokeds_plot, evokeds_plot_all_group, evokeds_plot_overlay_across_subjects
from fun_util_erp import evokeds_group_average,  evokeds_indiv_diff
from fun_util_eeg import print_step_label
from fun_stats_erp import view_significant_spatiotemp_clusters
from fun_codd_stats_erp import codd_spatiotemp_clusters_1samp

################################################################################
# Input Parameters 
################################################################################

# Paths ---------------------------- ---------------------------- ----------------------------
dir_mne = '/sps/cermep/cermep/experiments/DCM/ContextOdd/Ana_MNE'

erp_folder = 'erp_2-20Hz'
raw_folder = 'preproc'

# Filtering continuous data  ---------------------------- ---------------------

pref_continuous_in = 'preproc_1-40Hz_ica' 
pref_continuous_out = 'preproc_2-20Hz_ica' 

# Import epochs from 1-40Hz selection  ---------------------------- ---------------------

pref_epo_in = 'clean_1-40Hz' #-epo.fif, cleaned epochs from preprocs
pref_epo_out = 'clean_2-20Hz' #-epo.fif, cleaned epochs from preprocs


# epochs bounds
tmin, tmax = -0.2, 0.41 

# events for epochs

event_id = {'var_c/c4/c-/p0/std': 14121, 'var_c/c4/c-/p0/dev': 14122,'var_c/c4/c-/p0/std_oth': 14123,\
              'var_c/c5/c-/p0/std': 15121, 'var_c/c5/c-/p0/dev': 15122,'var_c/c5/c-/p0/std_oth': 15123,\
              'var_c/c6/c-/p0/std': 16121, 'var_c/c6/c-/p0/dev': 16122,'var_c/c6/c-/p0/std_oth': 16123,\
              'var_c/c2/c+/p0/std': 12221, 'var_c/c2/c+/p0/dev': 12222,'var_c/c2/c+/p0/std_oth': 12223,\
              'var_c/c3/c+/p0/std': 13221, 'var_c/c3/c+/p0/dev': 13222,'var_c/c3/c+/p0/std_oth': 13223,\
              'var_c/c4/c+/p0/std': 14221, 'var_c/c4/c+/p0/dev': 14222,'var_c/c4/c+/p0/std_oth': 14223,\
              'var_c/c6/c+/p0/std': 16221, 'var_c/c6/c+/p0/dev': 16222,'var_c/c6/c+/p0/std_oth': 16223,\
              'var_c/c7/c+/p0/std': 17221, 'var_c/c7/c+/p0/dev': 17222,'var_c/c7/c+/p0/std_oth': 17223,\
              'var_c/c8/c+/p0/std': 18221, 'var_c/c8/c+/p0/dev': 18222,'var_c/c8/c+/p0/std_oth': 18223,\
              'var_p/c2/c-/p+/std': 22111, 'var_p/c2/c-/p+/dev': 22112,'var_p/c2/c-/p+/std_oth': 22113,\
              'var_p/c3/c-/p+/std': 23111, 'var_p/c3/c-/p+/dev': 23112,'var_p/c3/c-/p+/std_oth': 23113,\
              'var_p/c4/c-/p+/std': 24111, 'var_p/c4/c-/p+/dev': 24112,'var_p/c4/c-/p+/std_oth': 24113,\
              'var_p/c4/c-/p0/std': 24121, 'var_p/c4/c-/p0/dev': 24122,'var_p/c4/c-/p0/std_oth': 24123,\
              'var_p/c5/c-/p0/std': 25121, 'var_p/c5/c-/p0/dev': 25122,'var_p/c5/c-/p0/std_oth': 25123,\
              'var_p/c6/c-/p0/std': 26121, 'var_p/c6/c-/p0/dev': 26122,'var_p/c6/c-/p0/std_oth': 26123,\
              'var_p/c6/c-/p-/std': 26131, 'var_p/c6/c-/p-/dev': 26132,'var_p/c6/c-/p-/std_oth': 26133,\
              'var_p/c7/c-/p-/std': 27131, 'var_p/c7/c-/p-/dev': 27132,'var_p/c7/c-/p-/std_oth': 27133,\
              'var_p/c8/c-/p-/std': 28131, 'var_p/c8/c-/p-/dev': 28132,'var_p/c8/c-/p-/std_oth': 28133                  }


# Evoked responses   ---------------------------- ---------------------

exclude_eeg = { 'sub-00': ['T7', 'TP9'],	 'sub-01': [],	'sub-02':['TP10', 'CP5', 'P4'],	 'sub-03': ['Oz'],	'sub-04': ['T7', 'T8'],\
             'sub-05': [],	 'sub-06': [],	'sub-07':[],	 'sub-08': ['O1', 'O2'],	'sub-09': ['T7', 'T8'],\
             'sub-10': [],    'sub-11': ['PO9'],   'sub-12':[],  'sub-13': ['T7', 'PO10'],    'sub-14': ['P3'],\
             'sub-15': ['T8'],    'sub-16': ['Oz'],   'sub-17':['PO9'],  'sub-18': [],    'sub-19': ['T7', 'T8', 'TP10'],\
             'sub-20': [],    'sub-21': [],   'sub-22':['P3'],	 'sub-23': ['CP1'],    'sub-24': [],\
             'sub-25': ['Oz', 'T8', 'TP9'],    'sub-26': [],   'sub-27':[],	 'sub-28': [],    'sub-29': ['CP5'],\
             'sub-30': ['T8', 'PO10'],    'sub-31': [],   'sub-32':['Oz'],	 'sub-33': ['T8'],    'sub-34': ['T8', 'T7'],\
             'sub-35': []	}

erp_list = ['var_p/std','var_p/dev', 'var_c/std','var_c/dev',  ] 
pref_erp = '2-20Hz'  # ===> sub-00.2-20Hz_var_p_dev-ave.fif 

# Plot options   ---------------------------- ---------------------

color_dict={'2-20Hz_var_c_std':'xkcd:deep green',\
            '2-20Hz_var_c_dev':'xkcd:violet blue',\
            '2-20Hz_var_c_mmn':'xkcd:red',\
            '2-20Hz_var_p_std':'xkcd:green',\
            '2-20Hz_var_p_dev':'xkcd:violet',\
            '2-20Hz_var_p_mmn':'xkcd:orange red'}
################################################################################
# Let's start!
################################################################################

#%%
################################################################################
# Filter (2-20Hz) continuous data and do the epoching
################################################################################
#subj_id_list = np.arange(0,36) # 0 to 35, to include sub-00 to sub-35
#lf, hf = 2,20
#
#for i_su in subj_id_list:
#    subj='sub-{:02}'.format(i_su) 
#    dir_imne = os.path.join(dir_mne, subj, 'preproc') #individual mne path
#
#    #### ---- Load raw data, unfilter if not achieved yet
#    f_raw_in = os.path.join(dir_imne, f'{subj}.{pref_continuous_in}.raw.fif') 
#    f_raw_out = os.path.join(dir_imne, f'{subj}.{pref_continuous_out}.raw.fif') 
#    
#    f_epo_in = os.path.join(dir_imne, f'{subj}.{pref_epo_in}-epo.fif') #1-40 Hz
#    f_epo_out = os.path.join(dir_imne, f'{subj}.{pref_epo_out}-epo.fif') # 2-20 Hz
#    
#    if os.path.isfile(f_epo_out): # epochs 2-20Hz already computed 
#        print(f'-------------------------> Epoching 2-20Hz already done for {subj}') 
#    else:
#        
#        if os.path.isfile(f_raw_out):# continuous 2-20Hz already computed 
#            raw_filt = read_raw_fif(f_raw_out, preload=True)
#        else:
#            f_raw = os.path.join(dir_imne, f'{subj}.{pref_continuous_in}.raw.fif') 
#            raw = read_raw_fif(f_raw, preload=True)
#           
#            print_step_label('BandPass - Filtering {}.....'.format(subj))    
#            raw_filt=   raw.filter(lf, hf,  l_trans_bandwidth='auto',filter_length='auto', phase='zero', fir_window='hann', fir_design='firwin')
#            raw_filt.save(fname=f_raw_out,overwrite=True)
#        
#        #### ----  import cleaned 1-40 Hz epochs
#        epochs_in = read_epochs(f_epo_in)  
#        picks = pick_types(epochs_in.info, eeg=True, stim=False,exclude=())
#        epochs_out = import_epochs(subj, raw_filt, epochs_in, picks,  event_id, tmin, tmax, plot=False)
#        epochs_out.save(f_epo_out)    
#
#
#  
#%% 
#################################################################################
## Compute individual ERPs (evoked object)
#################################################################################

subj_id_list = np.arange(0,36) # 0 to 35, to include sub-00 to sub-35
baseline = None
baseline = None

overwrite_ind = True

for i_su in subj_id_list:
    
    subj='sub-{:02}'.format(i_su) 
    dir_imne_erp = os.path.join(dir_mne, subj, erp_folder) #individual mne path
    dir_imne = os.path.join(dir_mne, subj, 'preproc') #individual mne path
    
    
    if overwrite_ind: # we  compute individual ERPs (std, dev ad difference in var_p, var_c)  
    
    #### ---- Load clean epochs (2-20 Hz)
        f_epochs=os.path.join(dir_imne, '{}.{}-epo.fif'.format(subj, pref_epo_out))
        epochs = read_epochs(f_epochs) 
        epochs.info['bads']=exclude_eeg[subj]
        epochs.interpolate_bads()
        
        #### Compute std, dev and mmn ERPs, and save *-ave.fif ----
        erp_list = ['dev','std' ] # 1 - 2 <=> dev - std 
        output_name = 'mmn'
        evokeds = evokeds_indiv_diff(dir_imne_erp, subj,epochs, baseline, erp_list, output_name,  neg = False, plot = None)
        erp_list.append(output_name)
        pref_evo_list=list()
        for idx, erp_name  in enumerate (erp_list):
            new_name = erp_name.replace('/', '_')
            print(new_name)
            if pref_erp:
                new_name = f'{pref_erp}_all_{new_name}'
            f_out = os.path.join(dir_imne_erp, '{}.{}-ave.fif'.format(subj, new_name))
            write_evokeds(f_out, evokeds[idx])
            pref_evo_list.append(new_name)
            
        #### Compute std, dev and mmn ERPs, and save *-ave.fif ----
#        erp_list = ['var_c/dev','var_c/std' ] # 1 - 2 <=> dev - std 
#        output_name = 'var_c/mmn'
#        evokeds = evokeds_indiv_diff(dir_imne_erp, subj,epochs, baseline, erp_list, output_name,  neg = False, plot = None)
#        erp_list.append(output_name)
#        pref_evo_list=list()
#        for idx, erp_name  in enumerate (erp_list):
#            new_name = erp_name.replace('/', '_')
#            print(new_name)
#            if pref_erp:
#                new_name = f'{pref_erp}_{new_name}'
#            f_out = os.path.join(dir_imne_erp, '{}.{}-ave.fif'.format(subj, new_name))
#            write_evokeds(f_out, evokeds[idx])
#            pref_evo_list.append(new_name)
#        
#        #### Compute std, dev and mmn ERPs, and save *-ave.fif ----
#        erp_list = ['var_p/dev','var_p/std' ] # 1 - 2 <=> dev - std 
#        output_name = 'var_p/mmn'
#        evokeds = evokeds_indiv_diff(dir_imne_erp, subj,epochs, baseline, erp_list, output_name,  neg = False, plot = None)
#        erp_list.append(output_name)
#        for idx, erp_name  in enumerate (erp_list):
#            new_name = erp_name.replace('/', '_')
#            print(new_name)
#            if pref_erp:
#                new_name = f'{pref_erp}_{new_name}'
#            f_out = os.path.join(dir_imne_erp, '{}.{}-ave.fif'.format(subj, new_name))
#            write_evokeds(f_out, evokeds[idx])
#            pref_evo_list.append(new_name)
#
#    else: 
#        erp_list = [ 'var_c/dev','var_c/std','var_c/mmn', 'var_p/dev','var_p/std','var_p/mmn']
#        pref_evo_list=list()
#        for idx, erp_name  in enumerate (erp_list):
#            new_name = erp_name.replace('/', '_')
#            if pref_erp:
#                new_name = f'{pref_erp}_{new_name}'
#            pref_evo_list.append(new_name)
#            
#            f_erp = os.path.join(dir_imne_erp, '{}.{}-ave.fif'.format(subj, new_name))
#    
#            if not os.path.exists(f_erp):
#                print('EVOKED FILES NOT FOUND for {}'.format(subj))
##                return
  


#%%
##################################################################################
### Compute group-average ERPs
##################################################################################
subj_group_avg = np.arange(0,36) # 0 to 35, to include sub-00 to sub-35
group_name = 'group36'

overwrite_group = True
erp_list = [ 'var_c/dev','var_c/std','var_c/mmn', 'var_p/dev','var_p/std','var_p/mmn']
erp_list = [ 'all_dev','all_std','all_mmn']
for erp_name in erp_list:
    pref_evoked = erp_name.replace('/', '_')
    if pref_erp:
        pref_evoked = f'{pref_erp}_{pref_evoked}'
    if overwrite_group:
            group_list, group_avg = evokeds_group_average(dir_mne, erp_folder, subj_group_avg , group_name,  pref_evoked,  save= True)
    else:
            group_list, group_avg = evokeds_group_average(dir_mne, erp_folder, subj_group_avg, group_name , pref_evoked,  save= False)

 
#%%
##################################################################################
### Plot group-average ERPs
##################################################################################
group_name = 'group36'
plot_type= [ 'traces', 'GFP']

#
#
plot_list =  [ '2-20Hz_var_c_std', '2-20Hz_var_c_dev', '2-20Hz_var_c_mmn']
colors = []
evokeds=list()
for name in plot_list:
    dir_imne_erp = os.path.join(dir_mne, group_name, erp_folder)
    f_evo = os.path.join(dir_imne_erp, '{}.{}-ave.fif'.format(group_name, name))
    evokeds.append(read_evokeds(f_evo, condition=0))
    colors.append(color_dict[name])
evokeds_plot(evokeds, plot_type, neg=True,vline=[ 0], colors=colors)
 
plot_list =  [ '2-20Hz_var_p_std', '2-20Hz_var_p_dev', '2-20Hz_var_p_mmn']
colors = []
evokeds=list()
for name in plot_list:
    dir_imne_erp = os.path.join(dir_mne, group_name, erp_folder)
    f_evo = os.path.join(dir_imne_erp, '{}.{}-ave.fif'.format(group_name, name))
    evokeds.append(read_evokeds(f_evo, condition=0))
    colors.append(color_dict[name])
evokeds_plot(evokeds, plot_type, neg=True,vline=[ 0], colors=colors)


plot_list =  [ '2-20Hz_var_c_std', '2-20Hz_var_p_std']
colors = [ ]
evokeds=list()
for name in plot_list:
    dir_imne_erp = os.path.join(dir_mne, group_name, erp_folder)
    f_evo = os.path.join(dir_imne_erp, '{}.{}-ave.fif'.format(group_name, name))
    evokeds.append(read_evokeds(f_evo, condition=0))
    colors.append(color_dict[name])
evokeds_plot(evokeds, plot_type, neg=True,vline=[ 0], colors=colors)

plot_list =  [ '2-20Hz_var_c_dev', '2-20Hz_var_p_dev']
colors = [ ]
evokeds=list()
for name in plot_list:
    dir_imne_erp = os.path.join(dir_mne, group_name, erp_folder)
    f_evo = os.path.join(dir_imne_erp, '{}.{}-ave.fif'.format(group_name, name))
    evokeds.append(read_evokeds(f_evo, condition=0))
    colors.append(color_dict[name])
evokeds_plot(evokeds, plot_type, neg=True,vline=[ 0], colors=colors)

plot_list =  [ '2-20Hz_var_c_mmn', '2-20Hz_var_p_mmn']
colors = [ ]
evokeds=list()
for name in plot_list:
    dir_imne_erp = os.path.join(dir_mne, group_name, erp_folder)
    f_evo = os.path.join(dir_imne_erp, '{}.{}-ave.fif'.format(group_name, name))
    evokeds.append(read_evokeds(f_evo, condition=0))
    colors.append(color_dict[name])
evokeds_plot(evokeds, plot_type, neg=True,vline=[ 0], colors=colors)




#%%                
#################################################################################
## Plot individual  ERPs
#################################################################################
plot_type= [ 'traces', 'GFP']
subj_id_list = np.arange(0,1)
plot_list =  [ '2-20Hz_var_p_std', '2-20Hz_var_p_dev','2-20Hz_var_p_mmn']
colors = [ ]
for name in plot_list:
    colors.append(color_dict[name])
evokeds_plot_all_group(dir_mne, erp_folder, subj_id_list, plot_list,   plot_type, neg=True, colors=colors)
#
#
#plot_list =  [ '2-20Hz_var_c_mmn', '2-20Hz_var_p_mmn']
#colors = [ ]
#for name in plot_list:
#    colors.append(color_dict[name])
#evokeds_plot_all_group(dir_mne, erp_folder, subj_id_list, plot_list,   plot_type, neg=True, colors=colors)
#
#
##plot_list =  [ '2-20Hz_var_c_mmn', '2-20Hz_var_p_mmn']
##colors = [ ]
##for name in plot_list:
##    colors.append(color_dict[name])
##evokeds_plot_all_group(dir_mne, erp_folder, subj_id_list, plot_list,   plot_type, neg=True, colors=colors)
#
#
#plot_type= [ 'topo']
#subj_id_list = np.arange(15,17)
#plot_list =  [ '2-20Hz_var_c_std']
#colors = [ ]
#for name in plot_list:
#    colors.append(color_dict[name])
#evokeds_plot_all_group(dir_mne, erp_folder, subj_id_list, plot_list,   plot_type, neg=False, colors=colors)

#%%
#################################################################################
## Overlay individual  ERPs
#################################################################################
# We can use MNE plot_evoked-compare : this provides a single figure per sensor, or the GFP or mean across sensors
# Here, we want to overlay ERPs (from multiple subjects) using the scalp view
erp_folder = 'erp_2-20Hz'
plot_list =  [ '2-20Hz_var_p_std'] #, '2-20Hz_var_p_dev','2-20Hz_var_p_mmn']
subj_id_list = np.arange(0,1)
group_name='group36'
plot_type= [ 'traces', 'GFP']


evokeds_plot_overlay_across_subjects(dir_mne, erp_folder, plot_list, subj_id_list, group_name=group_name, plot_type=plot_type)


#%%                
#################################################################################
## Stats : group-level analysis; spatio-temporal clusters
#################################################################################
# non-parametric Paired t-test, using spatio-temporal connectivity
# We build a data matrix X of size (n_subj, n_samples, n_sensors)
# Paired-data : we compute the difference X = A-B, and perform a t-test with the difference

# ......... 1) Prepare folder  stats      (common to all stat analysis)  ...................................
# .............................................................................................
group_name = 'group36'
dir_imne_erp = os.path.join(dir_mne, group_name, erp_folder)
stat_folder = 'stats'
dir_imne_stats = os.path.join(dir_imne_erp, stat_folder)

if not os.path.exists(dir_imne_stats):
    os.makedirs(dir_imne_stats)

# ......... 2) Define default parameters      (common to all stat analysis)  ...................................
# .............................................................................................
subj_id_list = np.arange(0,36)
p_threshold = 0.01 # for the cluster analysis (t-value)
nr_permutations=10000
tail = 0
# ......... 3) Define connectivity across sensors  (common to all stat analysis)  ...................................
# .............................................................................................

from mne.channels import find_ch_connectivity
f_evo = os.path.join(dir_imne_erp, '{}.{}-ave.fif'.format(group_name, '2-20Hz_var_c_dev'))
evoked = read_evokeds(f_evo, condition=0)

connectivity, ch_names = find_ch_connectivity(evoked.info, ch_type='eeg')
print(type(connectivity))
plt.imshow(connectivity.toarray(), cmap='gray', origin='lower', interpolation='nearest')
plt.xlabel('{} EEG sensors'.format(len(ch_names)))
plt.ylabel('{} EEG sensors'.format(len(ch_names)))
plt.title('Between-sensor adjacency')
#
# Try with BEM connectivity matrix
import scipy.io as sio 
f_conn_bem = '/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Reference_Files/Connectivity_15subj_th85_lefttop=Fp1.mat'
m = sio.loadmat(f_conn_bem)
conn = m['connectivity']
import scipy.sparse 
connectivity_om = scipy.sparse.csr_matrix(conn)
plt.imshow(connectivity_om.toarray(), cmap='gray', origin='lower', interpolation='nearest')
plt.xlabel('{} EEG sensors'.format(len(ch_names)))
plt.ylabel('{} EEG sensors'.format(len(ch_names)))
plt.title('Between-sensor adjacency')



#------------4)  MMN emergence in condition var_c ------------------------------

#stat_cond_list =  [ '2-20Hz_var_c_dev', '2-20Hz_var_c_std'] # X = A - B = DEV - STD
#stat_name = f'2-20Hz_var_c_A=dev_VS_B=std_permut{nr_permutations}'
#f_save = os.path.join(dir_imne_stats, '{}.{}.pkl'.format(group_name, stat_name))
#
#clusters_stats = codd_spatiotemp_clusters_1samp(dir_mne, erp_folder, subj_id_list,stat_cond_list, connectivity, p_threshold, nr_permutations = nr_permutations, tail=tail, save = f_save)
#
## View results !!!                             ...................................
#with  open(f_save, 'rb') as input:
#        a = pickle.load(input)
#cluster_stats = a
#evoked_dict_cond_a_cond_b = dict() # dictionary {'STD': group-evoked, 'DEV': group-evoked}
#colors = [ ]
#for name in stat_cond_list:    
#    dir_imne_erp = os.path.join(dir_mne, group_name, erp_folder)
#    f_evo = os.path.join(dir_imne_erp, '{}.{}-ave.fif'.format(group_name, name))
#    evoked_dict_cond_a_cond_b[name]=read_evokeds(f_evo, condition=0)
#    colors.append(color_dict[name])    
#
#p_clusters = 0.05
#view_significant_spatiotemp_clusters(cluster_stats, p_clusters, evoked_dict_cond_a_cond_b, colors)


#------------5)  MMN emergence in condition var_p ------------------------------

stat_cond_list =  [ '2-20Hz_var_p_dev', '2-20Hz_var_p_std'] # X = A - B = DEV - STD
stat_name = f'2-20Hz_var_p_A=dev_VS_B=std_permut{nr_permutations}'
f_save = os.path.join(dir_imne_stats, '{}.{}.pkl'.format(group_name, stat_name))

clusters_stats = codd_spatiotemp_clusters_1samp(dir_mne, erp_folder, subj_id_list,stat_cond_list, connectivity, p_threshold, nr_permutations = nr_permutations, tail=tail, save = f_save)

# View results !!!                             ...................................
stat_name = f'2-20Hz_var_p_dev_VS_std_{nr_permutations}_conn=om'
stat_name = f'2-20Hz_var_p_dev_VS_std_{nr_permutations}_conn=mne'
f_save = os.path.join(dir_imne_stats, '{}.{}.pkl'.format(group_name, stat_name))

with  open(f_save, 'rb') as input:
        a = pickle.load(input)
cluster_stats = a
evoked_dict_cond_a_cond_b = dict() # dictionary {'STD': group-evoked, 'DEV': group-evoked}
colors = [ ]
for name in stat_cond_list:    
    dir_imne_erp = os.path.join(dir_mne, group_name, erp_folder)
    f_evo = os.path.join(dir_imne_erp, '{}.{}-ave.fif'.format(group_name, name))
    evoked_dict_cond_a_cond_b[name]=read_evokeds(f_evo, condition=0)
    colors.append(color_dict[name])    

p_clusters = 0.01
view_significant_spatiotemp_clusters(cluster_stats, p_clusters, evoked_dict_cond_a_cond_b, colors)


#------------6)  Contrast var_p / var_c  in MMN ------------------------------

stat_cond_list =  [ '2-20Hz_var_p_mmn', '2-20Hz_var_c_mmn'] # X = A - B = DEV - STD
stat_name = f'2-20Hz_A=mmn_p_VS_B=mmn_c_permut{nr_permutations}'
f_save = os.path.join(dir_imne_stats, '{}.{}.pkl'.format(group_name, stat_name))

clusters_stats = codd_spatiotemp_clusters_1samp(dir_mne, erp_folder, subj_id_list,stat_cond_list, connectivity, p_threshold, nr_permutations = nr_permutations, tail=tail, save = f_save)

# View results !!!                             ...................................
with  open(f_save, 'rb') as input:
        a = pickle.load(input)
cluster_stats = a
evoked_dict_cond_a_cond_b = dict() # dictionary {'STD': group-evoked, 'DEV': group-evoked}
colors = [ ]
for name in stat_cond_list:    
    dir_imne_erp = os.path.join(dir_mne, group_name, erp_folder)
    f_evo = os.path.join(dir_imne_erp, '{}.{}-ave.fif'.format(group_name, name))
    evoked_dict_cond_a_cond_b[name]=read_evokeds(f_evo, condition=0)
    colors.append(color_dict[name])    

p_clusters = 0.1
view_significant_spatiotemp_clusters(cluster_stats, p_clusters, evoked_dict_cond_a_cond_b, colors)


#------------6)  Contrast var_p / var_c  in MMN ------------------------------

stat_cond_list =  [ '2-20Hz_var_p_dev', '2-20Hz_var_c_dev'] # X = A - B = DEV - STD
stat_name = f'2-20Hz_A=std_p_VS_B=std_c_permut{nr_permutations}'
f_save = os.path.join(dir_imne_stats, '{}.{}.pkl'.format(group_name, stat_name))

clusters_stats = codd_spatiotemp_clusters_1samp(dir_mne, erp_folder, subj_id_list,stat_cond_list, connectivity, p_threshold, nr_permutations = nr_permutations, tail=tail, save = f_save)

# View results !!!                             ...................................
with  open(f_save, 'rb') as input:
        a = pickle.load(input)
cluster_stats = a
evoked_dict_cond_a_cond_b = dict() # dictionary {'STD': group-evoked, 'DEV': group-evoked}
colors = [ ]
for name in stat_cond_list:    
    dir_imne_erp = os.path.join(dir_mne, group_name, erp_folder)
    f_evo = os.path.join(dir_imne_erp, '{}.{}-ave.fif'.format(group_name, name))
    evoked_dict_cond_a_cond_b[name]=read_evokeds(f_evo, condition=0)
    colors.append(color_dict[name])    

p_clusters = 0.05
view_significant_spatiotemp_clusters(cluster_stats, p_clusters, evoked_dict_cond_a_cond_b, colors)



#%%
##################################################################################
### Export Elan, stats - with / without baseline
##################################################################################
import scipy.io as sio 
no_baseline_export = True 

export_list = [ '2-20Hz_all_std',  '2-20Hz_all_dev',  '2-20Hz_all_mmn', 
               '2-20Hz_var_c_std',  '2-20Hz_var_c_dev',  '2-20Hz_var_c_mmn', 
               '2-20Hz_var_p_std',  '2-20Hz_var_p_dev',  '2-20Hz_var_p_mmn', 
               ] 

# Export *-ave.fif in *.mat files


subj_id_list = np.arange(0,36) # 0 to 35, to include sub-00 to sub-35

if no_baseline_export:
    elan_folder = 'stats_elan' # within erp_folder
else:
    elan_folder = 'stats_elan_baseline'

for i_su in subj_id_list:
    subj='sub-{:02}'.format(i_su) 
    dir_imne = os.path.join(dir_mne, subj, erp_folder) #individual mne path
    dir_ielan = os.path.join(dir_imne, elan_folder) 
    if not os.path.exists(dir_ielan):
        os.makedirs(dir_ielan)

    for name in export_list:    
        f_evo = os.path.join(dir_imne, '{}.{}-ave.fif'.format(subj, name))
        evoked=read_evokeds(f_evo, condition=0)
        if not no_baseline_export:
            evoked.apply_baseline(baseline=(-0.2, 0))
            name = f'{name}_baseline200'
        matinfo = {'d':evoked.data, \
                   'sens': evoked.ch_names, \
                   'times': evoked.times, \
                   'sfreq': evoked.info['sfreq']  }
        f_mat = os.path.join(dir_ielan, '{}.{}.mat'.format(subj, name))
        sio.savemat(f_mat, matinfo)
       
        
# export group avg
subj='group36'
dir_imne = os.path.join(dir_mne, subj, erp_folder) #individual mne path
dir_ielan = os.path.join(dir_imne, elan_folder) 
if not os.path.exists(dir_ielan):
    os.makedirs(dir_ielan)

for name in export_list:    
    f_evo = os.path.join(dir_imne, '{}.{}-ave.fif'.format(subj, name))
    evoked=read_evokeds(f_evo, condition=0)
    if not no_baseline_export:
        evoked.apply_baseline(baseline=(-0.2, 0))
        name = f'{name}_baseline200'
    matinfo = {'d':evoked.data, \
               'sens': evoked.ch_names, \
               'times': evoked.times, \
               'sfreq': evoked.info['sfreq']  }
    f_mat = os.path.join(dir_ielan, '{}.{}.mat'.format(subj, name))
    sio.savemat(f_mat, matinfo)        
