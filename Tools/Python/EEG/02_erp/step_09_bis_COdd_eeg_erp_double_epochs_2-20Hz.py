#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
====================================================================
Step 09 BIS - ERP   analysis : compute 2-20 Hz double epochs 
====================================================================

In addition to typical one-event epoching (-200, +410), we compute long epochs to derive "double ERPs" or "triple" ...
The aim is to investigate pre-stimulus period (and baseline drifts) 

We consider  continuous 2-20 Hz data, ica-corrected  (===> .../sub-xx/erp_2-20Hz/ sub-xx.clean_2-20Hz.raw.fif)
we compute long epochs (importing the rejection of step07)
We further drop epochs for which all stimuli in there are not good (we keep consecutive good ones)
Output ===> .../sub-xx/erp_2-20Hz/sub-xx.double_erp_clean_2-20Hz-epo.fif)

    @author: Françoise Lecaignard, francoise.lecaignard@inserm.fr
"""


###############################################################################
import numpy as np             # convention d'import
import os                       # path.join for file names
import matplotlib.pyplot as plt # from the MNE examples

import sys
# path to my own functions
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Python/EEG')



from mne import Epochs, pick_types, read_events, read_epochs
from mne import get_config, set_config
type(get_config('MNE_BROWSE_RAW_SIZE'))
set_config('MNE_BROWSE_RAW_SIZE', '20.0,10.0')
from mne.preprocessing import read_ica

from mne.io import read_raw_fif
from fun_eeg_misc import  print_step_label,import_epochs

#from fun_NN_template import  ...

################################################################################
# Input Parameters 
################################################################################

# Paths ----------------------------
dir_mne = '/sps/cermep/cermep/experiments/DCM/ContextOdd/Ana_MNE'

exclude_eeg = { 'sub-00': ['T7', 'TP9'],	 'sub-01': [],	'sub-02':['CP5', 'P4'],	 'sub-03': [],	'sub-04': ['PO10', 'T7'],\
             'sub-05': [],	 'sub-06': [],	'sub-07':[],	 'sub-08': [],	'sub-09': ['T7'],\
             'sub-10': [],    'sub-11': ['PO9'],   'sub-12':[],  'sub-13': [],    'sub-14': [],\
             'sub-15': [],    'sub-16': [],   'sub-17':[],  'sub-18': [],    'sub-19': ['T8'],\
             'sub-20': [],    'sub-21': [],   'sub-22':['T7'],	 'sub-23': ['CP1'],    'sub-24': [],\
             'sub-25': ['T8', 'FC2', 'Oz'],    'sub-26': [],   'sub-27':[],	 'sub-28': [],    'sub-29': [],\
             'sub-30': [],    'sub-31': [],   'sub-32':[],	 'sub-33': [],    'sub-34': [],\
             'sub-35': []	}

################################################################################
# Define Constant Variables
################################################################################

# ----------- erp data
# bandwidth
lf2, hf2 = 2, 20
# erp folder (.../sub-xx/erp_2-20Hz/)
erp_folder = f'erp_{lf2}-{hf2}Hz'

#prefix  2-20 Hz continuous data (filtered and ica)
pref_raw_cont = 'clean_{}-{}Hz'.format(lf2, hf2)
#prefix  2-20 Hz epochs  (clean, filtered and ica)
pref_epo_in = 'clean_{}-{}Hz'.format(lf2, hf2)

# epoch size (soa is 0.61 sec)
tmin, tmax = -1.1, 1.1
#prefix  2-20 Hz epochs  (clean, filtered and ica)
pref_epo_out = 'triple_erp_clean_{}-{}Hz'.format(lf2, hf2)

# ----------- event data
# orig events file (without any rejection), downsampled
eve_name = 'preproc.5d-eve'

# events for epochs
#Stimulus coding (five-digit format)
event_id_codd={'var_c/c4/c-/p0/std': 14121, 'var_c/c4/c-/p0/dev': 14122,'var_c/c4/c-/p0/std_oth': 14123,\
              'var_c/c5/c-/p0/std': 15121, 'var_c/c5/c-/p0/dev': 15122,'var_c/c5/c-/p0/std_oth': 15123,\
              'var_c/c6/c-/p0/std': 16121, 'var_c/c6/c-/p0/dev': 16122,'var_c/c6/c-/p0/std_oth': 16123,\
              'var_c/c2/c+/p0/std': 12221, 'var_c/c2/c+/p0/dev': 12222,'var_c/c2/c+/p0/std_oth': 12223,\
              'var_c/c3/c+/p0/std': 13221, 'var_c/c3/c+/p0/dev': 13222,'var_c/c3/c+/p0/std_oth': 13223,\
              'var_c/c4/c+/p0/std': 14221, 'var_c/c4/c+/p0/dev': 14222,'var_c/c4/c+/p0/std_oth': 14223,\
              'var_c/c6/c+/p0/std': 16221, 'var_c/c6/c+/p0/dev': 16222,'var_c/c6/c+/p0/std_oth': 16223,\
              'var_c/c7/c+/p0/std': 17221, 'var_c/c7/c+/p0/dev': 17222,'var_c/c7/c+/p0/std_oth': 17223,\
              'var_c/c8/c+/p0/std': 18221, 'var_c/c8/c+/p0/dev': 18222,'var_c/c8/c+/p0/std_oth': 18223,\
              'var_p/c2/c-/p+/std': 22111, 'var_p/c2/c-/p+/dev': 22112,'var_p/c2/c-/p+/std_oth': 22113,\
              'var_p/c3/c-/p+/std': 23111, 'var_p/c3/c-/p+/dev': 23112,'var_p/c3/c-/p+/std_oth': 23113,\
              'var_p/c4/c-/p+/std': 24111, 'var_p/c4/c-/p+/dev': 24112,'var_p/c4/c-/p+/std_oth': 24113,\
              'var_p/c4/c-/p0/std': 24121, 'var_p/c4/c-/p0/dev': 24122,'var_p/c4/c-/p0/std_oth': 24123,\
              'var_p/c5/c-/p0/std': 25121, 'var_p/c5/c-/p0/dev': 25122,'var_p/c5/c-/p0/std_oth': 25123,\
              'var_p/c6/c-/p0/std': 26121, 'var_p/c6/c-/p0/dev': 26122,'var_p/c6/c-/p0/std_oth': 26123,\
              'var_p/c6/c-/p-/std': 26131, 'var_p/c6/c-/p-/dev': 26132,'var_p/c6/c-/p-/std_oth': 26133,\
              'var_p/c7/c-/p-/std': 27131, 'var_p/c7/c-/p-/dev': 27132,'var_p/c7/c-/p-/std_oth': 27133,\
              'var_p/c8/c-/p-/std': 28131, 'var_p/c8/c-/p-/dev': 28132,'var_p/c8/c-/p-/std_oth': 28133                  }

# selected events (we don't compute all double epochs because sorting out good consecutive one takes a piece of time)
erp_list = ['var_c/std' , 'var_p/std' ] 
################################################################################
# Let's start!
################################################################################

# subj_id_list -----------------------
# applyto all
subj_id_list = np.arange(30,36) # 0 to 35, to include sub-00 to sub-35


for i_su in subj_id_list:
    
    subj='sub-{:02}'.format(i_su) 
    dir_imne_preproc = os.path.join(dir_mne, subj, 'preproc') #individual mne path
    dir_imne_erp = os.path.join(dir_mne, subj, erp_folder) #individual mne path
    
    f_events = os.path.join(dir_imne_preproc,  '{}.{}.fif'.format(subj,eve_name ))
    orig_events =read_events(f_events)
    
    print_step_label(subj)

    #### 1 ----- Load clean continuous 2-20 Hz (*.clean_2-20Hz.raw.fif)
    f_raw_cont = os.path.join(dir_imne_erp, f'{subj}.{pref_raw_cont}.raw.fif') # raw continuous data: downsampled,  filtered  (2-20 Hz) and ica-corrected
    raw_cont = read_raw_fif(f_raw_cont, preload=True)
    
    
    #### 2 ----  Create clean epochs (2-20Hz)   (*.clean_2-20Hz-epo.fif)
    # applying good epochs obtained in step07 (epochs_in) to current continuous cleaned data (raw_out)
    f_epochs_in = os.path.join(dir_imne_erp, '{}.{}-epo.fif'.format(subj, pref_epo_in))
    epochs_in = read_epochs(f_epochs_in)    
    picks = pick_types(raw_cont.info, eeg=True, stim=True,exclude=())
    epochs_clean = import_epochs(subj, raw_cont, epochs_in,  picks,  event_id_codd, tmin, tmax,  plot=False)
    
    # restriction to events in erp_list
    epochs_out = epochs_clean[erp_list]
    epochs_out.info['bads']=exclude_eeg[subj]
    
    #### 3 ----  We drop non-consecutive good epochs
    
    # sampling rate
    
    fs = epochs_out.info['sfreq']
 
    dur_pre = int(tmin *fs) # duration of pre-stim period in samples
    dur_post = int(tmax *fs) # duration of post-stim period in samples

    lat_orig = orig_events[:,0] # latencies (in sample) of original events, without rejection
    lat_clean = epochs_clean.events[:,0] # latencies (in sample) of all events, after early and autoreject rejection
    lat_out = epochs_out.events[:,0] # latencies (in sample) of events in erp_list, after early and autoreject rejection
    
    
    
    drop_bad = np.array([])
    print('find events in epochs ...')
    for idx_e, lat_e in enumerate(lat_out): #loop over all epochs in epochs_out
        
                    
        # find latency of stimuli in pre-stim period using orig events
        pre = [lat  for idx, lat in enumerate(lat_orig) if lat in np.arange(lat_e+dur_pre, lat_e) ] #
        # find latencystimuli in post-stim period
        post = [lat  for idx, lat in enumerate(lat_orig) if lat in np.arange(lat_e+1, lat_e+dur_post) ] #
        # test if pre and post epochs are good
        neighb = np.append(pre, post)
#        print(idx_e, neighb)
       
        
        for idx, lat in enumerate(neighb):
            if lat not in lat_clean: # if neighbouring events in the original set are not part of the cleaned epochs, it means that they are classified as bad
                drop_bad = np.append(drop_bad, [idx_e])
#                print('--------->', idx_e)
        
    drop_bad = np.unique(drop_bad)        
    print(f'drop {len(drop_bad) }bad epochs   ...')
    epochs_out.drop(drop_bad)
   
    
    #### 4 ----  We save
  
    f_epochs_out=os.path.join(dir_imne_erp, '{}.{}-epo.fif'.format(subj, pref_epo_out))
    epochs_out.save(f_epochs_out)   
            
   
 





    
    