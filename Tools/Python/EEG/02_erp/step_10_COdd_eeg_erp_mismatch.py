#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
===============================================
Step 10 - ERP MISMATCH analysis on 2-20 Hz epochs (-200, 410 ms)
===============================================

We conduct a typical mismatch analysis
The aim here is to compute typicla mismatch
and also to validate bad sensors
(they are interpolated for sensor-level analysis)

@author: Françoise Lecaignard, francoise.lecaignard@inserm.fr
"""


###############################################################################
import numpy as np             # convention d'import
import os                       # path.join for file names
import matplotlib.pyplot as plt # from the MNE examples
import warnings
import pickle
import sys
# path to my own functions
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Python/EEG')
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/BIDS/mne-bids')



from mne import Epochs, pick_types, read_events, read_epochs
from mne import write_evokeds, combine_evoked, read_evokeds
from mne import get_config, set_config
type(get_config('MNE_BROWSE_RAW_SIZE'))
set_config('MNE_BROWSE_RAW_SIZE', '20.0,10.0')
from mne.viz import  plot_compare_evokeds

#from fun_NN_template import  ...evokeds_group
from fun_10_erp_mismatch  import Codd_erp_00_big_MMN, Codd_erp_01, Codd_erp_01_1_doubleERP ,Codd_erp_02
from fun_erp_plot  import evokeds_plot,evokeds_plot_all_group, evokeds_plot_two_overlays_with_crop

################################################################################
# Input Parameters 
################################################################################

# Paths ----------------------------
dir_mne = '/sps/cermep/cermep/experiments/DCM/ContextOdd/Ana_MNE'



################################################################################
# Define Constant Variables
################################################################################
#bandwidth
lf, hf = 2, 20
erp_folder = 'erp_{}-{}Hz'.format(lf, hf)


#prefix  2-20 Hz epochs (good epochs, ica)
pref_epo = 'clean_{}-{}Hz'.format(lf, hf)


# events for epochs
#Stimulus coding (five-digit format)
event_id_codd={'var_c/c4/c-/p0/std': 14121, 'var_c/c4/c-/p0/dev': 14122,'var_c/c4/c-/p0/std_oth': 14123,\
              'var_c/c5/c-/p0/std': 15121, 'var_c/c5/c-/p0/dev': 15122,'var_c/c5/c-/p0/std_oth': 15123,\
              'var_c/c6/c-/p0/std': 16121, 'var_c/c6/c-/p0/dev': 16122,'var_c/c6/c-/p0/std_oth': 16123,\
              'var_c/c2/c+/p0/std': 12221, 'var_c/c2/c+/p0/dev': 12222,'var_c/c2/c+/p0/std_oth': 12223,\
              'var_c/c3/c+/p0/std': 13221, 'var_c/c3/c+/p0/dev': 13222,'var_c/c3/c+/p0/std_oth': 13223,\
              'var_c/c4/c+/p0/std': 14221, 'var_c/c4/c+/p0/dev': 14222,'var_c/c4/c+/p0/std_oth': 14223,\
              'var_c/c6/c+/p0/std': 16221, 'var_c/c6/c+/p0/dev': 16222,'var_c/c6/c+/p0/std_oth': 16223,\
              'var_c/c7/c+/p0/std': 17221, 'var_c/c7/c+/p0/dev': 17222,'var_c/c7/c+/p0/std_oth': 17223,\
              'var_c/c8/c+/p0/std': 18221, 'var_c/c8/c+/p0/dev': 18222,'var_c/c8/c+/p0/std_oth': 18223,\
              'var_p/c2/c-/p+/std': 22111, 'var_p/c2/c-/p+/dev': 22112,'var_p/c2/c-/p+/std_oth': 22113,\
              'var_p/c3/c-/p+/std': 23111, 'var_p/c3/c-/p+/dev': 23112,'var_p/c3/c-/p+/std_oth': 23113,\
              'var_p/c4/c-/p+/std': 24111, 'var_p/c4/c-/p+/dev': 24112,'var_p/c4/c-/p+/std_oth': 24113,\
              'var_p/c4/c-/p0/std': 24121, 'var_p/c4/c-/p0/dev': 24122,'var_p/c4/c-/p0/std_oth': 24123,\
              'var_p/c5/c-/p0/std': 25121, 'var_p/c5/c-/p0/dev': 25122,'var_p/c5/c-/p0/std_oth': 25123,\
              'var_p/c6/c-/p0/std': 26121, 'var_p/c6/c-/p0/dev': 26122,'var_p/c6/c-/p0/std_oth': 26123,\
              'var_p/c6/c-/p-/std': 26131, 'var_p/c6/c-/p-/dev': 26132,'var_p/c6/c-/p-/std_oth': 26133,\
              'var_p/c7/c-/p-/std': 27131, 'var_p/c7/c-/p-/dev': 27132,'var_p/c7/c-/p-/std_oth': 27133,\
              'var_p/c8/c-/p-/std': 28131, 'var_p/c8/c-/p-/dev': 28132,'var_p/c8/c-/p-/std_oth': 28133                  }


exclude_eeg = { 'sub-00': ['T7', 'TP9'],	 'sub-01': [],	'sub-02':['CP5', 'P4'],	 'sub-03': [],	'sub-04': ['PO10', 'T7'],\
             'sub-05': [],	 'sub-06': [],	'sub-07':[],	 'sub-08': [],	'sub-09': ['T7'],\
             'sub-10': [],    'sub-11': ['PO9'],   'sub-12':[],  'sub-13': [],    'sub-14': [],\
             'sub-15': [],    'sub-16': [],   'sub-17':[],  'sub-18': [],    'sub-19': ['T8'],\
             'sub-20': ['P3'],    'sub-21': [],   'sub-22':['T7'],	 'sub-23': ['CP1'],    'sub-24': [],\
             'sub-25': ['T8', 'FC2', 'Oz'],    'sub-26': [],   'sub-27':[],	 'sub-28': [],    'sub-29': [],\
             'sub-30': [],    'sub-31': [],   'sub-32':[],	 'sub-33': [],    'sub-34': [],\
             'sub-35': []	}


# Plot 
plot_erp_indiv = None # or 'traces', 'image' or 'topo' 


################################################################################
# Let's start!
################################################################################
################################################################################
# 0) MMN across the experiment
################################################################################
#subj_id_list = np.arange(0,36)
#group_name = 'group'
#
#group_list, group_avg = Codd_erp_00_big_MMN(dir_mne, subj_id_list,erp_folder, pref_epo, group_name, exclude_eeg, baseline,  overwrite_ind = False, overwrite_group=True)
#
#
####  plot group-level ERP
#plot_type= [ 'traces', 'topo']
#plot_list =  ['mmn'] #['std', 'dev' ] 
#evokeds=list()
#for name in plot_list:
#    dir_imne_erp = os.path.join(dir_mne, group_name, erp_folder)
#    f_evo = os.path.join(dir_imne_erp, '{}.{}-ave.fif'.format(group_name, name))
#    evokeds.append(read_evokeds(f_evo, condition=0))
#evokeds_plot(evokeds,  plot_type, neg=True)
#
#### => we see that we have a tiny MMN, and a drift in baseline
#### => let's see at the individual level
#
#plot_list =  [ 'std' , 'dev'] 
#plot_type= [ 'traces']
#subj_id_list = np.arange(0,17)
#evokeds_plot_all_group(dir_mne, erp_folder, subj_id_list, plot_list,   plot_type, neg=True)
#
#################################################################################
## 1) MMN in each condition (var_c, var_p)
#################################################################################
subj_id_list = np.append(np.arange(0,29),np.arange(30,36)) #np.arange(0,36)
group_name = 'group35' # 'group'
baseline = None #(-0.13, 0)
pref_erp = 'blNone' # #'triple_erp_bl200' # for the resulting evoked files 

group_list, group_avg = Codd_erp_01(dir_mne, subj_id_list,erp_folder, pref_epo, group_name, exclude_eeg, baseline,  pref_erp = pref_erp, overwrite_ind = True, overwrite_group=True)


###  plot group-level ERP
plot_type= [ 'traces']

#...............var_c : std / dev ...........................................
plot_list =  [f'{pref_erp}_var_c_std', f'{pref_erp}_var_c_dev' ] #,'var_c_mmn' ] 
col = ['tab:orange', 'tab:purple' ] #, 'tab:red']
evokeds=list()
for name in plot_list:
    dir_imne_erp = os.path.join(dir_mne, group_name, erp_folder)
    f_evo = os.path.join(dir_imne_erp, '{}.{}-ave.fif'.format(group_name, name))
    evokeds.append(read_evokeds(f_evo, condition=0))
evokeds_plot(evokeds, plot_type, neg=True,vline=[ 0], colors=col)

#...............var_p : std / dev ...........................................
plot_list =  [f'{pref_erp}_var_p_std', f'{pref_erp}_var_p_dev' ] #,'var_c_mmn' ] 
col = ['tab:olive', 'tab:green' ] #, 'tab:red']
evokeds=list()
for name in plot_list:
    dir_imne_erp = os.path.join(dir_mne, group_name, erp_folder)
    f_evo = os.path.join(dir_imne_erp, '{}.{}-ave.fif'.format(group_name, name))
    evokeds.append(read_evokeds(f_evo, condition=0))
evokeds_plot(evokeds, plot_type, neg=True,vline=[ 0], colors=col)

#...............std : var_c / var_p ...........................................
plot_list =  [f'{pref_erp}_var_c_std', f'{pref_erp}_var_p_std' ] #,'var_c_mmn' ] 
col = ['tab:orange', 'tab:olive' ] #, 'tab:red']
evokeds=list()
for name in plot_list:
    dir_imne_erp = os.path.join(dir_mne, group_name, erp_folder)
    f_evo = os.path.join(dir_imne_erp, '{}.{}-ave.fif'.format(group_name, name))
    evokeds.append(read_evokeds(f_evo, condition=0))
evokeds_plot(evokeds, plot_type, neg=True,vline=[ 0], colors=col)

#...............dev : var_c / var_p ...........................................
plot_list =  [f'{pref_erp}_var_c_std', f'{pref_erp}_var_p_std' ] #,'var_c_mmn' ] 
col = ['tab:purple', 'tab:green' ] #, 'tab:red']
evokeds=list()
for name in plot_list:
    dir_imne_erp = os.path.join(dir_mne, group_name, erp_folder)
    f_evo = os.path.join(dir_imne_erp, '{}.{}-ave.fif'.format(group_name, name))
    evokeds.append(read_evokeds(f_evo, condition=0))
evokeds_plot(evokeds, plot_type, neg=True,vline=[ 0], colors=col)

#...............mmn : var_c / var_p ...........................................
plot_list =  [f'{pref_erp}_var_c_mmn', f'{pref_erp}_var_p_mmn' ] #,'var_c_mmn' ] 
col = ['tab:red', 'tab:blue' ] #, 'tab:red']
evokeds=list()
for name in plot_list:
    dir_imne_erp = os.path.join(dir_mne, group_name, erp_folder)
    f_evo = os.path.join(dir_imne_erp, '{}.{}-ave.fif'.format(group_name, name))
    evokeds.append(read_evokeds(f_evo, condition=0))
evokeds_plot(evokeds, plot_type, neg=True,vline=[ 0], colors=col)


plot_type= [ 'topo']
plot_list = ['var_c_mmn', 'var_p_mmn'] # ['var_p_std', 'var_p_dev']  #,'var_p_mmn' ] 
evokeds=list()
for name in plot_list:
    dir_imne_erp = os.path.join(dir_mne, group_name, erp_folder)
    f_evo = os.path.join(dir_imne_erp, '{}.{}-ave.fif'.format(group_name, name))
    evokeds.append(read_evokeds(f_evo, condition=0))
evokeds_plot(evokeds, plot_type, neg=True,vline=[ 0], colors=col)



#
#################################################################################
## 1.a - temp) Double ERPs
#################################################################################
# The aim here is to see what's going on during the baseline: is the processing of pre-stimulus still on or achieved?
# For each subject, we compute [STDother STD] and [STD DEV] epochs -810 +410 ms
# Do the epoching (keeping clean epochs from above)
#Compute the evokeds
#compute the grand-average
#prefix Output 2-20 Hz epochs file for double epoching (two consecutive stimuli)

pref_epo_double =  'triple_erp_clean_{}-{}Hz'.format(lf, hf)
pref_double = 'triple_erp_blNone' # #'triple_erp_bl200' # for the resulting evoked files 
erp_list =  ['var_c/std','var_p/std' ]
subj_id_list = np.append(np.arange(0,29),np.arange(30,36)) #np.arange(0,36)
group_name = 'group'
baseline_double = None #(-0.81, -0.61) # (  (-0.81, -0.61)
group_list, group_avg = Codd_erp_01_1_doubleERP(dir_mne, subj_id_list,erp_folder, pref_epo_double, pref_double, group_name, erp_list, 
                                                exclude_eeg, baseline_double,   overwrite_ind = True, overwrite_group=True)
   

###  plot group-level ERP --------------------------------------------------
plot_type= [ 'traces']
plot_list =  [f'{pref_double}_var_c_std', f'{pref_double}_var_p_std' ] #,'var_c_mmn' ] 
#plot_list =  ['var_c_std_double', 'var_p_std_double' ] #,'var_c_mmn' ] 
evokeds=list()
for name in plot_list:
    dir_imne_erp = os.path.join(dir_mne, group_name, erp_folder)
    f_evo = os.path.join(dir_imne_erp, '{}.{}-ave.fif'.format(group_name, name))
    evokeds.append(read_evokeds(f_evo, condition=0))
evokeds_plot(evokeds, plot_type, neg=True, vline=[-0.61, 0, 0.61], colors=['tab:purple', 'tab:olive'])


###  plot group-level ERP --------------------------------------------------

plot_type= [ 'traces']
plot_list_pref=['triple_erp_blNone' ] # 'triple_erp_bl200']
plot_list_cond = [ 'var_c', 'var_p']
plot_list_stim = [ 'std'] #, 'dev']
evokeds=list()
for pref in plot_list_pref:     
    for stim in plot_list_stim:
        
        evokeds=list()
        for cond in plot_list_cond:
            dir_imne_erp = os.path.join(dir_mne, group_name, erp_folder)
            f_evo = os.path.join(dir_imne_erp, f'{group_name}.{pref}_{cond}_{stim}-ave.fif')
            evokeds.append(read_evokeds(f_evo, condition=0))

        evokeds_plot(evokeds, plot_type, neg=True, vline=[-0.61, 0, 0.61], colors=['tab:purple', 'tab:olive'])
        

###  plot individual ERP --------------------------------------------------
#### => let's see at the individual level

plot_type= [ 'traces']
subj_id_list = np.arange(0,36)
plot_list =  [ 'triple_erp_bl200_var_c_std',  'triple_erp_blNone_var_c_std'] 
evokeds_plot_all_group(dir_mne, erp_folder, subj_id_list, plot_list,   plot_type, neg=True, colors=['tab:purple', 'tab:olive' ])

# individual plots, overlay double and single to control (different epochs due to the consecutive rejection constraint in double ERPs)
subj_id_list = np.arange(10,36)
plot_list =  [ 'var_c_dev','var_c_dev_double']  # put the longer as 2nd
tmin, tmax = -0.2, 0.41
for i_su in subj_id_list:
        subj='sub-{:02}'.format(i_su) 
        dir_imne = os.path.join(dir_mne, subj, erp_folder) #individual mne path
        evo_list=list()
        for idx, erp_name  in enumerate (plot_list):
            evo_list.append(os.path.join(dir_imne, '{}.{}-ave.fif'.format(subj, erp_name)))
        
        evokeds=[read_evokeds(f_evo, condition=0) for f_evo in evo_list] # condition =0 because there is one evoked object per file
        
        evokeds_plot_two_overlays_with_crop(evokeds[0], evokeds[1], tmin, tmax , title=subj, neg=True, colors=['tab:purple','tab:red' ])


#################################################################################
## 1.b - temp) Simple ERPs with other pre-stim instead of -200-0 ms
#################################################################################
subj_id_list = np.arange(0,36)
group_name = 'group'
baseline = None #(-0.1, 0)
pref_epo = 'clean_{}-{}Hz_prestim100'.format(lf, hf) # computed in step09
pref_erp = 'prestim100' # output 

group_list, group_avg = Codd_erp_01(dir_mne, subj_id_list,erp_folder, pref_epo, group_name, exclude_eeg, baseline, pref_erp = pref_erp, overwrite_ind = True, overwrite_group=True)


###  plot group-level ERP
plot_type= [ 'traces']

plot_list =  [f'{pref_erp}_var_c_std', f'{pref_erp}_var_c_dev' , f'{pref_erp}_var_c_mmn' ] 
col = ['tab:orange', 'tab:purple' , 'tab:red']
evokeds=list()
for name in plot_list:
    dir_imne_erp = os.path.join(dir_mne, group_name, erp_folder)
    f_evo = os.path.join(dir_imne_erp, '{}.{}-ave.fif'.format(group_name, name))
    evokeds.append(read_evokeds(f_evo, condition=0))
evokeds_plot(evokeds, plot_type, neg=True,vline=[ 0], colors=col)

plot_list =  [f'{pref_erp}_var_p_std', f'{pref_erp}_var_p_dev', f'{pref_erp}_var_p_mmn' ] 
col = ['tab:green', 'tab:olive' , 'tab:blue']
evokeds=list()
for name in plot_list:
    dir_imne_erp = os.path.join(dir_mne, group_name, erp_folder)
    f_evo = os.path.join(dir_imne_erp, '{}.{}-ave.fif'.format(group_name, name))
    evokeds.append(read_evokeds(f_evo, condition=0))
evokeds_plot(evokeds, plot_type, neg=True,vline=[ 0], colors=col)

plot_list =  [f'{pref_erp}_var_p_mmn', f'{pref_erp}_var_c_mmn'] # ,'var_p_mmn' ] 
plot_type= [ 'topo']
evokeds=list()
for name in plot_list:
    dir_imne_erp = os.path.join(dir_mne, group_name, erp_folder)
    f_evo = os.path.join(dir_imne_erp, '{}.{}-ave.fif'.format(group_name, name))
    evokeds.append(read_evokeds(f_evo, condition=0))
evokeds_plot(evokeds, plot_type, neg=True,vline=[ 0], colors=col)

plot_list =  [f'{pref_erp}_var_c_dev', f'{pref_erp}_var_p_dev' ] #'var_c_mmn' ] 
col = ['tab:orange', 'tab:green' ] #, 'tab:red']
evokeds=list()
for name in plot_list:
    dir_imne_erp = os.path.join(dir_mne, group_name, erp_folder)
    f_evo = os.path.join(dir_imne_erp, '{}.{}-ave.fif'.format(group_name, name))
    evokeds.append(read_evokeds(f_evo, condition=0))
evokeds_plot(evokeds, plot_type, neg=True,vline=[ 0], colors=col)


################################################################################
# 2) Within var_p: contrast p+ / p0 / p-
################################################################################
subj_id_list = np.arange(0,36)
group_name = 'group'
baseline = None #(-0.1, 0)
pref_epo = 'clean_{}-{}Hz_prestim100'.format(lf, hf) # computed in step09
pref_erp = 'prestim100' # output 

group_list, group_avg = Codd_erp_02(dir_mne, subj_id_list,erp_folder, pref_epo, group_name, exclude_eeg, baseline, pref_erp = pref_erp, overwrite_ind = True, overwrite_group=True)


plot_type= [ 'traces']
p='p-'
plot_list =  [f'{pref_erp}_var_p_{p}_std', f'{pref_erp}_var_p_{p}_dev' , f'{pref_erp}_var_p_{p}_mmn' ] 
col = ['tab:orange', 'tab:purple' , 'tab:red']
evokeds=list()
for name in plot_list:
    dir_imne_erp = os.path.join(dir_mne, group_name, erp_folder)
    f_evo = os.path.join(dir_imne_erp, '{}.{}-ave.fif'.format(group_name, name))
    evokeds.append(read_evokeds(f_evo, condition=0))
evokeds_plot(evokeds, plot_type, neg=True,vline=[ 0], colors=col)
    
plot_type= [ 'traces']
stim='mmn'
plot_list =  [f'{pref_erp}_var_p_p+_{stim}', f'{pref_erp}_var_p_p0_{stim}' , f'{pref_erp}_var_p_p-_{stim}' ] 
col = ['tab:orange', 'tab:purple' , 'tab:red']
evokeds=list()
for name in plot_list:
    dir_imne_erp = os.path.join(dir_mne, group_name, erp_folder)
    f_evo = os.path.join(dir_imne_erp, '{}.{}-ave.fif'.format(group_name, name))
    evokeds.append(read_evokeds(f_evo, condition=0))
evokeds_plot(evokeds, plot_type, neg=True,vline=[ 0], colors=col)

plot_type= [ 'topo']
stim='mmn'
plot_list =  [f'{pref_erp}_var_p_p+_{stim}', f'{pref_erp}_var_p_p0_{stim}' , f'{pref_erp}_var_p_p-_{stim}' ] 
col = ['tab:orange', 'tab:purple' , 'tab:red']
evokeds=list()
for name in plot_list:
    dir_imne_erp = os.path.join(dir_mne, group_name, erp_folder)
    f_evo = os.path.join(dir_imne_erp, '{}.{}-ave.fif'.format(group_name, name))
    evokeds.append(read_evokeds(f_evo, condition=0))
evokeds_plot(evokeds, plot_type, neg=True,vline=[ 0], colors=col)
