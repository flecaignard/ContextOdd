#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
============================================================================
Step 10 BIS -   ERP MISMATCH analysis on 2-20 Hz epochs (-200, 410 ms)
                Time-Frequency analysis to better understand pre-stimulus activity
============================================================================

The aim here is to compute time-frequency plots of standard-deviant epochs (consecutive stimuli)
===> to clarify the slow component occuring before stimilus onset
===> to further identify bad subjects if any

@author: Françoise Lecaignard, francoise.lecaignard@inserm.fr
"""


###############################################################################
import numpy as np             # convention d'import
import os                       # path.join for file names
import matplotlib.pyplot as plt # from the MNE examples
import warnings
import pickle
import sys
# path to my own functions
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Python/EEG')
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/BIDS/mne-bids')



from mne import Epochs, pick_types, read_events, read_epochs
from mne import write_evokeds, combine_evoked, read_evokeds
from mne import get_config, set_config
type(get_config('MNE_BROWSE_RAW_SIZE'))
set_config('MNE_BROWSE_RAW_SIZE', '20.0,10.0')
from mne.viz import  plot_compare_evokeds, tight_layout
from mne.time_frequency import tfr_morlet
#from fun_NN_template import  ...evokeds_group
from fun_10_erp_mismatch  import Codd_erp_00_big_MMN, Codd_erp_01, Codd_erp_01_1_doubleERP ,Codd_erp_02
from fun_erp_plot  import evokeds_plot,evokeds_plot_all_group, evokeds_plot_two_overlays_with_crop

################################################################################
# Input Parameters 
################################################################################

# Paths ----------------------------
dir_mne = '/sps/cermep/cermep/experiments/DCM/ContextOdd/Ana_MNE'



################################################################################
# Define Constant Variables
################################################################################
#bandwidth
lf, hf = 2, 20
erp_folder = 'erp_{}-{}Hz'.format(lf, hf)


#prefix  2-20 Hz epochs (good epochs, ica)
pref_epo = 'clean_{}-{}Hz'.format(lf, hf)


# events for epochs
#Stimulus coding (five-digit format)
event_id_codd={'var_c/c4/c-/p0/std': 14121, 'var_c/c4/c-/p0/dev': 14122,'var_c/c4/c-/p0/std_oth': 14123,\
              'var_c/c5/c-/p0/std': 15121, 'var_c/c5/c-/p0/dev': 15122,'var_c/c5/c-/p0/std_oth': 15123,\
              'var_c/c6/c-/p0/std': 16121, 'var_c/c6/c-/p0/dev': 16122,'var_c/c6/c-/p0/std_oth': 16123,\
              'var_c/c2/c+/p0/std': 12221, 'var_c/c2/c+/p0/dev': 12222,'var_c/c2/c+/p0/std_oth': 12223,\
              'var_c/c3/c+/p0/std': 13221, 'var_c/c3/c+/p0/dev': 13222,'var_c/c3/c+/p0/std_oth': 13223,\
              'var_c/c4/c+/p0/std': 14221, 'var_c/c4/c+/p0/dev': 14222,'var_c/c4/c+/p0/std_oth': 14223,\
              'var_c/c6/c+/p0/std': 16221, 'var_c/c6/c+/p0/dev': 16222,'var_c/c6/c+/p0/std_oth': 16223,\
              'var_c/c7/c+/p0/std': 17221, 'var_c/c7/c+/p0/dev': 17222,'var_c/c7/c+/p0/std_oth': 17223,\
              'var_c/c8/c+/p0/std': 18221, 'var_c/c8/c+/p0/dev': 18222,'var_c/c8/c+/p0/std_oth': 18223,\
              'var_p/c2/c-/p+/std': 22111, 'var_p/c2/c-/p+/dev': 22112,'var_p/c2/c-/p+/std_oth': 22113,\
              'var_p/c3/c-/p+/std': 23111, 'var_p/c3/c-/p+/dev': 23112,'var_p/c3/c-/p+/std_oth': 23113,\
              'var_p/c4/c-/p+/std': 24111, 'var_p/c4/c-/p+/dev': 24112,'var_p/c4/c-/p+/std_oth': 24113,\
              'var_p/c4/c-/p0/std': 24121, 'var_p/c4/c-/p0/dev': 24122,'var_p/c4/c-/p0/std_oth': 24123,\
              'var_p/c5/c-/p0/std': 25121, 'var_p/c5/c-/p0/dev': 25122,'var_p/c5/c-/p0/std_oth': 25123,\
              'var_p/c6/c-/p0/std': 26121, 'var_p/c6/c-/p0/dev': 26122,'var_p/c6/c-/p0/std_oth': 26123,\
              'var_p/c6/c-/p-/std': 26131, 'var_p/c6/c-/p-/dev': 26132,'var_p/c6/c-/p-/std_oth': 26133,\
              'var_p/c7/c-/p-/std': 27131, 'var_p/c7/c-/p-/dev': 27132,'var_p/c7/c-/p-/std_oth': 27133,\
              'var_p/c8/c-/p-/std': 28131, 'var_p/c8/c-/p-/dev': 28132,'var_p/c8/c-/p-/std_oth': 28133                  }


exclude_eeg = { 'sub-00': ['T7', 'TP9'],	 'sub-01': [],	'sub-02':['CP5', 'P4'],	 'sub-03': [],	'sub-04': ['PO10', 'T7'],\
             'sub-05': [],	 'sub-06': [],	'sub-07':[],	 'sub-08': [],	'sub-09': ['T7'],\
             'sub-10': [],    'sub-11': ['PO9'],   'sub-12':[],  'sub-13': [],    'sub-14': [],\
             'sub-15': [],    'sub-16': [],   'sub-17':[],  'sub-18': [],    'sub-19': ['T8'],\
             'sub-20': ['P3'],    'sub-21': [],   'sub-22':['T7'],	 'sub-23': ['CP1'],    'sub-24': [],\
             'sub-25': ['T8', 'FC2', 'Oz'],    'sub-26': [],   'sub-27':[],	 'sub-28': [],    'sub-29': [],\
             'sub-30': [],    'sub-31': [],   'sub-32':[],	 'sub-33': [],    'sub-34': [],\
             'sub-35': []	}



################################################################################
# Let's start!
################################################################################
################################################################################
# Working with epochs [ STD STD DEV] in 2-20 Hz- triple ERPs
################################################################################
pref_epo_double =  'triple_erp_clean_{}-{}Hz'.format(lf, hf)
pref_double = 'triple_erp_blNone' # #'triple_erp_bl200' # for the resulting evoked files 
erp_list =  ['var_c/std','var_p/std' ]
subj_id_list = np.arange(28,31)
group_name = 'group'
freqs = np.logspace(*np.log10([1, 20]), num=8)
n_cycles = freqs / 2.  # different number of cycle per frequency
baseline = (-0.81, -0.61) #None

for i_su in subj_id_list:
    subj='sub-{:02}'.format(i_su) 
    dir_imne_erp = os.path.join(dir_mne, subj, erp_folder) #individual mne path
    
    f_epochs=os.path.join(dir_imne_erp, '{}.{}-epo.fif'.format(subj, pref_epo_double))
    epochs = read_epochs(f_epochs) 
    picks = pick_types(epochs.info, eeg=True, stim=True,exclude=())
    power, itc = tfr_morlet(epochs, freqs=freqs, n_cycles=n_cycles, use_fft=True,
                        return_itc=True, decim=3, n_jobs=1)
    
    power.plot_topo( baseline=baseline, mode='logratio', title='Average power', vmin =-0.1, vmax=0.1 )
#    power.plot([11], baseline=baseline, mode='logratio', title=power.ch_names[11])
#
#    power.plot_topomap(ch_type='eeg',  fmin=18, fmax=22,
#                   baseline=baseline, mode='logratio')
#
##    tight_layout()
#    plt.show()

#power.plot_joint(baseline=(-0.5, 0), mode='mean', tmin=-.5, tmax=2,
#                 timefreqs=[(.5, 10), (1.3, 8)])
