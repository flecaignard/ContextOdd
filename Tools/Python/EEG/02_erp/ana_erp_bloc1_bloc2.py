#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
====================================================================
Compute typical MMN analysis 
Here we compare first bloc VS. second one, in var_p and var_c (separately)
We could expect no difference in var_p, and a  decrease of the MMN in bloc2 in var_c 
<=> the uninformative nature of c+/ c- variation has been learnt

may2019 (tests for HBM Roma poster)
====================================================================
This analysis is a typical deviant-minus-standard analysis

We work here in the 2-20 Hz bandwith 
We don't use baseline correction

We need to assign bloc code f (1 or 2) for each stim 
       
       
    @author: Françoise Lecaignard, francoise.lecaignard@inserm.fr
"""


###############################################################################
import numpy as np             # convention d'import
import os                       # path.join for file names
import matplotlib.pyplot as plt # from the MNE examples
import pickle
import sys
# path to my own functions
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Python/EEG')
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Python/EEG/02_erp')
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Python/EEG/01_preprocessings')



from mne import Epochs, pick_types, read_events, read_epochs, find_layout
from mne import write_evokeds, read_evokeds
from mne import get_config, set_config
type(get_config('MNE_BROWSE_RAW_SIZE'))
set_config('MNE_BROWSE_RAW_SIZE', '20.0,10.0')
from mne.preprocessing import read_ica
from mne.viz import plot_topomap,plot_compare_evokeds, tight_layout

from mne.io import read_raw_fif

#from fun_NN_template import  ...
from fun_util_epoch import import_epochs
from fun_plot_erp import evokeds_plot, evokeds_plot_all_group
from fun_util_erp import evokeds_group_average,  evokeds_indiv_diff
from fun_util_eeg import print_step_label
from fun_stats_erp import view_significant_spatiotemp_clusters
from fun_codd_stats_erp import codd_spatiotemp_clusters_1samp
from fun_02_COdd_eeg_events import codd_eeg_rename_events_bloc1_bloc2
################################################################################
# Input Parameters 
################################################################################

# Paths ---------------------------- ---------------------------- ----------------------------
dir_mne = '/sps/cermep/cermep/experiments/DCM/ContextOdd/Ana_MNE'

erp_folder = 'erp_2-20Hz'
raw_folder = 'preproc'


# epochs 2-20 Hz  ---------------------------- ---------------------
pref_continuous = 'preproc_2-20Hz_ica' 

pref_epo_in = 'clean_2-20Hz' #-epo.fif, cleaned epochs from preprocs
pref_epo_out = 'clean_2-20Hz_6d' #-epo.fif, resulting cleaned epochs, being equal to pref_epo_in with 6-digit coding


# epochs bounds
tmin, tmax = -0.2, 0.41 

# events for epochs

event_id = {'b1/var_c/c4/c-/p0/std': 114121, 'b1/var_c/c4/c-/p0/dev': 114122,'b1/var_c/c4/c-/p0/std_oth': 114123,\
              'b1/var_c/c5/c-/p0/std': 115121, 'b1/var_c/c5/c-/p0/dev': 115122,'b1/var_c/c5/c-/p0/std_oth': 115123,\
              'b1/var_c/c6/c-/p0/std': 116121, 'b1/var_c/c6/c-/p0/dev': 116122,'b1/var_c/c6/c-/p0/std_oth': 116123,\
              'b1/var_c/c2/c+/p0/std': 112221, 'b1/var_c/c2/c+/p0/dev': 112222,'b1/var_c/c2/c+/p0/std_oth': 112223,\
              'b1/var_c/c3/c+/p0/std': 113221, 'b1/var_c/c3/c+/p0/dev': 113222,'b1/var_c/c3/c+/p0/std_oth': 113223,\
              'b1/var_c/c4/c+/p0/std': 114221, 'b1/var_c/c4/c+/p0/dev': 114222,'b1/var_c/c4/c+/p0/std_oth': 114223,\
              'b1/var_c/c6/c+/p0/std': 116221, 'b1/var_c/c6/c+/p0/dev': 116222,'b1/var_c/c6/c+/p0/std_oth': 116223,\
              'b1/var_c/c7/c+/p0/std': 117221, 'b1/var_c/c7/c+/p0/dev': 117222,'b1/var_c/c7/c+/p0/std_oth': 117223,\
              'b1/var_c/c8/c+/p0/std': 118221, 'b1/var_c/c8/c+/p0/dev': 118222,'b1/var_c/c8/c+/p0/std_oth': 118223,\
              'b1/var_p/c2/c-/p+/std': 122111, 'b1/var_p/c2/c-/p+/dev': 122112,'b1/var_p/c2/c-/p+/std_oth': 122113,\
              'b1/var_p/c3/c-/p+/std': 123111, 'b1/var_p/c3/c-/p+/dev': 123112,'b1/var_p/c3/c-/p+/std_oth': 123113,\
              'b1/var_p/c4/c-/p+/std': 124111, 'b1/var_p/c4/c-/p+/dev': 124112,'b1/var_p/c4/c-/p+/std_oth': 124113,\
              'b1/var_p/c4/c-/p0/std': 124121, 'b1/var_p/c4/c-/p0/dev': 124122,'b1/var_p/c4/c-/p0/std_oth': 124123,\
              'b1/var_p/c5/c-/p0/std': 125121, 'b1/var_p/c5/c-/p0/dev': 125122,'b1/var_p/c5/c-/p0/std_oth': 125123,\
              'b1/var_p/c6/c-/p0/std': 126121, 'b1/var_p/c6/c-/p0/dev': 126122,'b1/var_p/c6/c-/p0/std_oth': 126123,\
              'b1/var_p/c6/c-/p-/std': 126131, 'b1/var_p/c6/c-/p-/dev': 126132,'b1/var_p/c6/c-/p-/std_oth': 126133,\
              'b1/var_p/c7/c-/p-/std': 127131, 'b1/var_p/c7/c-/p-/dev': 127132,'b1/var_p/c7/c-/p-/std_oth': 127133,\
              'b1/var_p/c8/c-/p-/std': 128131, 'b1/var_p/c8/c-/p-/dev': 128132,'b1/var_p/c8/c-/p-/std_oth': 128133, \
              'b2/var_c/c4/c-/p0/std': 214121, 'b2/var_c/c4/c-/p0/dev': 214122,'b2/var_c/c4/c-/p0/std_oth': 214123,\
              'b2/var_c/c5/c-/p0/std': 215121, 'b2/var_c/c5/c-/p0/dev': 215122,'b2/var_c/c5/c-/p0/std_oth': 215123,\
              'b2/var_c/c6/c-/p0/std': 216121, 'b2/var_c/c6/c-/p0/dev': 216122,'b2/var_c/c6/c-/p0/std_oth': 216123,\
              'b2/var_c/c2/c+/p0/std': 212221, 'b2/var_c/c2/c+/p0/dev': 212222,'b2/var_c/c2/c+/p0/std_oth': 212223,\
              'b2/var_c/c3/c+/p0/std': 213221, 'b2/var_c/c3/c+/p0/dev': 213222,'b2/var_c/c3/c+/p0/std_oth': 213223,\
              'b2/var_c/c4/c+/p0/std': 214221, 'b2/var_c/c4/c+/p0/dev': 214222,'b2/var_c/c4/c+/p0/std_oth': 214223,\
              'b2/var_c/c6/c+/p0/std': 216221, 'b2/var_c/c6/c+/p0/dev': 216222,'b2/var_c/c6/c+/p0/std_oth': 216223,\
              'b2/var_c/c7/c+/p0/std': 217221, 'b2/var_c/c7/c+/p0/dev': 217222,'b2/var_c/c7/c+/p0/std_oth': 217223,\
              'b2/var_c/c8/c+/p0/std': 218221, 'b2/var_c/c8/c+/p0/dev': 218222,'b2/var_c/c8/c+/p0/std_oth': 218223,\
              'b2/var_p/c2/c-/p+/std': 222111, 'b2/var_p/c2/c-/p+/dev': 222112,'b2/var_p/c2/c-/p+/std_oth': 222113,\
              'b2/var_p/c3/c-/p+/std': 223111, 'b2/var_p/c3/c-/p+/dev': 223112,'b2/var_p/c3/c-/p+/std_oth': 223113,\
              'b2/var_p/c4/c-/p+/std': 224111, 'b2/var_p/c4/c-/p+/dev': 224112,'b2/var_p/c4/c-/p+/std_oth': 224113,\
              'b2/var_p/c4/c-/p0/std': 224121, 'b2/var_p/c4/c-/p0/dev': 224122,'b2/var_p/c4/c-/p0/std_oth': 224123,\
              'b2/var_p/c5/c-/p0/std': 225121, 'b2/var_p/c5/c-/p0/dev': 225122,'b2/var_p/c5/c-/p0/std_oth': 225123,\
              'b2/var_p/c6/c-/p0/std': 226121, 'b2/var_p/c6/c-/p0/dev': 226122,'b2/var_p/c6/c-/p0/std_oth': 226123,\
              'b2/var_p/c6/c-/p-/std': 226131, 'b2/var_p/c6/c-/p-/dev': 226132,'b2/var_p/c6/c-/p-/std_oth': 226133,\
              'b2/var_p/c7/c-/p-/std': 227131, 'b2/var_p/c7/c-/p-/dev': 227132,'b2/var_p/c7/c-/p-/std_oth': 227133,\
              'b2/var_p/c8/c-/p-/std': 228131, 'b2/var_p/c8/c-/p-/dev': 228132,'b2/var_p/c8/c-/p-/std_oth': 228133       }

# events file (renamed and downsampled events)
pref_eve_orig = 'preproc.5d'
pref_eve_out = 'preproc.6d'

# Evoked responses   ---------------------------- ---------------------

exclude_eeg = { 'sub-00': ['T7', 'TP9'],	 'sub-01': [],	'sub-02':['TP10', 'CP5', 'P4'],	 'sub-03': ['Oz'],	'sub-04': ['T7', 'T8'],\
             'sub-05': [],	 'sub-06': [],	'sub-07':[],	 'sub-08': ['O1', 'O2'],	'sub-09': ['T7', 'T8'],\
             'sub-10': [],    'sub-11': ['PO9'],   'sub-12':[],  'sub-13': ['T7', 'PO10'],    'sub-14': ['P3'],\
             'sub-15': ['T8'],    'sub-16': ['Oz'],   'sub-17':['PO9'],  'sub-18': [],    'sub-19': ['T7', 'T8', 'TP10'],\
             'sub-20': [],    'sub-21': [],   'sub-22':['P3'],	 'sub-23': ['CP1'],    'sub-24': [],\
             'sub-25': ['Oz', 'T8', 'TP9'],    'sub-26': [],   'sub-27':[],	 'sub-28': [],    'sub-29': ['CP5'],\
             'sub-30': ['T8', 'PO10'],    'sub-31': [],   'sub-32':['Oz'],	 'sub-33': ['T8'],    'sub-34': ['T8', 'T7'],\
             'sub-35': []	}

# compute ERPs for std, dev and compute difference too:
erp_compute_dict = {    'b1/var_c/mmn': ['b1/var_c/dev','b1/var_c/std'], # dev first, std second because we do : (1) dev -  (2) std 
                        'b2/var_c/mmn': ['b2/var_c/dev','b2/var_c/std'], # dev first, std second because we do : (1) dev -  (2) std 
                        'b1/var_p/mmn': ['b1/var_p/dev','b1/var_p/std'], # dev first, std second because we do : (1) dev -  (2) std 
                        'b2/var_p/mmn': ['b2/var_p/dev','b2/var_p/std'], # dev first, std second because we do : (1) dev -  (2) std 
                        
                        
                        }

pref_erp = '2-20Hz'  # ===> sub-00.2-20Hz_var_c_c-_dev-ave.fif 

# Plot options   ---------------------------- ---------------------

color_dict={'2-20Hz_var_c_c-_std':'xkcd:green',\
            '2-20Hz_var_c_c-_dev':'xkcd:violet blue',\
            '2-20Hz_var_c_c-_mmn':'xkcd:wine',\
            '2-20Hz_var_c_c+_std':'xkcd:vivid green',\
            '2-20Hz_var_c_c+_dev':'xkcd:vivid purple',\
            '2-20Hz_var_c_c+_mmn':'xkcd:orange red' ,\
            '2-20Hz_var_c_std':'xkcd:black',\
            '2-20Hz_var_c_dev':'xkcd:black',\
            '2-20Hz_var_c_mmn':'xkcd:black' ,\
            }

################################################################################
# Let's start!
################################################################################


#%% 
#################################################################################
## Recode events : add bloc session (1 /2) - 6 digit codes
#################################################################################
# Three atypical subjects
# sub-04 : start code of bloc2 is missing
# sub-16 : 5 blocs, bloc1 is irrelevant; bloc2: first sounds are irrelevant
# sub-19: 5 blocs; bloc1 is irrelevant



#subj_id_list = np.arange(0,36) # 0 to 35, to include sub-00 to sub-35
#
#
#for i_su in subj_id_list:
#    subj='sub-{:02}'.format(i_su) 
#    dir_imne_erp = os.path.join(dir_mne, subj, erp_folder) #individual mne path
#    dir_imne = os.path.join(dir_mne, subj, 'preproc') #individual mne path
#
#
#    # we open events from acquisition (renamed, downsampled) 
#    f_events_5d = os.path.join(dir_imne, '{}.{}-eve.fif'.format(subj, pref_eve_orig)) 
#    events=read_events(f_events_5d)
#    
#    f_events_6d = os.path.join(dir_imne, '{}.{}-eve.fif'.format(subj, pref_eve_out)) 
#    codd_eeg_rename_events_bloc1_bloc2(subj, f_events_5d,f_events_6d, plot=False)
#    print('')

#%% 
#################################################################################
## Compute epochs 
#################################################################################
# subj_id_list -----------------------
# applyto all
subj_id_list = np.arange(0,36) # 0 to 35, to include sub-00 to sub-35


for i_su in subj_id_list:
    
    subj='sub-{:02}'.format(i_su) 
    dir_imne_preproc = os.path.join(dir_mne, subj, raw_folder) #individual mne path
    dir_imne_erp = os.path.join(dir_mne, subj, erp_folder) #individual mne path
    f_epochs_out=os.path.join(dir_imne_erp, '{}.{}-epo.fif'.format(subj, pref_epo_out))
    
    if os.path.exists(f_epochs_out):
        print(f'------------------------------------------- {subj}: file {pref_epo_out} already exists')
    else:
        #### 2 ----- Load clean single epochs 
        f_epochs_in = os.path.join(dir_imne_preproc, '{}.{}-epo.fif'.format(subj, pref_epo_in))
        epochs_in = read_epochs(f_epochs_in)    
        
        #### 3 ----- Import epochs from 5d-epoching , and change event_id 
        epochs_out = epochs_in.copy() # selected events, with 5d codes
        # selected events
        f_events_6d = os.path.join(dir_imne_preproc, '{}.{}-eve.fif'.format(subj, pref_eve_out)) 
        events=read_events(f_events_6d) # all events, without rejection etc
        [lat, ind_5d, ind_6d] = np.intersect1d(epochs_out.events[:,0], events[:,0], return_indices=True)
        epochs_out.events[ind_5d,2] = events[ind_6d,2]
        epochs_out.event_id = event_id
        
    #    epochs_out.info['bads']=exclude_eeg[subj]
    
        
        #### 4 ----  We save
        f_epochs_out=os.path.join(dir_imne_erp, '{}.{}-epo.fif'.format(subj, pref_epo_out))
        epochs_out.save(f_epochs_out)   
     
        
         
#%% 
#################################################################################
## Compute individual ERPs (evoked object)
#################################################################################

subj_id_list = np.arange(0,36) # 0 to 35, to include sub-00 to sub-35
baseline = None

overwrite_ind = True

for i_su in subj_id_list:
    
    subj='sub-{:02}'.format(i_su) 
    dir_imne_erp = os.path.join(dir_mne, subj, erp_folder) #individual mne path
    dir_imne = os.path.join(dir_mne, subj, 'preproc') #individual mne path
    
    
    if overwrite_ind: # we  compute individual ERPs (std, dev ad difference in var_p, var_c)  
    
    #### ---- Load clean epochs (2-20 Hz)
        f_epochs=os.path.join(dir_imne_erp, '{}.{}-epo.fif'.format(subj, pref_epo_out))
        epochs = read_epochs(f_epochs) 
        epochs.info['bads']=exclude_eeg[subj]
        epochs.interpolate_bads()
        
        for key, value   in erp_compute_dict.items():
            #### Compute std, dev and mmn ERPs, and save *-ave.fif ----
            # erp_list = [dev, std ] # 1 - 2 <=> dev - std 
            output_name = key
            erp_list = value.copy() # this is beacuse of the erp_list.append below, otherwise it adds the append thing in the dictionary too (???)
            print('-----------------')
            print(output_name, erp_list)
            print('-----------------')
            evokeds = evokeds_indiv_diff(dir_imne_erp, subj,epochs, baseline, erp_list, output_name,  neg = False, plot = None)
            erp_list.append(output_name)
            pref_evo_list=list()
            for idx, erp_name  in enumerate (erp_list):
                new_name = erp_name.replace('/', '_')
                print(new_name)
                if pref_erp:
                    new_name = f'{pref_erp}_{new_name}'
                f_out = os.path.join(dir_imne_erp, '{}.{}-ave.fif'.format(subj, new_name))
                write_evokeds(f_out, evokeds[idx])
                pref_evo_list.append(new_name)
        

  


#%%
##################################################################################
### Compute group-average ERPs
##################################################################################
subj_group_avg = np.arange(0,36) # 0 to 35, to include sub-00 to sub-35
group_name = 'group36'

overwrite_group = True
erp_list = [ 'b1/var_c/dev','b1/var_c/std', 'b1/var_c/mmn',
            'b2/var_c/dev','b2/var_c/std', 'b2/var_c/mmn',
            'b1/var_p/dev','b1/var_p/std', 'b1/var_p/mmn',
            'b2/var_p/dev','b2/var_p/std', 'b2/var_p/mmn' ]


for erp_name in erp_list:
    pref_evoked = erp_name.replace('/', '_')
    if pref_erp:
        pref_evoked = f'{pref_erp}_{pref_evoked}'
    if overwrite_group:
            group_list, group_avg = evokeds_group_average(dir_mne, erp_folder, subj_group_avg , group_name,  pref_evoked,  save= True)
    else:
            group_list, group_avg = evokeds_group_average(dir_mne, erp_folder, subj_group_avg, group_name , pref_evoked,  save= False)

 
#%%
##################################################################################
### Plot group-average ERPs
##################################################################################
#group_name = 'group36'
#plot_type= [ 'traces']
#
#
#
#plot_list =  [ '2-20Hz_var_c_c-_dev','2-20Hz_var_c_c-_std', '2-20Hz_var_c_c-_mmn'] 
##plot_list =  [ '2-20Hz_var_c_c+_dev','2-20Hz_var_p_p0_std', '2-20Hz_var_p_p0_mmn']
##plot_list =  [ '2-20Hz_var_p_p0_dev','2-20Hz_var_c_c+_std', '2-20Hz_var_c_c+_mmn' ]
#plot_list =  [ '2-20Hz_var_c_c-_std', '2-20Hz_var_c_c+_std'] 
#plot_list =  [ '2-20Hz_var_c_c-_dev', '2-20Hz_var_c_c+_dev'] 
##plot_list =  [ '2-20Hz_var_c_c-_mmn', '2-20Hz_var_c_c+_mmn'] 
##plot_list =  [ '2-20Hz_var_c_c-_mmn', '2-20Hz_var_p_p0_mmn', '2-20Hz_var_c_c+_mmn', '2-20Hz_var_p_mmn'] 
##plot_list =  [ '2-20Hz_var_c_c-_std', '2-20Hz_var_p_p0_std', '2-20Hz_var_c_c+_std'] #, '2-20Hz_var_p_std'] 
##plot_list =  [ '2-20Hz_var_c_c-_dev',  '2-20Hz_var_p_p0_dev', '2-20Hz_var_c_c+_dev', '2-20Hz_var_p_dev'] 
#
#colors = []
#evokeds=list()
#for name in plot_list:
#    dir_imne_erp = os.path.join(dir_mne, group_name, erp_folder)
#    f_evo = os.path.join(dir_imne_erp, '{}.{}-ave.fif'.format(group_name, name))
#    evokeds.append(read_evokeds(f_evo, condition=0))
#    colors.append(color_dict[name])
#evokeds_plot(evokeds, plot_type, neg=True,vline=[ 0], colors=colors)
# 


#%%
##################################################################################
### Export Elan, stats
##################################################################################
import scipy.io as sio 
elan_folder = 'stats_elan' # within erp_folder

export_list = [ '2-20Hz_b1_var_c_std',  '2-20Hz_b1_var_c_dev',  '2-20Hz_b1_var_c_mmn', 
               '2-20Hz_b2_var_c_std',  '2-20Hz_b2_var_c_dev',  '2-20Hz_b2_var_c_mmn', 
               '2-20Hz_b1_var_p_std',  '2-20Hz_b1_var_p_dev',  '2-20Hz_b1_var_p_mmn', 
               '2-20Hz_b2_var_p_std',  '2-20Hz_b2_var_p_dev',  '2-20Hz_b2_var_p_mmn', 
               ] 

pref_erp = '2-20Hz'  # ===> sub-00.2-20Hz_var_p_dev-ave.fif 

# Export *-ave.fif in *.mat files


subj_id_list = np.arange(0,36) # 0 to 35, to include sub-00 to sub-35


for i_su in subj_id_list:
    subj='sub-{:02}'.format(i_su) 
    dir_imne = os.path.join(dir_mne, subj, erp_folder) #individual mne path
    dir_ielan = os.path.join(dir_imne, elan_folder) 
    if not os.path.exists(dir_ielan):
        os.makedirs(dir_ielan)

    for name in export_list:    
        f_evo = os.path.join(dir_imne, '{}.{}-ave.fif'.format(subj, name))
        evoked=read_evokeds(f_evo, condition=0)
        matinfo = {'d':evoked.data, \
                   'sens': evoked.ch_names, \
                   'times': evoked.times, \
                   'sfreq': evoked.info['sfreq']  }
        f_mat = os.path.join(dir_ielan, '{}.{}.mat'.format(subj, name))
        sio.savemat(f_mat, matinfo)
       
        
# export group avg
subj='group36'
dir_imne = os.path.join(dir_mne, subj, erp_folder) #individual mne path
dir_ielan = os.path.join(dir_imne, elan_folder) 
if not os.path.exists(dir_ielan):
    os.makedirs(dir_ielan)

for name in export_list:    
    f_evo = os.path.join(dir_imne, '{}.{}-ave.fif'.format(subj, name))
    evoked=read_evokeds(f_evo, condition=0)
    matinfo = {'d':evoked.data, \
               'sens': evoked.ch_names, \
               'times': evoked.times, \
               'sfreq': evoked.info['sfreq']  }
    f_mat = os.path.join(dir_ielan, '{}.{}.mat'.format(subj, name))
    sio.savemat(f_mat, matinfo)        


#%%                
#################################################################################
## Stats : group-level analysis; spatio-temporal clusters
#################################################################################
# non-parametric Paired t-test, using spatio-temporal connectivity
# We build a data matrix X of size (n_subj, n_samples, n_sensors)
# Paired-data : we compute the difference X = A-B, and perform a t-test with the difference

# ......... 1) Prepare folder  stats      (common to all stat analysis)  ...................................
# .............................................................................................
group_name = 'group36'
dir_imne_erp = os.path.join(dir_mne, group_name, erp_folder)
stat_folder = 'stats'
dir_imne_stats = os.path.join(dir_imne_erp, stat_folder)

if not os.path.exists(dir_imne_stats):
    os.makedirs(dir_imne_stats)

# ......... 2) Define default parameters      (common to all stat analysis)  ...................................
# .............................................................................................
subj_id_list = np.arange(0,36)
p_threshold = 0.01 # for the cluster analysis (t-value)
nr_permutations=10000
tail = 0
# ......... 3) Define connectivity across sensors  (common to all stat analysis)  ...................................
# .............................................................................................

from mne.channels import find_ch_connectivity
f_evo = os.path.join(dir_imne_erp, '{}.{}-ave.fif'.format(group_name, '2-20Hz_var_c_dev'))
evoked = read_evokeds(f_evo, condition=0)

connectivity, ch_names = find_ch_connectivity(evoked.info, ch_type='eeg')
print(type(connectivity))
plt.imshow(connectivity.toarray(), cmap='gray', origin='lower', interpolation='nearest')
plt.xlabel('{} EEG sensors'.format(len(ch_names)))
plt.ylabel('{} EEG sensors'.format(len(ch_names)))
plt.title('Between-sensor adjacency')
#
# Try with BEM connectivity matrix
import scipy.io as sio 
f_conn_bem = '/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Reference_Files/Connectivity_15subj_th09.mat'
m = sio.loadmat(f_conn_bem)
conn = m['connectivity']
plt.imshow(conn, cmap='gray', origin='lower', interpolation='nearest')
plt.xlabel('{} EEG sensors'.format(len(ch_names)))
plt.ylabel('{} EEG sensors'.format(len(ch_names)))
plt.title('Between-sensor adjacency')
#

#------------4)  MMN emergence in condition var_c ------------------------------

stat_cond_list =  [ '2-20Hz_var_c_dev', '2-20Hz_var_c_std'] # X = A - B = DEV - STD
stat_name = f'2-20Hz_var_c_A=dev_VS_B=std_permut{nr_permutations}_bem'
f_save = os.path.join(dir_imne_stats, '{}.{}.pkl'.format(group_name, stat_name))

clusters_stats = codd_spatiotemp_clusters_1samp(dir_mne, erp_folder, subj_id_list,stat_cond_list, conn, p_threshold, nr_permutations = nr_permutations, tail=tail, save = f_save)
#
## View results !!!                             ...................................
stat_name = f'2-20Hz_var_c_A=dev_VS_B=std_permut{nr_permutations}'
f_save = os.path.join(dir_imne_stats, '{}.{}.pkl'.format(group_name, stat_name))
with  open(f_save, 'rb') as input:
        a = pickle.load(input)
cluster_stats = a
evoked_dict_cond_a_cond_b = dict() # dictionary {'STD': group-evoked, 'DEV': group-evoked}
colors = [ ]
for name in stat_cond_list:    
    dir_imne_erp = os.path.join(dir_mne, group_name, erp_folder)
    f_evo = os.path.join(dir_imne_erp, '{}.{}-ave.fif'.format(group_name, name))
    evoked_dict_cond_a_cond_b[name]=read_evokeds(f_evo, condition=0)
    colors.append(color_dict[name])    

p_clusters = 0.01
view_significant_spatiotemp_clusters(cluster_stats, p_clusters, evoked_dict_cond_a_cond_b, colors)

stat_name = f'2-20Hz_var_c_A=dev_VS_B=std_permut{nr_permutations}_bem'
f_save_bem = os.path.join(dir_imne_stats, '{}.{}.pkl'.format(group_name, stat_name))
with  open(f_save_bem, 'rb') as input:
        a = pickle.load(input)
cluster_stats = a
evoked_dict_cond_a_cond_b = dict() # dictionary {'STD': group-evoked, 'DEV': group-evoked}
colors = [ ]
for name in stat_cond_list:    
    dir_imne_erp = os.path.join(dir_mne, group_name, erp_folder)
    f_evo = os.path.join(dir_imne_erp, '{}.{}-ave.fif'.format(group_name, name))
    evoked_dict_cond_a_cond_b[name]=read_evokeds(f_evo, condition=0)
    colors.append(color_dict[name])    

p_clusters = 0.01
view_significant_spatiotemp_clusters(cluster_stats, p_clusters, evoked_dict_cond_a_cond_b, colors)

  