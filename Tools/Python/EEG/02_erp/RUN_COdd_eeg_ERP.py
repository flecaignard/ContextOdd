#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
=====================================================================================
CONTEXTODD EEG - ERP ANALYSIS

We work with 1-40 Hz epochs, downsampled (500 Hz), ICA-corrected, peak-to-peak rejection
(Depending on Further analysis, we will see whether we restrict bw to 2-20 Hz)
======================================================================================
Created on Tue Feb 05 2019

@author: Francoise Lecaignard
"""


#############################################################################################
## Import packages          -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################

import warnings
import sys      # path to my own functions
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Python/EEG')


warnings.filterwarnings("ignore",category=DeprecationWarning)
print(__doc__)



############################################################################################
## Paths for the present analysis.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################

# EEG data -  path to the data, analyzed with MNE  ( onwards Step01 )
dir_mne='/sps/cermep/cermep/experiments/DCM/ContextOdd/Ana_MNE'


#############################################################################################
## Compute STD-STD-DEV ERPs .-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
# We have to do the epoching here, using threshold and bad sensors from Preprocs Step08
# Because we want consecutive good epochs

