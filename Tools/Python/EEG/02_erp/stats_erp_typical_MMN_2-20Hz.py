#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
====================================================================
Stats for typical MMN analysis  
====================================================================
This script goes with ERPs computed in ana_er_typical_MMN_2-20Hz
(stats are separated to be launch using command line, CPU/Spyder issues)

We work here in the 2-20 Hz bandwith 
We don't use baseline correction

Output 
       =====>        group data in group36/erp_2-20Hz
       =====>        group36.2-20Hz_var_c_A=dev_VS_B=std_permut10000.pkl
       =====>        group36.2-20Hz_var_p_A=dev_VS_B=std_permut10000.pkl
    
    @author: Françoise Lecaignard, francoise.lecaignard@inserm.fr
"""


###############################################################################
import numpy as np             # convention d'import
import os                       # path.join for file names
import matplotlib.pyplot as plt # from the MNE examples
import pickle
import sys
# path to my own functions
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Python/EEG')
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Python/EEG/02_erp')



import scipy.io as sio 
import scipy.sparse 

from mne import Epochs, pick_types, read_events, read_epochs, find_layout
from mne import write_evokeds, read_evokeds
from mne import get_config, set_config
type(get_config('MNE_BROWSE_RAW_SIZE'))
set_config('MNE_BROWSE_RAW_SIZE', '20.0,10.0')
from mne.preprocessing import read_ica
from mne.viz import plot_topomap,plot_compare_evokeds, tight_layout
from mne.channels import find_ch_connectivity
from mne.io import read_raw_fif

#from fun_NN_template import  ...
from fun_util_epoch import import_epochs
from fun_plot_erp import evokeds_plot, evokeds_plot_all_group, evokeds_plot_overlay_across_subjects
from fun_util_erp import evokeds_group_average,  evokeds_indiv_diff
from fun_util_eeg import print_step_label
from fun_stats_erp import view_significant_spatiotemp_clusters
from fun_codd_stats_erp import codd_spatiotemp_clusters_1samp

################################################################################
# Input Parameters 
################################################################################

# Paths ---------------------------- ---------------------------- ----------------------------
dir_mne = '/sps/cermep/cermep/experiments/DCM/ContextOdd/Ana_MNE'
erp_folder = 'erp_2-20Hz'
pref_erp = '2-20Hz'  # ===> sub-00.2-20Hz_var_p_dev-ave.fif 





################################################################################
# Let's start!
################################################################################


#%%                
#################################################################################
## Stats : group-level analysis; spatio-temporal clusters
#################################################################################
# non-parametric Paired t-test, using spatio-temporal connectivity
# We build a data matrix X of size (n_subj, n_samples, n_sensors)
# Paired-data : we compute the difference X = A-B, and perform a t-test with the difference

# ......... 1) Prepare folder  stats      (common to all stat analysis)  ...................................
# .............................................................................................
group_name = 'group36'
dir_imne_erp = os.path.join(dir_mne, group_name, erp_folder)
stat_folder = 'stats'
dir_imne_stats = os.path.join(dir_imne_erp, stat_folder)

if not os.path.exists(dir_imne_stats):
    os.makedirs(dir_imne_stats)

# ......... 2) Define default parameters      (common to all stat analysis)  ...................................
# .............................................................................................
subj_id_list = np.arange(0,36)
p_threshold = 0.01 # for the cluster analysis (t-value)
nr_permutations=10000
tail = 0
# ......... 3) Define connectivity across sensors  (common to all stat analysis)  ...................................
# .............................................................................................


f_evo = os.path.join(dir_imne_erp, '{}.{}-ave.fif'.format(group_name, '2-20Hz_var_c_dev'))
evoked = read_evokeds(f_evo, condition=0)

connectivity_mne, ch_names = find_ch_connectivity(evoked.info, ch_type='eeg')
#print(type(connectivity))
#plt.imshow(connectivity_mne.toarray(), cmap='gray', origin='lower', interpolation='nearest')
#plt.xlabel('{} EEG sensors'.format(len(ch_names)))
#plt.ylabel('{} EEG sensors'.format(len(ch_names)))
#plt.title('Between-sensor adjacency')
##
# Try with BEM connectivity matrix

f_conn_bem = '/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Reference_Files/Connectivity_15subj_th85_sym.mat'
m = sio.loadmat(f_conn_bem)
conn = m['connectivity']

connectivity_om = scipy.sparse.csr_matrix(conn)
#plt.imshow(connectivity_om.toarray(), cmap='gray', origin='lower', interpolation='nearest')
#plt.xlabel('{} EEG sensors'.format(len(ch_names)))
#plt.ylabel('{} EEG sensors'.format(len(ch_names)))
#plt.title('Between-sensor adjacency')



##------------4)  MMN emergence in condition var_c ------------------------------
#
#stat_cond_list =  [ '2-20Hz_var_c_dev', '2-20Hz_var_c_std'] # X = A - B = DEV - STD
#stat_name = f'2-20Hz_var_c_A=dev_VS_B=std_permut{nr_permutations}'
#f_save = os.path.join(dir_imne_stats, '{}.{}.pkl'.format(group_name, stat_name))
#
#clusters_stats = codd_spatiotemp_clusters_1samp(dir_mne, erp_folder, subj_id_list,stat_cond_list, connectivity, p_threshold, nr_permutations = nr_permutations, tail=tail, save = f_save)
#

#------------5)  MMN emergence in condition var_p ------------------------------

stat_cond_list =  [ '2-20Hz_var_p_dev', '2-20Hz_var_p_std'] # X = A - B = DEV - STD
#stat_name = f'2-20Hz_var_p_dev_VS_std_{nr_permutations}_conn=mne'
#f_save = os.path.join(dir_imne_stats, '{}.{}.pkl'.format(group_name, stat_name))
#clusters_stats = codd_spatiotemp_clusters_1samp(dir_mne, erp_folder, subj_id_list,stat_cond_list, connectivity_mne, p_threshold, nr_permutations = nr_permutations, tail=tail, save = f_save)

stat_name = f'2-20Hz_var_p_dev_VS_std_{nr_permutations}_conn=om'
f_save = os.path.join(dir_imne_stats, '{}.{}.pkl'.format(group_name, stat_name))
clusters_stats = codd_spatiotemp_clusters_1samp(dir_mne, erp_folder, subj_id_list,stat_cond_list, connectivity_om, p_threshold, nr_permutations = nr_permutations, tail=tail, save = f_save)


stat_name = f'2-20Hz_var_p_dev_VS_std_{nr_permutations}_conn=none'
f_save = os.path.join(dir_imne_stats, '{}.{}.pkl'.format(group_name, stat_name))
clusters_stats = codd_spatiotemp_clusters_1samp(dir_mne, erp_folder, subj_id_list,stat_cond_list, None, p_threshold, nr_permutations = nr_permutations, tail=tail, save = f_save)


#
##------------6)  Contrast var_p / var_c  in MMN ------------------------------
#
#stat_cond_list =  [ '2-20Hz_var_p_mmn', '2-20Hz_var_c_mmn'] # X = A - B = DEV - STD
#stat_name = f'2-20Hz_A=mmn_p_VS_B=mmn_c_permut{nr_permutations}'
#f_save = os.path.join(dir_imne_stats, '{}.{}.pkl'.format(group_name, stat_name))
#
#clusters_stats = codd_spatiotemp_clusters_1samp(dir_mne, erp_folder, subj_id_list,stat_cond_list, connectivity, p_threshold, nr_permutations = nr_permutations, tail=tail, save = f_save)
#
#
#
###------------6)  Contrast var_p / var_c  in MMN ------------------------------
##
#stat_cond_list =  [ '2-20Hz_var_p_dev', '2-20Hz_var_c_dev'] # X = A - B = DEV - STD
#stat_name = f'2-20Hz_A=std_p_VS_B=std_c_permut{nr_permutations}'
#f_save = os.path.join(dir_imne_stats, '{}.{}.pkl'.format(group_name, stat_name))
#
#clusters_stats = codd_spatiotemp_clusters_1samp(dir_mne, erp_folder, subj_id_list,stat_cond_list, connectivity, p_threshold, nr_permutations = nr_permutations, tail=tail, save = f_save)
#
