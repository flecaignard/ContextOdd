#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
====================================================================
Compute typical MMN analysis 
Here we compare extreme chunk size (2,3,7,8) in the two contexts


may2019 (tests for HBM Roma poster)
====================================================================
This analysis is a typical deviant-minus-standard analysis

We work here in the 2-20 Hz bandwith 
We don't use baseline correction

We need to assign start/end codes for each stim 
       
       
    @author: Françoise Lecaignard, francoise.lecaignard@inserm.fr
"""


###############################################################################
import numpy as np             # convention d'import
import os                       # path.join for file names
import matplotlib.pyplot as plt # from the MNE examples
import pickle
import sys
# path to my own functions
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Python/EEG')
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Python/EEG/02_erp')
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Python/EEG/01_preprocessings')



from mne import Epochs, pick_types, read_events, read_epochs, find_layout
from mne import write_evokeds, read_evokeds
from mne import get_config, set_config
type(get_config('MNE_BROWSE_RAW_SIZE'))
set_config('MNE_BROWSE_RAW_SIZE', '20.0,10.0')
from mne.preprocessing import read_ica
from mne.viz import plot_topomap,plot_compare_evokeds, tight_layout
from mne import write_evokeds, combine_evoked, read_evokeds, grand_average

from mne.io import read_raw_fif

#from fun_NN_template import  ...
from fun_util_epoch import import_epochs
from fun_plot_erp import evokeds_plot, evokeds_plot_all_group
from fun_util_erp import evokeds_group_average,  evokeds_indiv_diff
from fun_util_eeg import print_step_label
from fun_stats_erp import view_significant_spatiotemp_clusters
from fun_codd_stats_erp import codd_spatiotemp_clusters_1samp
from fun_02_COdd_eeg_events import codd_eeg_rename_events_bloc1_bloc2, codd_eeg_rename_events_deb_fin
################################################################################
# Input Parameters 
################################################################################

# Paths ---------------------------- ---------------------------- ----------------------------
dir_mne = '/sps/cermep/cermep/experiments/DCM/ContextOdd/Ana_MNE'

erp_folder = 'erp_2-20Hz'
raw_folder = 'preproc'


# epochs 2-20 Hz  ---------------------------- ---------------------
pref_continuous = 'preproc_2-20Hz_ica' 

pref_epo_in = 'clean_2-20Hz' #-epo.fif, cleaned epochs from preprocs
pref_epo_out = 'clean_2-20Hz_7d' #-epo.fif, resulting cleaned epochs, being equal to pref_epo_in with 7-digit coding


# epochs bounds
tmin, tmax = -0.2, 0.41 

# events for epochs

event_id = {  'deb/b1/var_c/c4/c-/p0/std': 1114121, 'deb/b1/var_c/c4/c-/p0/dev': 1114122,'deb/b1/var_c/c4/c-/p0/std_oth': 1114123,\
              'deb/b1/var_c/c5/c-/p0/std': 1115121, 'deb/b1/var_c/c5/c-/p0/dev': 1115122,'deb/b1/var_c/c5/c-/p0/std_oth': 1115123,\
              'deb/b1/var_c/c6/c-/p0/std': 1116121, 'deb/b1/var_c/c6/c-/p0/dev': 1116122,'deb/b1/var_c/c6/c-/p0/std_oth': 1116123,\
              'deb/b1/var_c/c2/c+/p0/std': 1112221, 'deb/b1/var_c/c2/c+/p0/dev': 1112222,'deb/b1/var_c/c2/c+/p0/std_oth': 1112223,\
              'deb/b1/var_c/c3/c+/p0/std': 1113221, 'deb/b1/var_c/c3/c+/p0/dev': 1113222,'deb/b1/var_c/c3/c+/p0/std_oth': 1113223,\
              'deb/b1/var_c/c4/c+/p0/std': 1114221, 'deb/b1/var_c/c4/c+/p0/dev': 1114222,'deb/b1/var_c/c4/c+/p0/std_oth': 1114223,\
              'deb/b1/var_c/c6/c+/p0/std': 1116221, 'deb/b1/var_c/c6/c+/p0/dev': 1116222,'deb/b1/var_c/c6/c+/p0/std_oth': 1116223,\
              'deb/b1/var_c/c7/c+/p0/std': 1117221, 'deb/b1/var_c/c7/c+/p0/dev': 1117222,'deb/b1/var_c/c7/c+/p0/std_oth': 1117223,\
              'deb/b1/var_c/c8/c+/p0/std': 1118221, 'deb/b1/var_c/c8/c+/p0/dev': 1118222,'deb/b1/var_c/c8/c+/p0/std_oth': 1118223,\
              'deb/b1/var_p/c2/c-/p+/std': 1122111, 'deb/b1/var_p/c2/c-/p+/dev': 1122112,'deb/b1/var_p/c2/c-/p+/std_oth': 1122113,\
              'deb/b1/var_p/c3/c-/p+/std': 1123111, 'deb/b1/var_p/c3/c-/p+/dev': 1123112,'deb/b1/var_p/c3/c-/p+/std_oth': 1123113,\
              'deb/b1/var_p/c4/c-/p+/std': 1124111, 'deb/b1/var_p/c4/c-/p+/dev': 1124112,'deb/b1/var_p/c4/c-/p+/std_oth': 1124113,\
              'deb/b1/var_p/c4/c-/p0/std': 1124121, 'deb/b1/var_p/c4/c-/p0/dev': 1124122,'deb/b1/var_p/c4/c-/p0/std_oth': 1124123,\
              'deb/b1/var_p/c5/c-/p0/std': 1125121, 'deb/b1/var_p/c5/c-/p0/dev': 1125122,'deb/b1/var_p/c5/c-/p0/std_oth': 1125123,\
              'deb/b1/var_p/c6/c-/p0/std': 1126121, 'deb/b1/var_p/c6/c-/p0/dev': 1126122,'deb/b1/var_p/c6/c-/p0/std_oth': 1126123,\
              'deb/b1/var_p/c6/c-/p-/std': 1126131, 'deb/b1/var_p/c6/c-/p-/dev': 1126132,'deb/b1/var_p/c6/c-/p-/std_oth': 1126133,\
              'deb/b1/var_p/c7/c-/p-/std': 1127131, 'deb/b1/var_p/c7/c-/p-/dev': 1127132,'deb/b1/var_p/c7/c-/p-/std_oth': 1127133,\
              'deb/b1/var_p/c8/c-/p-/std': 1128131, 'deb/b1/var_p/c8/c-/p-/dev': 1128132,'deb/b1/var_p/c8/c-/p-/std_oth': 1128133, \
              'deb/b2/var_c/c4/c-/p0/std': 1214121, 'deb/b2/var_c/c4/c-/p0/dev': 1214122,'deb/b2/var_c/c4/c-/p0/std_oth': 1214123,\
              'deb/b2/var_c/c5/c-/p0/std': 1215121, 'deb/b2/var_c/c5/c-/p0/dev': 1215122,'deb/b2/var_c/c5/c-/p0/std_oth': 1215123,\
              'deb/b2/var_c/c6/c-/p0/std': 1216121, 'deb/b2/var_c/c6/c-/p0/dev': 1216122,'deb/b2/var_c/c6/c-/p0/std_oth': 1216123,\
              'deb/b2/var_c/c2/c+/p0/std': 1212221, 'deb/b2/var_c/c2/c+/p0/dev': 1212222,'deb/b2/var_c/c2/c+/p0/std_oth': 1212223,\
              'deb/b2/var_c/c3/c+/p0/std': 1213221, 'deb/b2/var_c/c3/c+/p0/dev': 1213222,'deb/b2/var_c/c3/c+/p0/std_oth': 1213223,\
              'deb/b2/var_c/c4/c+/p0/std': 1214221, 'deb/b2/var_c/c4/c+/p0/dev': 1214222,'deb/b2/var_c/c4/c+/p0/std_oth': 1214223,\
              'deb/b2/var_c/c6/c+/p0/std': 1216221, 'deb/b2/var_c/c6/c+/p0/dev': 1216222,'deb/b2/var_c/c6/c+/p0/std_oth': 1216223,\
              'deb/b2/var_c/c7/c+/p0/std': 1217221, 'deb/b2/var_c/c7/c+/p0/dev': 1217222,'deb/b2/var_c/c7/c+/p0/std_oth': 1217223,\
              'deb/b2/var_c/c8/c+/p0/std': 1218221, 'deb/b2/var_c/c8/c+/p0/dev': 1218222,'deb/b2/var_c/c8/c+/p0/std_oth': 1218223,\
              'deb/b2/var_p/c2/c-/p+/std': 1222111, 'deb/b2/var_p/c2/c-/p+/dev': 1222112,'deb/b2/var_p/c2/c-/p+/std_oth': 1222113,\
              'deb/b2/var_p/c3/c-/p+/std': 1223111, 'deb/b2/var_p/c3/c-/p+/dev': 1223112,'deb/b2/var_p/c3/c-/p+/std_oth': 1223113,\
              'deb/b2/var_p/c4/c-/p+/std': 1224111, 'deb/b2/var_p/c4/c-/p+/dev': 1224112,'deb/b2/var_p/c4/c-/p+/std_oth': 1224113,\
              'deb/b2/var_p/c4/c-/p0/std': 1224121, 'deb/b2/var_p/c4/c-/p0/dev': 1224122,'deb/b2/var_p/c4/c-/p0/std_oth': 1224123,\
              'deb/b2/var_p/c5/c-/p0/std': 1225121, 'deb/b2/var_p/c5/c-/p0/dev': 1225122,'deb/b2/var_p/c5/c-/p0/std_oth': 1225123,\
              'deb/b2/var_p/c6/c-/p0/std': 1226121, 'deb/b2/var_p/c6/c-/p0/dev': 1226122,'deb/b2/var_p/c6/c-/p0/std_oth': 1226123,\
              'deb/b2/var_p/c6/c-/p-/std': 1226131, 'deb/b2/var_p/c6/c-/p-/dev': 1226132,'deb/b2/var_p/c6/c-/p-/std_oth': 1226133,\
              'deb/b2/var_p/c7/c-/p-/std': 1227131, 'deb/b2/var_p/c7/c-/p-/dev': 1227132,'deb/b2/var_p/c7/c-/p-/std_oth': 1227133,\
              'deb/b2/var_p/c8/c-/p-/std': 1228131, 'deb/b2/var_p/c8/c-/p-/dev': 1228132,'deb/b2/var_p/c8/c-/p-/std_oth': 1228133,\
              'fin/b1/var_c/c4/c-/p0/std': 2114121, 'fin/b1/var_c/c4/c-/p0/dev': 2114122,'fin/b1/var_c/c4/c-/p0/std_oth': 2114123,\
              'fin/b1/var_c/c5/c-/p0/std': 2115121, 'fin/b1/var_c/c5/c-/p0/dev': 2115122,'fin/b1/var_c/c5/c-/p0/std_oth': 2115123,\
              'fin/b1/var_c/c6/c-/p0/std': 2116121, 'fin/b1/var_c/c6/c-/p0/dev': 2116122,'fin/b1/var_c/c6/c-/p0/std_oth': 2116123,\
              'fin/b1/var_c/c2/c+/p0/std': 2112221, 'fin/b1/var_c/c2/c+/p0/dev': 2112222,'fin/b1/var_c/c2/c+/p0/std_oth': 2112223,\
              'fin/b1/var_c/c3/c+/p0/std': 2113221, 'fin/b1/var_c/c3/c+/p0/dev': 2113222,'fin/b1/var_c/c3/c+/p0/std_oth': 2113223,\
              'fin/b1/var_c/c4/c+/p0/std': 2114221, 'fin/b1/var_c/c4/c+/p0/dev': 2114222,'fin/b1/var_c/c4/c+/p0/std_oth': 2114223,\
              'fin/b1/var_c/c6/c+/p0/std': 2116221, 'fin/b1/var_c/c6/c+/p0/dev': 2116222,'fin/b1/var_c/c6/c+/p0/std_oth': 2116223,\
              'fin/b1/var_c/c7/c+/p0/std': 2117221, 'fin/b1/var_c/c7/c+/p0/dev': 2117222,'fin/b1/var_c/c7/c+/p0/std_oth': 2117223,\
              'fin/b1/var_c/c8/c+/p0/std': 2118221, 'fin/b1/var_c/c8/c+/p0/dev': 2118222,'fin/b1/var_c/c8/c+/p0/std_oth': 2118223,\
              'fin/b1/var_p/c2/c-/p+/std': 2122111, 'fin/b1/var_p/c2/c-/p+/dev': 2122112,'fin/b1/var_p/c2/c-/p+/std_oth': 2122113,\
              'fin/b1/var_p/c3/c-/p+/std': 2123111, 'fin/b1/var_p/c3/c-/p+/dev': 2123112,'fin/b1/var_p/c3/c-/p+/std_oth': 2123113,\
              'fin/b1/var_p/c4/c-/p+/std': 2124111, 'fin/b1/var_p/c4/c-/p+/dev': 2124112,'fin/b1/var_p/c4/c-/p+/std_oth': 2124113,\
              'fin/b1/var_p/c4/c-/p0/std': 2124121, 'fin/b1/var_p/c4/c-/p0/dev': 2124122,'fin/b1/var_p/c4/c-/p0/std_oth': 2124123,\
              'fin/b1/var_p/c5/c-/p0/std': 2125121, 'fin/b1/var_p/c5/c-/p0/dev': 2125122,'fin/b1/var_p/c5/c-/p0/std_oth': 2125123,\
              'fin/b1/var_p/c6/c-/p0/std': 2126121, 'fin/b1/var_p/c6/c-/p0/dev': 2126122,'fin/b1/var_p/c6/c-/p0/std_oth': 2126123,\
              'fin/b1/var_p/c6/c-/p-/std': 2126131, 'fin/b1/var_p/c6/c-/p-/dev': 2126132,'fin/b1/var_p/c6/c-/p-/std_oth': 2126133,\
              'fin/b1/var_p/c7/c-/p-/std': 2127131, 'fin/b1/var_p/c7/c-/p-/dev': 2127132,'fin/b1/var_p/c7/c-/p-/std_oth': 2127133,\
              'fin/b1/var_p/c8/c-/p-/std': 2128131, 'fin/b1/var_p/c8/c-/p-/dev': 2128132,'fin/b1/var_p/c8/c-/p-/std_oth': 2128133, \
              'fin/b2/var_c/c4/c-/p0/std': 2214121, 'fin/b2/var_c/c4/c-/p0/dev': 2214122,'fin/b2/var_c/c4/c-/p0/std_oth': 2214123,\
              'fin/b2/var_c/c5/c-/p0/std': 2215121, 'fin/b2/var_c/c5/c-/p0/dev': 2215122,'fin/b2/var_c/c5/c-/p0/std_oth': 2215123,\
              'fin/b2/var_c/c6/c-/p0/std': 2216121, 'fin/b2/var_c/c6/c-/p0/dev': 2216122,'fin/b2/var_c/c6/c-/p0/std_oth': 2216123,\
              'fin/b2/var_c/c2/c+/p0/std': 2212221, 'fin/b2/var_c/c2/c+/p0/dev': 2212222,'fin/b2/var_c/c2/c+/p0/std_oth': 2212223,\
              'fin/b2/var_c/c3/c+/p0/std': 2213221, 'fin/b2/var_c/c3/c+/p0/dev': 2213222,'fin/b2/var_c/c3/c+/p0/std_oth': 2213223,\
              'fin/b2/var_c/c4/c+/p0/std': 2214221, 'fin/b2/var_c/c4/c+/p0/dev': 2214222,'fin/b2/var_c/c4/c+/p0/std_oth': 2214223,\
              'fin/b2/var_c/c6/c+/p0/std': 2216221, 'fin/b2/var_c/c6/c+/p0/dev': 2216222,'fin/b2/var_c/c6/c+/p0/std_oth': 2216223,\
              'fin/b2/var_c/c7/c+/p0/std': 2217221, 'fin/b2/var_c/c7/c+/p0/dev': 2217222,'fin/b2/var_c/c7/c+/p0/std_oth': 2217223,\
              'fin/b2/var_c/c8/c+/p0/std': 2218221, 'fin/b2/var_c/c8/c+/p0/dev': 2218222,'fin/b2/var_c/c8/c+/p0/std_oth': 2218223,\
              'fin/b2/var_p/c2/c-/p+/std': 2222111, 'fin/b2/var_p/c2/c-/p+/dev': 2222112,'fin/b2/var_p/c2/c-/p+/std_oth': 2222113,\
              'fin/b2/var_p/c3/c-/p+/std': 2223111, 'fin/b2/var_p/c3/c-/p+/dev': 2223112,'fin/b2/var_p/c3/c-/p+/std_oth': 2223113,\
              'fin/b2/var_p/c4/c-/p+/std': 2224111, 'fin/b2/var_p/c4/c-/p+/dev': 2224112,'fin/b2/var_p/c4/c-/p+/std_oth': 2224113,\
              'fin/b2/var_p/c4/c-/p0/std': 2224121, 'fin/b2/var_p/c4/c-/p0/dev': 2224122,'fin/b2/var_p/c4/c-/p0/std_oth': 2224123,\
              'fin/b2/var_p/c5/c-/p0/std': 2225121, 'fin/b2/var_p/c5/c-/p0/dev': 2225122,'fin/b2/var_p/c5/c-/p0/std_oth': 2225123,\
              'fin/b2/var_p/c6/c-/p0/std': 2226121, 'fin/b2/var_p/c6/c-/p0/dev': 2226122,'fin/b2/var_p/c6/c-/p0/std_oth': 2226123,\
              'fin/b2/var_p/c6/c-/p-/std': 2226131, 'fin/b2/var_p/c6/c-/p-/dev': 2226132,'fin/b2/var_p/c6/c-/p-/std_oth': 2226133,\
              'fin/b2/var_p/c7/c-/p-/std': 2227131, 'fin/b2/var_p/c7/c-/p-/dev': 2227132,'fin/b2/var_p/c7/c-/p-/std_oth': 2227133,\
              'fin/b2/var_p/c8/c-/p-/std': 2228131, 'fin/b2/var_p/c8/c-/p-/dev': 2228132,'fin/b2/var_p/c8/c-/p-/std_oth': 2228133    }

# events file (renamed and downsampled events)
pref_eve_7d = 'preproc.7d'

# Evoked responses   ---------------------------- ---------------------

exclude_eeg = { 'sub-00': ['T7', 'TP9'],	 'sub-01': [],	'sub-02':['TP10', 'CP5', 'P4'],	 'sub-03': ['Oz'],	'sub-04': ['T7', 'T8'],\
             'sub-05': [],	 'sub-06': [],	'sub-07':[],	 'sub-08': ['O1', 'O2'],	'sub-09': ['T7', 'T8'],\
             'sub-10': [],    'sub-11': ['PO9'],   'sub-12':[],  'sub-13': ['T7', 'PO10'],    'sub-14': ['P3'],\
             'sub-15': ['T8'],    'sub-16': ['Oz'],   'sub-17':['PO9'],  'sub-18': [],    'sub-19': ['T7', 'T8', 'TP10'],\
             'sub-20': [],    'sub-21': [],   'sub-22':['P3'],	 'sub-23': ['CP1'],    'sub-24': [],\
             'sub-25': ['Oz', 'T8', 'TP9'],    'sub-26': [],   'sub-27':[],	 'sub-28': [],    'sub-29': ['CP5'],\
             'sub-30': ['T8', 'PO10'],    'sub-31': [],   'sub-32':['Oz'],	 'sub-33': ['T8'],    'sub-34': ['T8', 'T7'],\
             'sub-35': []	}

# compute ERPs for std, dev and compute difference too:
#erp_compute_dict = {    'var_c/c+/mmn': ['var_c/c+/dev','var_c/c+/std'],
#                        'var_p/p0/mmn': ['var_p/p0/dev','var_p/p0/std'],
#                        'deb/b1/var_c/c-/mmn': ['deb/b1/var_c/c-/dev','deb/b1/var_c/c-/std'], # dev first, std second because we do : (1) dev -  (2) std 
#                        'fin/b1/var_c/c-/mmn': ['fin/b1/var_c/c-/dev','fin/b1/var_c/c-/std'], 
#                        'deb/b2/var_c/c-/mmn': ['deb/b2/var_c/c-/dev','deb/b2/var_c/c-/std'], 
#                        'fin/b2/var_c/c-/mmn': ['fin/b2/var_c/c-/dev','fin/b2/var_c/c-/std'], 
#                        'deb/b1/var_p/p0/mmn': ['deb/b1/var_p/p0/dev','deb/b1/var_p/p0/std'], # dev first, std second because we do : (1) dev -  (2) std 
#                        'fin/b1/var_p/p0/mmn': ['fin/b1/var_p/p0/dev','fin/b1/var_p/p0/std'], 
#                        'deb/b2/var_p/p0/mmn': ['deb/b2/var_p/p0/dev','deb/b2/var_p/p0/std'], 
#                        'fin/b2/var_p/p0/mmn': ['fin/b2/var_p/p0/dev','fin/b2/var_p/p0/std'], 
#}

pref_erp = '2-20Hz'  # ===> sub-00.2-20Hz_var_c_c-_dev-ave.fif 

# Plot options   ---------------------------- ---------------------



################################################################################
# Let's start!
################################################################################

#%%
#################################################################################
## Compute c-/2,3,4 = short_var_c , and  c-/6,7,8 = long_car_p
#################################################################################

subj_id_list = np.arange(0,36) # 0 to 35, to include sub-00 to sub-35
baseline = None


overwrite_ind = True

for i_su in subj_id_list:
    
    subj='sub-{:02}'.format(i_su) 
    dir_imne_erp = os.path.join(dir_mne, subj, erp_folder) #individual mne path
    
    if overwrite_ind: # we  compute individual ERPs (std, dev ad difference in var_p, var_c)  
        
        f_epochs=os.path.join(dir_imne_erp, '{}.{}-epo.fif'.format(subj, pref_epo_out))
        epochs = read_epochs(f_epochs) 
        epochs.info['bads']=exclude_eeg[subj]
        epochs.interpolate_bads()
        
        # chunk "2378" in var_c
        erp_list = [['var_c/c+/c2/dev', 'var_c/c+/c3/dev', 'var_c/c+/c7/dev', 'var_c/c+/c8/dev'],
                    ['var_c/c+/c2/std', 'var_c/c+/c3/std', 'var_c/c+/c7/std', 'var_c/c+/c8/std'],
                     ]
        erp_list_name = ['var_c_loose_2378_dev',  'var_c_loose_2378_std']
        evokeds = [epochs[name].average().apply_baseline(baseline) for name in erp_list]
        for i_ev, name in enumerate(erp_list_name):
            evokeds[i_ev].comment = '{}, {}'.format(subj, name)
        
        diff = combine_evoked(evokeds, weights=(1,-1) ) #[evokeds[0], -evokeds[1]], weights='equal')
        evokeds.append(diff)
        evokeds[-1].comment = '{}'.format('var_c_loose_2378_mmn')
        
        pref_evo_list=['var_c_loose_2378_dev', 'var_c_loose_2378_std','var_c_loose_2378_mmn']
        for idx, erp_name  in enumerate (pref_evo_list):
            print(erp_name)
            if pref_erp:
                new_name = f'{pref_erp}_{erp_name}'
            f_out = os.path.join(dir_imne_erp, '{}.{}-ave.fif'.format(subj, new_name))
            write_evokeds(f_out, evokeds[idx])
            
            
         # chunk "2378" in var_c
        erp_list = [['var_p/p+/c2/dev', 'var_p/p+/c3/dev', 'var_p/p-/c7/dev', 'var_p/p-/c8/dev'],
                    ['var_p/p+/c2/std', 'var_p/p+/c3/std', 'var_p/p-/c7/std', 'var_p/p-/c8/std'],
                     ]
        erp_list_name = ['var_p_loose_2378_dev',  'var_p_loose_2378_std']
        evokeds = [epochs[name].average().apply_baseline(baseline) for name in erp_list]
        for i_ev, name in enumerate(erp_list_name):
            evokeds[i_ev].comment = '{}, {}'.format(subj, name)
        
        diff = combine_evoked(evokeds, weights=(1,-1) ) #[evokeds[0], -evokeds[1]], weights='equal')
        evokeds.append(diff)
        evokeds[-1].comment = '{}'.format('var_p_loose_2378_mmn')
        
        pref_evo_list=['var_p_loose_2378_dev', 'var_p_loose_2378_std','var_p_loose_2378_mmn']
        for idx, erp_name  in enumerate (pref_evo_list):
            print(erp_name)
            if pref_erp:
                new_name = f'{pref_erp}_{erp_name}'
            f_out = os.path.join(dir_imne_erp, '{}.{}-ave.fif'.format(subj, new_name))
            write_evokeds(f_out, evokeds[idx])  

     
        
         

#%%
##################################################################################
### Compute group-average ERPs
##################################################################################
subj_group_avg = np.arange(0,36) # 0 to 35, to include sub-00 to sub-35
group_name = 'group36'

overwrite_group = True
          
erp_list = [ 'var_c_loose_2378_std' , 'var_c_loose_2378_dev' ,   'var_c_loose_2378_mmn' ,              
             'var_p_loose_2378_std' , 'var_p_loose_2378_dev' ,   'var_p_loose_2378_mmn' , 
                              ]                     
for erp_name in erp_list:
    pref_evoked = erp_name.replace('/', '_')
    if pref_erp:
        pref_evoked = f'{pref_erp}_{pref_evoked}'
    if overwrite_group:
            group_list, group_avg = evokeds_group_average(dir_mne, erp_folder, subj_group_avg , group_name,  pref_evoked,  save= True)
    else:
            group_list, group_avg = evokeds_group_average(dir_mne, erp_folder, subj_group_avg, group_name , pref_evoked,  save= False)

 
#%%
##################################################################################
### Plot group-average ERPs
##################################################################################
#group_name = 'group36'
#plot_type= [ 'traces']
#
#
#
#plot_list =  [ '2-20Hz_var_c_c-_dev','2-20Hz_var_c_c-_std', '2-20Hz_var_c_c-_mmn'] 
##plot_list =  [ '2-20Hz_var_c_c+_dev','2-20Hz_var_p_p0_std', '2-20Hz_var_p_p0_mmn']
##plot_list =  [ '2-20Hz_var_p_p0_dev','2-20Hz_var_c_c+_std', '2-20Hz_var_c_c+_mmn' ]
#plot_list =  [ '2-20Hz_var_c_c-_std', '2-20Hz_var_c_c+_std'] 
#plot_list =  [ '2-20Hz_var_c_c-_dev', '2-20Hz_var_c_c+_dev'] 
##plot_list =  [ '2-20Hz_var_c_c-_mmn', '2-20Hz_var_c_c+_mmn'] 
##plot_list =  [ '2-20Hz_var_c_c-_mmn', '2-20Hz_var_p_p0_mmn', '2-20Hz_var_c_c+_mmn', '2-20Hz_var_p_mmn'] 
##plot_list =  [ '2-20Hz_var_c_c-_std', '2-20Hz_var_p_p0_std', '2-20Hz_var_c_c+_std'] #, '2-20Hz_var_p_std'] 
##plot_list =  [ '2-20Hz_var_c_c-_dev',  '2-20Hz_var_p_p0_dev', '2-20Hz_var_c_c+_dev', '2-20Hz_var_p_dev'] 
#
#colors = []
#evokeds=list()
#for name in plot_list:
#    dir_imne_erp = os.path.join(dir_mne, group_name, erp_folder)
#    f_evo = os.path.join(dir_imne_erp, '{}.{}-ave.fif'.format(group_name, name))
#    evokeds.append(read_evokeds(f_evo, condition=0))
#    colors.append(color_dict[name])
#evokeds_plot(evokeds, plot_type, neg=True,vline=[ 0], colors=colors)
# 


#%%
##################################################################################
### Export Elan, stats
##################################################################################
# Export *-ave.fif in *.mat files
import scipy.io as sio 
no_baseline_export = False 


if no_baseline_export:
    elan_folder = 'stats_elan' # within erp_folder
else:
    elan_folder = 'stats_elan_baseline'

    
export_list = [ '2-20Hz_var_c_loose_2378_std' , '2-20Hz_var_c_loose_2378_dev' ,   '2-20Hz_var_c_loose_2378_mmn' ,              
             '2-20Hz_var_p_loose_2378_std' , '2-20Hz_var_p_loose_2378_dev' ,   '2-20Hz_var_p_loose_2378_mmn' , 
                              ]                    

subj_id_list = np.arange(0,36) # 0 to 35, to include sub-00 to sub-35


for i_su in subj_id_list:
    subj='sub-{:02}'.format(i_su) 
    dir_imne = os.path.join(dir_mne, subj, erp_folder) #individual mne path
    dir_ielan = os.path.join(dir_imne, elan_folder) 
    if not os.path.exists(dir_ielan):
        os.makedirs(dir_ielan)

    for name in export_list:    
        f_evo = os.path.join(dir_imne, '{}.{}-ave.fif'.format(subj, name))
        evoked=read_evokeds(f_evo, condition=0)
        if not no_baseline_export:
            evoked.apply_baseline(baseline=(-0.2, 0))
            name = f'{name}_baseline200'
        matinfo = {'d':evoked.data, \
                   'sens': evoked.ch_names, \
                   'times': evoked.times, \
                   'sfreq': evoked.info['sfreq']  }
        f_mat = os.path.join(dir_ielan, '{}.{}.mat'.format(subj, name))
        sio.savemat(f_mat, matinfo)
       
        
# export group avg
subj='group36'
dir_imne = os.path.join(dir_mne, subj, erp_folder) #individual mne path
dir_ielan = os.path.join(dir_imne, elan_folder) 
if not os.path.exists(dir_ielan):
    os.makedirs(dir_ielan)

for name in export_list:    
    f_evo = os.path.join(dir_imne, '{}.{}-ave.fif'.format(subj, name))
    evoked=read_evokeds(f_evo, condition=0)
    if not no_baseline_export:
            evoked.apply_baseline(baseline=(-0.2, 0))
            name = f'{name}_baseline200'
    matinfo = {'d':evoked.data, \
               'sens': evoked.ch_names, \
               'times': evoked.times, \
               'sfreq': evoked.info['sfreq']  }
    f_mat = os.path.join(dir_ielan, '{}.{}.mat'.format(subj, name))
    sio.savemat(f_mat, matinfo)        

