#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Home-made Functions for stats analysis in EEG-MEG Evoked responses
Created on Tue Mar  5 09:52:07 2019

@author: Françoise Lecaignard, francoise.lecaignard@inserm.fr
"""

import numpy as np             # convention d'import
import os                       # path.join for file names
import matplotlib.pyplot as plt # from the MNE examples
from bisect import bisect

from mne import Epochs, Annotations, pick_types, find_events, write_events, read_events, find_layout
from mne.channels import read_layout
from mne.io import concatenate_raws, read_raw_brainvision, read_raw_fif, find_edf_events
from mne.datasets import eegbci
from mne.decoding import CSP
from mne.viz import plot_events, plot_ica_overlay
from mne.preprocessing import ICA, read_ica,  find_eog_events
from mne import write_evokeds, combine_evoked, read_evokeds, grand_average
from mne.viz import plot_topomap,plot_compare_evokeds, tight_layout
from mpl_toolkits.axes_grid1 import make_axes_locatable
from scipy import stats as stats
from mne.stats import spatio_temporal_cluster_1samp_test
import pickle


import sys      # path to my own functions
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Python/EEG')


print(__doc__)

import sys      # path to my own functions
from fun_plot_erp  import evokeds_plot




#############################################################################################
##  o*o*o*o       Compute Permutations Tests ( DSpatio-temporal  Clusters  ) *o*o*o*o*o*o*o*o*
#############################################################################################
def codd_spatiotemp_clusters_1samp(dir_mne, erp_folder, subj_id_list,stat_cond_list, connectivity, p_threshold, nr_permutations=1000, tail=0, save = None):
    
    # wrapper for spatio_temporal_cluster_1samp_test
    # Comparing cond_a vs. cond_b
    #
    # inputs
    #        dir_mnepath to MNE analyis
    #        erp_folder : name of folder within subject folder : dir_mne/subj/erp_folder
    #        subj_id_list: of the form subj_id_list = np.arange(0,36)
    #        stat_cond_list  : list of conditions,  with names corresponding to evoked files :   [ '2-20Hz_var_c_dev', '2-20Hz_var_p_std']
    #        connectivity object (spatial connectivity across EEG sensors (output of find_ch_connectivity))
    #        p_threshold : threshold for the cluster analysis
    #        save : None or filename
    
    list_cond_a = list()
    list_cond_b = list()
    
    
    evoked_cond_a = [] # list of individual evoked (nr_subj elements)
    evoked_cond_b = []
    
    for i_su in subj_id_list:
        
        subj='sub-{:02}'.format(i_su) 
        dir_imne_erp = os.path.join(dir_mne, subj, erp_folder) #individual mne path
        
        
        f_evo = os.path.join(dir_imne_erp, '{}.{}-ave.fif'.format(subj, stat_cond_list[0]))
        evokeds = read_evokeds(f_evo)
        mtx_evoked_cond_a = np.expand_dims(evokeds[0].data ,2) # size : nr_sens * nr_samples *1 : data of subject #i_su
        evoked_cond_a.append(evokeds[0])
        f_evo = os.path.join(dir_imne_erp, '{}.{}-ave.fif'.format(subj, stat_cond_list[1]))
        evokeds = read_evokeds(f_evo)
        mtx_evoked_cond_b = np.expand_dims(evokeds[0].data ,2)
        evoked_cond_b.append(evokeds[0])
        # if we need to crop data, adapt the following lines for both conditions above :
        #mtx_evoked_cond_a = np.delete(mtx_evoked_cond_a, np.s_[0 : 200],1)
        #mtx_evoked_cond_a = np.delete(mtx_evoked_cond_a, np.s_[501 : 601],1)
        list_cond_a.append(mtx_evoked_cond_a) # list of nr_subj matrices
        list_cond_b.append(mtx_evoked_cond_b)   
    
        
    ev_cond_a = np.concatenate(list_cond_a, axis = 2)  
    ev_cond_b = np.concatenate(list_cond_b, axis = 2)
    
    ev_cond_a = np.transpose(ev_cond_a, (2,1,0)) #matrix:  nr_subj * nr_samples * nr_sens
    ev_cond_b = np.transpose(ev_cond_b, (2,1,0))  
    X = ev_cond_a - ev_cond_b  #matrix:  nr_subj * nr_samples * nr_sens
    
    # ......... 2) Perform the permutations                     ...................................
    # .............................................................................................
    # spatio_temporal_cluster_1samp_test
    # Non-parametric cluster-level paired t-test for spatio-temporal data.	
    # output:
    #           t_obs : array, shape (n_times * n_vertices,) t-statistic observed for all variables.
    #           clusters : list, List type defined by out_type above.
    #           clusterpv : array, P-value for each cluster
    #           H0 : array, shape (n_permutations,), Max cluster level stats observed under permutation
    
    n_subjects = len(subj_id_list)
    t_threshold = -stats.distributions.t.ppf(p_threshold / 2., n_subjects - 1)
    cluster_stats = spatio_temporal_cluster_1samp_test(X, threshold=t_threshold, n_permutations = nr_permutations, tail=tail,  connectivity=connectivity, n_jobs=1,   verbose=None)
   
    if save:
        with  open(save, 'wb') as output:
            pickle.dump(cluster_stats, output, pickle.HIGHEST_PROTOCOL)


    return cluster_stats