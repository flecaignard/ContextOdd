#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 15 20:48:29 2018

@author: francoise
"""

## Attempts to re-organize preprocessings scripts (A. Corneylie)


#############################################################################################
## Import packages          -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
# import argparse               # add input arguments

import numpy as np             # convention d'import
import os                       # path.join for file names
import matplotlib.pyplot as plt # from the MNE examples
import csv
import warnings
import argparse

from mne import Epochs, pick_types, find_events, write_events, read_events, read_epochs ,make_field_map
from mne import write_evokeds, combine_evoked, read_evokeds
from mne.channels import read_layout, Montage, read_montage
from mne.io import concatenate_raws, read_raw_brainvision, read_raw_fif, find_edf_events
from mne.datasets import eegbci
from mne.decoding import CSP
from mne.viz import plot_events, plot_ica_components, plot_ica_overlay, plot_evoked_topo
from mne.preprocessing import ICA, read_ica, create_eog_epochs, find_eog_events
from mne.time_frequency import tfr_morlet

print(__doc__)

from mne import get_config, set_config
type(get_config('MNE_BROWSE_RAW_SIZE'))
set_config('MNE_BROWSE_RAW_SIZE', '20.0,10.0')




import sys      # path to my own functions
sys.path.append('/Users/francoise/Documents/Projets/ContextOdd/ContextOdd_Git/Tools/Python/EEG')


warnings.filterwarnings("ignore",category=DeprecationWarning)

from fun_erp_util import evokeds_indiv_diff,evokeds_group_average #, evokeds_indiv_double_erp
from fun_eeg_misc import import_epochs
 
########################################################################################
# 00- BIG MMN: across all condition s(quality check)
########################################################################################
# This function computes:
# -  (if not existing) individual ERPs for conditions (std, dev) 
# -  difference  mmn = dev - std
# - 3 group averages (std, dev, mmn) 
########################################################################################


########################################################################################
# 01- Global MMN: all standards (preceding a deviant)  and deviants  in each condition (var_c, var_p)
########################################################################################
# This function computes:
# -  (if not existing) individual ERPs for conditions (std, dev) in conditions (var_c, var_p)
# - their difference  mmn = dev - std
# - 6 group averages (std, dev, mmn) * (var_c, var_p)

########################################################################################

def Codd_erp_tf_01(dir_mne, subj_id_list,erp_folder, pref_epo,  group_name, exclude_eeg, baseline,   pref_erp = None, overwrite_ind = False, overwrite_group=False):
    # dir_mne = path the the MNE analysis
    # subj_id_list = of the form  np.arange(0,36) to process sub-00, ..., sub-35
    # erp_folder = ERP files  are in dir_mne / sub-nn / erp_fodler
    # pref_epo = prefix for epochs files, like 'erp_2-20Hz' ...
    # pref_erp = prefix for output evoked files if necessary; ex: sub-xx.prestim150_var_p_mmn-ave.fif if 'prestim_150'
    # group name = dir_mne/group_name/folder_name/group_name.pref_evoked-ave.fif: 'group' , 'group34' ...
    # exclude_eeg = dictionary with a list of sensors to be interpolate d(or rejected) for each subject {'sub-00': [], sub-01: ['Fp1', 'Cz'], ...}
    # baseline = (-0.15, 0) for instance (in seconds)
     # overwrite = boolean
   
    # ----- INDIV LEVEL ----------------------------------------------
    for i_su in subj_id_list:
        subj='sub-{:02}'.format(i_su) 
        dir_imne = os.path.join(dir_mne, subj, erp_folder) #individual mne path
    
        
        if overwrite_ind: # we  compute individual ERPs (std, dev ad difference in var_p, var_c)  
            
            #### ---- Load clean epochs (2-20 Hz)
            f_epochs=os.path.join(dir_imne, '{}.{}-epo.fif'.format(subj, pref_epo))
            epochs = read_epochs(f_epochs) 
            epochs.info['bads']=exclude_eeg[subj]
            epochs.interpolate_bads()
            
            #### Compute std, dev and mmn ERPs, and save *-ave.fif ----
            erp_list = ['var_c/dev','var_c/std' ] # 1 - 2 <=> dev - std 
            output_name = 'var_c/mmn'
            evokeds = evokeds_indiv_diff(dir_imne, subj,epochs, baseline, erp_list, output_name,  neg = False, plot = None)
            erp_list.append(output_name)
            pref_evo_list=list()
            for idx, erp_name  in enumerate (erp_list):
                new_name = erp_name.replace('/', '_')
                print(new_name)
                if pref_erp:
                    new_name = f'{pref_erp}_{new_name}'
                f_out = os.path.join(dir_imne, '{}.{}-ave.fif'.format(subj, new_name))
                write_evokeds(f_out, evokeds[idx])
                pref_evo_list.append(new_name)
            
            erp_list = ['var_p/dev','var_p/std' ] # 1 - 2 <=> dev - std 
            output_name = 'var_p/mmn'
            evokeds = evokeds_indiv_diff(dir_imne, subj,epochs, baseline, erp_list, output_name,  neg = False, plot = None)
            erp_list.append(output_name)
            for idx, erp_name  in enumerate (erp_list):
                new_name = erp_name.replace('/', '_')
                print(new_name)
                if pref_erp:
                    new_name = f'{pref_erp}_{new_name}'
                f_out = os.path.join(dir_imne, '{}.{}-ave.fif'.format(subj, new_name))
                write_evokeds(f_out, evokeds[idx])
                pref_evo_list.append(new_name)

        else: 
            erp_list = [ 'var_c/dev','var_c/std','var_c/mmn', 'var_p/dev','var_p/std','var_p/mmn']
            pref_evo_list=list()
            for idx, erp_name  in enumerate (erp_list):
                new_name = erp_name.replace('/', '_')
                if pref_erp:
                    new_name = f'{pref_erp}_{new_name}'
                pref_evo_list.append(new_name)
                
                f_erp = os.path.join(dir_imne, '{}.{}-ave.fif'.format(subj, new_name))
    
                if not os.path.exists(f_erp):
                    print('EVOKED FILES NOT FOUND for {}'.format(subj))
                    return
      
            


    # ----- GROUP LEVEL ----------------------------------------------
    for pref_evoked in pref_evo_list:
        if overwrite_group:
            group_list, group_avg = evokeds_group_average(dir_mne, erp_folder, subj_id_list , group_name,  pref_evoked,  save= True)
        else:
            group_list, group_avg = evokeds_group_average(dir_mne, erp_folder, subj_id_list, group_name , pref_evoked,  save= False)
        
    return group_list, group_avg 
#    
   
