#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Home-made Functions for EEG-MEG Plots
Created on Tue Mar  5 09:52:07 2019

@author: Françoise Lecaignard, francoise.lecaignard@inserm.fr
"""

import numpy as np             # convention d'import
import os                       # path.join for file names
import matplotlib.pyplot as plt # from the MNE examples
from bisect import bisect

import matplotlib.patches as patches  # noqa



from mne import Epochs, Annotations, pick_types, find_events, write_events, read_events
from mne.channels import read_layout
from mne.io import concatenate_raws, read_raw_brainvision, read_raw_fif, find_edf_events
from mne.datasets import eegbci
from mne.decoding import CSP
from mne.viz import plot_events, plot_ica_overlay
from mne.preprocessing import ICA, read_ica,  find_eog_events


import sys      # path to my own functions
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/autoreject')

from autoreject import  RejectLog, set_matplotlib_defaults  
#from autoreject.viz import plot_epochs



print(__doc__)

import sys      # path to my own functions
#sys.path.append('/Users/francoise/Documents/Projets/ContextOdd/EEG_Data/Temp_AnaEEG/python')



#############################################################################################
##  o*o*o*o*o*o*o*o*o*o*o*o*o*o* plot histograms of sensor trheshold     *o*o*o*o*o*o*o*o*o*o*o*o*o*
#############################################################################################


def plot_autoreject_threshold_hist(threshes, mod, xlim):
    #histogram of channel-level thresholds as inferred by AutoReject
    #this code comes from mne auyoreject github example
    
    #threshes = output of compute_thresholds
    # mod = 'eeg' or 'meg'
    # xlim : could be (45, 125)  for eeg, (400, 900) for meg
    #threshes = compute_thresholds(epochs,  method='random_search', random_state=42, augment=False, verbose='progressbar')

 
    if mod == 'eeg':
        unit = r'uV' #r'fT/cm'
        scaling = 1e6 #1e13
        
    else:
        if mod == 'meg':
            unit = r'fT/cm'
            scaling = 1e13

    plt.figure(figsize=(6, 5))
    plt.tick_params(axis='x', which='both', bottom='off', top='off')
    plt.tick_params(axis='y', which='both', left='off', right='off')
    
    plt.hist(scaling * np.array(list(threshes.values())), 30,
             color='g', alpha=0.4)
    plt.xlabel('Threshold (%s)' % unit)
    plt.ylabel('Number of sensors')
    plt.xlim(xlim)
    plt.tight_layout()
    plt.show()
 
#############################################################################################
##  o*o*o*o*o*o*o*o*o*o*o*o*o*o* plot reject_log.labels = Nsensors * Nepochs   *o*o*o*o*o*o*o*o*o*o*o*o*o*
#############################################################################################
#this code comes from mne auyoreject github example


def   plot_autoreject_labels(reject_log, title):
    
    set_matplotlib_defaults(plt, style='seaborn-white')
    
    labels = reject_log.labels.transpose()

    plt.matshow(labels, cmap=plt.get_cmap('viridis'))
    plt.yticks(range(len(reject_log.ch_names)), reject_log.ch_names)
#    plt.xticks(range(labels.shape[1]), labels[1])
    plt.rcParams.update({'font.size':8})
    ax = plt.gca()
    plt.xlabel('Epochs')
    plt.ylabel('Sensors')
    plt.title(title)
    #plt.colorbar()
    plt.show()   
#############################################################################################
##  o*o*o*o*o*o*o*o*o*o*o*o*o*o*       DEBUG              *o*o*o*o*o*o*o*o*o*o*o*o*o*
#############################################################################################


if __name__== '__main__':
    
    ### debug raw_jump_detetc
    dir_mne='/sps/inter/crnl/users/pduret/ContextOdd/ContextOdd_Data/eMNE'
    i_su = 0
    subj='sub-{:02}'.format(i_su) 
    dir_imne = os.path.join(dir_mne, subj, 'preproc') #individual mne path
    f_raw_erp = os.path.join(dir_imne, '{}.preproc_2-20Hz.raw.fif'.format(subj)) 
    raw_erp = read_raw_fif(f_raw_erp, preload=True)

