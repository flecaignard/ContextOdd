#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Home-made Functions for EEG-MEG Plots
Created on Tue Mar  5 09:52:07 2019

@author: Françoise Lecaignard, francoise.lecaignard@inserm.fr
"""

import numpy as np             # convention d'import
import os                       # path.join for file names
import matplotlib.pyplot as plt # from the MNE examples
from bisect import bisect

from mne import Epochs, Annotations, pick_types, find_events, write_events, read_events
from mne.channels import read_layout
from mne.io import concatenate_raws, read_raw_brainvision, read_raw_fif, find_edf_events
from mne.datasets import eegbci
from mne.decoding import CSP
from mne.viz import plot_events, plot_ica_overlay
from mne.preprocessing import ICA, read_ica,  find_eog_events


import sys      # path to my own functions
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/autoreject')

from autoreject import  RejectLog
#from autoreject.viz import plot_epochs



print(__doc__)

import sys      # path to my own functions
#sys.path.append('/Users/francoise/Documents/Projets/ContextOdd/EEG_Data/Temp_AnaEEG/python')



#############################################################################################
##  o*o*o*o*o*o*o*o*o*o*o*o*o*o* epoching and epoch rejection      *o*o*o*o*o*o*o*o*o*o*o*o*o*
#############################################################################################

def plot_epochs_drop(epochs_orig, epochs_drop, event_id_plot, picks_plot, scalings):
    # Plot epochs with good ones in black, and bad ones in red
    # Epochs may have been created with some events, and here we want to plot some of them only
    # Ex : events = ('aud', 'vis') and we want to plot 'aud'
    #
    # epochs_orig= mne Epoch, correspond to the original version, prior to artifact rejection
    # epochs_drop = mne Epoch, correspond to the version resulting from artifact rejection
    # event_id_plot = subset of event to be plotted
    # picks  = subset of sensors to be plotted (speed concern)
    # scalings   = dictionary
    # title = str
    
    nr_epochs = len(epochs_orig)
    
    # Build up a Pos array (!) : three columns, sample - event_id - rej (0/1)
    # hum
    recap_events = np.zeros((nr_epochs,3))
    recap_events[:,0] = epochs_orig.events[:,0] # samples
    recap_events[:,1] = epochs_orig.events[:,2] # event_id
    rej_idxs = [idx for idx, sample in enumerate(epochs_orig.events[:,0]) if sample not in epochs_drop.events[:,0]]
    recap_events[rej_idxs,2] = 1
    
    
    # Restrict epochs to current event_id_plot
    epochs_orig_red = epochs_orig.copy()
    
    eventidplot_rm_idxs = [idx for idx, eve in enumerate(recap_events[:,1]) if eve  not in event_id_plot]
    epochs_orig_red.drop(eventidplot_rm_idxs)
    epochs_orig_red.drop_bad()
    epochs_orig_red.pick_channels(picks_plot)
    
    recap_events_red = np.delete(recap_events,eventidplot_rm_idxs, axis=0) # supp raws
    
#    eventidplot_drop_idxs = [idx_orig for idx_orig in eventidplot_orig_idxs if recap_events[idx_orig,2] == 0  ]
#    epochs_drop_red.drop(eventidplot_drop_idxs)
#    epochs_drop_red.drop_bad()
    
    nr_epochs_red = len(epochs_orig_red)
    nr_chan_red = epochs_orig_red.info['nchan']

    bad_epochs=np.zeros((nr_epochs_red,)) #default: good =0, bad=1
    labels = np.zeros((nr_epochs_red, nr_chan_red)) 
    
    bad_idxs = [idx for idx, drop in enumerate(recap_events_red[:,2]) if drop == 1]
    bad_epochs[bad_idxs] = 1

     

#    print('  Dropped {} epochs out of {}'.format(len(epochs_orig.events)-len(epochs_rej.events),len(epochs_orig.events) ))
#    print('  Dropped %0.1f%% of epochs' % (epochs_rej.drop_log_stats(),))
#    

    reject_log = RejectLog(bad_epochs,labels, epochs_orig_red.info['ch_names'])
    reject_log.plot_epochs( epochs_orig_red,  scalings=scalings ) #, n_epochs=20, n_channels=32, title='pouet')
    
    return epochs_orig_red



#############################################################################################
##  o*o*o*o*o*o*o*o*o*o*o*o*o*o*       DEBUG              *o*o*o*o*o*o*o*o*o*o*o*o*o*
#############################################################################################


if __name__== '__main__':
    
    ### debug raw_jump_detetc
    dir_mne='/sps/inter/crnl/users/pduret/ContextOdd/ContextOdd_Data/eMNE'
    i_su = 0
    subj='sub-{:02}'.format(i_su) 
    dir_imne = os.path.join(dir_mne, subj, 'preproc') #individual mne path
    f_raw_erp = os.path.join(dir_imne, '{}.preproc_2-20Hz.raw.fif'.format(subj)) 
    raw_erp = read_raw_fif(f_raw_erp, preload=True)

