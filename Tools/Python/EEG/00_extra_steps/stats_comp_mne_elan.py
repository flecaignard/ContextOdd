#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
========================================================================================
Perform ERP stats with Elan to guide MNE threshold definition in spatio-temporal clusters  
========================================================================================
This analysis rests on dev, std, mmn ERPs (ana_erp_typical_mismatch)
We export *-evo.fif into mat files
Using Matlab, we create *.p files
Using bash commands, we do the Elan stats
Output Folder
       =====> sub-XX/preproc/sub-XX.clean_2-20Hz_ica.raw.fif
       =====> sub-XX/erp_1-40Hz/sub-XX.STD-STD-DEV_1-40Hz_var_p_dev-ave.fif
       =====> sub-XX/erp_1-40Hz/sub-XX.STD-STD-DEV_1-40Hz_var_c_dev-ave.fif

    @author: Françoise Lecaignard, francoise.lecaignard@inserm.fr
"""


###############################################################################
import numpy as np             # convention d'import
import os                       # path.join for file names
import matplotlib.pyplot as plt # from the MNE examples
import pickle
import sys
# path to my own functions
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Python/EEG')
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Python/EEG/02_erp')



from mne import Epochs, pick_types, read_events, read_epochs, find_layout
from mne import write_evokeds, read_evokeds
from mne import get_config, set_config
type(get_config('MNE_BROWSE_RAW_SIZE'))
set_config('MNE_BROWSE_RAW_SIZE', '20.0,10.0')
from mne.preprocessing import read_ica
from mne.viz import plot_topomap,plot_compare_evokeds, tight_layout

from mne.io import read_raw_fif

#from fun_NN_template import  ...
from fun_util_epoch import import_epochs
from fun_plot_erp import evokeds_plot, evokeds_plot_all_group
from fun_util_erp import evokeds_group_average,  evokeds_indiv_diff
from fun_util_eeg import print_step_label
from fun_stats_erp import view_significant_spatiotemp_clusters
from fun_codd_stats_erp import codd_spatiotemp_clusters_1samp

################################################################################
# Input Parameters 
################################################################################

# Paths ---------------------------- ---------------------------- ----------------------------
dir_mne = '/sps/cermep/cermep/experiments/DCM/ContextOdd/Ana_MNE'

erp_folder = 'erp_2-20Hz'
elan_folder = 'stats_elan' # within erp_folder

evo_list = ['2-20Hz_var_p_std','2-20Hz_var_p_dev','2-20Hz_var_p_mmn',  '2-20Hz_var_c_std','2-20Hz_var_c_dev', '2-20Hz_var_c_mmn' ] 
pref_erp = '2-20Hz'  # ===> sub-00.2-20Hz_var_p_dev-ave.fif 


################################################################################
# Let's start!
################################################################################

#%%
###############################################################################
# Export *-ave.fif in *.mat files
###############################################################################
import scipy.io as sio 

subj_id_list = np.arange(0,36) # 0 to 35, to include sub-00 to sub-35


for i_su in subj_id_list:
    subj='sub-{:02}'.format(i_su) 
    dir_imne = os.path.join(dir_mne, subj, erp_folder) #individual mne path
    dir_ielan = os.path.join(dir_imne, elan_folder) 
    if not os.path.exists(dir_ielan):
        os.makedirs(dir_ielan)

    for name in evo_list:    
        f_evo = os.path.join(dir_imne, '{}.{}-ave.fif'.format(subj, name))
        evoked=read_evokeds(f_evo, condition=0)
        matinfo = {'d':evoked.data, \
                   'sens': evoked.ch_names, \
                   'times': evoked.times, \
                   'sfreq': evoked.info['sfreq']  }
        f_mat = os.path.join(dir_ielan, '{}.{}.mat'.format(subj, name))
        sio.savemat(f_mat, matinfo)
       
        
# export group avg
subj='group36'
dir_imne = os.path.join(dir_mne, subj, erp_folder) #individual mne path
dir_ielan = os.path.join(dir_imne, elan_folder) 
if not os.path.exists(dir_ielan):
    os.makedirs(dir_ielan)

for name in evo_list:    
    f_evo = os.path.join(dir_imne, '{}.{}-ave.fif'.format(subj, name))
    evoked=read_evokeds(f_evo, condition=0)
    matinfo = {'d':evoked.data, \
               'sens': evoked.ch_names, \
               'times': evoked.times, \
               'sfreq': evoked.info['sfreq']  }
    f_mat = os.path.join(dir_ielan, '{}.{}.mat'.format(subj, name))
    sio.savemat(f_mat, matinfo)        


  
