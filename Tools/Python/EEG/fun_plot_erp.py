#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Home-made Functions for EEG-MEG Plots
Created on Tue Mar  5 09:52:07 2019

@author: Françoise Lecaignard, francoise.lecaignard@inserm.fr
"""

import numpy as np             # convention d'import
import os                       # path.join for file names
import matplotlib.pyplot as plt # from the MNE examples
from bisect import bisect

from mne import Epochs, Annotations, pick_types, find_events, write_events, read_events,read_evokeds
from mne.channels import read_layout
from mne.io import concatenate_raws, read_raw_brainvision, read_raw_fif, find_edf_events
from mne.datasets import eegbci
from mne.decoding import CSP
from mne.viz import plot_events, plot_ica_overlay, plot_evoked_topo,plot_compare_evokeds
from mne.preprocessing import ICA, read_ica,  find_eog_events


import sys      # path to my own functions
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/autoreject')




print(__doc__)

import sys      # path to my own functions
#sys.path.append('/Users/francoise/Documents/Projets/ContextOdd/EEG_Data/Temp_AnaEEG/python')


#############################################################################################
##  o*o*o*o*o*o*o*o*o*o*o*o*o*o*       Plot evoked responses      *o*o*o*o*o*o*o*o*o*o*o*o*o*
#############################################################################################

def evokeds_plot(evokeds,  plot_type, neg=True, vline=None, colors = None):
    #evokeds = list of evoked object
    # plot_type : list of plots. ex: ['traces', 'topo']
    # neg = True to plot negative peaks upwards :-)
    print(colors)
    if neg:
        for i_ev in np.arange(len(evokeds)):
                evokeds[i_ev] = evokeds[i_ev].__neg__()
    if 'traces' in plot_type:
        plot_evoked_topo(evokeds, vline=vline, color=colors)
    if 'GFP' in plot_type:
        evokeds_gfp = dict()
        for i_ev in np.arange(len(evokeds)):
            print(evokeds[i_ev].comment)
#            evokeds_gfp[evokeds[i_ev].comment] = evokeds[i_ev]
            name = evokeds[i_ev].comment
            evokeds_gfp[f'cond_{i_ev} - {name}'] = evokeds[i_ev] #here i don't understand: if i just say evokeds_gfp[name], then cond2 and cond3 are exchnaged ???
        print(evokeds_gfp)
        plot_compare_evokeds(evokeds_gfp, title='GFP', picks=None,colors=colors)

    if 'image' in plot_type:
        for i_ev in np.arange(len(evokeds)):
                evokeds[i_ev].plot_image()
    if 'topo' in plot_type:
        for i_ev in np.arange(len(evokeds)):
            evokeds[i_ev].plot_topomap(times='interactive', vmin=-2, vmax=2, title=evokeds[i_ev].comment)
#            evokeds[i_ev].plot(window_title='{}, {}'.format('group', evokeds[i_ev].comment),scalings = dict(eeg=1e6), ylim = dict(eeg=[-5,5]) )



#############################################################################################
##  o*o*o*o*o      Plot multiple ERPs of one subject (one figure per subject)      *o*o*o*o*o*o
#############################################################################################

def evokeds_plot_all_group(dir_mne, folder_name, subj_id_list, pref_evoked_list,   plot_type, neg=True, colors = None):
    #`dir_mne = pathto MNE analysis
    # folder_name : all evokeds files are in dir_mne/sub-XX/folder_name
    # pref_evoked_list : list of the evoked files = ['erp1', 'erp24 , 'erp3'] will plot dir_mne/sub-XX/folder_name/sub-nn.erp1-ave.fif etc 
    # plot_type : list of plots. ex: ['traces', 'topo']
    # neg = True to plot negative peaks upwards :-)
        # group_evoked=
    for i_su in subj_id_list:
        subj='sub-{:02}'.format(i_su) 
        dir_imne = os.path.join(dir_mne, subj, folder_name) #individual mne path
        evo_list=list()
        for idx, erp_name  in enumerate (pref_evoked_list):
            evo_list.append(os.path.join(dir_imne, '{}.{}-ave.fif'.format(subj, erp_name)))
        
        print(evo_list)
        evokeds=[read_evokeds(f_evo, condition=0) for f_evo in evo_list] # condition =0 because there is one evoked object per file
        print(evokeds)
        if neg:
            for i_ev in np.arange(len(evokeds)):
                evokeds[i_ev] = evokeds[i_ev].__neg__()
        if 'traces' in plot_type:
            plot_evoked_topo(evokeds, title=subj,scalings=dict(eeg=1e6), ylim=dict(eeg=[-5,5]), color=colors)
        if 'GFP' in plot_type:
            evokeds_plot(evokeds,  ['GFP'], neg=neg, vline=[0], colors = colors)  
            
    
        if 'image' in plot_type:
            for i_ev in np.arange(len(evokeds)):
                evokeds[i_ev].plot_image()
        if 'topo' in plot_type:
            for i_ev in np.arange(len(evokeds)):
                evokeds[i_ev].plot_topomap(times='interactive',title=subj, vmin=-1.0, vmax=1.0)
                #evokeds[i_ev].plot(window_title='{}, {}'.format('group', evokeds[i_ev].comment),scalings = dict(eeg=1e6), ylim = dict(eeg=[-5,5]) )

#############################################################################################
##  o*o*o*o*o      Plot multiple ERPs of different subject (one figure overlaying subjects      *o*o*o*o*o*o
#############################################################################################
# We could use MNE plot_evoked-compare : this provides a single figure per sensor, or the GFP or mean across sensors
# Here, we want to overlay ERPs (from multiple subjects) using the scalp view

def evokeds_plot_overlay_across_subjects(dir_mne, erp_folder, plot_list, subj_id_list, group_name=None, plot_type=['traces']):
    #dir_mne = string, path the to MNE analysis (BIDS architecture): /dir_mne/sub-XX
    #erp_folder= string, path to individual ERPs folder :  /dir_mne/sub-XX/erp_folder/
    #plot_list = list of prefixe for evoked files : /dir_mne/sub-XX/erp_folder/sub-XX.pref-evo.fif
    #subj_id_list = numpy array of subject indices (np.arange(2,6))
    #group_name = None if not to be plotted, group name otherwise : /dir_mne/group_name/erp_folder/group_name.pref-evo.fif
    #plot_type = list of plots, in 'traces', 'GFP', 'image', 'topo'


    evokeds=[]
    for cond in plot_list:
        
        if group_name:
        
            dir_imne_erp = os.path.join(dir_mne, group_name, erp_folder)
            f_evo = os.path.join(dir_imne_erp, '{}.{}-ave.fif'.format(group_name, cond))
            evokeds.append(read_evokeds(f_evo, condition=0))
            evokeds[-1].comment=f'{group_name} - {cond}'
        
        for i_su in subj_id_list:
            subj='sub-{:02}'.format(i_su) 
            dir_imne_erp = os.path.join(dir_mne, subj, erp_folder)
            f_evo = os.path.join(dir_imne_erp, '{}.{}-ave.fif'.format(subj, cond))
            evokeds.append(read_evokeds(f_evo, condition=0))
            evokeds[-1].comment=f'{subj} - {cond}'
    
    evokeds_plot(evokeds, plot_type, neg=True,vline=[ 0])    #
#############################################################################################
##  o*o*o*o*o*o*o*o*o*o*o*o*o*o*       Plot two evoked with one being longer than the other, crop to align   *o*o*o*o*o*o*o*o*o*o*o*o*o*
#############################################################################################

def evokeds_plot_two_overlays_with_crop(evoked_short, evoked_long, tmin, tmax , title=None, neg=True, colors = None):
    #`dir_mne = pathto MNE analysis
    # folder_name : all evokeds files are in dir_mne/sub-XX/folder_name
    # pref_evoked_list : list of the evoked files = ['erp1', 'erp24 , 'erp3'] will plot dir_mne/sub-XX/folder_name/sub-nn.erp1-ave.fif etc 
    # plot_type : list of plots. ex: ['traces', 'topo']
    # neg = True to plot negative peaks upwards :-)
        # group_evoked=
   
    evoked_long.crop(tmin, tmax)
    if neg:
            evoked_long = evoked_long.__neg__()
            evoked_short = evoked_short.__neg__()
    evoked_long.comment=f'{evoked_short.comment}_cropped'
    plot_evoked_topo([evoked_short, evoked_long], title=title, scalings=dict(eeg=1e6), ylim=dict(eeg=[-5,5]), color=colors)
        
#############################################################################################
##  o*o*o*o*o*o*o*o*o*o*o*o*o*o*       DEBUG              *o*o*o*o*o*o*o*o*o*o*o*o*o*
#############################################################################################


#if __name__== '__main__':
#    
#    ### debug raw_jump_detetc
#    dir_mne='/sps/inter/crnl/users/pduret/ContextOdd/ContextOdd_Data/eMNE'
#    i_su = 0
#    subj='sub-{:02}'.format(i_su) 
#    dir_imne = os.path.join(dir_mne, subj, 'preproc') #individual mne path
#    f_raw_erp = os.path.join(dir_imne, '{}.preproc_2-20Hz.raw.fif'.format(subj)) 
#    raw_erp = read_raw_fif(f_raw_erp, preload=True)

