#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
==================================================================================
UTIL SCRIPT - View data at a particular latency, and compute peak-to-peak amplitude
==================================================================================

Single  subject


@author: Françoise Lecaignard, francoise.lecaignard@inserm.fr

Created on Mon Apr  1 10:21:46 2019

"""



###############################################################################
import numpy as np             # convention d'import
import os
import matplotlib.pyplot as plt # from the MNE examples
import sys      # path to my own functions
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Python/EEG')

from mne.io import read_raw_fif
from mne import get_config, set_config
type(get_config('MNE_BROWSE_RAW_SIZE'))
set_config('MNE_BROWSE_RAW_SIZE', '20.0,10.0')
import pickle
from mne import pick_types

from fun_util_eeg import peak_to_peak_raw,find_event_at_a_latency
################################################################################
# Define Input Parameters -  Constant Variables
################################################################################

# Path ----------------------------
dir_mne = '/sps/cermep/cermep/experiments/DCM/ContextOdd/Ana_MNE'
rawfile_folder = 'preproc'


# ----------- enter subject:
i_su = 19
# ----------- enter filename for continuous data:
lf, hf = 1,40
pref_raw = f'preproc_{lf}-{hf}Hz_ica'

subj='sub-{:02}'.format(i_su) 
dir_imne_raw = os.path.join(dir_mne, subj, rawfile_folder) #individual mne path
f_raw = os.path.join(dir_imne_raw, f'{subj}.{pref_raw}.raw.fif') # raw continuous data: downsampled,  filtered   and ica-corrected

# ----------- enter latency (seconds):
lat = 2800.13 #1032.47 # sec
# ----------- enter starting latency (negative value, in seconds):
pre_lat = -0.5
# ----------- enter ending latency (positive value, in seconds):
post_lat = 0.5
#  ----------- sensor subset
pick_chan = []

plot = False

# ----------- enter event files if you want to get the closest event to current latency:
f_eve=[]
f_eve = os.path.join(dir_imne_raw, f'{subj}.good_bad_events.pkl') 
print(f_eve)
pkl= True

################################################################################
# Let's start!
################################################################################




raw = read_raw_fif(f_raw, preload=True)
if pick_chan:
    rawmini = raw.pick_channels(pick_chan)
else:
    rawmini = raw


    
# peak-to-peak value at a particular latency
picks = pick_types(rawmini.info, eeg=True, stim=True,exclude=())

peak_to_peak_raw(rawmini, lat, pre_lat, post_lat, picks, thresh=None, plot=plot)
print(raw.time_as_index(lat))
#
#
## find closest event
## events = from pickle file (above): be careful that epoch index is relative to surviving epochs !
#
if os.path.isfile(f_eve):
    print('Epoch corresponding to current latency:')
    if pkl:
        with  open(f_eve, 'rb') as input:
            a = pickle.load(input)
            pkl_events = a[0]
    
    s, idx, event_info = find_event_at_a_latency(lat, rawmini, pkl_events)
    print(f'Latency {lat} sec <=> sample {s}')
    print(f'Within epoch #{idx}')
    print(f'events[{idx}] = {pkl_events[idx]}')

#        
