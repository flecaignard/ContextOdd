#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
===============================================
Step NN - What it is about
===============================================

Short description
Includes output description

Created on Mon Feb 11 17:57:17 2019

@author: Françoise Lecaignard, francoise.lecaignard@inserm.fr
"""



###############################################################################
import numpy as np             # convention d'import
import os
import sys      # path to my own functions
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Python/EEG')
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/BIDS/mne-bids')



#from fun_NN_template import  ...

################################################################################
# Input Parameters 
################################################################################

# Paths ----------------------------
dir_mne = '/sps/cermep/cermep/experiments/DCM/ContextOdd/Ana_MNE'



################################################################################
# Define Constant Variables
################################################################################


# Plot raw data
plot=False
################################################################################
# Let's start!
################################################################################

# subj_id_list -----------------------
# test on one subject
subj_id_list = np.arange(0,1) # sub-00
# applyto all
subj_id_list = np.arange(5,36) # 0 to 35, to include sub-00 to sub-35


#
for i_su in subj_id_list:
    subj='sub-{:02}'.format(i_su) 
    
    