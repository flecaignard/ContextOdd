#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 12 10:17:20 2019

@author: Francoise Lecaignard, francoise.lecaignard@inserm.fr
"""

### Test  MNE-Bids - simple script

import os                       # path.join for file names
from mne.io import read_raw_brainvision
from mne_bids import write_raw_bids,make_bids_basename
import sys      # path to my own functions
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Python/EEG')


dir_raw_acq='/sps/cermep/cermep/experiments/DCM/ContextOdd/Raw_Data/EEG/20180406_BREME'
dir_raw_acq='/sps/cermep/cermep/experiments/DCM/ContextOdd/Raw_Data/EEG/20180511_BARTH'
dir_bids='/sps/cermep/cermep/experiments/DCM/ContextOdd/Raw_Data/debug_bids'

sub='BARTH'
sub_id=9
for run in [1,2]:
    f_in = os.path.join(dir_raw_acq,'{}_{:04}.vhdr'.format(sub, run))
    raw = read_raw_brainvision(f_in, preload=False )
    bids_basename = make_bids_basename(subject='{:02}'.format(sub_id),  task=None, run=run)
    print(bids_basename)
    write_raw_bids(raw, bids_basename, output_path=dir_bids, overwrite=True)