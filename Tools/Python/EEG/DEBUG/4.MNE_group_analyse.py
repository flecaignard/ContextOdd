# -*- coding: utf-8 -*-
"""
Created on Wed Jan 23 15:02:17 2019

@author: Melodie
"""

# =============================================================================
# import# 
# =============================================================================
import os
import mne
import matplotlib.pyplot as plt
import numpy as np
from mne import Epochs, io, pick_types, find_events
from mne.viz import plot_compare_evokeds
from mne.channels import find_ch_connectivity
from mne.stats import spatio_temporal_cluster_test
from mne.viz import plot_topomap, plot_layout
from mpl_toolkits.axes_grid1 import make_axes_locatable
from mne.viz import plot_compare_evokeds
import matplotlib.pyplot as plt



# =============================================================================
# path and subjects
# =============================================================================
MNE_path = 'H:/EnfantsSains/Manip2/PROCESS_DATA/process_data_mne/'
subjects = ['S01','S02','S03','S05','S06','S07','S08','S09','S10','S11','S12','S13','S14','S15','S16','S17','S18','S19'];


# =============================================================================
# create group evoked
# =============================================================================
#all_evokeds = [list() for _ in range(8)]  # Container for all the categories
#
#for subj in range(len(subjects)): 
#    sujname = subjects[subj]
##    evname =  os.path.join(MNE_path, '5.{}-ave.fif'.format(sujname))
#    evname =  os.path.join(MNE_path, '5.{}-with_BL_corr-ave.fif'.format(sujname))
#    evokeds = mne.read_evokeds(evname)
#    
#    for idx, evoked in enumerate(evokeds):
#        all_evokeds[idx].append(evoked)  # Insert to the container
#
#for idx, evokeds in enumerate(all_evokeds):
#    all_evokeds[idx] = mne.combine_evoked(evokeds, 'equal') #.plot_joint()  # Combine subjects
#
##fngrandav = os.path.join(MNE_path, 'grand_average-ave.fif')
#fngrandav = os.path.join(MNE_path, 'grand_average-with_BL_corr-ave.fif')
#mne.evoked.write_evokeds(fngrandav,all_evokeds)

# =============================================================================
# load all_evokeds and sep by condition 
# =============================================================================

#fngrandav = os.path.join(MNE_path, 'grand_average-ave.fif')
fngrandav = os.path.join(MNE_path, 'grand_average-with_BL_corr-ave.fif')
all_evokeds = mne.read_evokeds(fngrandav)

evoked_T_C4 = all_evokeds[0]
evoked_NT_C4 = all_evokeds[1] 
evoked_T_IM = all_evokeds[2]
evoked_NT_IM = all_evokeds[3] 
evoked_T_AR = all_evokeds[4]
evoked_NT_AR = all_evokeds[5] 
evoked_T = all_evokeds[6] 
evoked_NT = all_evokeds[7]


# =============================================================================
# correction du signal 
# =============================================================================
#my_data_T = evoked_T_IM.data
#my_data_T=my_data_T*1000000
#my_data_NT = evoked_NT_IM.data
#my_data_NT=my_data_NT*1000000
#
#mean_T = np.mean(my_data_T,axis = 1)
#mean_NT = np.mean(my_data_NT,axis = 1)
#nb_T = evoked_T_IM.nave
#nb_NT = evoked_NT_IM.nave
#mean_tot = (mean_T*nb_T + mean_NT*nb_NT)/(nb_T+nb_NT)
#
#mean_expended = np.outer(mean_tot, np.ones(801))
#my_new_data_T = my_data_T - mean_expended
#my_new_data_NT = my_data_NT-mean_expended
#
#my_data_T_mean = np.mean(my_data_T,axis = 0)
#my_data_NT_mean = np.mean(my_data_NT,axis = 0)
#my_new_data_T_mean = np.mean(my_new_data_T,axis = 0)
#my_new_data_NT_mean = np.mean(my_new_data_NT,axis = 0)
#
#plt.plot(my_data_T_mean)
#plt.plot(my_data_NT_mean)
#plt.plot(my_new_data_T_mean)
#plt.plot(my_new_data_NT_mean)


# =============================================================================
# plot_joint 
# =============================================================================

#topomap_args= {'head_pos' :dict(center=(0.45,0.65),scale=(0.75,0.65))}
#head_pos=head_pos=dict(center=(0.45,0.65),scale=(0.75,0.65))

ylim = dict(eeg=[-5, 10])
ts_args = dict(ylim=ylim)

posit=dict(center=(0,0),scale=(0.2,0.2))
topomap_args = dict(head_pos = posit)


evoked_T.plot_joint(title='Targets all games', times=[.08, .2,.32],ts_args = ts_args)#,topomap_args= topomap_args)
evoked_T_C4.plot_joint(title='Targets C4', times=[.08, .2,.32],ts_args = ts_args)
evoked_T_IM.plot_joint(title='Targets IM', times=[.08, .2,.32],ts_args = ts_args)
evoked_T_AR.plot_joint(title='Targets AR', times=[.08, .2,.32],ts_args = ts_args)

evoked_NT.plot_joint(title='Non Targets all games', times=[.08, .2,.32])
evoked_NT_C4.plot_joint(title='Non Targets C4', times=[.08, .2,.32])
evoked_NT_IM.plot_joint(title='Non Targets IM', times=[.08, .2,.32])
evoked_NT_AR.plot_joint(title='Non Targets AR', times=[.08, .2,.32])



# =============================================================================
# plot_compare_evokeds
# =============================================================================
title = 'All the games'
ch_inds = np.array([0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15])
colors = {"Targets": "green", "NoTargets": 'red'}
evokedss = {'Targets' : evoked_T, 
            'NoTargets' :evoked_NT}
plot_compare_evokeds(evokedss, title=title, picks=ch_inds,colors=colors, show=False, split_legend=True, truncate_yaxis='max_ticks')



#all the games on the same graph
evoked_dict = dict()
evoked_dict['T/C4'] = evoked_T_C4
evoked_dict['NT/C4'] = evoked_NT_C4
evoked_dict['T/IM'] = evoked_T_IM
evoked_dict['NT/IM'] = evoked_NT_IM
evoked_dict['T/AR'] = evoked_T_AR
evoked_dict['NT/AR'] = evoked_NT_AR
print(evoked_dict)

colors = dict(T="Green", NT="Red")
linestyles = dict(C4='-', IM='--', AR=':')
ch_inds = np.array([0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15])
title = ''
fig = mne.viz.plot_compare_evokeds(evoked_dict, picks=ch_inds, colors=colors,
                             linestyles=linestyles, split_legend=True, title = title)



# =============================================================================
# plot_evoked_topo
# =============================================================================
#sujname = subjects[subj]
#evname =  os.path.join(MNE_path, '5.{}-TESTave.fif'.format(sujname))
#evokeds = mne.read_evokeds(evname)
#    
#evoked_T = evokeds[6] 
#evoked_NT = evokeds[7]



title = 'all games'
all_evokeds=[evoked_T,evoked_NT]
mne.viz.plot_evoked_topo(all_evokeds,title = title)
                        
title = 'Connecticut4'
all_evokeds=[evoked_T_C4,evoked_NT_C4]
mne.viz.plot_evoked_topo(all_evokeds,title = title)

title = 'IceMemory'
all_evokeds=[evoked_T_IM,evoked_NT_IM]
mne.viz.plot_evoked_topo(all_evokeds,title = title)

title = 'Amageddon'
all_evokeds=[evoked_T_AR,evoked_NT_AR]
mne.viz.plot_evoked_topo(all_evokeds,title = title)



 
# =============================================================================
# spatio_temporal_cluster_test
# =============================================================================

#create the matrix for the test all the subjects all the games, test on 0 :500 ms
list_T = list()
list_NT = list()
for subj in range(len(subjects)): 
    sujname = subjects[subj]
    evname =  os.path.join(MNE_path, '5.{}-ave.fif'.format(sujname))
    evokeds = mne.read_evokeds(evname)
    mtx_evoked_T = np.expand_dims(evokeds[6].data ,2)
    mtx_evoked_T = np.delete(mtx_evoked_T, np.s_[0 : 200],1)
    mtx_evoked_T = np.delete(mtx_evoked_T, np.s_[501 : 601],1)
    mtx_evoked_NT = np.expand_dims(evokeds[7].data ,2)
    mtx_evoked_NT = np.delete(mtx_evoked_NT, np.s_[0 : 200],1)
    mtx_evoked_NT = np.delete(mtx_evoked_NT, np.s_[501 : 601],1)
    list_T.append(mtx_evoked_T)
    list_NT.append(mtx_evoked_NT)   

ev_T = np.concatenate(list_T, axis = 2)
ev_NT = np.concatenate(list_NT, axis = 2)

ev_T = np.transpose(ev_T, (2,1,0))
ev_NT = np.transpose(ev_NT, (2,1,0))  
X = [ev_T,ev_NT]


#connectivity
connectivity, ch_names = find_ch_connectivity(evoked_T.info, ch_type='eeg')
print(type(connectivity))
plt.imshow(connectivity.toarray(), cmap='gray', origin='lower', interpolation='nearest')
plt.xlabel('{} EEG sensors'.format(len(ch_names)))
plt.ylabel('{} EEG sensors'.format(len(ch_names)))
plt.title('Between-sensor adjacency')
 
#%%
#spatio_temporal_cluster_test
threshold =None # very high, but the test is quite sensitive on this data
p_accept = 0.05 # set family-wise p-value
cluster_stats = spatio_temporal_cluster_test(X, n_permutations=1000,threshold=threshold, tail=1,n_jobs=1,connectivity=connectivity)
T_obs, clusters, p_values, _ = cluster_stats
good_cluster_inds = np.where(p_values < p_accept)[0]


#visulalisation
colors = {"Targets": "green", "NoTargets": 'red'} # configure variables for visualization
pos = mne.find_layout(evoked_T.info,ch_type='eeg').pos # get sensor positions via layout

evoked_dict_T_NT = {'Targets' : evoked_T, # dictionnary T and NT for all sub 
            'NoTargets' :evoked_NT}


# loop over clusters
for i_clu, clu_idx in enumerate(good_cluster_inds):
    # unpack cluster information, get unique indices
    time_inds, space_inds = np.squeeze(clusters[clu_idx])
    ch_inds = np.unique(space_inds)
    time_inds = np.unique(time_inds)
    f_map = T_obs[time_inds, ...].mean(axis=0)
    time_indss = [i + 200 for i in time_inds]
    time_inds = np.asarray(time_indss)
    # get signals at the sensors contributing to the cluster
    sig_times = evoked_T.times[time_inds]
    # create spatial mask
    mask = np.zeros((f_map.shape[0], 1), dtype=bool)
    mask[ch_inds, :] = True
    # initialize figure
    fig, ax_topo = plt.subplots(1, 1, figsize=(10, 3))
    # plot average test statistic and mark significant sensors
    image, _ = plot_topomap(f_map,pos, mask=mask, axes=ax_topo, cmap='Reds',
                            vmin=np.min, vmax=np.max, show=False, 
                            head_pos=dict(center=(0.45,0.65),scale=(0.75,0.65)))
    # create additional axes (for ERF and colorbar)
    divider = make_axes_locatable(ax_topo)
    # add axes for colorbar
    ax_colorbar = divider.append_axes('right', size='5%', pad=0.05)
    plt.colorbar(image, cax=ax_colorbar)
    ax_topo.set_xlabel('Averaged F-map ({:0.3f} - {:0.3f} s)'.format(*sig_times[[0, -1]]))
    # add new axis for time courses and plot time courses
    ax_signals = divider.append_axes('right', size='300%', pad=1.2)
    title = 'Cluster #{0}, {1} sensor'.format(i_clu+1, len(ch_inds))
    if len(ch_inds) > 1:
        title += "s (mean)"
    plot_compare_evokeds(evoked_dict_T_NT, title=title, picks=ch_inds, axes=ax_signals,colors=colors, show=False,split_legend=True ,show_legend = False, truncate_yaxis='max_ticks')
    # plot temporal cluster extent   
    ymin, ymax = ax_signals.get_ylim()
    ax_signals.fill_betweenx((ymin, ymax), sig_times[0], sig_times[-1],color='orange', alpha=0.3)
    # clean up viz
    mne.viz.tight_layout(fig=fig)
    fig.subplots_adjust(bottom=.05)
    plt.show()
    
#    figname = (MNE_path + '7.Cluster_3.png')
#    fig.savefig(figname)   
#    plt.close(fig)
    
  
#%%


# =============================================================================
# figure cluster 2 
# =============================================================================

clu_idx = 1
time_inds, space_inds = np.squeeze(clusters[clu_idx])
ch_inds = np.unique(space_inds)
time_inds = np.unique(time_inds)
ch_name = evokeds[4].ch_names
keepch_name = list()
for x in ch_inds:
    keepch_name.append(ch_name[x])
clu_idx = 2
i_clu = 2
time_inds, space_inds = np.squeeze(clusters[clu_idx])
ch_inds = np.unique(space_inds)
time_inds = np.unique(time_inds)
f_map = T_obs[time_inds, ...].mean(axis=0)
ch_name = evokeds[4].ch_names
for x in ch_inds:
    keepch_name.append(ch_name[x])    
pick_ch = mne.pick_channels(evokeds[4].ch_names, include=keepch_name)
time_indss = [i + 200 for i in time_inds]
time_inds = np.asarray(time_indss)

sig_times = evoked_T.times[time_inds]
# create spatial mask
mask = np.zeros((f_map.shape[0], 1), dtype=bool)
mask[pick_ch, :] = True
    # initialize figure
fig, ax_topo = plt.subplots(1, 1, figsize=(10, 3))
    # plot average test statistic and mark significant sensors
image, _ = plot_topomap(f_map,pos, mask=mask, axes=ax_topo, cmap='Reds',
                        vmin=np.min, vmax=np.max, show=False, 
                        head_pos=dict(center=(0.45,0.65),scale=(0.75,0.65)))
    # create additional axes (for ERF and colorbar)
divider = make_axes_locatable(ax_topo)
    # add axes for colorbar
ax_colorbar = divider.append_axes('right', size='5%', pad=0.05)
plt.colorbar(image, cax=ax_colorbar)
ax_topo.set_xlabel('Averaged F-map ({:0.3f} - {:0.3f} s)'.format(*sig_times[[0, -1]]))
    # add new axis for time courses and plot time courses
ax_signals = divider.append_axes('right', size='300%', pad=1.2)
title = 'Cluster #{0}, {1} sensor'.format(i_clu , len(pick_ch))
if len(pick_ch) > 1:
    title += "s (mean)"
plot_compare_evokeds(evoked_dict_T_NT, title=title, picks=pick_ch, axes=ax_signals,colors=colors, show=False, split_legend=True, show_legend = False,truncate_yaxis='max_ticks')
    # plot temporal cluster extent split_legend=True
ymin, ymax = ax_signals.get_ylim()
ax_signals.fill_betweenx((ymin, ymax), sig_times[0], sig_times[-1],color='orange', alpha=0.3)
    # clean up viz
mne.viz.tight_layout(fig=fig)
fig.subplots_adjust(bottom=.05)
plt.show()

#figname = (MNE_path + '7.Cluster_2.png')
#fig.savefig(figname)   
#plt.close(fig)

# =============================================================================
# analyse on cluster 1
# =============================================================================

clu_idx = 0
time_inds, space_inds = np.squeeze(clusters[clu_idx])
ch_inds = np.unique(space_inds)
time_inds = np.unique(time_inds)
ch_name = evokeds[4].ch_names
keepch_name = list()
for x in ch_inds:
    keepch_name.append(ch_name[x])
pick_ch = mne.pick_channels(evokeds[4].ch_names, include=keepch_name)
time_indss = [i + 200 for i in time_inds]
time_inds = np.asarray(time_indss)

    
conditions = ['evoked_T_C4' ,'evoked_NT_C4' , 'evoked_T_IM' , 'evoked_NT_IM', 'evoked_T_AR' , 'evoked_NT_AR']
meansforallsubj = list()
N_by_suj_all_subj = list()
for subj in range(len(subjects)): 
    sujname = subjects[subj]
#    evname =  os.path.join(MNE_path, '5.{}-ave.fif'.format(sujname))
    evname =  os.path.join(MNE_path, '5.{}-with_BL_corr-ave.fif'.format(sujname))
    evokeds = mne.read_evokeds(evname)
    meansbysuj = list()
    N_by_suj = list()
    for condition in range(len(conditions)): 
        T_data = evokeds[condition].data
        N = evokeds[condition].nave        
        V = T_data[pick_ch,:] 
        W = V[:,time_inds]
        M = W.mean()
        meansbysuj.append(M)
        N_by_suj.append(N)
    meansforallsubj.append(meansbysuj) 
    N_by_suj_all_subj.append(N_by_suj)
        


myfile = open('H:/EnfantsSains/Manip2/PROCESS_DATA/process_data_mne/Cluster1.txt', 'w')
myfile.write("sujet jeu T_NT ampl \n")
for subj in range(len(subjects)): 
    sujname = subjects[subj]
    listsubj = meansforallsubj[subj]
    myfile.write('{} C4 T {} \n'.format(sujname, listsubj[0]))
    myfile.write('{} C4 NT {} \n'.format(sujname, listsubj[1]))
    myfile.write('{} IM T {} \n'.format(sujname, listsubj[2]))
    myfile.write('{} IM NT {} \n'.format(sujname, listsubj[3]))
    myfile.write('{} AR T {} \n'.format(sujname, listsubj[4]))
    myfile.write('{} AR NT {} \n'.format(sujname, listsubj[5]))
myfile.close()



# =============================================================================
# analyse on cluster 2
# =============================================================================

clu_idx = 1
time_inds, space_inds = np.squeeze(clusters[clu_idx])
ch_inds = np.unique(space_inds)
time_inds = np.unique(time_inds)
ch_name = evokeds[4].ch_names
keepch_name = list()
for x in ch_inds:
    keepch_name.append(ch_name[x])
clu_idx = 2
time_inds, space_inds = np.squeeze(clusters[clu_idx])
ch_inds = np.unique(space_inds)
time_inds = np.unique(time_inds)
ch_name = evokeds[4].ch_names
for x in ch_inds:
    keepch_name.append(ch_name[x])    
pick_ch = mne.pick_channels(evokeds[4].ch_names, include=keepch_name)
time_indss = [i + 200 for i in time_inds]
time_inds = np.asarray(time_indss)

    
conditions = ['evoked_T_C4' ,'evoked_NT_C4' , 'evoked_T_IM' , 'evoked_NT_IM', 'evoked_T_AR' , 'evoked_NT_AR']
meansforallsubj = list()
N_by_suj_all_subj = list()
for subj in range(len(subjects)): 
    sujname = subjects[subj]
#    evname =  os.path.join(MNE_path, '5.{}-ave.fif'.format(sujname))
    evname =  os.path.join(MNE_path, '5.{}-with_BL_corr-ave.fif'.format(sujname))
    evokeds = mne.read_evokeds(evname)
    meansbysuj = list()
    N_by_suj = list()
    for condition in range(len(conditions)): 
        T_data = evokeds[condition].data
        N = evokeds[condition].nave        
        V = T_data[pick_ch,:] 
        W = V[:,time_inds]
        M = W.mean()
        meansbysuj.append(M)
        N_by_suj.append(N)
    meansforallsubj.append(meansbysuj) 
    N_by_suj_all_subj.append(N_by_suj)
        


myfile = open('H:/EnfantsSains/Manip2/PROCESS_DATA/process_data_mne/Cluster2.txt', 'w')
myfile.write("sujet jeu T_NT ampl \n")
for subj in range(len(subjects)): 
    sujname = subjects[subj]
    listsubj = meansforallsubj[subj]
    myfile.write('{} C4 T {} \n'.format(sujname, listsubj[0]))
    myfile.write('{} C4 NT {} \n'.format(sujname, listsubj[1]))
    myfile.write('{} IM T {} \n'.format(sujname, listsubj[2]))
    myfile.write('{} IM NT {} \n'.format(sujname, listsubj[3]))
    myfile.write('{} AR T {} \n'.format(sujname, listsubj[4]))
    myfile.write('{} AR NT {} \n'.format(sujname, listsubj[5]))
myfile.close()

 
# =============================================================================
# analyse on cluster 3
# =============================================================================

clu_idx = 3
time_inds, space_inds = np.squeeze(clusters[clu_idx])
ch_inds = np.unique(space_inds)
time_inds = np.unique(time_inds)
ch_name = evokeds[4].ch_names
keepch_name = list()
for x in ch_inds:
    keepch_name.append(ch_name[x])
pick_ch = mne.pick_channels(evokeds[4].ch_names, include=keepch_name)
time_indss = [i + 200 for i in time_inds]
time_inds = np.asarray(time_indss)

    
conditions = ['evoked_T_C4' ,'evoked_NT_C4' , 'evoked_T_IM' , 'evoked_NT_IM', 'evoked_T_AR' , 'evoked_NT_AR']
meansforallsubj = list()
N_by_suj_all_subj = list()
for subj in range(len(subjects)): 
    sujname = subjects[subj]
#    evname =  os.path.join(MNE_path, '5.{}-ave.fif'.format(sujname))
    evname =  os.path.join(MNE_path, '5.{}-with_BL_corr-ave.fif'.format(sujname))
    evokeds = mne.read_evokeds(evname)
    meansbysuj = list()
    N_by_suj = list()
    for condition in range(len(conditions)): 
        T_data = evokeds[condition].data
        N = evokeds[condition].nave        
        V = T_data[pick_ch,:] 
        W = V[:,time_inds]
        M = W.mean()
        meansbysuj.append(M)
        N_by_suj.append(N)
    meansforallsubj.append(meansbysuj) 
    N_by_suj_all_subj.append(N_by_suj)
        


myfile = open('H:/EnfantsSains/Manip2/PROCESS_DATA/process_data_mne/Cluster3.txt', 'w')
myfile.write("sujet jeu T_NT ampl \n")
for subj in range(len(subjects)): 
    sujname = subjects[subj]
    listsubj = meansforallsubj[subj]
    myfile.write('{} C4 T {} \n'.format(sujname, listsubj[0]))
    myfile.write('{} C4 NT {} \n'.format(sujname, listsubj[1]))
    myfile.write('{} IM T {} \n'.format(sujname, listsubj[2]))
    myfile.write('{} IM NT {} \n'.format(sujname, listsubj[3]))
    myfile.write('{} AR T {} \n'.format(sujname, listsubj[4]))
    myfile.write('{} AR NT {} \n'.format(sujname, listsubj[5]))
myfile.close()
   



# =============================================================================
# good perfromer
# =============================================================================

good_perfromer = ['S01','S02','S03','S05','S06','S08','S10','S12','S13','S18','S19'];

bad_performer = ['S07','S09','S11','S14','S15','S16','S17'];



    