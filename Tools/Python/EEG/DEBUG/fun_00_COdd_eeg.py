#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue 05 Feb 2019

@author: Françoise Lecaignard
"""

## Conversion of Raw brainAmp Data into BIDS


#############################################################################################
## Import packages          -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
# import argparse               # add input arguments

import numpy as np             # convention d'import
import os                       # path.join for file names
import matplotlib.pyplot as plt # from the MNE examples
import csv
import warnings



warnings.filterwarnings("ignore",category=DeprecationWarning)


import sys      # path to my own functions
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Python/EEG')
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/BIDS/mne-bids')


import mne
from mne.io import read_raw_brainvision
from mne import events_from_annotations, find_events
from mne_bids import write_raw_bids, make_bids_basename, make_bids_folders
from mne_bids.utils import print_dir_tree

print(__doc__)

from mne import get_config, set_config
type(get_config('MNE_BROWSE_RAW_SIZE'))
set_config('MNE_BROWSE_RAW_SIZE', '15.0,8.0')


#############################################################################################
## -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.    00_FUNCTIONS    --.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################

def  codd_eeg_extract_rawdataset_list(file_acq_list, subj_code):
    # file_acq_list: filename containing acquisition dataset name(s)
    # subj_code: acquisition codename- eg: 'HENJI' for Jimy H.
    datecode = []
    file_pref = []
    
    with open(file_acq_list, 'r') as f:
        reader = csv.reader(f,delimiter = '\t')
        for row in reader:
            if subj_code in row[0]:
                print(row[0])
                print('\n')
                datecode.append(row[0])
                file_pref.append(row[1])
        
    return datecode, file_pref

def codd_eeg_get_data_for_bids(dir_raw_acq, datecode, runfile_pref):
    # provides MNE raw and events from raw brainamp data to inform MNE-bidsifier
    #
    # dir_raw_acq: path the acquisition datasets
    # datecode: str, name of the folder containing raw data (eg: 20180512_HENJI)
    # runfile_pref: str, name of the prefix for eeg file, eg: HENJI_0001.vhdr (without vhdr extension)
      
    f_in = os.path.join(dir_raw_acq,datecode, '{}.vhdr'.format(runfile_pref))
    if os.path.isfile(f_in) :
        print('Found raw data file: ok')
    else:
        print('Raw data file not found- abandon', f_in)
        return
    
    raw = read_raw_brainvision(f_in, preload=False )
    # events
    event_id =  {'Start': 16,  'End': 32, 'Std': 1, 'Dev': 2}
     #events = find_events(raw, shortest_event=2, stim_channel='STI 014', output='onset')
    events, event_id = events_from_annotations(raw, event_id=event_id) #, regexp=None, use_rounding=True, chunk_duration=None, verbose=None)
    return raw, events, event_id
    

def codd_eeg_bids(dir_raw_acq, dir_raw_bids, subj_id, datecode, runfile_pref):
    # creates bids folder and rename raw data with bids name
    # In ContextOdd-EEG, bids filename will be of the form: sub-01_run-1
    #
    # dir_raw_acq: path the acquisition datasets
    # dir_raw_bids: path to the bids folder
    # subj_id: 1 to create sub01, etc
    # run_id: number of the current run
    # datecode: str, name of the folder containing raw data (eg: 20180512_HENJI)
    # runfile_pref: str, name of the prefix for eeg file, eg: HENJI_0001.vhdr (without vhdr extension)
    
    #recover ruid from runfile_pref
    run_id = runfile_pref[-1]
    # bids filename
    bids_basename = make_bids_basename(subject='{:02}'.format(subj_id),  task=None, run=run_id)
    print(bids_basename)
    # get raw and events from original file
    raw, events, event_id = codd_eeg_get_data_for_bids(dir_raw_acq, datecode, runfile_pref)
    event_id =  {'Start': 16,  'End': 32, 'Std': 1, 'Dev': 2}
    
#    # test debug
    dict_color_event = {16: 'red', 1: 'black', 2: 'magenta', 32: 'red'}
    scal = dict(eeg=10e-5)
    raw.plot(events=events,  event_color = dict_color_event, n_channels=raw.info['nchan'],remove_dc = True,  duration=2, highpass = None, scalings=scal) #, show_options=False)
    plt.show()
#    # good, raw and events are readable
    
    # and write bids!
    write_raw_bids(raw, bids_basename, dir_raw_bids, events_data=events, event_id = event_id, overwrite=True)
#
#


#
##############################################################################################
### -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.    TEST and DEBUG     --.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
##############################################################################################
if __name__== '__main__':
#    
#    #############################################################################################
#    ## Init Paths              -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#    #############################################################################################
#    
#   
#
#    #############################################################################################
#    ## Init Variables              -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#    #############################################################################################
#    dir_ref = '/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Reference_Files'
#    file_acq_list = '{}/List_EEG_Raw_Datasets.txt'.format(dir_ref)
    dir_raw_acq = '/sps/cermep/cermep/experiments/DCM/ContextOdd/Raw_Data/EEG'
    dir_raw_bids = '/sps/cermep/cermep/experiments/DCM/ContextOdd/Raw_Data/EEG_BIDS'
#    

## DEBUG Step 00 ########################################################################
    subj_code = 'BREME'
    subj_id = 3
    datecode = '20180406_BREME'
    runfile_pref = 'BREME_0002'
   
#    raw, events, event_id = codd_eeg_get_data_for_bids(dir_raw_acq, datecode[0], runfile_pref[0])
#    print(event_id)
    codd_eeg_bids(dir_raw_acq, dir_raw_bids, subj_id, datecode, runfile_pref)
    print_dir_tree(dir_raw_bids)