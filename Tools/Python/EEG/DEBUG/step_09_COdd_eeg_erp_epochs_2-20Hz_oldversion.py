#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
====================================================================
Step 09 - ERP   analysis : compute 2-20 Hz epochs (-200, 410 ms)
====================================================================

Now that preprocssings have been done (step00 to step07) and validated (step08), we can start a typical  ERP analyis
We consider epochs data in the 2-20 Hz range
To that aim, we do the following:
    1) we compute continuous 2-20 Hz data, ica-corrected  (===> .../sub-xx/erp_2-20Hz/ sub-xx.clean_2-20Hz.raw.fif)
    2) we compute 2-20 Hz ica-corrected  epochs, dropping early rejection bad epochs (===> .../sub-xx/erp_2-20Hz/sub-xx.temp_early_2-20Hz-epo.fif)
    2) we apply the second autoreject rejection to these epochs (===> .../sub-xx/erp_2-20Hz/sub-xx.clean_2-20Hz-epo.fif)

    @author: Françoise Lecaignard, francoise.lecaignard@inserm.fr
"""


###############################################################################
import numpy as np             # convention d'import
import os                       # path.join for file names
import matplotlib.pyplot as plt # from the MNE examples
import warnings
import pickle
import sys
# path to my own functions
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Python/EEG')
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/BIDS/mne-bids')
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/toolboxes/autoreject')


from autoreject import compute_thresholds, get_rejection_threshold
from autoreject import set_matplotlib_defaults  # noqa
set_matplotlib_defaults(plt)
from autoreject import AutoReject

from mne import Epochs, pick_types, read_events, read_epochs
from mne import get_config, set_config
type(get_config('MNE_BROWSE_RAW_SIZE'))
set_config('MNE_BROWSE_RAW_SIZE', '20.0,10.0')
from mne.preprocessing import read_ica

from mne.io import read_raw_fif
from fun_eeg_misc import  print_step_label,import_epochs

#from fun_NN_template import  ...

################################################################################
# Input Parameters 
################################################################################

# Paths ----------------------------
dir_mne = '/sps/cermep/cermep/experiments/DCM/ContextOdd/Ana_MNE'



################################################################################
# Define Constant Variables
################################################################################
# prefix of raw downsampled data
pref_raw_in = f'preproc'

# ----------- preproc data
lf1, hf1 = 1,40
# prefix for ica files
pref_ica = f'preproc_{lf1}-{hf1}Hz'
# prefix of clean  epochs, output of the autoreject step (step07) => contains good epochs
pref_epo_in = 'clean_{}-{}Hz'.format(lf1, hf1)
# Reject_log prefix file (created in step07)
pref_ar_reject = 'AutoReject_output300'

# Notch frequency array (Hz)
power_freq_array = np.arange(50, 201, 50)


# ----------- erp data
# bandwidth
lf2, hf2 = 2, 20
# erp folder (.../sub-xx/erp_2-20Hz/)
erp_folder = f'erp_{lf2}-{hf2}Hz'

#prefix  2-20 Hz continuous data (filtered and ica)
pref_raw_out = 'clean_{}-{}Hz'.format(lf2, hf2)
#prefix  2-20 Hz epochs  (clean, filtered and ica)
pref_epo_out = 'clean_{}-{}Hz'.format(lf2, hf2)

# ----------- event data
# events file to get total number of auditory stimuli 
eve_name = 'preproc.5d-eve'
# events for epochs
#Stimulus coding (five-digit format)
event_id_codd={'var_c/c4/c-/p0/std': 14121, 'var_c/c4/c-/p0/dev': 14122,'var_c/c4/c-/p0/std_oth': 14123,\
              'var_c/c5/c-/p0/std': 15121, 'var_c/c5/c-/p0/dev': 15122,'var_c/c5/c-/p0/std_oth': 15123,\
              'var_c/c6/c-/p0/std': 16121, 'var_c/c6/c-/p0/dev': 16122,'var_c/c6/c-/p0/std_oth': 16123,\
              'var_c/c2/c+/p0/std': 12221, 'var_c/c2/c+/p0/dev': 12222,'var_c/c2/c+/p0/std_oth': 12223,\
              'var_c/c3/c+/p0/std': 13221, 'var_c/c3/c+/p0/dev': 13222,'var_c/c3/c+/p0/std_oth': 13223,\
              'var_c/c4/c+/p0/std': 14221, 'var_c/c4/c+/p0/dev': 14222,'var_c/c4/c+/p0/std_oth': 14223,\
              'var_c/c6/c+/p0/std': 16221, 'var_c/c6/c+/p0/dev': 16222,'var_c/c6/c+/p0/std_oth': 16223,\
              'var_c/c7/c+/p0/std': 17221, 'var_c/c7/c+/p0/dev': 17222,'var_c/c7/c+/p0/std_oth': 17223,\
              'var_c/c8/c+/p0/std': 18221, 'var_c/c8/c+/p0/dev': 18222,'var_c/c8/c+/p0/std_oth': 18223,\
              'var_p/c2/c-/p+/std': 22111, 'var_p/c2/c-/p+/dev': 22112,'var_p/c2/c-/p+/std_oth': 22113,\
              'var_p/c3/c-/p+/std': 23111, 'var_p/c3/c-/p+/dev': 23112,'var_p/c3/c-/p+/std_oth': 23113,\
              'var_p/c4/c-/p+/std': 24111, 'var_p/c4/c-/p+/dev': 24112,'var_p/c4/c-/p+/std_oth': 24113,\
              'var_p/c4/c-/p0/std': 24121, 'var_p/c4/c-/p0/dev': 24122,'var_p/c4/c-/p0/std_oth': 24123,\
              'var_p/c5/c-/p0/std': 25121, 'var_p/c5/c-/p0/dev': 25122,'var_p/c5/c-/p0/std_oth': 25123,\
              'var_p/c6/c-/p0/std': 26121, 'var_p/c6/c-/p0/dev': 26122,'var_p/c6/c-/p0/std_oth': 26123,\
              'var_p/c6/c-/p-/std': 26131, 'var_p/c6/c-/p-/dev': 26132,'var_p/c6/c-/p-/std_oth': 26133,\
              'var_p/c7/c-/p-/std': 27131, 'var_p/c7/c-/p-/dev': 27132,'var_p/c7/c-/p-/std_oth': 27133,\
              'var_p/c8/c-/p-/std': 28131, 'var_p/c8/c-/p-/dev': 28132,'var_p/c8/c-/p-/std_oth': 28133                  }

# epoch size
tmin, tmax = -0.2, 0.41

################################################################################
# Let's start!
################################################################################

# subj_id_list -----------------------
# test on one subject
subj_id_list = np.arange(4,5) # sub-00
# applyto all
subj_id_list = np.arange(0,36) # 0 to 35, to include sub-00 to sub-35


#
for i_su in subj_id_list:
    subj='sub-{:02}'.format(i_su) 
    dir_imne_preproc = os.path.join(dir_mne, subj, 'preproc') #individual mne path
    dir_imne_erp = os.path.join(dir_mne, subj, erp_folder) #individual mne path
    
    
    #### 1 ----- Create clean continuous 2-20 Hz (*.clean_2-20Hz.raw.fif)
    f_raw_in = os.path.join(dir_imne_preproc, f'{subj}.{pref_raw_in}.raw.fif') # raw continuous data: unfiltered & downsampled data
    f_raw_out = os.path.join(dir_imne_erp, f'{subj}.{pref_raw_out}.raw.fif') # raw continuous data: downsampled,  filtered  (2-20 Hz) and ica-corrected
    if os.path.isfile(f_raw_out):
        raw_out = read_raw_fif(f_raw_out, preload=True)
    else:
        raw_in = read_raw_fif(f_raw_in, preload=True)
       
        print_step_label('Notch - Filtering {}.....'.format(subj))    
        raw_out =   raw_in.notch_filter(power_freq_array, filter_length='auto', phase='zero')
        
        print_step_label('BandPass - Filtering {}.....'.format(subj))    
        raw_out.filter(lf2, hf2,  l_trans_bandwidth='auto',filter_length='auto', phase='zero', fir_window='hann', fir_design='firwin')
        
        print_step_label('ICA correction {}.....'.format(subj))    
        f_ica = os.path.join(dir_imne_preproc, '{}.{}-ica.fif'.format(subj, pref_ica))
        ica=read_ica(f_ica)
        ica.apply(raw_out)
    
        
        print_step_label('Save raw {}.....'.format(subj))
        raw_out.save(fname=f_raw_out,overwrite=True)
    
    #### 2 ----  Create clean epochs (2-20Hz)   (*.clean_2-20Hz-epo.fif)
    # applying good epochs obtained in step07 (epochs_in) to current continuous cleaned data (raw_out)
    f_epochs_in = os.path.join(dir_imne_preproc, '{}.{}.fif'.format(subj, pref_epo_in))
    epochs_in = read_epochs(f_epochs_in)    
    picks = pick_types(raw_out.info, eeg=True, stim=True,exclude=())
    epochs_out = import_epochs(subj, raw_out, epochs_in,  picks,  event_id_codd, tmin, tmax,  plot=False)
    

    #### ---- Load early epochs (2-20 Hz)
    f_early_epochs = os.path.join(dir_imne, '{}.{}-epo.fif'.format(subj, pref_early_epochs)) 
    if not os.path.isfile(f_early_epochs):
        print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        print('{} : file {} not found => go to step_05 !!!'.format(subj, f_early_epochs))
        print('    ABANDON ')
        print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
    
    
    else:
        epochs_early = read_epochs(f_early_epochs) 
        print('{} : {} epochs after early rejection (step 04) '.format(subj, len(epochs_early)))
        
        ### apply ica
        f_ica = os.path.join(dir_imne, '{}.{}-ica.fif'.format(subj, pref_ica))
        ica=read_ica(f_ica)
        ica.apply(epochs_early)
    
        ### load autoreject results
        f_ar_reject = os.path.join(dir_imne, '{}.{}.pkl'.format(subj, pref_ar_reject)) 
        with  open(f_ar_reject, 'rb') as input:
            a = pickle.load(input)
            kappa = a[0]
            T = a[1]
            bad_epochs = a[2]
            labels = a[3]
            
        ### test if files are compatible
        if len(epochs_early) != len(bad_epochs):
            print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
            print('{} : Not the same number of epochs in  {} and autoreject file !!!'.format(subj, f_early_epochs))
            print('{} : {} in {}, {} in autoreject (bad_epochs) '.format(subj, len(epochs_early), f_early_epochs), len(bad_epochs))
            print('    ABANDON ')
            print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        
        else:
            
            epochs_erp = epochs_early.copy()
            epochs_erp.drop(bad_epochs)
            
            # total number of events
            f_events = os.path.join(dir_imne, '{}.{}.fif'.format(subj, eve_name)) 
            events=read_events(f_events)
            print('-----------------------------------------------------------------')
            print('{} : {} events (total); {} epochs in early rejection; {} epochs for the ERP analysis'.format(subj, 
                  len(events), len(epochs_early), len(epochs_erp) ))
            print('-----------------------------------------------------------------')
            
            # save
            
            if not os.path.exists(dir_imne_erp):
                os.makedirs(dir_imne_erp)
                
            f_epochs_out=os.path.join(dir_imne_erp, '{}.{}-epo.fif'.format(subj, pref_epo_out))
            epochs_erp.save(f_epochs_out)   
            
            if plot:
                epochs_test = read_epochs(f_epochs_out) 
                epochs_test.plot()





    
    