#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 26 15:25:09 2019

@author: lecaigna
"""
from array import array
import numpy as np             # convention d'import
import os
import sys      # path to my own functions
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Python/EEG')
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/autoreject')

from mne.io import read_raw_fif
from mne import Epochs, pick_types, read_events
from autoreject import AutoReject
from autoreject import get_rejection_threshold, RejectLog

from fun_eeg_misc import  print_step_label, count_events

################################################################################
# Input Parameters 
################################################################################

# Paths ----------------------------
dir_mne = '/sps/cermep/cermep/experiments/DCM/ContextOdd/Ana_MNE'
event_id_codd={'var_c/c4/c-/p0/std': 14121, 'var_c/c4/c-/p0/dev': 14122,'var_c/c4/c-/p0/std_oth': 14123,\
              'var_c/c5/c-/p0/std': 15121, 'var_c/c5/c-/p0/dev': 15122,'var_c/c5/c-/p0/std_oth': 15123,\
              'var_c/c6/c-/p0/std': 16121, 'var_c/c6/c-/p0/dev': 16122,'var_c/c6/c-/p0/std_oth': 16123,\
              'var_c/c2/c+/p0/std': 12221, 'var_c/c2/c+/p0/dev': 12222,'var_c/c2/c+/p0/std_oth': 12223,\
              'var_c/c3/c+/p0/std': 13221, 'var_c/c3/c+/p0/dev': 13222,'var_c/c3/c+/p0/std_oth': 13223,\
              'var_c/c4/c+/p0/std': 14221, 'var_c/c4/c+/p0/dev': 14222,'var_c/c4/c+/p0/std_oth': 14223,\
              'var_c/c6/c+/p0/std': 16221, 'var_c/c6/c+/p0/dev': 16222,'var_c/c6/c+/p0/std_oth': 16223,\
              'var_c/c7/c+/p0/std': 17221, 'var_c/c7/c+/p0/dev': 17222,'var_c/c7/c+/p0/std_oth': 17223,\
              'var_c/c8/c+/p0/std': 18221, 'var_c/c8/c+/p0/dev': 18222,'var_c/c8/c+/p0/std_oth': 18223,\
              'var_p/c2/c-/p-/std': 22111, 'var_p/c2/c-/p-/dev': 22112,'var_p/c2/c-/p-/std_oth': 22113,\
              'var_p/c3/c-/p-/std': 23111, 'var_p/c3/c-/p-/dev': 23112,'var_p/c3/c-/p-/std_oth': 23113,\
              'var_p/c4/c-/p-/std': 24111, 'var_p/c4/c-/p-/dev': 24112,'var_p/c4/c-/p-/std_oth': 24113,\
              'var_p/c4/c-/p0/std': 24121, 'var_p/c4/c-/p0/dev': 24122,'var_p/c4/c-/p0/std_oth': 24123,\
              'var_p/c5/c-/p0/std': 25121, 'var_p/c5/c-/p0/dev': 25122,'var_p/c5/c-/p0/std_oth': 25123,\
              'var_p/c6/c-/p0/std': 26121, 'var_p/c6/c-/p0/dev': 26122,'var_p/c6/c-/p0/std_oth': 26123,\
              'var_p/c6/c-/p-/std': 26131, 'var_p/c6/c-/p-/dev': 26132,'var_p/c6/c-/p-/std_oth': 26133,\
              'var_p/c7/c-/p-/std': 27131, 'var_p/c7/c-/p-/dev': 27132,'var_p/c7/c-/p-/std_oth': 27133,\
              'var_p/c8/c-/p-/std': 28131, 'var_p/c8/c-/p-/dev': 28132,'var_p/c8/c-/p-/std_oth': 28133                  }

eve_name = 'preproc.5d-eve'

# epoch size
tmin, tmax = -0.2, 0.4

# filter epochs
lf, hf = 1, 40 #Hz

# epoch rejection on threshold (peak-2-peak)
rej_thresh = 250e-6 #uV

################################################################################
################################################################################



subj = 'sub-29'
dir_imne = os.path.join(dir_mne, subj, 'preproc') #individual mne path
f_raw = os.path.join(dir_imne, '{}.preproc.raw.fif'.format(subj)) 
f_events = os.path.join(dir_imne, '{}.{}.fif'.format(subj, eve_name)) 
raw = read_raw_fif(f_raw, preload=False) 
events=read_events(f_events)

# prep epochs
f_epoch_out=os.path.join(dir_mne, '{}.preproc_{}-{}Hz-epo.fif'.format(subj, lf, hf))
picks = pick_types(raw.info, eeg=True, stim=True,exclude=())
reject=dict(eeg=rej_thresh)
exclude_eeg_ind = []
print_step_label('Epoching---  {}.....({} uV)'.format(subj, rej_thresh*1e6))  

if exclude_eeg_ind:
    raw.info['bads']=exclude_eeg_ind
    print('excluded sensors for rejection: {}'.format(exclude_eeg_ind))


# Epochs with rejection
epochs_params=dict(events=events, event_id=event_id_codd, 
                 tmin=tmin, tmax=tmax, 
                 reject=reject, picks=picks, 
                 baseline=None, verbose=False)      
epochs_rej = Epochs(raw, **epochs_params)

# Epochs without rejection
epochs_params=dict(events=events,
                 tmin=tmin, tmax=tmax, 
                 reject=None, picks=picks, 
                 baseline=None, verbose=False)      
epochs_orig = Epochs(raw, **epochs_params)



epochs_rej.load_data()
print('  Dropped %0.1f%% of epochs' % (epochs_rej.drop_log_stats(),))
epochs_rej.plot_drop_log()

nr_events = events.shape[0]
nr_chan = epochs_rej.info['nchan']

bad_epochs=[False]*nr_events #default: good epochs, bad=False
chan_epochs_matrix  = [[0]*nr_events]*nr_chan # matrix n_chan * n_epochs, 0 if sensor good for this epoch, 1 if bad, 2 if interpolated
ch_names = epochs_rej.info['ch_names']

#bad_list=[]
for i_eve in np.arange(0,nr_events):
    if epochs_rej.drop_log[i_eve]:
        print(epochs_rej.drop_log[i_eve])
        for i_drop, reason_drop in enumerate(epochs_rej.drop_log[i_eve]):
            #print('------', reason_drop)
            if reason_drop in epochs_rej.info['ch_names']:
                row = epochs_rej.info['ch_names'].index(reason_drop)
                #print(row)
                chan_epochs_matrix[row] =[1]*nr_events
                bad_epochs[i_eve] = True #bad, or ignored (start, end codes)
        print('BAD? ', bad_epochs[i_eve])
#        if bad_epochs[i_eve]==True:
#            bad_list.append(i_eve)
    #else:
        #do nothing
        
#for i in np.arange(0,len(epochs.drop_log)):
#    print(epochs.drop_log[i], bad_epochs[i])
labels = np.array(chan_epochs_matrix).transpose((1,0))
reject_log = RejectLog(bad_epochs,labels, epochs_rej.info['ch_names'])
scal = dict(eeg=10e-4)
reject_log.plot_epochs(epochs_orig,    scalings=scal)
reject_log.plot()
#plot_epochs(epochs_orig, scalings=scal, n_epochs=20,  n_channels=32, bad_epochs_idx=np.array(bad_list))

#epochs.plot(events=events, n_channels=raw.info['nchan'],   scalings=scal)

#picks = pick_types(epochs.info, meg=False, eeg=True, stim=False)
#picks=np.arange(0,32,2)
#ar = AutoReject(picks=picks, random_state=42, n_jobs=1)
#
#epochs_clean = ar.fit_transform(epochs)  
#
##This will automatically clean an epochs object read in using MNE-Python. To get the rejection dictionary, simply do:
#
#reject = get_rejection_threshold(epochs)  
#
#
#epochs_ar, reject_log = ar.fit_transform(this_epoch, return_log=True)
import mne
from mne.io import read_raw_brainvision
f_raw_in = '/sps/cermep/cermep/experiments/DCM/ContextOdd/Raw_Data/EEG_BIDS/sub-03/eeg/sub-03_run-2.vhdr'
raw = read_raw_brainvision(f_raw_in, preload=True )