#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 26 19:08:58 2019

@author: lecaigna
"""


from array import array
import numpy as np             # convention d'import
import os
import sys      # path to my own functions
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Python/EEG')
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/autoreject')

from mne.io import read_raw_fif
from mne import Epochs, pick_types, read_events
from autoreject import AutoReject
from autoreject import get_rejection_threshold, RejectLog
from autoreject.viz import plot_epochs


### needed parameters (it won' work with you, but it gives an idea)
dir_mne = '/sps/cermep/cermep/experiments/DCM/ContextOdd/Ana_MNE'
event_id_codd={'std': 14121, 'dev': 14122}
eve_name = 'preproc.5d-eve'

# epoch size
tmin, tmax = -0.2, 0.4

# filter epochs
lf, hf = 1, 40 #Hz

# epoch rejection on threshold (peak-2-peak)
rej_thresh = 450e-6 #uV
lf, hf = 1, 40

### load data ---------------------
subj = 'sub-29'
dir_imne = os.path.join(dir_mne, subj, 'preproc') #individual mne path
f_raw = os.path.join(dir_imne, '{}.preproc.raw.fif'.format(subj)) 
f_events = os.path.join(dir_imne, '{}.{}.fif'.format(subj, eve_name)) 
raw = read_raw_fif(f_raw, preload=False) 
events=read_events(f_events)

####  prep epochs --------------
picks = pick_types(raw.info, eeg=True, stim=True,exclude=())
reject=dict(eeg=rej_thresh)

### Epochs without rejection, with the native nr of events (for the plot, below, so that the nr of epochs is the same)
epochs_params=dict(events=events,event_id=event_id_codd,
                 tmin=tmin, tmax=tmax, 
                 reject=None, picks=picks, 
                 baseline=None, verbose=False)      
epochs_orig = Epochs(raw, **epochs_params)
epochs_orig.drop_bad()

### Epochs with thresh- rejection
epochs_params=dict(events=events, event_id=event_id_codd, 
                 tmin=tmin, tmax=tmax, 
                 reject=reject, picks=picks, 
                 baseline=None, verbose=False)      
epochs_rej = Epochs(raw, **epochs_params)
epochs_rej.drop_bad()
epochs_rej.load_data()
print('  Dropped %0.1f%% of epochs' % (epochs_rej.drop_log_stats(),))
epochs_rej.plot_drop_log()

### Prepare data for the reject_log
#RuntimeError: Since bad epochs have not been dropped, the length of the Epochs is not known. Load the Epochs with preload=True, or call Epochs.drop_bad(). To find the number of events in the Epochs, use len(Epochs.events).




nr_epochs = len(epochs_orig.events) #all epochs selected in event_id dict, good and bad
nr_chan = epochs_orig.info['nchan']

bad_epochs=[False]*nr_epochs #default: good epochs, bad=False
chan_epochs_matrix  = [[0]*nr_epochs]*nr_chan # matrix n_chan * n_epochs, 0 if sensor good for this epoch, 1 if bad, 2 if interpolated
ch_names = epochs_rej.info['ch_names']

for i_ep in np.arange(0,nr_epochs):
    #get index from original events list
    ind_ep = np.where(events[:,0]==epochs_orig.events[i_ep][0])[0] # based on event sample index
    if len(ind_ep)==1:
        ind_ep = ind_ep[0]
        
    #if this epoch good or bad?
    if epochs_rej.drop_log[ind_ep]: # if not empty, then bad
        bad_epochs[i_ep] = True 
    print('Epoch #{} ({}): BAD = {} -  {}'.format(i_ep+1, events[ind_ep,2], bad_epochs[i_ep], epochs_rej.drop_log[ind_ep]))
        
labels = np.array(chan_epochs_matrix).transpose((1,0))
reject_log = RejectLog(bad_epochs,labels, epochs_rej.info['ch_names'])
scal = dict(eeg=10e-4)
reject_log.plot_epochs(epochs_orig,   scalings=scal) #,n_channels= nr_chan
reject_log.plot() # here i don't know what it does

#plot_epochs(epochs_orig, scalings=scal, n_epochs=20,  n_channels=32, bad_epochs_idx=np.array(bad_list)) #this does not work, too bad!
