#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
=================================================================
Step 08 - Autoreject to drop bad epochs + Peak-to-peak rejection
=================================================================

Working with -200 +410 ms early-rejection and ica-corrected  epochs, 1-40 Hz bandpass, (created in step07, *.preproc_early_1-40Hz_ica-epo.fif)
We use autoreject (AR) from mne to compute automatically sensor thresholds and identify bad sensors
We don't use the repair-epoch tool (interpolation)
We further  add (after AR) an extra-rejection (100 uV threshold) to remove isolated artefact invisible to AR (due to kappa bound)
Outputs ======> /preproc/sub-xx.AutoReject_training_epochs300.pkl <=> epochs selected to train AR
        ======> /preproc/sub-xx.AutoReject_output300.pkl <=> autoreject output, reject_log objects 
        ======> /preproc/sub-xx.clean_1-40Hz-epo.fif <=> cleaned epochs in 1-40 Hz (ica, early and autoreject dropping)

Created on Mon Feb 11 17:57:17 2019

@author: Françoise Lecaignard, francoise.lecaignard@inserm.fr
"""



################################################################################

###############################################################################
import numpy as np             # convention d'import
import os
import sys      # path to my own functions
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Python/EEG')
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/toolboxes/autoreject')
import math
import matplotlib.pyplot as plt # from the MNE examples
import pickle
import tqdm # progress bars

from mne.io import read_raw_fif
from mne import Epochs, pick_types, read_events, read_epochs, pick_events
from mne import get_config, set_config
type(get_config('MNE_BROWSE_RAW_SIZE'))
set_config('MNE_BROWSE_RAW_SIZE', '20.0,10.0')
from mne.preprocessing import read_ica
from mne.viz import plot_evoked_topo

from autoreject import compute_thresholds, get_rejection_threshold
from autoreject import set_matplotlib_defaults  # noqa
set_matplotlib_defaults(plt)
from autoreject import AutoReject

from fun_util_eeg import  print_step_label, count_events, peak_to_peak_raw
from fun_plot_eeg import  plot_epochs_drop
from fun_plot_autoreject import  plot_autoreject_threshold_hist,plot_autoreject_labels
#from fun_08_autoreject import codd_eeg_sample_ar_training_epochs,run_autoreject
from fun_util_epoch import get_dropped_epochs_idx, import_epochs
################################################################################
# Input Parameters 
################################################################################

# Paths ----------------------------
dir_mne = '/sps/cermep/cermep/experiments/DCM/ContextOdd/Ana_MNE'


### ----------------------------input Filenames----------------------------

# Input --- Pref epoch file (early rejection, ica correction)
pref_epo_in = 'preproc_early_1-40Hz_ica'

# events file to get total number of auditory stimuli (renamed and downsampled events)
eve_name = 'preproc.5d-eve'

### ----------------------------bad sensors ###----------------------------
# These sensors are excluded from ICA fit, but will not be dropped in the resulting epochs
# we use drops from step_05 PSD inspection

# exclude eeg:  Do Not Modify Here !
psd_bad_sensors = { 'sub-00': ['T7', 'TP9'],	 'sub-01': [],	'sub-02':[],	 'sub-03': [],	'sub-04': ['T7', 'T8'],\
             'sub-05': [],	 'sub-06': [],	'sub-07':[],	 'sub-08': [],	'sub-09': ['T7'],\
             'sub-10': [],    'sub-11': ['PO9'],   'sub-12':[],  'sub-13': [],    'sub-14': [],\
             'sub-15': ['T8'],    'sub-16': [],   'sub-17':[],  'sub-18': [],    'sub-19': ['T7', 'T8', 'TP10'],\
             'sub-20': [],    'sub-21': [],   'sub-22':[],	 'sub-23': [],    'sub-24': [],\
             'sub-25': ['Oz'],    'sub-26': [],   'sub-27':[],	 'sub-28': [],    'sub-29': [],\
             'sub-30': [],    'sub-31': [],   'sub-32':[],	 'sub-33': [],    'sub-34': [],\
             'sub-35': []	}
exclude_eeg = psd_bad_sensors

### ----------------------------AutoReject (AR) specs----------------------------


# Numbre of sampled epochs to fit the autoreject, and associated prefix for filename (pickle file)
nr_learn_epochs = 300

# Nr of EEG sensors (total)
nr_eeg = 32

### ----------------------------Peak-2-Peak (p2p) specs----------------------------
# individual peak-2-peak amplitude threshold , in V
threshold_eeg = { 'sub-00': 100e-6,	 'sub-01': 100e-6,	'sub-02':100e-6,	 'sub-03': 100e-6,	'sub-04': 100e-6,\
         'sub-05': 100e-6,	 'sub-06': 100e-6,	'sub-07':100e-6,	 'sub-08': 100e-6,	'sub-09': 100e-6,\
         'sub-10': 100e-6,    'sub-11': 100e-6,   'sub-12':100e-6,  'sub-13': 100e-6,    'sub-14': 100e-6,\
         'sub-15': 100e-6,    'sub-16': 100e-6,   'sub-17':100e-6,  'sub-18': 100e-6,    'sub-19': 100e-6,\
         'sub-20': 100e-6,    'sub-21': 100e-6,   'sub-22':100e-6,	 'sub-23': 100e-6,    'sub-24': 100e-6,\
         'sub-25': 100e-6,    'sub-26': 100e-6,   'sub-27':100e-6,	 'sub-28': 100e-6,    'sub-29': 100e-6,\
         'sub-30': 100e-6,    'sub-31': 100e-6,   'sub-32':100e-6,	 'sub-33': 100e-6,    'sub-34': 100e-6,\
         'sub-35': 100e-6	}

### ------ ------ ------ Testing rejection threshold on 2-20 Hz ERPs ----------------------------
# Testing events (we choose these because they rest on ~120 events)
erp_list = ['var_p/c4/c-/p+/std_oth', 'var_c/c4/c+/p0/std_oth']
#event_id_plot = [event_id_codd[name] for name in erp_list]
## Testing sensors
#picks_plot = ('Fp1', 'Fp2', 'Fz' 'FCz', 'Cz', 'Pz', 'Oz', 'TP9', 'TP10')
# Testing filter
#lf, hf = 2,20 #Hz
##Testing ERP baseline
#baseline = (-0.2, 0) # [-100 0] ms

### ----------------------------output Filenames----------------------------

# Output --- Pref pickle file with selected epochs for training
pref_ar_learn_epochs = f'AutoReject_training_epochs{nr_learn_epochs}'

# Output --- Pref autoreject fit
pref_ar_out = f'AutoReject_output{nr_learn_epochs}'

#prefix for pickle file containing bad_epochs = boolean vector ; summary of AutoReject and Peak-to-Peak
pref_rej_out = f'clean_arp2p'

## Pref epoch file = selected epochs after ealry rejection (step04) and current autoreject
# data are ica-correcyed 
pref_epo_out = 'clean_1-40Hz'

#prefix for good/bad events (pickle file, to plot raw data with good and bad events)
pref_goodbad_events = 'ar_rej_events'







# Plot epochs
plot_raw=False
plot_epochs=False


################################################################################
# Let's start!
################################################################################

# subj_id_list -----------------------
# test on one subject
subj_id_list = np.arange(0,1) # sub-00
# applyto all
#subj_id_list = np.arange(1,36) # 0 to 35, to include sub-00 to sub-35

#


for i_su in subj_id_list:
    
    subj='sub-{:02}'.format(i_su) 
    dir_imne = os.path.join(dir_mne, subj, 'preproc') #individual mne path
    
    sens_exclude = exclude_eeg[subj]
    

    # 1) we take  epochs with early rejection and ica-correction (setp07))
    ################################################################################
    f_epochs_in=os.path.join(dir_imne, '{}.{}-epo.fif'.format(subj, pref_epo_in))
    epochs_in = read_epochs(f_epochs_in) 
    events_early=epochs_in.events #  does not refer to original events (nr = 6048), but to the resulting ones after jump-like noise rejection (step 04)
    
    picks = pick_types(epochs_in.info, meg=False, eeg=True, stim=False,  eog=False, exclude=sens_exclude)

            
    # 2) selection of few representative epochs to train AutoReject
    ################################################################################
    # Testings were in favor of ~ 300 epochs
    # Output : ========> sub-XX.AutoReject_training_epochs300.pkl 
#    f_ar_trainingtest = os.path.join(dir_imne, '{}.{}.pkl'.format(subj, pref_ar_learn_epochs)) 
#    if not os.path.isfile(f_ar_trainingtest):
#        learn_epochs, sample_idxs = codd_eeg_sample_ar_training_epochs(epochs_in,nr_learn_epochs, f_ar_trainingtest)
#    else:
#        with  open(f_ar_trainingtest, 'rb') as input:
#            a = pickle.load(input)
#            learn_epochs = a[0]
#            sample_idxs = a[1]


    
    # 3) Fit autoreject on selected epochs, with no interpolation allowed
    ################################################################################
    # Fit of autoreject on a subset of epochs (nr=300) => threshes (per channel) and kappa (AutoReject output = consensus = nr of channels required to declare an epoch as bad, in percentage)
    # Application of T and kappa to teh whole set of epochs => reject_log object with bad_epochs vector and labels matrix (N_epochs *N_Sensors) - No interpolation allowed
    # Labels matrix allows computing the contribution of each sensor in global rejection
    # Output : ========> sub-XX.AutoReject_output300.pkl file, 
    
    
#    nr_good = nr_eeg - len(sens_exclude)
    
#    kappa=None # estimated by AR
#    picks_ar = picks
#    
#    
#    ar, reject_log, pc = run_autoreject(subj, epochs_in, picks_ar,  sens_exclude, learn_epochs, epochs_in.event_id, erp_list,   mod='eeg', kappa=kappa, plot_learn=False, plot_erp=False)
#
#    print('-------- {},    {} % rejection'.format(sens_exclude, pc))
##    
#            
#    f_ar_out = os.path.join(dir_imne, '{}.{}.pkl'.format(subj, pref_ar_out)) 
#    bad_epochs = reject_log.bad_epochs
#    labels = reject_log.labels       
#    with  open(f_ar_out, 'wb') as output:
#        pickle.dump((ar, bad_epochs, labels), output, pickle.HIGHEST_PROTOCOL)
#        
#        
##    with  open(f_ar_out, 'rb') as input:
##        a = pickle.load(input)
##        ar = a[0]
##        bad_epochs = a[1]
##        labels = a[2]
#    
#    print(f'=============> AutoReject: dropped {sum(bad_epochs)} epochs')


#     # 4) peak-to peak rejection on remaining epochs
#    ################################################################################
#    # Because we do AutoReject without interpolation, we end up with bad signals not being dropped because
#    # not present in more than kappa electrodes (a typical example is an isolated non-physiological stuff in one sensor)
#    # To compensate for this, we add this additional peak-to-peak thresholding
    
    picks_p2p = picks
    nr_epo = len(epochs_in.events)
    nr_sens = len(picks_p2p) # wrt picks !!!
    th = threshold_eeg[subj] 
    
    # load AR outputs (in case step4 is launched afterwards)
    f_ar_out = os.path.join(dir_imne, '{}.{}.pkl'.format(subj, pref_ar_out)) 
    with  open(f_ar_out, 'rb') as input:
        a = pickle.load(input)
        ar = a[0]
        bad_epochs_ar = a[1] # boolean, size = nr_epo 
        labels = a[2]

    bad_epochs_p2p = np.zeros(nr_epo, dtype=bool) # all epochs to False
    nr_epo_post_ar = nr_epo - sum(bad_epochs_ar) # nr of surviving epochs after AR
    
    count_contrib = np.zeros(len(picks_p2p)) # a value per sensor - be aware, indices of electrodes relative to picks subset
    for i_ep in np.arange(nr_epo):
        if not bad_epochs_ar[i_ep]: # we only look at surviving epochs from AR <=> bad_epochs_ar = False
            epo = epochs_in[i_ep].get_data()[0]
            
#            print(epo.shape) # sensors * samples
            d = epo[picks_p2p,:]
            mi = np.amin(d, axis=1) # min per raw 
            ma = np.amax(d, axis=1)
            p2p = np.abs(ma -mi)
            h = np.where(p2p > th)[0]
            if h.any():#not empty
                bad_epochs_p2p[i_ep] = True
                count_contrib[h]+=1 # eah time a sensor causes a rejection, we add 1
    pc_contrib = count_contrib *100 / nr_epo_post_ar 
    print('Peak-to-Peak Rejection post AR (pc relative to surviving AR epochs')
    for i_sens, sens  in enumerate(epochs_in.ch_names):
        if i_sens in picks:
            i = np.where(picks==i_sens)[0]
            print(f'{sens} -----> {np.round(pc_contrib[i])[0]}pc')
        else:
            print(f'{sens}: set as bad')
        
        
    bad_epochs = bad_epochs_p2p + bad_epochs_ar
    print(f'=============> AutoReject         : dropped {sum(bad_epochs_ar)} epochs <=> {np.round(100*sum(bad_epochs_ar)/nr_epo)}pc')
    print(f'=============> P2P (post AR)      : dropped {sum(bad_epochs_p2p)} epochs <=> {np.round(100*sum(bad_epochs_p2p)/nr_epo)}pc')
    print(f'=============> TOTAL              : dropped {sum(bad_epochs)} epochs <=> {np.round(100*sum(bad_epochs)/nr_epo)}pc')
#    
#    reject = dict(eeg=threshold_eeg[subj])
#    picks = pick_types(raw_bw1.info, eeg=True, stim=True,exclude=())
#    exclude_eeg_ind = exclude_eeg[subj]
#    raw_bw1.info['bads']=exclude_eeg_ind
#    print('excluded sensors for rejection: {}'.format(exclude_eeg_ind))
#
#    epochs_rej_params=dict(events=events_early, event_id=event_id_codd, 
#                     tmin=tmin, tmax=tmax, 
#                     reject=reject, picks=picks, 
#                     baseline=None, verbose=False) 
#    if len(bad_epochs) == len(events_early[:,0]):     
#        epochs_bw1_rej = Epochs(raw_bw1, **epochs_rej_params)
#        events_pre = epochs_bw1_rej.events
#        epochs_bw1_rej.load_data()
#        events_post = epochs_bw1_rej.events
#        drop_idx, events_log = get_dropped_epochs_idx(events_pre, events_post)
#    
#    print(f'=============> Peak-2-Peak Rejection: dropped {len(drop_idx)} epochs')
#    bad_epochs_tot = bad_epochs.copy() #np.full(len(bad_epochs), False, dtype=bool)
#    bad_epochs_tot[drop_idx] = True
#    print(f'=============> TOTAL Rejection: dropped {sum(bad_epochs_tot)} epochs')
#    nr_drop_tot = sum(bad_epochs_tot)
#    nr_drop_p2p = len(drop_idx)
#    nr_drop_ar = sum(bad_epochs)
#    
##    f_rej_out = os.path.join(dir_imne, '{}.{}.pkl'.format(subj, pref_rej_out)) 
##    with  open(f_rej_out, 'wb') as output:
##        pickle.dump((bad_epochs_tot), output, pickle.HIGHEST_PROTOCOL)
##
#    # 6) Save epochs
#    ################################################################################
#    # Early rejection and autoreject led to cleaned epochs, available dfor subsequent analysis
#    # We save them, in the 1-40 Hz bandwidth
#    # For further use with other continuous data (other filters etc), just run import_epochs function in fun_eeg_misc to import these epoch selection onto raw continuous data
#    # Output : ========> sub-XX.clean_1-40Hz-epo.fif file, 
#
#    epochs_bw1.drop(bad_epochs_tot)
#    f_epoch_out=os.path.join(dir_imne, '{}.{}-epo.fif'.format(subj, pref_epo_out))
#    epochs_bw1.save(f_epoch_out) 
#
#    # 7) Save save good/bad epochs in an event file (*.pkl)
#    ################################################################################
#   
#    f_events = os.path.join(dir_imne, '{}.{}.fif'.format(subj, eve_name)) 
#    events=read_events(f_events)
#    for idx, lat in enumerate(events[:,0]):
#         if lat in epochs_bw1.events[:,0]:
#                events[idx,2] = 100
#         else:
#                events[idx,2] = 200
#       
#    print('------------------------ save good / bad events ---------------------------')
#    f_goodbad_events = os.path.join(dir_imne, '{}.{}.pkl'.format(subj, pref_rej_out)) 
#    with  open(f_goodbad_events, 'wb') as output:
#               pickle.dump((events, exclude_eeg_ind, nr_drop_tot, nr_drop_p2p, nr_drop_ar, bad_epochs_tot), output, pickle.HIGHEST_PROTOCOL)
   
#import pickle
#for i_su in subj_id_list:
#    
#    subj='sub-{:02}'.format(i_su) 
#    dir_imne = os.path.join(dir_mne, subj, 'preproc')  
#    f_rej_out = os.path.join(dir_imne, '{}.{}.pkl'.format(subj, pref_rej_out)) 
#    with  open(f_rej_out, 'rb') as input:
#            a = pickle.load(input)
#    
#    events = a[0]
#    exclude_eeg_ind = a[1]
#    nr_drop_tot = a[2]
#    nr_drop_p2p = a[3]
#    nr_drop_ar = a[4]
#    print(subj, exclude_eeg_ind,nr_drop_ar, nr_drop_p2p, nr_drop_tot )