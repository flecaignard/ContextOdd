#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
===============================================
Step 08 - Summarize preprocessings info
===============================================

Preprocessings have been achieved in step00 to step07
We end up with 
- 1-40 Hz epochs (-200 +410 ms), with early rejection (big jump-like artifacts) => sub-XX.preproc_early_1-40Hz-epo.fif
- 1-40 Hz epochs (-200 +410 ms), with early a,d autoreject rejection  => sub-XX.clean_1-40Hz-epo.fif

- an ica correction learnt on 1-40 Hz early-good epochs => sub-XX.preproc_1-40Hz-ica.fif

- autoreject outputs stored in sub-XX.AutoReject_output300.pkl (pickle file), which contains the good.drop epochs (indexed to the subset of epochs surviving early rejection)

Here, we summarize the global rejection (% of ok and dropped epochs), as well as recap the bad sensors identified in early rejection (step04) 
We also plot typical ERPs on either 1-40 Hz epochs, or 2-20 Hz (for the latter, step09 should be processed first !)

Created on Mon Mar 11 2019

@author: Françoise Lecaignard, francoise.lecaignard@inserm.fr
"""



###############################################################################
import numpy as np             # convention d'import
import os                       # path.join for file names
import matplotlib.pyplot as plt # from the MNE examples
import warnings
import pickle
import sys
# path to my own functions
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Python/EEG')
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/BIDS/mne-bids')
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/toolboxes/autoreject')


from autoreject import compute_thresholds, get_rejection_threshold
from autoreject import set_matplotlib_defaults  # noqa
set_matplotlib_defaults(plt)
from autoreject import AutoReject

from mne import Epochs, pick_types, read_events, read_epochs
from mne import get_config, set_config
type(get_config('MNE_BROWSE_RAW_SIZE'))
set_config('MNE_BROWSE_RAW_SIZE', '20.0,10.0')
from mne.preprocessing import read_ica
from mne.viz import plot_evoked_topo
#from mne.viz import plot

#from fun_NN_template import  ...
from fun_eeg_misc import  count_events
################################################################################
# Input Parameters 
################################################################################

# Paths ----------------------------
dir_mne = '/sps/cermep/cermep/experiments/DCM/ContextOdd/Ana_MNE'



################################################################################
# Define Constant Variables
################################################################################
#bandwith used for preprocs
lfbw1, hfbw1=1, 40


#bandwith used for ERP (testing SNR here)
lfbw2, hfbw2=2, 20

# prefix for ica files
pref_ica = 'preproc_1-40Hz'

# prefix of early-rejection epochs
pref_early_epochs = 'preproc_early_{}-{}Hz'.format(lfbw1, hfbw1)
# prefix of clean  epochs (eraly rejection and autoreject, and ica-corrected)
pref_clean_epochs = 'clean_{}-{}Hz'.format(lfbw1, hfbw1)

#Reject_log prefix file (created in step07)
pref_ar_reject = 'AutoReject_output300' #_noloop'

#prefix for bad sensors list
pref_bad_sensors = 'preproc_bad_sensors' #_noloop'

#prefix Output 2-20 Hz epochs file (created in step_09 ! this is to plot 2-20 Hz,with plot_bw2=True )
pref_epo_erp = 'erp_{}-{}Hz'.format(lfbw2, hfbw2)
#pref_epo_erp = 'erp_{}-{}Hz_noloop'.format(lfbw2, hfbw2)

# events file to get total number of auditory stimuli 
eve_name = 'preproc.5d-eve'

# events for epochs
#Stimulus coding (five-digit format)
event_id_codd={'var_c/c4/c-/p0/std': 14121, 'var_c/c4/c-/p0/dev': 14122,'var_c/c4/c-/p0/std_oth': 14123,\
              'var_c/c5/c-/p0/std': 15121, 'var_c/c5/c-/p0/dev': 15122,'var_c/c5/c-/p0/std_oth': 15123,\
              'var_c/c6/c-/p0/std': 16121, 'var_c/c6/c-/p0/dev': 16122,'var_c/c6/c-/p0/std_oth': 16123,\
              'var_c/c2/c+/p0/std': 12221, 'var_c/c2/c+/p0/dev': 12222,'var_c/c2/c+/p0/std_oth': 12223,\
              'var_c/c3/c+/p0/std': 13221, 'var_c/c3/c+/p0/dev': 13222,'var_c/c3/c+/p0/std_oth': 13223,\
              'var_c/c4/c+/p0/std': 14221, 'var_c/c4/c+/p0/dev': 14222,'var_c/c4/c+/p0/std_oth': 14223,\
              'var_c/c6/c+/p0/std': 16221, 'var_c/c6/c+/p0/dev': 16222,'var_c/c6/c+/p0/std_oth': 16223,\
              'var_c/c7/c+/p0/std': 17221, 'var_c/c7/c+/p0/dev': 17222,'var_c/c7/c+/p0/std_oth': 17223,\
              'var_c/c8/c+/p0/std': 18221, 'var_c/c8/c+/p0/dev': 18222,'var_c/c8/c+/p0/std_oth': 18223,\
              'var_p/c2/c-/p+/std': 22111, 'var_p/c2/c-/p+/dev': 22112,'var_p/c2/c-/p+/std_oth': 22113,\
              'var_p/c3/c-/p+/std': 23111, 'var_p/c3/c-/p+/dev': 23112,'var_p/c3/c-/p+/std_oth': 23113,\
              'var_p/c4/c-/p+/std': 24111, 'var_p/c4/c-/p+/dev': 24112,'var_p/c4/c-/p+/std_oth': 24113,\
              'var_p/c4/c-/p0/std': 24121, 'var_p/c4/c-/p0/dev': 24122,'var_p/c4/c-/p0/std_oth': 24123,\
              'var_p/c5/c-/p0/std': 25121, 'var_p/c5/c-/p0/dev': 25122,'var_p/c5/c-/p0/std_oth': 25123,\
              'var_p/c6/c-/p0/std': 26121, 'var_p/c6/c-/p0/dev': 26122,'var_p/c6/c-/p0/std_oth': 26123,\
              'var_p/c6/c-/p-/std': 26131, 'var_p/c6/c-/p-/dev': 26132,'var_p/c6/c-/p-/std_oth': 26133,\
              'var_p/c7/c-/p-/std': 27131, 'var_p/c7/c-/p-/dev': 27132,'var_p/c7/c-/p-/std_oth': 27133,\
              'var_p/c8/c-/p-/std': 28131, 'var_p/c8/c-/p-/dev': 28132,'var_p/c8/c-/p-/std_oth': 28133                  }

# plot ERPs
plot_bw2=False
# Plot 
plot_bw1=True
# two ERPs (126 epochs max)
erp_list = ['var_p/c4/c-/p+/std_oth', 'var_c/c4/c+/p0/std_oth']
baseline = (-0.2, 0) # [-100 0] ms


################################################################################
# Let's start!
################################################################################

# subj_id_list -----------------------
# test on one subject
subj_id_list = np.arange(35,36) # sub-00
# applyto all
#subj_id_list = np.arange(1,36) # 0 to 35, to include sub-00 to sub-35


#
for i_su in subj_id_list:
    subj='sub-{:02}'.format(i_su) 
    dir_imne = os.path.join(dir_mne, subj, 'preproc') #individual mne path
    

    #### ---- Load early epochs (1-40 hz)
    f_early_epochs = os.path.join(dir_imne, '{}.{}-epo.fif'.format(subj, pref_early_epochs)) 
    epochs_early = read_epochs(f_early_epochs) 
    print('{} : {} epochs after early rejection (step 04) '.format(subj, len(epochs_early)))
    
    #### ---- Load clean epochs (1-40 hz)
    f_clean_epochs = os.path.join(dir_imne, '{}.{}-epo.fif'.format(subj, pref_clean_epochs)) 
    epochs_clean = read_epochs(f_clean_epochs) 
    
    ### load autoreject results
    f_ar_reject = os.path.join(dir_imne, '{}.{}.pkl'.format(subj, pref_ar_reject)) 
    with  open(f_ar_reject, 'rb') as input:
        a = pickle.load(input)
        kappa = a[0]
        T = a[1]
        bad_epochs = a[2]
        labels = a[3]
        
    ### test if files are compatible
    if len(epochs_early) != len(bad_epochs):
        print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        print('{} : Not the same number of epochs in  {} and autoreject file !!!'.format(subj, f_early_epochs))
        print('{} : {} in {}, {} in autoreject (bad_epochs) '.format(subj, len(epochs_early), f_early_epochs), len(bad_epochs))
        print('    ABANDON ')
        print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
    
    else:
        
        # total number of events
        f_events = os.path.join(dir_imne, '{}.{}.fif'.format(subj, eve_name)) 
        events=read_events(f_events)
        tot = len(events)
        tot_early = len(epochs_early)
        tot_ar = len(epochs_clean)
        rej_pc_global = round(100*(tot-tot_ar)/tot)
        rej_pc_ar = round(100*(tot_early-tot_ar)/tot_early)
        
        # list bad sensors (those having NaN in labels)
        # yes I don't know how to islotae NaN values in Python...
        # and I could have stored sens_exclude in the pickle file in step07 ... pfff
        count = labels.sum(axis=0)*100/tot_early # should be NaN
        bad_sensors = list()
        for idx, name in enumerate(epochs_clean.ch_names):
        
            if count[idx]>=0:
                print(idx, name, round(count[idx]))
            else:
                print(idx, name, 'set as bad' )
                bad_sensors.append(name)

        f_bad_sensors = os.path.join(dir_imne, '{}.{}.pkl'.format(subj, pref_bad_sensors)) 
        if not os.path.exists(f_bad_sensors):
            with  open(f_bad_sensors, 'wb') as output:
                pickle.dump((bad_sensors), output, pickle.HIGHEST_PROTOCOL)
                
                 
        

   
        if plot_bw1:
            print('youpi')
            evokeds = [epochs_clean[name].average().apply_baseline(baseline) for name in erp_list]
            id_to_be_counted = list()
            for i_ev, name in enumerate(erp_list):
                evokeds[i_ev].comment = '{}, {}-{}Hz'.format(name, lfbw1, hfbw1)
                id_to_be_counted.append(event_id_codd[name])
            plot_evoked_topo(evokeds, ylim=dict(eeg=[-5,5]))
            count_events(epochs_clean.events, id_to_be_counted, events_ref=epochs_early.events)
            del evokeds

        if plot_bw2:
                dir_imne_erp = os.path.join(dir_mne, subj, 'erp_{}-{}Hz'.format(lfbw2, hfbw2)) #individual mne path
                f_epochs_erp=os.path.join(dir_imne_erp, '{}.{}-epo.fif'.format(subj, pref_epo_erp))
                if os.path.exists(f_epochs_erp):
                    epochs_erp = read_epochs(f_epochs_erp) 
                    epochs_erp.info['bads'] = []
                    evokeds = [epochs_erp[name].average().apply_baseline(baseline) for name in erp_list]
                    for i_ev, name in enumerate(erp_list):
                        evokeds[i_ev].comment = '{}, {}-{}Hz'.format(name, lfbw2, hfbw2)
                    plot_evoked_topo(evokeds, ylim=dict(eeg=[-5,5]), title=subj)
                    del evokeds
                
        print('-----------------------------------------------------------------')
        print('{} : {} events (total); {} epochs in early rejection; {} epochs for the ERP analysis'.format(subj, tot, tot_early, tot_ar) )
        print('----- : total rejection = {} %, autoreject rejection = {} %'.format(rej_pc_global, rej_pc_ar))
        print('----- : kappa = {} % '.format(round(100* kappa['eeg'])))
        print('----- : bad sensors ({} + STI 014)= {}'.format(len(bad_sensors)-1, bad_sensors))       
        print('-----------------------------------------------------------------')

                





    
    