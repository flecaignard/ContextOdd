#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
===============================================
Step 07 - Autoreject to drop bad epochs
===============================================

Working with -00 +410 ms epochs, 1-40 Hz bandpass,
We use autoreject from mne to compute automatically sensor thresholds and identify bad sensors
We don't use the repair-epoch tool
Output = ?

Created on Mon Feb 11 17:57:17 2019

@author: Françoise Lecaignard, francoise.lecaignard@inserm.fr
"""


################################################################################
# Precisely,
# 1) we take the 1-40Hz continuous data ,and apply the ica correction 
# 2) we import raw-unfiltered trial selection
# 3) we compute the epoching on 1-40Hz continuous with selected events, and apply NO artifact rejection
# 4) we compute auto-reject as follows
# -------------- no interpolation of signals: an epoch is either good or bad across all sensors
# -------------- we test the use of estimated consensus versus fixed one (parameter kappa: percentage of bad sensors to declare an epoch as bad)
# ---- 4.1) we launch autoreject with exclusion of bad sensors identified in step04 => threshold per channel T, kappa 
# ---- 4.2) we decide if additional bad sensors, based on T
# ---- 4.3) if yes, we relaunch 4.1 to get the updated (T, kappa)  with new subset of sensors
# ---- 4.4) plots of std ERPs (with n= 120) to decide if it is fine or not; if not, then we should fix kappa ?

################################################################################

###############################################################################
import numpy as np             # convention d'import
import os
import sys      # path to my own functions
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Python/EEG')
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/toolboxes/autoreject')

import matplotlib.pyplot as plt # from the MNE examples
import pickle
import tqdm # progress bars

from mne.io import read_raw_fif
from mne import Epochs, pick_types, read_events, read_epochs, pick_events
from mne import get_config, set_config
type(get_config('MNE_BROWSE_RAW_SIZE'))
set_config('MNE_BROWSE_RAW_SIZE', '20.0,10.0')
from mne.preprocessing import read_ica
from mne.viz import plot_evoked_topo

from autoreject import compute_thresholds, get_rejection_threshold
from autoreject import set_matplotlib_defaults  # noqa
set_matplotlib_defaults(plt)
from autoreject import AutoReject

from fun_eeg_misc import  print_step_label, count_events, import_epochs, peak_to_peak_raw
from fun_eeg_plot import  plot_epochs_drop
from fun_autoreject_plot import  plot_autoreject_threshold_hist,plot_autoreject_labels
from fun_07_autoreject import codd_eeg_sample_ar_training_epochs,launch_and_see_autoreject

################################################################################
# Input Parameters 
################################################################################

# Paths ----------------------------
dir_mne = '/sps/cermep/cermep/experiments/DCM/ContextOdd/Ana_MNE'



################################################################################
# Define Constant Variables
################################################################################
# Prefix for "raw" continuous raw file , large band, 1-40 Hz or so
pref_raw_bw1 = 'preproc_1-40'

# Prefix for filtered continuous raw file , ERP-bandwdth, 2-20 Hz or so
pref_raw_bw2 = 'preproc_2-20Hz'

# prefix for ica files
pref_ica = 'preproc_1-40Hz'


# Pref epoch file, contains epochs from raw 1-40 Hz data (step05)
pref_epo_bw1 = 'preproc_1-40Hz'

# Numbre of sampled epochs to fit the autoreject, and associated prefix for filename (pickle file)
nr_learn_epochs = 150
pref_ar_learn_epochs = 'AutoReject_training_epochs{}'.format(nr_learn_epochs)

#Output file
pref_ar_out = 'AutoReject_output'

## Pref epoch file, contains epochs from raw 1-40 Hz data (step05) + rejection on ica data
## This is the output file
#pref_epo_out = 'final_1-40Hz'
#
# events for epochs
#Stimulus coding (five-digit format)
event_id_codd={'var_c/c4/c-/p0/std': 14121, 'var_c/c4/c-/p0/dev': 14122,'var_c/c4/c-/p0/std_oth': 14123,\
              'var_c/c5/c-/p0/std': 15121, 'var_c/c5/c-/p0/dev': 15122,'var_c/c5/c-/p0/std_oth': 15123,\
              'var_c/c6/c-/p0/std': 16121, 'var_c/c6/c-/p0/dev': 16122,'var_c/c6/c-/p0/std_oth': 16123,\
              'var_c/c2/c+/p0/std': 12221, 'var_c/c2/c+/p0/dev': 12222,'var_c/c2/c+/p0/std_oth': 12223,\
              'var_c/c3/c+/p0/std': 13221, 'var_c/c3/c+/p0/dev': 13222,'var_c/c3/c+/p0/std_oth': 13223,\
              'var_c/c4/c+/p0/std': 14221, 'var_c/c4/c+/p0/dev': 14222,'var_c/c4/c+/p0/std_oth': 14223,\
              'var_c/c6/c+/p0/std': 16221, 'var_c/c6/c+/p0/dev': 16222,'var_c/c6/c+/p0/std_oth': 16223,\
              'var_c/c7/c+/p0/std': 17221, 'var_c/c7/c+/p0/dev': 17222,'var_c/c7/c+/p0/std_oth': 17223,\
              'var_c/c8/c+/p0/std': 18221, 'var_c/c8/c+/p0/dev': 18222,'var_c/c8/c+/p0/std_oth': 18223,\
              'var_p/c2/c-/p+/std': 22111, 'var_p/c2/c-/p+/dev': 22112,'var_p/c2/c-/p+/std_oth': 22113,\
              'var_p/c3/c-/p+/std': 23111, 'var_p/c3/c-/p+/dev': 23112,'var_p/c3/c-/p+/std_oth': 23113,\
              'var_p/c4/c-/p+/std': 24111, 'var_p/c4/c-/p+/dev': 24112,'var_p/c4/c-/p+/std_oth': 24113,\
              'var_p/c4/c-/p0/std': 24121, 'var_p/c4/c-/p0/dev': 24122,'var_p/c4/c-/p0/std_oth': 24123,\
              'var_p/c5/c-/p0/std': 25121, 'var_p/c5/c-/p0/dev': 25122,'var_p/c5/c-/p0/std_oth': 25123,\
              'var_p/c6/c-/p0/std': 26121, 'var_p/c6/c-/p0/dev': 26122,'var_p/c6/c-/p0/std_oth': 26123,\
              'var_p/c6/c-/p-/std': 26131, 'var_p/c6/c-/p-/dev': 26132,'var_p/c6/c-/p-/std_oth': 26133,\
              'var_p/c7/c-/p-/std': 27131, 'var_p/c7/c-/p-/dev': 27132,'var_p/c7/c-/p-/std_oth': 27133,\
              'var_p/c8/c-/p-/std': 28131, 'var_p/c8/c-/p-/dev': 28132,'var_p/c8/c-/p-/std_oth': 28133                  }



# epoch size
tmin, tmax = -0.2, 0.41

# ------ Testing rejection threshold on 2-20 Hz ERPs
# Testing events (we choose these because they rest on ~120 events)
erp_list = ['var_p/c4/c-/p+/std_oth', 'var_c/c4/c+/p0/std_oth']
event_id_plot = [event_id_codd[name] for name in erp_list]
# Testing sensors
picks_plot = ('Fp1', 'Fp2', 'Fz' 'FCz', 'Cz', 'Pz', 'Oz', 'TP9', 'TP10')
# Testing filter
lf, hf = 2,20 #Hz
#Testing ERP baseline
baseline = (-0.2, 0) # [-100 0] ms

# individual peak-2-peak amplitude threshold , in V
threshold_eeg = { 'sub-00': 85e-6,	 'sub-01': 85e-6,	'sub-02':100e-6,	 'sub-03': 100e-6,	'sub-04': 100e-6,\
             'sub-05': 100e-6,	 'sub-06': 100e-6,	'sub-07':100e-6,	 'sub-08': 100e-6,	'sub-09': 100e-6,\
             'sub-10': 100e-6,    'sub-11': 100e-6,   'sub-12':100e-6,  'sub-13': 100e-6,    'sub-14': 100e-6,\
             'sub-15': 100e-6,    'sub-16': 100e-6,   'sub-17':100e-6,  'sub-18': 100e-6,    'sub-19': 100e-6,\
             'sub-20': 100e-6,    'sub-21': 100e-6,   'sub-22':100e-6,	 'sub-23': 100e-6,    'sub-24': 100e-6,\
             'sub-25': 100e-6,    'sub-26': 100e-6,   'sub-27':100e-6,	 'sub-28': 100e-6,    'sub-29': 100e-6,\
             'sub-30': 100e-6,    'sub-31': 100e-6,   'sub-32':100e-6,	 'sub-33': 100e-6,    'sub-34': 100e-6,\
             'sub-35': 100e-6	}

# indvidual comments
#sub-00: 70 uV gives nice baseline, but rm 36%- 80uV rm 17%- we go for 85 uV = 11%, baseline is a compormise
### bad sensors ###
# This dictionary combines bad sensors from step04 (exclide_eeg) and bad sensors identified here
# To be filled in with current preprocs- 
# These sensors are set as bad, but not dropped
exclude_eeg = { 'sub-00': [],	 'sub-01': [],	'sub-02':['CP5', 'P4'],	 'sub-03': [],	'sub-04': ['PO10'],\
             'sub-05': [],	 'sub-06': [],	'sub-07':[],	 'sub-08': [],	'sub-09': [],\
             'sub-10': [],    'sub-11': ['PO9'],   'sub-12':[],  'sub-13': [],    'sub-14': [],\
             'sub-15': [],    'sub-16': [],   'sub-17':[],  'sub-18': [],    'sub-19': [],\
             'sub-20': [],    'sub-21': [],   'sub-22':['T7'],	 'sub-23': ['CP1'],    'sub-24': [],\
             'sub-25': ['T8', 'FC2', 'Oz'],    'sub-26': [],   'sub-27':[],	 'sub-28': [],    'sub-29': [],\
             'sub-30': [],    'sub-31': [],   'sub-32':[],	 'sub-33': [],    'sub-34': [],\
             'sub-35': []	}

exclude_eeg_output = { 'sub-00': [],	 'sub-01': ['T8', 'FC6', 'CP6'],	'sub-02':['CP5', 'P4'],	 'sub-03': ['F8', 'FC6', 'C3', 'T8'],	'sub-04': ['PO10', 'Fp2', 'F3', 'T7', 'FC6', 'T8'],\
             'sub-05': ['Fp1', 'F4', 'FC1', 'FC2', 'C3', 'Cz', 'PO10', 'F3', 'F8', 'CP2', 'O2', 'FC6', 'C4', 'Fz', 'CP1'],	 'sub-06': ['T7'],	\
             'sub-07': ['CP5', 'CP1', 'CP2', 'P7', 'PO9'],	 'sub-08': ['FC2', 'C4', 'CP6', 'P3', 'P8', 'Fp1', 'FC6', 'P7', 'O1', 'Fp2', 'PO9'],	'sub-09': ['T7', 'C3', 'CP6', 'Pz', 'P4', 'O2', 'Fp1', 'F7', 'F3', 'Fz', 'FC2', 'Cz'],\
             'sub-10': ['T8', 'O1', 'Oz', 'O2'],    'sub-11': ['PO9', 'Fp2', 'T7'],   'sub-12':[],  'sub-13': [],    'sub-14': [],\
             'sub-15': [],    'sub-16': [],   'sub-17':[],  'sub-18': [],    'sub-19': [],\
             'sub-20': [],    'sub-21': [],   'sub-22':['T7'],	 'sub-23': ['CP1'],    'sub-24': [],\
             'sub-25': ['T8', 'FC2', 'Oz'],    'sub-26': [],   'sub-27':[],	 'sub-28': [],    'sub-29': [],\
             'sub-30': [],    'sub-31': [],   'sub-32':[],	 'sub-33': [],    'sub-34': [],\
             'sub-35': []	}



# Number of dropped epoched for each subject (out of 6048) 
# nr_ok, kappa, rej_pc, quality

ok_epochs_outpur = { 'sub-00': (6006, 95, 0, 'erp++'),	 'sub-01': (5426, 25, 9, 'erp--'),	'sub-02':(4195, 10, 26, 'erp+'),	 'sub-03': (5744, 60, 2, 'bl-erpok'),	'sub-04': (4781, 25, 17, 'erp-'),\
             'sub-05': (5802, 85, 3, 'erp++'),	 'sub-06': (5315, 30, 7, 'erpok'),	'sub-07':(5401, 20, 8, 'lbokerp-'),	 'sub-08': (5938, 95, 1, 'lbokerp-'),	'sub-09': (5535, 35, 8, 'erp-'),\
             'sub-10': (1844, 25, 20, 'bl--'),    'sub-11': (5433,40,9,'erpok'),   'sub-12':6,  'sub-13': 344,    'sub-14': 166,\
             'sub-15': 31,    'sub-16': 509,   'sub-17':8,  'sub-18': 82,    'sub-19': 58,\
             'sub-20': 45,    'sub-21': 95,   'sub-22':593,	 'sub-23': 382,    'sub-24': 6,\
             'sub-25': 52,    'sub-26': 35,   'sub-27':87,	 'sub-28': 431,    'sub-29': 165,\
             'sub-30': 229,    'sub-31': 304,   'sub-32':390,	 'sub-33': 155,    'sub-34': 361,\
             'sub-35': 212	}

# Plot epochs
plot_raw=False
plot_epochs=False


################################################################################
# Let's start!
################################################################################

# subj_id_list -----------------------
# test on one subject
subj_id_list = np.arange(11,12) # sub-00
# applyto all
#subj_id_list = np.arange(1,36) # 0 to 35, to include sub-00 to sub-35


#
for i_su in subj_id_list:
    
    subj='sub-{:02}'.format(i_su) 
    dir_imne = os.path.join(dir_mne, subj, 'preproc') #individual mne path

    # 1) we take the 1-40Hz continuous data ,and apply the ica correction
    ################################################################################
    f_raw_bw1 = os.path.join(dir_imne, '{}.{}.raw.fif'.format(subj, pref_raw_bw1)) 
    raw_bw1 = read_raw_fif(f_raw_bw1, preload=True)
    f_ica = os.path.join(dir_imne, '{}.{}-ica.fif'.format(subj, pref_ica))
    raw_bw1_uncorr = raw_bw1.copy()
    ica=read_ica(f_ica)
    raw_bw1 = ica.apply(raw_bw1)

    # 2) we take events, from original epochs derived from raw-unfiltered data (setp04))
    ################################################################################
    f_epochs_bw1=os.path.join(dir_imne, '{}.{}-epo.fif'.format(subj, pref_epo_bw1))
    epochs_bw1 = read_epochs(f_epochs_bw1) 
    ica.apply(epochs_bw1)
    events_orig=epochs_bw1.events # orig does not refer to original events (nr = 6048), but to the resulting ones after jump-like noise rejection (step 04)
    
            
    # 3) selection of few representative epochs to train AutoReject
    ################################################################################
    # let's try with 120 epochs
    f_ar_trainingtest = os.path.join(dir_imne, '{}.{}.pkl'.format(subj, pref_ar_learn_epochs)) 
    if not os.path.isfile(f_ar_trainingtest):
        learn_epochs, sample_idxs = codd_eeg_sample_ar_training_epochs(epochs_bw1,nr_learn_epochs, f_ar_trainingtest)
    else:
        with  open(f_ar_trainingtest, 'rb') as input:
            a = pickle.load(input)
            learn_epochs = a[0]
            sample_idxs = a[1]


    
    # 4) Fit autoreject on selected epochs, with no interpolation allowed
    ################################################################################
    # outut = threshes (channel level) , and kappa K : % of sensors above threshes to declare an epoch as bad
    # K is not used because it may be affected by bad sensors, not identified yet
    
    sens_exclude = exclude_eeg[subj]
    flag_stop = False
    rej_contrib = 30 # percentage of contribution of sesnors in global artifact rejection
    loop_id=0
    while not flag_stop:
        print('Iter #{}'.format(loop_id))
        picks_ar = pick_types(epochs_bw1.info, meg=False, eeg=True, stim=False,  eog=False, exclude=sens_exclude)
        ar, reject_log, epochs_bw1_clean, sens_exclude, pc, flag_stop = launch_and_see_autoreject(subj, epochs_bw1, picks_ar,  sens_exclude, learn_epochs, event_id_codd, erp_list, maxflag = rej_contrib,  mod='eeg', kappa=None, plot_learn=False)

        print('-------- {},    {} % rejection'.format(sens_exclude, pc))
        loop_id = loop_id +1
            
    f_ar_out = os.path.join(dir_imne, '{}.{}.pkl'.format(subj, pref_ar_out))        
    with  open(f_ar_out, 'wb') as output:
        pickle.dump((reject_log, sens_exclude), output, pickle.HIGHEST_PROTOCOL)
        
        
    
    #free kappa - consensus
    # ar, reject_log, epochs_bw1_clean, rej_list = launch_and_see_autoreject(subj, epochs_bw1, picks_ar,  learn_epochs, event_id_codd, erp_list, maxflag = 30,  mod='eeg', kappa=None, plot_learn=False)
    
    # kappa fixed to 3 sensors
#    kappa = 3/len(picks_ar)
#    ar2, reject_log2, epochs_bw1_clean2 = launch_and_see_autoreject(subj, epochs_bw1, picks_ar,  learn_epochs, event_id_codd, erp_list, mod='eeg', kappa=0.1, plot_learn=False)
#
    
 
    
    #    # 4) we compute 2-20Hz continous data, and apply the ica correction
    #    ################################################################################
    #    f_raw_bw2 = os.path.join(dir_imne, '{}.{}.raw.fif'.format(subj, pref_raw_bw2)) 
    #    if not os.path.isfile(f_raw_bw2):
#        raw_bw2 = raw_bw1.filter(lf, hf,  l_trans_bandwidth='auto',filter_length='auto', phase='zero', fir_window='hann', fir_design='firwin')
#        raw_bw2.save(fname=f_raw_bw2,overwrite=True)
#        
#    else:
#        raw_bw2 = read_raw_fif(f_raw_bw2, preload=True)
#    # raw_bw2 is ica-corrected since applying on corrected raw_bw1: test ok
#    
#    
#    raw_bw2.info['bads']=exclude_eeg_ind
#    if plot_raw:
#        raw_bw2.plot( n_channels=raw_bw2.info['nchan'],remove_dc = True,  duration=30, highpass = None, scalings=dict(eeg=10e-5))
#     
#    # 5) we apply the epoching to 2-20 Hz data without artifact rejection, using the resulting 1-40 HJz selected events
#    ################################################################################
#    picks = pick_types(raw_bw2.info, eeg=True, stim=True,exclude=())
#    epochs_bw2 = import_epochs(subj, raw_bw2, epochs_bw1_rej,  picks,  event_id_codd, tmin, tmax,  plot=False)
#    if plot_epochs:
#        epochs_bw2.plot(scalings= dict(eeg=10e-5), n_epochs=20, n_channels=32, title=subj, events=None, event_colors=None, show=True, block=False, decim='auto', noise_cov=None)
##    # MANUAL STEP (IF EXPLORATION NECESSARY) - start .........
##    # comment / uncomment if needed
##    # want to see epochs?
##    # epochs_bw2.plot(scalings= dict(eeg=10e-5), n_epochs=20, n_channels=32, title=subj, events=None, event_colors=None, show=True, block=False, decim='auto', noise_cov=None)
##    epochs_bw2['var_p/c4/c-/p0/std'].plot(scalings= dict(eeg=10e-5), n_epochs=20, n_channels=32, title=subj, events=None, event_colors=None, show=True, block=False, decim='auto', noise_cov=None)
##    # want to compute peak-to-peak value within en epoch for an event in particular?
##    epoch_id = 98 #idx relative to all events in epochs_bw2
##    sample = epochs_bw2.events[epoch_id-1,0]
##    lat = epochs_bw2.events[epoch_id-1,0] / epochs_bw2.info['sfreq'] # sec
##    pre_lat = -0.2
##    post_lat = 0.41
##    print(raw_bw1.info['lowpass'], raw_bw1.info['highpass'])
##    peak_to_peak_raw(raw_bw1, lat, pre_lat, post_lat, picks, thresh=None, plot=False)
##    # MANUAL STEP (IF EXPLORATION NECESSARY) - end .........
#
#    # 6) we compute the 2-20 Hz dev and std ERPs (interpolation of bad sensors), and decide about the resulting SNR
#    ################################################################################
#    evokeds = [epochs_bw2[name].average().apply_baseline(baseline) for name in erp_list]
#    id_to_be_counted = list()
#    for i_ev, name in enumerate(erp_list):
#        evokeds[i_ev].comment = '{}, {}uV'.format(name, threshold_eeg[subj]*1e6)
#        id_to_be_counted.append(event_id_codd[name])
#    plot_evoked_topo(evokeds, ylim=dict(eeg=[-5,5]))
#    count_events(epochs_bw2.events, id_to_be_counted, events_ref=events_orig)
##  
##
###    epochs_erp.plot(events = epochs_erp['var_p/c4/c-/p0/std'].events)
###    events_test = pick_events(epochs_erp.events, include=[25121])
###    epochs_erp.plot(picks=(1,2,3), events=events_test)
###  
##    # FINALLY: save
#    ################################################################################
##    f_epoch_out=os.path.join(dir_imne, '{}.{}-epo.fif'.format(subj, pref_epo_out))
##    epochs_rej.save(f_epoch_out)    
##

#################################################################################
# Tests
################################################################################
    

# test on raw data, a peak-to-peak value at a particular latency
#lat = 1296.15 # sec
#pre_lat = -0.1
#post_lat = 0.1
#peak_to_peak_raw(raw, lat, pre_lat, post_lat, picks, thresh=None, plot=False)


##plot raw data
#scal = dict(eeg=10e-5)
#raw.plot( n_channels=raw.info['nchan'],remove_dc = True,  duration=30, highpass = None, scalings=scal)
