#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
===============================================
Step 07 - Autoreject to drop bad epochs
===============================================

Working with -200 +410 ms early-rejection epochs, 1-40 Hz bandpass, (created in step05)
We use autoreject (AR) from mne to compute automatically sensor thresholds and identify bad sensors
We don't use the repair-epoch tool
Output ======> /preproc/sub-xx.AutoReject_training_epochs300.pkl <=> epochs selected to train AR
       ======> /preproc/sub-xx.preproc_bad_sensors_noiter.pkl <=> list of bad sensors
       ======> /preproc/sub-xx.AutoReject_output300_noiter.pkl <=> autoreject output, reject_log objects 

Created on Mon Feb 11 17:57:17 2019

@author: Françoise Lecaignard, francoise.lecaignard@inserm.fr
"""


################################################################################
# Precisely,
# 1) we take the 1-40Hz continuous downsampled data ,and apply the ica correction 
# 2) we import raw-unfiltered epoch selection (from step04, to derive what will be referred as early epochs)
# 3) we compute the (early) epoching on 1-40Hz continuous with selected events, and apply NO artifact rejection
# 4) we compute auto-reject as follows
# -------------- no interpolation of signals: an epoch is either good or bad across all sensors
# -------------- we  use the estimated consensus  (parameter kappa: percentage of bad sensors to declare an epoch as bad)
# ---- 4.1) we launch autoreject with exclusion of bad sensors identified in step04 => threshold per channel T, kappa 
# ---- 4.2) we decide if additional bad sensors, based on T
# ---- 4.3) if yes, we relaunch 4.1 to get the updated (T, kappa)  with new subset of sensors
# ---- 4.4) plots of std ERPs (with n= 120) to decide if it is fine or not; if not, then we should fix kappa ?

################################################################################

###############################################################################
import numpy as np             # convention d'import
import os
import sys      # path to my own functions
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Python/EEG')
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/toolboxes/autoreject')

import matplotlib.pyplot as plt # from the MNE examples
import pickle
import tqdm # progress bars

from mne.io import read_raw_fif
from mne import Epochs, pick_types, read_events, read_epochs, pick_events
from mne import get_config, set_config
type(get_config('MNE_BROWSE_RAW_SIZE'))
set_config('MNE_BROWSE_RAW_SIZE', '20.0,10.0')
from mne.preprocessing import read_ica
from mne.viz import plot_evoked_topo

from autoreject import compute_thresholds, get_rejection_threshold
from autoreject import set_matplotlib_defaults  # noqa
set_matplotlib_defaults(plt)
from autoreject import AutoReject

from fun_eeg_misc import  print_step_label, count_events, import_epochs, peak_to_peak_raw
from fun_eeg_plot import  plot_epochs_drop
from fun_autoreject_plot import  plot_autoreject_threshold_hist,plot_autoreject_labels
from fun_07_autoreject import codd_eeg_sample_ar_training_epochs,launch_and_see_autoreject

################################################################################
# Input Parameters 
################################################################################

# Paths ----------------------------
dir_mne = '/sps/cermep/cermep/experiments/DCM/ContextOdd/Ana_MNE'



################################################################################
# Define Constant Variables
################################################################################
# Prefix for "raw" continuous raw file , large band, 1-40 Hz or so
pref_raw_bw1 = 'preproc_1-40Hz'

# prefix for ica files
pref_ica = 'preproc_1-40Hz'


# Pref epoch file, contains epochs from raw 1-40 Hz data (step05)
pref_epo_bw1 = 'preproc_early_1-40Hz'

# Numbre of sampled epochs to fit the autoreject, and associated prefix for filename (pickle file)
nr_learn_epochs = 300
pref_ar_learn_epochs = 'AutoReject_training_epochs{}'.format(nr_learn_epochs)



#Output file

# we tested two options : either we iterate the ar fit to reject bad sensors contribution to more than 30% of global trial rejection
# and alternatively: we keep all sensors, and use kappa as learned in the first iteration

# iterate <=> rej_contrib = 30 (example)
# no iterate <=> rej_contrib = 100
# This parameter was initially for iterating on kappa and threshes while removing bad sensors (above rej_contrib)
# But it turns out useless because estimated kappa does account for these so no need to remove bad sensors in the top of autoreject
# I shoudl clear this part to avoid confusion...
rej_contrib = 100 # percentage of contribution of sesnors in global artifact rejection
    
#pref_ar_out = 'AutoReject_output{}_iter'.format(nr_learn_epochs)
pref_ar_out = 'AutoReject_output{}_noiter'.format(nr_learn_epochs)

#prefix for bad sensors list
#pref_bad_sensors = 'preproc_bad_sensors_iter'
pref_bad_sensors = 'preproc_bad_sensors_noiter'

## Pref epoch file, contains epochs from raw 1-40 Hz data (step05) + rejection on ica data
## This is the output file
#pref_epo_out = 'final_1-40Hz'
#
# events for epochs
#Stimulus coding (five-digit format)
event_id_codd={'var_c/c4/c-/p0/std': 14121, 'var_c/c4/c-/p0/dev': 14122,'var_c/c4/c-/p0/std_oth': 14123,\
              'var_c/c5/c-/p0/std': 15121, 'var_c/c5/c-/p0/dev': 15122,'var_c/c5/c-/p0/std_oth': 15123,\
              'var_c/c6/c-/p0/std': 16121, 'var_c/c6/c-/p0/dev': 16122,'var_c/c6/c-/p0/std_oth': 16123,\
              'var_c/c2/c+/p0/std': 12221, 'var_c/c2/c+/p0/dev': 12222,'var_c/c2/c+/p0/std_oth': 12223,\
              'var_c/c3/c+/p0/std': 13221, 'var_c/c3/c+/p0/dev': 13222,'var_c/c3/c+/p0/std_oth': 13223,\
              'var_c/c4/c+/p0/std': 14221, 'var_c/c4/c+/p0/dev': 14222,'var_c/c4/c+/p0/std_oth': 14223,\
              'var_c/c6/c+/p0/std': 16221, 'var_c/c6/c+/p0/dev': 16222,'var_c/c6/c+/p0/std_oth': 16223,\
              'var_c/c7/c+/p0/std': 17221, 'var_c/c7/c+/p0/dev': 17222,'var_c/c7/c+/p0/std_oth': 17223,\
              'var_c/c8/c+/p0/std': 18221, 'var_c/c8/c+/p0/dev': 18222,'var_c/c8/c+/p0/std_oth': 18223,\
              'var_p/c2/c-/p+/std': 22111, 'var_p/c2/c-/p+/dev': 22112,'var_p/c2/c-/p+/std_oth': 22113,\
              'var_p/c3/c-/p+/std': 23111, 'var_p/c3/c-/p+/dev': 23112,'var_p/c3/c-/p+/std_oth': 23113,\
              'var_p/c4/c-/p+/std': 24111, 'var_p/c4/c-/p+/dev': 24112,'var_p/c4/c-/p+/std_oth': 24113,\
              'var_p/c4/c-/p0/std': 24121, 'var_p/c4/c-/p0/dev': 24122,'var_p/c4/c-/p0/std_oth': 24123,\
              'var_p/c5/c-/p0/std': 25121, 'var_p/c5/c-/p0/dev': 25122,'var_p/c5/c-/p0/std_oth': 25123,\
              'var_p/c6/c-/p0/std': 26121, 'var_p/c6/c-/p0/dev': 26122,'var_p/c6/c-/p0/std_oth': 26123,\
              'var_p/c6/c-/p-/std': 26131, 'var_p/c6/c-/p-/dev': 26132,'var_p/c6/c-/p-/std_oth': 26133,\
              'var_p/c7/c-/p-/std': 27131, 'var_p/c7/c-/p-/dev': 27132,'var_p/c7/c-/p-/std_oth': 27133,\
              'var_p/c8/c-/p-/std': 28131, 'var_p/c8/c-/p-/dev': 28132,'var_p/c8/c-/p-/std_oth': 28133                  }



# epoch size
tmin, tmax = -0.2, 0.41

# ------ Testing rejection threshold on 2-20 Hz ERPs
# Testing events (we choose these because they rest on ~120 events)
erp_list = ['var_p/c4/c-/p+/std_oth', 'var_c/c4/c+/p0/std_oth']
#event_id_plot = [event_id_codd[name] for name in erp_list]
## Testing sensors
#picks_plot = ('Fp1', 'Fp2', 'Fz' 'FCz', 'Cz', 'Pz', 'Oz', 'TP9', 'TP10')
# Testing filter
lf, hf = 2,20 #Hz
#Testing ERP baseline
baseline = (-0.2, 0) # [-100 0] ms


# indvidual comments
#sub-00: 70 uV gives nice baseline, but rm 36%- 80uV rm 17%- we go for 85 uV = 11%, baseline is a compormise
### bad sensors ###
# This dictionary combines bad sensors from step04 (exclide_eeg) 
# These sensors are set as bad fo the AR modeling, but not dropped in epochs
exclude_eeg = { 'sub-00': [],	 'sub-01': [],	'sub-02':['CP5', 'P4'],	 'sub-03': [],	'sub-04': ['PO10'],\
             'sub-05': [],	 'sub-06': [],	'sub-07':[],	 'sub-08': [],	'sub-09': [],\
             'sub-10': [],    'sub-11': ['PO9'],   'sub-12':[],  'sub-13': [],    'sub-14': [],\
             'sub-15': [],    'sub-16': [],   'sub-17':[],  'sub-18': [],    'sub-19': [],\
             'sub-20': [],    'sub-21': [],   'sub-22':['T7'],	 'sub-23': ['CP1'],    'sub-24': [],\
             'sub-25': ['T8', 'FC2', 'Oz'],    'sub-26': [],   'sub-27':[],	 'sub-28': [],    'sub-29': [],\
             'sub-30': [],    'sub-31': [],   'sub-32':[],	 'sub-33': [],    'sub-34': [],\
             'sub-35': []	}





# Plot epochs
plot_raw=False
plot_epochs=False


################################################################################
# Let's start!
################################################################################

# subj_id_list -----------------------
# test on one subject
subj_id_list = np.arange(19,36) # sub-00
# applyto all
#subj_id_list = np.arange(1,36) # 0 to 35, to include sub-00 to sub-35


#
for i_su in subj_id_list:
    
    subj='sub-{:02}'.format(i_su) 
    dir_imne = os.path.join(dir_mne, subj, 'preproc') #individual mne path

    # 1) we take the 1-40Hz continuous data ,and apply the ica correction
    ################################################################################
    f_raw_bw1 = os.path.join(dir_imne, '{}.{}.raw.fif'.format(subj, pref_raw_bw1)) 
    raw_bw1 = read_raw_fif(f_raw_bw1, preload=True)
    f_ica = os.path.join(dir_imne, '{}.{}-ica.fif'.format(subj, pref_ica))
    raw_bw1_uncorr = raw_bw1.copy()
    ica=read_ica(f_ica)
    raw_bw1 = ica.apply(raw_bw1)

    # 2) we take events, from original epochs derived from raw-unfiltered data (setp04))
    ################################################################################
    f_epochs_bw1=os.path.join(dir_imne, '{}.{}-epo.fif'.format(subj, pref_epo_bw1))
    epochs_bw1 = read_epochs(f_epochs_bw1) 
    ica.apply(epochs_bw1)
    events_orig=epochs_bw1.events # orig does not refer to original events (nr = 6048), but to the resulting ones after jump-like noise rejection (step 04)
    
            
    # 3) selection of few representative epochs to train AutoReject
    ################################################################################
    # let's try with 120 epochs
    f_ar_trainingtest = os.path.join(dir_imne, '{}.{}.pkl'.format(subj, pref_ar_learn_epochs)) 
    if not os.path.isfile(f_ar_trainingtest):
        learn_epochs, sample_idxs = codd_eeg_sample_ar_training_epochs(epochs_bw1,nr_learn_epochs, f_ar_trainingtest)
    else:
        with  open(f_ar_trainingtest, 'rb') as input:
            a = pickle.load(input)
            learn_epochs = a[0]
            sample_idxs = a[1]


    
    # 4) Fit autoreject on selected epochs, with no interpolation allowed
    ################################################################################
    # Iterative process until no sensor contributes more than 30% (rej_contrib) in trial rejection
    # each iter provides : 
    #------------ fit of autoreject on a subset of epochs (nr=300) => threshes (per channel) and kappa (AutoReject output = consensus = nr of channels required to declare an epoch as bad, in percentage)
    # ------------ application of T and kappa to teh whole set of epochs => reject_log object with bad_epochs vector and labels matrix (N_epochs *N_Sensors) - No interpolation allowed
    # ------------ labels allows computing the contribution of each sensor in global rejection
    # Output : sub-XX.AutoReject_output300.pkl file
    
    sens_exclude = exclude_eeg[subj]
    flag_stop = False
    rej_contrib = 100 # percentage of contribution of sesnors in global artifact rejection
    loop_id=0
    while not flag_stop:
        print('Iter #{}'.format(loop_id))
        picks_ar = pick_types(epochs_bw1.info, meg=False, eeg=True, stim=False,  eog=False, exclude=sens_exclude)
        ar, reject_log, sens_exclude, pc, flag_stop = launch_and_see_autoreject(subj, epochs_bw1, picks_ar,  sens_exclude, learn_epochs, event_id_codd, erp_list, maxflag = rej_contrib,  mod='eeg', kappa=None, plot_learn=False, plot_erp=False)

        print('-------- {},    {} % rejection'.format(sens_exclude, pc))
        loop_id = loop_id +1
            
    f_ar_out = os.path.join(dir_imne, '{}.{}.pkl'.format(subj, pref_ar_out)) 
    kappa = ar.consensus_
    T = ar.threshes_
    bad_epochs = reject_log.bad_epochs
    labels = reject_log.labels       
    with  open(f_ar_out, 'wb') as output:
        pickle.dump((kappa, T, bad_epochs, labels), output, pickle.HIGHEST_PROTOCOL)
    
    bad_sensors = sens_exclude
    f_bad_sensors = os.path.join(dir_imne, '{}.{}.pkl'.format(subj, pref_bad_sensors)) 
    with  open(f_bad_sensors, 'wb') as output:
                pickle.dump((bad_sensors), output, pickle.HIGHEST_PROTOCOL)
    
        
