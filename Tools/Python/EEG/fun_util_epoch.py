import numpy as np             # convention d'import
import os                       # path.join for file names
import matplotlib.pyplot as plt # from the MNE examples
from bisect import bisect

from mne import Epochs, Annotations, pick_types, find_events, write_events, read_events
from mne.channels import read_layout
from mne.io import concatenate_raws, read_raw_brainvision, read_raw_fif, find_edf_events
from mne.datasets import eegbci
from mne.decoding import CSP
from mne.viz import plot_events, plot_ica_overlay
from mne.preprocessing import ICA, read_ica,  find_eog_events


import sys      # path to my own functions
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/toolboxes/autoreject')

from autoreject import  RejectLog
#from autoreject.viz import plot_epochs



print(__doc__)

import sys      # path to my own functions
#sys.path.append('/Users/francoise/Documents/Projets/ContextOdd/EEG_Data/Temp_AnaEEG/python')


#############################################################################################
##  o*o*o*o*o*o*o*o*o*o*o*o*o*o* epoching and epoch rejection      *o*o*o*o*o*o*o*o*o*o*o*o*o*
#############################################################################################
def import_epochs(subj, raw, epochs_in,  picks,  event_id, tmin, tmax,  plot=True):
    # Epoching of raw data, without rejection, importing a list of good/bad epochs already computed from a previous epoching step
    #
    #subj = name of subject, for plot title basically
    #raw =  mne raw object
    #epochs_in = epoch mne object, obtained from a prior epoching step
    #picks =  mne  object (maybe be used to drop bad sensors)
    #event_id = event id dictionary (compatible with epochs_in.events)
    #tmin, tmax: = pre-stim and post-stim in seconds
    #plot = boolean 
    
    # NOTE that bad sensors (if any, declared as bad in raw.info['bad']) will not be dropped
    # This is because they could be either fropped or intrepolated => this should be handled in a separate process
    
    #Epoching without rejection
    epochs_params=dict(events=epochs_in.events,event_id=event_id,
                 tmin=tmin, tmax=tmax, 
                 reject=None, picks=picks, detrend=None,
                 baseline=None, verbose=False)      
    epochs_out = Epochs(raw, **epochs_params)
    
    print('  Dropped {} epochs out of {}'.format(len(epochs_in.events)-len(epochs_out.events),len(epochs_in.events) ))
    
     
    if plot:
            epochs_out.plot(scalings= dict(eeg=10e-5), n_epochs=20, n_channels=32, title=subj, events=None, event_colors=None, show=True, block=False, decim='auto', noise_cov=None)
            epochs_in.plot(scalings= dict(eeg=10e-5), n_epochs=20, n_channels=32, title=subj, events=None, event_colors=None, show=True, block=False, decim='auto', noise_cov=None)
     
    return epochs_out
    

def get_dropped_epochs_idx(events_pre, events_post):
    # Return the indices of dropped epoched
    # It is possiblee that I miss something in MNE epoching but ...
    # When epoching raw data, the resulting Epochs object is made of good epochs, and we loose the information of dropped epochs
    #
    # events_pre = MNE events tab (raw.events or epochs.events)  N_events * 3 [sample 0 code]
    # events_post = MNE events tab, resulting from the epochining
    #
    # This function is based on event sample to recover dropped ones
    drop_idx = [idx for idx, sample in enumerate(events_pre[:,0]) if sample not in events_post[:,0]]
    summary = np.zeros(len(events_pre[:,0]))
    summary[drop_idx]=1
    return drop_idx, summary
    
            
    
def epoch_rej_and_plot(subj, raw, events, picks, thresh_dict, event_id, tmin, tmax, plot=True, picks_plot = None, verbose=True):
    # Epoching of raw data, with rejection of epochs  based on peak2peak threshold
    #if plot: plot of all epochs, showing good and bad ones
    #
    #subj = name of subject, for plot title basically
    #raw =  mne raw object
    #events =  mne  object
    #picks =  mne  object
    #thresh_dict =  dictironary for threshold amplitude per modality
    #event_id = event id dictionary
    #tmin, tmax: = pre-stim and post-stim in seconds
    #plot = boolean 
    #verbose = boolean
    
    #Epoching without rejection, to get the selected events based on event_id
    epochs_params=dict(events=events,event_id=event_id,
                 tmin=tmin, tmax=tmax, 
                 reject=None, picks=picks, 
                 baseline=None, verbose=False)      
    epochs_orig = Epochs(raw, **epochs_params)
    #epochs_orig.drop_bad()

    ### Epochs with thresh- rejection
    epochs_params=dict(events=events, event_id=event_id, 
                 tmin=tmin, tmax=tmax, 
                 reject=thresh_dict, picks=picks, detrend=None,
                 baseline=None, verbose=False)      
    epochs_rej = Epochs(raw, **epochs_params)
    epochs_rej.drop_bad()
    #epochs_rej.load_data()
    print('  Dropped {} epochs out of {}'.format(len(epochs_orig.events)-len(epochs_rej.events),len(epochs_orig.events) ))
    print('  Dropped %0.1f%% of epochs' % (epochs_rej.drop_log_stats(),))
    

    ### Reject_log
     
    nr_epochs = len(epochs_orig.events) #all epochs selected in event_id dict, good and bad
    nr_chan = epochs_orig.info['nchan']
    print('chan', nr_chan)
    
    # first version of code
    bad_epochs=[False]*nr_epochs #default: good epochs, bad=False
    chan_epochs_matrix  = [[0]*nr_epochs]*nr_chan # matrix n_chan * n_epochs, 0 if sensor good for this epoch, 1 if bad, 2 if interpolated

    for i_ep in np.arange(0,nr_epochs):
        #get index from original events list
        ind_ep = np.where(events[:,0]==epochs_orig.events[i_ep][0])[0] # based on event sample index
        if len(ind_ep)==1:
            ind_ep = ind_ep[0]
            
        #if this epoch good or bad?
        if epochs_rej.drop_log[ind_ep]: # if not empty, then bad
            bad_epochs[i_ep] = True 
            if verbose:
                print('Epoch #{} ( code={}, t_raw={}): BAD  -  {}'.format(i_ep+1, events[ind_ep,2], events[ind_ep,0]/raw.info['sfreq'], epochs_rej.drop_log[ind_ep]))
            
    labels = np.array(chan_epochs_matrix).transpose((1,0))
    
#    # Mainak Jas 's version
#    bad_epochs=np.zeros((nr_epochs,)) #default: good epochs, bad=False
#    labels = np.zeros((nr_epochs, nr_chan)) 
#    bad_idxs = [idx for idx, drop in enumerate(epochs_rej.drop_log) if drop !=['IGNORE']]
#    bad_epochs[bad_idxs] = 1

    
    reject_log = RejectLog(bad_epochs,labels, epochs_orig.info['ch_names'])
    scal = dict(eeg=10e-4)
    
    if plot:
            reject_log.plot_epochs( epochs_orig, picks = picks_plot,  scalings=scal ) #, n_epochs=20, n_channels=32, title='pouet')
            epochs_rej.plot_drop_log(subject=subj)
    
    return epochs_rej
    

       

def epoch_rejection_explore_threshold(raw, events, event_id, picks, tmin, tmax, threshold_range=np.linspace(1500e-6, 600e-6, 5)):
    # printscreen of the percentage of rejected epochs for different values of artifact rejectin threshold
    # can be used with peak_to_peak_raw to refine threshold value
    #        
    # to be adapted for MEG, and EEG-MEG
    #
    # inputs:
    #   raw =  [mne-raw data]
    #   events = [mne-events], contains all events
    #   event_id = [mne-event_id]
    #   picks = [mne-picks] subset of sensors to be include in computation,  eg raw.pick_types(eeg=True, exclude='bads)
    #   tmin, tmax = [float] defines with tmax the epoch interval in seconds (for each event in event_id)
    #   threshold_range = [numpy array of float] artifact threshold values  in V or T (peak-to-peak rejection)
    # output:
    #   NONE
    #
    # versions: 1) M. Maby, FLecaignard - 12-06-18
    #

  
    epochs_params=dict(events=events, event_id=event_id, 
                     tmin=tmin, tmax=tmax, 
                     reject=None, picks=picks, 
                     baseline=None, verbose=False)      
    epochs = Epochs(raw, **epochs_params)
    epochs.load_data()
    print(len(epochs))
    for i in range(len(threshold_range)):
        rej_thresh = threshold_range[i]
        epochs.drop_bad(reject={'eeg': rej_thresh}, verbose=False)
        perc_chan = epochs.drop_log_stats()
        print('Threshold {:.1f} uV : {:.1f}% rejection'.format(rej_thresh*1e6, perc_chan))


def epoch_rejection_adaptive_threshold(raw, events, event_id, picks, tmin, tmax, thresh_min=50e-6, thresh_max=150e-6, pc_epochs=10.0):
    # compute the rejection-threshold value that provides exactly a specific percentage of rejected epoch   
    # search is performed within the threshold interval [thresh_min, thresh_max], dichotomical seeking
    ### WORK IN PROGRESS !!!! ####
    # and to be adapted for MEG, and EEG-MEG
    #
    # inputs:
    #   raw =  [mne-raw data]
    #   events = [mne-events], contains all events
    #   event_id = [mne-event_id]
    #   picks = [mne-picks] subset of sensors to be include in computation,  eg raw.pick_types(eeg=True, exclude='bads)
    #   tmin, tmax = [float] defines with tmax the epoch interval in seconds (for each event in event_id)
    #   thresh_min, thresh_max = [float] boundaries of the threshold interval
    #   pc_epochs = the targeted percentage of epochs to be rejected with resulting threshold
    #
    #   output:
    #       threshold_out = [float] threshold value allowing the rejection of pc_epochs% of trials
    #
    # versions: 1) M. Maby, FLecaignard - 12-06-18
    #

     
    epochs_params=dict(events=events, event_id=event_id, 
                     tmin=tmin, tmax=tmax, 
                     reject=None, picks=picks, 
                     baseline=None, verbose=False)      
    epochs = Epochs(raw, **epochs_params)
   
    th = [thresh_min, thresh_max]
    pc, eps = 100.0, 1.0
    while not abs(pc - thresh_channel) < eps:
        pc  = [epochs.drop_bad(reject={'eeg': th[1]}, verbose=False).drop_log_stats(),  ## lower uV value gives larger rejection pc
        epochs.drop_bad(reject={'eeg': th[0]}, verbose=False).drop_log_stats() ]## lower uV value gives larger rejection pc    
        print('---')
        print(pc, th)

        if pc[0] <= pc_epochs <= pc[1]:
            i = bisect(pc,pc_epochs ) # provides index of the closest value
            pc = pc[i]
            threshold_out = th[i] 
            th = np.sort([th[i], (th[1]-th[0])/2])
            
        else:
            print('failed to find a threshold within {} , {} with {}% rejection'.format(thresh_min, thresh_max, pc_epochs))
            
    return threshold_out

           
