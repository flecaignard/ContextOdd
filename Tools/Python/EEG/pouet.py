#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
==================================================================================
DEBUG
==================================================================================


@author: Françoise Lecaignard, francoise.lecaignard@inserm.fr
"""



###############################################################################
import numpy as np             # convention d'import
import os
import matplotlib.pyplot as plt # from the MNE examples
import sys      # path to my own functions
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Python/EEG')
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/BIDS/mne-bids')

from mne.io import read_raw_fif
from mne import Epochs, pick_types, read_events, read_epochs
from mne import get_config, set_config
type(get_config('MNE_BROWSE_RAW_SIZE'))
set_config('MNE_BROWSE_RAW_SIZE', '20.0,10.0')
from mne.viz import  tight_layout
#from fun_NN_template import  ...

import pickle
################################################################################
# Input Parameters 
################################################################################

# Paths ----------------------------
dir_mne = '/sps/cermep/cermep/experiments/DCM/ContextOdd/Ana_MNE'

# ----------- continuous data folder
folder = 'preproc'
# ----------- prefix  of  continuous data 
pref_raw = 'preproc_1-40Hz_ica'
pref_epo_in = 'preproc_early_1-40Hz_ica'


# -----------   events file (pickle file)
eve_name = 'good_bad_events'

# -----------   plot options
dict_color_event = {300: 'green', 200: 'red', 100:'magenta'}
scal = dict(eeg=10e-5)

# subset of electrodes
subset = []
#subset = ['Oz', 'TP9', 'TP10', 'PO10' ,'PO9', 'O1', 'O2']

################################################################################
# Let's start!
################################################################################
i_su =0

subj='sub-{:02}'.format(i_su) 
dir_imne_preproc = os.path.join(dir_mne, subj, 'preproc') #individual mne path
dir_imne_raw = os.path.join(dir_mne, subj, folder) #individual mne path

#### 1 ----- load continuous data
f_raw = os.path.join(dir_imne_preproc, f'{subj}.{pref_raw}.raw.fif') #  continuous data
raw = read_raw_fif(f_raw, preload=True)


#### 1 ----- load epochs
f_epochs_in=os.path.join(dir_imne_preproc, '{}.{}-epo.fif'.format(subj, pref_epo_in))
epochs_in = read_epochs(f_epochs_in) 
    
#### 2 ----- load good/bad events

f_events_pkl = os.path.join(dir_imne_raw, f'{subj}.{eve_name}.pkl') ### created in step_08 summarize
with  open(f_events_pkl, 'rb') as input:
    a = pickle.load(input)

events = a[0]

# we compare p2p within 1st epoch : frow raw data and from epochs
    
D = raw.get_data()
s_epo = 3924
d_cont = D[:,s_epo-100:s_epo+205+1] # -200 ms +410 ms
mi = np.amin(d_cont, axis = 1) # ddim0 = sensors (minimum per sensor for each epoch)
ma = np.amax(d_cont, axis = 1) # dim0 = sensors (minimum per sensor for each epoch)
p2p = np.abs(ma -mi)

d_epo = epochs_in[0].get_data()[0]
mi = np.amin(d_epo, axis = 1) # ddim0 = sensors (minimum per sensor for each epoch)
ma = np.amax(d_epo, axis = 1) # dim0 = sensors (minimum per sensor for each epoch)
z = np.abs(ma -mi)
p2p = np.vstack((p2p,z))

