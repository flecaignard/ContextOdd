import numpy as np             # convention d'import
import os                       # path.join for file names
import matplotlib.pyplot as plt # from the MNE examples
from bisect import bisect

from mne import Epochs, Annotations, pick_types, find_events, write_events, read_events
from mne.channels import read_layout
from mne.io import concatenate_raws, read_raw_brainvision, read_raw_fif, find_edf_events
from mne.datasets import eegbci
from mne.decoding import CSP
from mne.viz import plot_events, plot_ica_overlay
from mne.preprocessing import ICA, read_ica,  find_eog_events


import sys      # path to my own functions
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/toolboxes/autoreject')

from autoreject import  RejectLog
#from autoreject.viz import plot_epochs



print(__doc__)

import sys      # path to my own functions
#sys.path.append('/Users/francoise/Documents/Projets/ContextOdd/EEG_Data/Temp_AnaEEG/python')

#############################################################################################
##  o*o*o*o*o*o*o*o*o*o*o*o*o*o* very much MISC functions  o*o*o*o*o*o*o*o*o*o*o*o*o*o*o*o*o*o*o*o*
#############################################################################################

def print_step_label(step_label):
    #not really useful but so lovely :-)
    print('  ')
    print('  ')
    print('-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o')
    print("-o-o-o-o-o-o-o-o-o-o     {}    -o-o-o-o-o-o-o-o-o-o      ".format(step_label))
    print('-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o')
    print('  ')
    print('  ')
    
    
    
#############################################################################################
##  o*o*o*o*o*o*o*o*o*o*o*o*o*o* measures on raw data o*o*o*o*o*o*o*o*o*o*o*o*o*o*o*o*o*o*o*o*
#############################################################################################


def peak_to_peak_raw(raw, lat, pre_lat, post_lat, picks, thresh=None, plot=False):
    # printscreen of peak-to-peak value over the time interval defined as [lat+pre_lat lat+post_lat]
    # inputs:
    #   raw =  [mne-raw data]
    #   lat =  [float] latency in sec (center of time interval)- eg: 450 sec
    #   pre_lat = [float] in sec- eg: -0.5 sec
    #   post_lat = [float] in sec- eg: 2 sec => this would give time interval [449.5 452] sec
    #   picks = [mne-picks] subset of sensors to be include in computation,  eg raw.pick_types(eeg=True, exclude='bads)
    #   thresh = [float, optional] threshold value  in V or T. Sensor is considered as bad if peak-to-peak > threshold
    #   plot = [boolean,  optional], plot raw data within pre-post time interval 
    #
    # output:
    #   NONE
    #
    # versions: 1) FLecaignard - 12-06-18

    t_idx = raw.time_as_index([lat+pre_lat, lat+post_lat])  
    print('Time interval around {} :[ {} {} ] sec.'.format(lat, lat+pre_lat, lat+post_lat))
     # print(t_idx[0],t_idx[1])
  
    data, times = raw[:, t_idx[0]:t_idx[1]]
     #print(data.shape, times.shape)
    amp_range = np.ptp(data, axis=1)
     # get max value => ind_max
     # plt.plot(data[ind_max,:])
    for ind, val in enumerate(amp_range):
        if thresh and val >= thresh:
            print('{}: {:0.1f} uV -------> REJ'.format(raw.ch_names[ind], val*1e6)) # reduce nr of decimals
        else:
            print('{}: {:0.1f} uV'.format(raw.ch_names[ind], val*1e6)) # reduce nr of decimals

    else:
            print('{}: {:0.1f} uV'.format(raw.ch_names[ind], val*1e6)) # reduce nr of decimals

    if plot:
        if thresh:
            scal = dict(eeg=thresh)
        else:
            scal = dict(eeg=20e-5)
        raw.crop(lat+pre_lat,lat+post_lat).plot(n_channels=raw.info['nchan'],remove_dc = True,   scalings=scal)
         
#############################################################################################
##  o*o*o*o*o*o*o*o*o*o*o*o*o*o* find the event closest to a particular latency in raw data     *o*o*o*o*o*o*o*o*o*o*o*o*o*
#############################################################################################
        
def find_event_at_a_latency(lat, raw, events):
    # find the indice of the closest event at particular latency
    # ex: screening raw data, you see an artifact => what event is eventually affected by this noise?
    # 
    #inputs:
    # lat = latency in seconds
    # raw = continuous data , MNE object
    # events = MNE event matrix (N_event *3)
    
    # convert latency seconds to samples:
    s = raw.time_as_index(lat)[0]
    
    eve_samples = events[:,0]
    eve = np.asarray(eve_samples)
    idx = (np.abs(eve - s)).argmin()
    return s, idx, events[idx,:]
#############################################################################################
##  o*o*o*o*o*o*o*o*o*o*o*o*o*o* epoching and epoch rejection      *o*o*o*o*o*o*o*o*o*o*o*o*o*
#############################################################################################

def epoch_rej_and_plot(subj, raw, events, picks, thresh_dict, event_id, tmin, tmax, plot=True, picks_plot = None, verbose=True):
    # Epoching of raw data, with rejection of epochs  based on peak2peak threshold
    #if plot: plot of all epochs, showing good and bad ones
    #
    #subj = name of subject, for plot title basically
    #raw =  mne raw object
    #events =  mne  object
    #picks =  mne  object
    #thresh_dict =  dictironary for threshold amplitude per modality
    #event_id = event id dictionary
    #tmin, tmax: = pre-stim and post-stim in seconds
    #plot = boolean 
    #verbose = boolean
    
    #Epoching without rejection, to get the selected events based on event_id
    epochs_params=dict(events=events,event_id=event_id,
                 tmin=tmin, tmax=tmax, 
                 reject=None, picks=picks, 
                 baseline=None, verbose=False)      
    epochs_orig = Epochs(raw, **epochs_params)
    #epochs_orig.drop_bad()

    ### Epochs with thresh- rejection
    epochs_params=dict(events=events, event_id=event_id, 
                 tmin=tmin, tmax=tmax, 
                 reject=thresh_dict, picks=picks, detrend=None,
                 baseline=None, verbose=False)      
    epochs_rej = Epochs(raw, **epochs_params)
    epochs_rej.drop_bad()
    #epochs_rej.load_data()
    print('  Dropped {} epochs out of {}'.format(len(epochs_orig.events)-len(epochs_rej.events),len(epochs_orig.events) ))
    print('  Dropped %0.1f%% of epochs' % (epochs_rej.drop_log_stats(),))
    

    ### Reject_log
     
    nr_epochs = len(epochs_orig.events) #all epochs selected in event_id dict, good and bad
    nr_chan = epochs_orig.info['nchan']
    print('chan', nr_chan)
    
    # first version of code
    bad_epochs=[False]*nr_epochs #default: good epochs, bad=False
    chan_epochs_matrix  = [[0]*nr_epochs]*nr_chan # matrix n_chan * n_epochs, 0 if sensor good for this epoch, 1 if bad, 2 if interpolated

    for i_ep in np.arange(0,nr_epochs):
        #get index from original events list
        ind_ep = np.where(events[:,0]==epochs_orig.events[i_ep][0])[0] # based on event sample index
        if len(ind_ep)==1:
            ind_ep = ind_ep[0]
            
        #if this epoch good or bad?
        if epochs_rej.drop_log[ind_ep]: # if not empty, then bad
            bad_epochs[i_ep] = True 
            if verbose:
                print('Epoch #{} ( code={}, t_raw={}): BAD  -  {}'.format(i_ep+1, events[ind_ep,2], events[ind_ep,0]/raw.info['sfreq'], epochs_rej.drop_log[ind_ep]))
            
    labels = np.array(chan_epochs_matrix).transpose((1,0))
    
#    # Mainak Jas 's version
#    bad_epochs=np.zeros((nr_epochs,)) #default: good epochs, bad=False
#    labels = np.zeros((nr_epochs, nr_chan)) 
#    bad_idxs = [idx for idx, drop in enumerate(epochs_rej.drop_log) if drop !=['IGNORE']]
#    bad_epochs[bad_idxs] = 1

    
    reject_log = RejectLog(bad_epochs,labels, epochs_orig.info['ch_names'])
    scal = dict(eeg=10e-4)
    
    if plot:
            reject_log.plot_epochs( epochs_orig, picks = picks_plot,  scalings=scal ) #, n_epochs=20, n_channels=32, title='pouet')
            epochs_rej.plot_drop_log(subject=subj)
    
    return epochs_rej
    


       
def plot_raw_rej_events(raw, events, epochs, rm_useless=[], scal= dict(eeg=10e-5)):
    # plot raw data with events classified as good (green) and bad (red) to easily see if threshold value for epoch rejection is conservative or not
    # can be used with peak_to_peak_raw to refine threshold value
    # inputs:
    #   raw =  [mne-raw data]
    #   events = [mne-events], contains all events
    #   epochs = [mne-epochs], results from an epoching with rejection, thus contains the accepted events
    #   scal = [dict], scalings for the raw plot
    #
    # output:
    #   NONE
    #
    # versions: 1) FLecaignard - 12-06-18

    events_sort=events.copy()
    events_good= epochs.events
    
    #remove irrelevant events
    if rm_useless:
        for i_u, code_u in enumerate(rm_useless):
#            print(code_u)            
            ind_u = np.where(events_sort[:,2]==code_u)[0] 
            events_sort = np.delete(events_sort, ind_u, 0) # rm rows of indices in ind_u
    print('Number of total events in events: {} - (!!! hard-coded)'.format(6048)  )      
    
    # set all events to code 2
    events_sort[:,2]=2
    
    # intersect epochs.events and events_sort
    good, sort_ind, good_ind = np.intersect1d(events_sort[:,0], events_good[:,0], return_indices=True)
    #print(good)
    print('Number of good events in events: {} '.format(len(good)))  
    print('Number of bad events in events: {} '.format(6048 - len(good)))  
    events_sort[sort_ind,2]=1
    #print(events_sort)
    
    # and plot
    dict_event = {'good':1, 'rej':2} 
    dict_color = {1: 'green', 2: 'red'}  
    raw.plot(events=events_sort, event_id=dict_event, event_color = dict_color, n_channels=raw.info['nchan'],remove_dc = True,  duration=30,   scalings=scal) #, show_options=False)
    plt.show()

def count_events(events, id_to_be_counted, events_ref=None):
    # summarize the number of events (printscreen), useful after trial rejection
    # inputs:
    #   events = [mne-events], contains all events
    #   id_to_be_counted = [list of int], list of event codes to be summarized
    #   events_ref = [mne-events, optional], contains all events prior to artifact rejection, allows to know the current accepted nr of events wrt the total
    # output:
    #   NONE
    #
    # versions: 1) FLecaignard - 12-06-18


    for i_c, code_c in enumerate(id_to_be_counted):
        #print(i_c, code_c)
        if len(events_ref):
            nr_ref = len(np.where(events_ref[:,2]==code_c)[0] )
            nr_cur = len(np.where(events[:,2]==code_c)[0] )
            print('Event {}: {} / {}'.format(code_c, nr_cur, nr_ref))
        
        else:
            nr_cur = len(np.where(events[:,2]==code_c)[0] )
            print('Event {}: {} '.format(code_c, nr_cur))


def epoch_rejection_explore_threshold(raw, events, event_id, picks, tmin, tmax, threshold_range=np.linspace(1500e-6, 600e-6, 5)):
    # printscreen of the percentage of rejected epochs for different values of artifact rejectin threshold
    # can be used with peak_to_peak_raw to refine threshold value
    #        
    # to be adapted for MEG, and EEG-MEG
    #
    # inputs:
    #   raw =  [mne-raw data]
    #   events = [mne-events], contains all events
    #   event_id = [mne-event_id]
    #   picks = [mne-picks] subset of sensors to be include in computation,  eg raw.pick_types(eeg=True, exclude='bads)
    #   tmin, tmax = [float] defines with tmax the epoch interval in seconds (for each event in event_id)
    #   threshold_range = [numpy array of float] artifact threshold values  in V or T (peak-to-peak rejection)
    # output:
    #   NONE
    #
    # versions: 1) M. Maby, FLecaignard - 12-06-18
    #

  
    epochs_params=dict(events=events, event_id=event_id, 
                     tmin=tmin, tmax=tmax, 
                     reject=None, picks=picks, 
                     baseline=None, verbose=False)      
    epochs = Epochs(raw, **epochs_params)
    epochs.load_data()
    print(len(epochs))
    for i in range(len(threshold_range)):
        rej_thresh = threshold_range[i]
        epochs.drop_bad(reject={'eeg': rej_thresh}, verbose=False)
        perc_chan = epochs.drop_log_stats()
        print('Threshold {:.1f} uV : {:.1f}% rejection'.format(rej_thresh*1e6, perc_chan))


def epoch_rejection_adaptive_threshold(raw, events, event_id, picks, tmin, tmax, thresh_min=50e-6, thresh_max=150e-6, pc_epochs=10.0):
    # compute the rejection-threshold value that provides exactly a specific percentage of rejected epoch   
    # search is performed within the threshold interval [thresh_min, thresh_max], dichotomical seeking
    ### WORK IN PROGRESS !!!! ####
    # and to be adapted for MEG, and EEG-MEG
    #
    # inputs:
    #   raw =  [mne-raw data]
    #   events = [mne-events], contains all events
    #   event_id = [mne-event_id]
    #   picks = [mne-picks] subset of sensors to be include in computation,  eg raw.pick_types(eeg=True, exclude='bads)
    #   tmin, tmax = [float] defines with tmax the epoch interval in seconds (for each event in event_id)
    #   thresh_min, thresh_max = [float] boundaries of the threshold interval
    #   pc_epochs = the targeted percentage of epochs to be rejected with resulting threshold
    #
    #   output:
    #       threshold_out = [float] threshold value allowing the rejection of pc_epochs% of trials
    #
    # versions: 1) M. Maby, FLecaignard - 12-06-18
    #

     
    epochs_params=dict(events=events, event_id=event_id, 
                     tmin=tmin, tmax=tmax, 
                     reject=None, picks=picks, 
                     baseline=None, verbose=False)      
    epochs = Epochs(raw, **epochs_params)
   
    th = [thresh_min, thresh_max]
    pc, eps = 100.0, 1.0
    while not abs(pc - thresh_channel) < eps:
        pc  = [epochs.drop_bad(reject={'eeg': th[1]}, verbose=False).drop_log_stats(),  ## lower uV value gives larger rejection pc
        epochs.drop_bad(reject={'eeg': th[0]}, verbose=False).drop_log_stats() ]## lower uV value gives larger rejection pc    
        print('---')
        print(pc, th)

        if pc[0] <= pc_epochs <= pc[1]:
            i = bisect(pc,pc_epochs ) # provides index of the closest value
            pc = pc[i]
            threshold_out = th[i] 
            th = np.sort([th[i], (th[1]-th[0])/2])
            
        else:
            print('failed to find a threshold within {} , {} with {}% rejection'.format(thresh_min, thresh_max, pc_epochs))
            
    return threshold_out

           
#############################################################################################
##  o*o*o*o*o*o*o*o*o*o*o*o*o*o*         jump! jump!              *o*o*o*o*o*o*o*o*o*o*o*o*o*
#############################################################################################
def raw_jump_detect(raw, picks, jump_thresh, jump_dur, jump_tw,  plot = True, save=False, f_jump_out = None,f_raw_out=None):
    # detection of signal jumps (EEG, MEG) on raw data
    # each jump is assigned an event and a time interval so that events in this interval do not enter subsequent analyses (using mne Annotations)
    #
    # inputs:
    #   raw =  [mne-raw data]
    #   picks = [mne-picks]
    #   jump_thresh = [float], peak-to-peak threshold amplitude in Volt or Tesla
    #   jump_dur = [float], minimum duration for jump occurence to trigger a detection
    #   jump_tw = duration of the time window (centered wrt jump latency), in sec
    #   f_jump_out = name of file to save annontations
    #   f_raw_out = raw file with annotations
    #   output:
    #       
    #
    # versions: 1) M. Maby, FLecaignard - 12-06-18 (inspired from eegdeljump!)
    #
    
#    raw.load_data()
    
#    picks=[30]
    sfreq = raw.info['sfreq']
    jump_dur_samp = np.int(np.round(jump_dur*sfreq))
    
    oversampled_jump_dur = 4 * jump_dur # we consider as one all jumps occuring in 4*jump_duration.If jump rise = 25 ms, all jumps detected within 100 ms will be collapsed
    oversampled_jump_dur = 1 # set to 1 sec
    m_data, times = raw[:,:]
    
    # debug
#    data_temp = np.random.randint(20, size=15)
#    jump_thresh = 10
#    jump_dur = 3
    print(picks, raw.info['bads'], 'ouais')
    jumps = [] # expressed in samples
    print(m_data.size)
    for i_sens, sens in enumerate(picks):
        
        if not raw.ch_names[sens] in raw.info['bads']:
            data_temp = m_data[sens, :]
            diff_data = data_temp[:-jump_dur_samp] - data_temp[jump_dur_samp:]
            jump_ind = np.where(np.abs(diff_data)>= jump_thresh)[0]
            jump_lat = times[jump_ind]
#            print(jump_lat)
            if jump_ind.size>0:
                # remove too close jumps if any
                diff_jump = jump_lat[:-1] - jump_lat[1:] 
                over_ind = np.where(np.abs(diff_jump)<= oversampled_jump_dur)[0]
                jump_lat = np.delete(jump_lat, over_ind)
                jump_ind = np.delete(jump_ind, over_ind)
                for i_j, jmp in enumerate(jump_lat):
                    print('Channel {}: jump found at latency {} sec'.format(raw.ch_names[sens], jmp))
                    jumps.append(jmp-jump_tw/2)
            else:
                print('No jumps found for channel {}'.format(raw.ch_names[sens]))
        
    
    #remove several occurence of the same jumps (across channels)
    jumps = np.array(jumps)
    jumps = np.sort(jumps)
    diff_jump = jumps[:-1] - jumps[1:] 
    over_ind = np.where(np.abs(diff_jump)<= 1.0)[0] # 1 sec
    jumps = np.delete(jumps, over_ind)
    print(jumps)
    
    if save:
        if jumps.size ==0:
            #if len(jumps)==0:
            onset = [0.0] # dummy jump at t=0.0 sec
            duration = np.repeat(0.05, len(onset))
        else:
            onset = jumps 
            duration = np.repeat(jump_tw, len(onset))
        
        
        description = ['bad_jump']*len(onset) # labels starting with bad* will be excluded from epoching
        annotations = Annotations(onset, duration, description)
        annotations.save(f_jump_out)
        raw.set_annotations(annotations)
        raw.save(f_raw_out, overwrite=True)
        print(raw.annotations)
        
#    print(jumps.size)
    if plot:
        scal = dict(eeg=10e-5)
        raw = read_raw_fif(f_raw_out, preload=False)
        raw.plot( n_channels=raw.info['nchan'],remove_dc = True,  duration=30, highpass = None, scalings=scal) #, show_options=False)
    
#############################################################################################
##  o*o*o*o*o*o*o*o*o*o*o*o*o*o*       ICA              *o*o*o*o*o*o*o*o*o*o*o*o*o*
#############################################################################################
    
def ica_view_correction(raw, ica,  scalings, plot_eeg=[],  view_start=0, view_stop=0):
    # plot pre- and post- ica correction
    # raw = mne raw object to be corrected
    # ica = mne object
    # plot_eeg : subset of sensors to be plotted with overlay, in additional figures
    # view_start and stop: define a time window (sec)
    # scalings = dict(eeg=10e-5)
    
    raw.plot(n_channels=raw.info['nchan'],remove_dc = True,  duration=30, highpass = None, scalings=scalings) #, show_options=False)
    
    raw_corr = raw.copy()
    raw_corr = ica.apply(raw_corr)
    raw_corr.plot(n_channels=raw.info['nchan'],remove_dc = True,  duration=30, highpass = None, scalings=scalings) #, show_options=False)

    if plot_eeg:
        for sens in plot_eeg:
            ind_eeg = raw.ch_names.index(sens)
            print(ind_eeg, sens)
            plot_ica_overlay(ica,raw, picks=ind_eeg, start=view_start, stop=view_stop, title='{}, {}'.format(subj_id, sens))


def ica_plot_blink_evoked(subj, raw, ica, plot_raw=False, blink_id=888,thresh=110e-6, lf=1, hf=10):
    # quick plot of blink evoked response
    # raw = mne raw object to be corrected
    # ica = mne object
    
    blink_events = find_eog_events(raw, ch_name='Fp1', event_id=blink_id,thresh=thresh, l_freq=lf, h_freq=hf)
    print('{}----> found {} blinks'.format(subj, len(blink_events)))
    if plot_raw:
        scal = dict(eeg=10e-5)
        raw.plot(events=blink_events,   n_channels=raw.info['nchan'],remove_dc = True,  duration=30, highpass = None, scalings=scal) #, show_options=False)

    
    blink_epochs = Epochs(raw, events=blink_events, event_id=blink_id, picks=None, tmin=-0.5,tmax=0.5,  reject=dict(eeg=300e-6))
    blink_epochs.average().plot_topo(ylim=dict(eeg=[-150, 150]), title='{}, {} blinks.'.format(subj, len(blink_epochs.selection)))
    print('----------> pre-ICA: keeping {} blink_events'.format(len(blink_epochs.selection) ))
    raw_corr = raw.copy()
    raw_corr = ica.apply(raw_corr)
    
    blink_epochs = Epochs(raw_corr, events=blink_events[blink_epochs.selection,:], event_id=blink_id,picks=None,  tmin=-0.5,tmax=0.5, reject=None)
    blink_epochs.average().plot_topo(ylim=dict(eeg=[-150, 150]), title='{}, {} compo.'.format(subj, len(ica.exclude)))
    print('----------> post-ICA: keeping {} blink_events'.format(len(blink_epochs.selection) ))
  


#############################################################################################
##  o*o*o*o*o*o*o*o*o*o*o*o*o*o*       DEBUG              *o*o*o*o*o*o*o*o*o*o*o*o*o*
#############################################################################################


if __name__== '__main__':
    
    ### debug raw_jump_detetc
    dir_mne='/sps/inter/crnl/users/pduret/ContextOdd/ContextOdd_Data/eMNE'
    subj_id='eSu11'
    lf, hf = 1, 40 #Hz
    raw = read_raw_fif(os.path.join(dir_mne, '{}.preproc50_1Hz.raw.fif'.format(subj_id)), preload=True)
    ica=read_ica(os.path.join(dir_mne, '{}.{}-ica.fif'.format(subj_id, 'preproc50_1Hz_epoch')))
    raw = ica.apply(raw)
    raw.filter(lf, hf)
    picks = pick_types(raw.info, eeg=True, stim=False,exclude=())
    
    jump_thresh = 250e-6
    jump_dur = 40e-3
    jump_tw = 2
    f_jump_out = os.path.join(dir_mne, '{}.{}-annot.fif'.format(subj_id, 'preproc50_1-40Hz'))
    f_raw_out = os.path.join(dir_mne, '{}.{}.raw.fif'.format(subj_id, 'preproc50_1Hz_annot'))
    raw_jump_detect(raw, picks, jump_thresh, jump_dur, jump_tw,save=True, f_jump_out =f_jump_out,f_raw_out=f_raw_out)
  

