import matplotlib.pyplot as plt
import numpy as np



def my_imagesc(mat2d, plot_type = 'auto'):
    # mat2d: numpy.darray, eg: mat2d = np.random.random((10,10))
    fig, ax1 =  plt.subplots(nrows=1, figsize=(6,10))
    
    if plot_type == 'auto':
        ax1.imshow(mat2d, extent=[0,100,0,1], aspect='auto')
        ax1.set_title('Auto-scaled Aspect')

    if plot_type == 'default':
        ax1.imshow(mat2d, extent=[0,100,0,1])
        ax1.set_title('Default')


    if plot_type == 'manual':
        ax1.imshow(mat2d, extent=[0,100,0,1], aspect=100)
        ax1.set_title('Manually Set Aspect')
        
    plt.tight_layout()
    plt.show()
    
    return fig


#fig, (ax1, ax2, ax3) = plt.subplots(nrows=3, figsize=(6,10))
#
#ax1.imshow(grid, extent=[0,100,0,1])
#ax1.set_title('Default')
#
#ax2.imshow(grid, extent=[0,100,0,1], aspect='auto')
#ax2.set_title('Auto-scaled Aspect')
#
#ax3.imshow(grid, extent=[0,100,0,1], aspect=100)
#ax3.set_title('Manually Set Aspect')
#
#plt.tight_layout()
#plt.show()