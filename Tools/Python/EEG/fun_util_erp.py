#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Home-made Functions for EEG-MEG Plots
Created on Tue Mar  5 09:52:07 2019

@author: Françoise Lecaignard, francoise.lecaignard@inserm.fr
"""

import numpy as np             # convention d'import
import os                       # path.join for file names
import matplotlib.pyplot as plt # from the MNE examples
from bisect import bisect

from mne import Epochs, Annotations, pick_types, find_events, write_events, read_events
from mne.channels import read_layout
from mne.io import concatenate_raws, read_raw_brainvision, read_raw_fif, find_edf_events
from mne.datasets import eegbci
from mne.decoding import CSP
from mne.viz import plot_events, plot_ica_overlay
from mne.preprocessing import ICA, read_ica,  find_eog_events
from mne import write_evokeds, combine_evoked, read_evokeds, grand_average


import sys      # path to my own functions
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Python/EEG')


print(__doc__)

import sys      # path to my own functions
from fun_plot_erp  import evokeds_plot


#############################################################################################
##  o*o*o*o*o*o*o*o*o*o*o*o*o*o*       Compute a difference between two ERPs      *o*o*o*o*o*o*o*o*o*o*o*o*o*
#############################################################################################

def evokeds_indiv_diff(dir_mne_erp, subj,epochs, baseline, erp_list, output_name,  neg = False, plot = None):
    # compute the difference between two ERPs
    #
    # subj = str, subject name, of the form sub-NN
    # epochs = MNE epochs objects
    # baseline = (t1, t2) in sec for baseline correction : ex: (-0.1 0)
    # erp_list = list of events to be substracted : erp_list = ['A', 'B'] leading to output = A - B
    # output_name = 'C' to label A-B = C
    # neg = boolean, whether we want to have negative values up (historical view of the MMN for instance) or not
    # plot = None (if not), 'traces' , 'topo'
    
    evokeds = [epochs[name].average().apply_baseline(baseline) for name in erp_list]
    for i_ev, name in enumerate(erp_list):
        evokeds[i_ev].comment = '{}, {}'.format(subj, name)
                    
    diff = combine_evoked(evokeds, weights=(1,-1) ) #[evokeds[0], -evokeds[1]], weights='equal')
    
    #diff.data *=2
    evokeds.append(diff)
    evokeds[-1].comment = '{}'.format(output_name)
    
    
    
  
    if plot:
        evokeds_plot(evokeds, plot_type=plot, neg=neg)
            
    return evokeds

##############################################################################################
###  o*o*o*o*o*o*o*o*o*o*o*o*o*o*         Double ERPs              *o*o*o*o*o*o*o*o*o*o*o*o*o*
##############################################################################################
## This function allows to create epochs related to an event, and includes preceding ones so that we have [ev1 ev2]
## and baseline defined as needed (relative to ev1 or ev2 or ...)
## This implies to sort consecutive events that have both been set as good
#
#def evokeds_indiv_double_erp(epochs, raw, event_name, soa, tmin, tmax):
#    # epochs: MNE object, cleaned epochs initially computed (typically surrouding a single event)
#    # raw : MNE object, continuous data to apply the new epoching on
#    # event_name : label of the event of interest, it is the 2nd one in [ev1 ev2]
#    

#####################################################################################################################
#  Group-average of ERPs
#####################################################################################################################

def evokeds_group_average(dir_mne, folder_name, subj_id_list ,group_name,  pref_evoked,  save= True):
    #dir_mne : general path to the MNE analysis
    # folder name: such that evoked files are stored in dir_mne/subj/folder_name
    # subj_id_list = of the form  np.arange(0,36) to process sub-00, ..., sub-35
    # pref_evoked = *.pref_evoked-ave.fif for eahc subject, and group average
    # group name = dir_mne/group_name/folder_name/group_name.pref_evoked-ave.fif: 'group' , 'group34' ...
    # save : to store group average in dir_mne/group/folder_name/group.pref_evoked-ave.fif
  
    
    # group_evoked=
    f_evo_group_list=list()
    for i_su in subj_id_list:
        subj='sub-{:02}'.format(i_su) 
        dir_imne_erp = os.path.join(dir_mne, subj, folder_name) #individual mne path
        f_evo_group_list.append(os.path.join(dir_imne_erp, '{}.{}-ave.fif'.format(subj, pref_evoked)))
   
    group_evoked=[read_evokeds(f_evo)[0] for f_evo in f_evo_group_list]
       
#    group_average = grand_average(group_evoked[0], interpolate_bads=False, drop_bads=False)       
    
    group_average = combine_evoked(group_evoked, 'equal')  # Combine subjects
    group_average.comment = pref_evoked

    if save:
        dir_imne_erp = os.path.join(dir_mne, group_name, folder_name) #individual mne path
        if not os.path.exists(dir_imne_erp):
            os.makedirs(dir_imne_erp)
            
        f_evo_group = os.path.join(dir_imne_erp, '{}.{}-ave.fif'.format(group_name, pref_evoked))
        write_evokeds(f_evo_group,group_average)
    
    return group_evoked[0], group_average

#############################################################################################
##  o*o*o*o*o*o*o*o*o*o*o*o*o*o*       DEBUG              *o*o*o*o*o*o*o*o*o*o*o*o*o*
#############################################################################################


#if __name__== '__main__':
#    
#    ### debug raw_jump_detetc
#    dir_mne='/sps/inter/crnl/users/pduret/ContextOdd/ContextOdd_Data/eMNE'
#    i_su = 0
#    subj='sub-{:02}'.format(i_su) 
#    dir_imne = os.path.join(dir_mne, subj, 'preproc') #individual mne path
#    f_raw_erp = os.path.join(dir_imne, '{}.preproc_2-20Hz.raw.fif'.format(subj)) 
#    raw_erp = read_raw_fif(f_raw_erp, preload=True)

