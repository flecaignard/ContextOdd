#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Home-made Functions for stats analysis in EEG-MEG Evoked responses
Created on Tue Mar  5 09:52:07 2019

@author: Françoise Lecaignard, francoise.lecaignard@inserm.fr
"""

import numpy as np             # convention d'import
import os                       # path.join for file names
import matplotlib.pyplot as plt # from the MNE examples
from bisect import bisect

from mne import Epochs, Annotations, pick_types, find_events, write_events, read_events, find_layout
from mne.channels import read_layout
from mne.io import concatenate_raws, read_raw_brainvision, read_raw_fif, find_edf_events
from mne.datasets import eegbci
from mne.decoding import CSP
from mne.viz import plot_events, plot_ica_overlay
from mne.preprocessing import ICA, read_ica,  find_eog_events
from mne import write_evokeds, combine_evoked, read_evokeds, grand_average
from mne.viz import plot_topomap,plot_compare_evokeds, tight_layout
from mpl_toolkits.axes_grid1 import make_axes_locatable


import sys      # path to my own functions
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Python/EEG')


print(__doc__)

import sys      # path to my own functions
from fun_plot_erp  import evokeds_plot


#############################################################################################
##  o*o*o*o*o*o*o*o*o*o*o*o*o*o*       View Significant Clusters    *o*o*o*o*o*o*o*o*o*o*o*o*
#############################################################################################

def view_significant_spatiotemp_clusters(cluster_stats, p_clusters, evoked_dict_cond_a_cond_b, colors):
    # for each cluster obtained with spatio_temporal_cluster_1samp_test : plot scalp map of F_values (mean)
    # and  evoked response averaged withon the cluster
    # inputs:
    #        cluster : output of spatio_temporal_cluster_1samp_test
    #        p_clusters : p threshold to select clusters (family-wise p-value)
    #        evoked_dict_cond_a_cond_b : dictionary with condition names as keys, and corresponding evoked object as values (typically group evoked)
    #        colors: list of colors

    
    # extract cluster info
    t_obs, clusters, p_values, _ = cluster_stats
    # select clusters
    sel_cluster_inds = np.where(p_values < p_clusters)[0]

    
    
    # plot clusters
    for i_clu, clu_idx in enumerate(sel_cluster_inds):
        # unpack cluster information
        time_inds, space_inds = np.squeeze(clusters[clu_idx]) # equivalent to time_inds =clusters[clu_idx][0] 
        # get unique indices: what significant sensors, what signifiant time samples?
        ch_inds = np.unique(space_inds)
        time_inds = np.unique(time_inds)
        # compute the average F_map (?)
        f_map = t_obs[time_inds, ...].mean(axis=0)
        
        # get signals at the sensors contributing to the cluster
        k = [*evoked_dict_cond_a_cond_b][0] # first key of evked dictionary
        evoked = evoked_dict_cond_a_cond_b[k]
        sig_times = evoked.times[time_inds]
        pos = find_layout(evoked.info,ch_type='eeg').pos # get sensor positions via layout

        # create spatial mask
        mask = np.zeros((f_map.shape[0], 1), dtype=bool)
        mask[ch_inds, :] = True
        # initialize figure
        fig, ax_topo = plt.subplots(1, 1, figsize=(10, 3))
        # plot average test statistic and mark significant sensors
        image, _ = plot_topomap(f_map,pos, mask=mask, axes=ax_topo,
                                vmin=np.min, vmax=np.max, show=False)
        
#        plot_topomap(f_map,pos, mask=mask, axes=ax_topo, cmap='Reds',
#                                vmin=np.min, vmax=np.max, show=False, 
#                                head_pos=dict(center=(0.45,0.65),scale=(0.75,0.65)))
        
        # create additional axes (for ERF and colorbar)
        divider = make_axes_locatable(ax_topo)
        # add axes for colorbar
        ax_colorbar = divider.append_axes('right', size='5%', pad=0.05)
        plt.colorbar(image, cax=ax_colorbar)
        ax_topo.set_xlabel('Averaged F-map ({:0.3f} - {:0.3f} s)'.format(*sig_times[[0, -1]]))
        # add new axis for time courses and plot time courses
        ax_signals = divider.append_axes('right', size='300%', pad=1.2)
        title = 'Cluster #{0}, {1} sensor'.format(i_clu+1, len(ch_inds))
        if len(ch_inds) > 1:
            title += "s (mean)"
        plot_compare_evokeds(evoked_dict_cond_a_cond_b, title=title, picks=ch_inds, axes=ax_signals,colors=colors, show=False,split_legend=True ,show_legend = False, truncate_yaxis='max_ticks')
        # plot temporal cluster extent   
        ymin, ymax = ax_signals.get_ylim()
        ax_signals.fill_betweenx((ymin, ymax), sig_times[0], sig_times[-1],color='orange', alpha=0.3)
        # clean up viz
        tight_layout(fig=fig)
        fig.subplots_adjust(bottom=.05)
        plt.show()
    
#    figname = (MNE_path + '7.Cluster_3.png')
#    fig.savefig(figname)   
#    plt.close(fig)
    
  



