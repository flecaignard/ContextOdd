"""
===========================================
Convert ContextOdd EEG data to BIDS format
===========================================

This script adapts MNE-Bids convert_eeg_to_bids.py
# Authors: Stefan Appelhoff <stefan.appelhoff@mailbox.org>
# "we need to build a directory structure and supply meta data
# files to properly *bidsify* this data".


"""

# Authors: Françoise Lecaignard, inspired by Pauline Duret ContextOdd_bidsifier_group.py
# Created on Tue 05 Feb 2019

# Most comments come from the original script (convert_eeg_to_bids.py)
###############################################################################
# import argparse
import os
import os.path as op
import re
from collections import defaultdict

import shutil as sh


import sys      # path to my own functions
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Python/EEG')
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/BIDS/mne-bids')


from mne_bids.utils import print_dir_tree

from fun_00_COdd_eeg import  codd_eeg_bids

################################################################################
# Get input parameters 
################################################################################

# Paths     ----------------
dir_raw_acq = '/sps/cermep/cermep/experiments/DCM/ContextOdd/Raw_Data/EEG'
dir_raw_bids = '/sps/cermep/cermep/experiments/DCM/ContextOdd/Raw_Data/EEG_BIDS'

# Subj_List ----------------
# Test on one subject:
subj_list = {'MERAR': 24}
subj_list = {'NAVMA': 0}
## Apply to the group:
#subj_list = {'NAVMA': 0,	 'POSRI': 1,	 'BOULA':2,	 'BREME': 3,	   'VENAN':	4,\
#             'BIGOP': 5,	 'LANJU': 6,	 'LOQJE':7,	 'PICJO': 8,	   'EDDAR':	9,\
#             'ADOBE': 10,'THIMA': 11,'BOUAU':12, 'MASLO': 13,  'ANTRA':14,\
#             'DURMA': 15,'BERAM': 16,'ALLMA':17, 'VIAMA': 18,  'BARTH':19,\
#             'COULE': 20,'CHMMI': 21,'MIGAN':22,	 'PEGAR': 23,  'MERAR':24,\
#             'SANLU': 25,'ESTYO': 26,'FERMA':27,	 'ADANI': 28,  'COTCL':29,\
#             'ANDNI': 30,'SOUMA': 31,'KOCPI':32,	 'TALCA': 33,  'MERAM':34,\
#             'LEDGA': 35	}

# individual info

sexes = {0: 2 , 1: 1, 2: 2, 3: 2 , 4: 2, \
             5: 2 , 6: 2, 7: 1, 8: 1, 9: 1,\
             10: 1, 11: 2 , 12: 1 , 13: 2, 14: 2,\
             15: 1, 16: 2, 17:2 , 18: 2 , 19: 1,\
             20: 2, 21: 1, 22: 2, 23: 1, 24: 1, \
             25: 2, 26: 1, 27: 1, 28: 2, 29: 2, \
             30: 1 , 31:2 , 32: 1, 33: 2 , 34: 2, \
             35: 1}

birthdays = {0: (1990,4,24), 1: (1991, 11, 8), 2: (1997,9,9), 3: (1998,4,3), 4: (1996,9,20), \
             5: (1998,3,27), 6: (1997,4,30), 7:(1997,11,23) , 8: (1992,4,2), 9: (1993,2,13),\
             10:(1996,6,1) , 11: (1995, 10, 11), 12: (1991,3,16), 13: (1996,2,10), 14: (1996,12,11),\
             15:(1993,7,17) , 16: (1993,6,1), 17:(1992,3,26) , 18:(1992, 2, 17), 19: (1998,6,6),\
             20: (1997,4,18), 21: (1985,6,18), 22: (1994,10,28), 23: (1999,3,22) , 24: (1991,2,12), \
             25:(1990,9,19) , 26: (1992,1,31), 27: (1989,10,2), 28: (1986,12,2), 29:(1985,5,14), \
             30: (1994,4,7), 31:(1987, 5,8) , 32: (1986,7,4), 33: (1981,12,11) , 34: (1987,3,13), \
             35: (1994,7,26)}

################################################################################
# Formatting as BIDS 
################################################################################
# Examples of raw BrainAmp data:
# 20180503_ANTRA/ANTRA__0001.vhdr
# 20180406_BREME/BREME_0001.vhdr 20180406_BREME/BREME_0002.vhdr
#
# Output filename in the BIDS architexture:  sub-XX_run-1 
# There is no task name, because there was only one condition here - passive listening
# some subjects have two runs, because of manual errors duing acquisition
# and some have one run, but termed as 4, again because of manual errors

#print_dir_tree(dir_raw_acq)

subj_datecode = dict()  #  EEG folder of the form 20180503_ANTRA
subj_runs_id = defaultdict(list) # ids of run(s) for each subject
subj_runs_fname = defaultdict(list) # e.g.: ANTRA__0001.vhdr


folder_list = os.listdir(dir_raw_acq)

# identify participant folders (one per subject), among all folders in dir_raw_acq
# BrainAmp folder are formatted: e.g.: 20180524_SOUMA
for folder in folder_list: 
    eegfolder = re.match( r'[0-9]{8}_(.*)', folder) 
    if eegfolder:       #obey BrainAmp format
        #recover datecode and subj_code
        subj_datecode[eegfolder[1]] = (f'{eegfolder[0]}')

# Identify run(s) for each subject in subj_list

for subj_code, subj_id in subj_list.items():
    print(subj_code)    
    print(subj_list[subj_code],subj_datecode[subj_code] ) 
    
    file_list = os.listdir(op.join(dir_raw_acq, subj_datecode[subj_code]))
    for file in file_list:
        eegfile = re.match( r'(.*)\.vhdr', file) 
        if eegfile:       #one or many vhdr files
            subj_runs_id[subj_code].append(file[-6]) 
            subj_runs_fname[subj_code].append(file) 

    
# Bidsify each file    
for subj_code, subj_id in subj_list.items():
    list_runs_fname = subj_runs_fname[subj_code]
    list_runs_id = subj_runs_id[subj_code]
    
    for i_frun, frun in enumerate(list_runs_fname):
        sex = sexes[subj_id]
        birthday = birthdays[subj_id]
        codd_eeg_bids(dir_raw_acq, dir_raw_bids, subj_id, list_runs_id[i_frun], subj_datecode[subj_code], frun, sex, birthday)
        print(subj_datecode[subj_code], list_runs_id[i_frun],  frun)

    print('SUBJECT {}-sub-{:02}: DONE'.format(subj_code, subj_id))

###############################################################################
# What does our fresh BIDS directory look like?
print_dir_tree(dir_raw_bids)

###############################################################################
#


###############################################################################
# MNE-BIDS has created a suitable directory structure for us, and among other
# meta data files, it started an `events.tsv` and `channels.tsv` and made an
# initial `dataset_description` on top!
#
# Now it's time to manually check the BIDS directory and the meta files to add
# all the information that MNE-BIDS could not infer. For instance, you must
# describe EEGReference and EEGGround yourself. It's easy to find these by
# searching for "n/a" in the sidecar files.
#
# Remember that there is a convenient javascript tool to validate all your BIDS
# directories called the "BIDS-validator", available as a web version and a
# command line tool:
#
# Web version: https://bids-standard.github.io/bids-validator/
#
# Command line tool: https://www.npmjs.com/package/bids-validator
