#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
===============================================
Step 03 - Downsampling
===============================================
We reduce data from  a sampling rate of 1000 Hz to 500 Hz
Downsampling also applies to events
Output = *.preproc.raw.fif, *.preproc.5d-eve.fif

Created on Mon Feb 12 17:57:17 2019

@author: Françoise Lecaignard, francoise.lecaignard@inserm.fr
"""


###############################################################################
import numpy as np             # convention d'import
import os
import sys      # path to my own functions
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Python/EEG')
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/BIDS/mne-bids')



from fun_03_downsample import  codd_eeg_downsample_raw_events

################################################################################
# Input Parameters 
################################################################################

# Paths ----------------------------
dir_mne = '/sps/cermep/cermep/experiments/DCM/ContextOdd/Ana_MNE'



################################################################################
# Define Constant Variables
################################################################################
# New sample rate
new_freq=500
# Plot raw data
plot=False
################################################################################
# Let's start!
################################################################################

# subj_id_list -----------------------
# test on one subject
#subj_id_list = np.arange(0,1) # sub-00
# applyto all
subj_id_list = np.arange(0,36) # 0 to 35, to include sub-00 to sub-35


#
for i_su in subj_id_list:
    subj='sub-{:02}'.format(i_su) 
    codd_eeg_downsample_raw_events(dir_mne, subj, new_freq, plot=plot )
    
    
# test original sampling rate / new sampling rate
#from mne.io import read_raw_fif
#    
#subj='sub-00'
#dir_imne = os.path.join(dir_mne, subj, 'preproc') #individual mne path
#
#f_raw = os.path.join(dir_imne, '{}.preproc.raw.fif'.format(subj))
#raw = read_raw_fif(f_raw, preload=True)    
#raw.info