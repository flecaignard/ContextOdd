#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
===============================================
Step 05 - Filter raw epochs
===============================================

Raw data (unfiltered) has been epoched (-200 +410 ms) in step 04, with removal of bad segments and identification of bad sensors (*.preproc_early-epo.fif)
Here, we apply to the raw continuous data 1) a notch filter to remove powerline artifact
And 2) a 1-40 Hz band-pass filter
====> output = sub-XX.preproc_1-40Hz.raw.fif
Then we import the epoching  from step04. 
====> output = sub-XX.preproc_early_1-40Hz-epo.fif


Created on Mon Feb 28th

@author: Françoise Lecaignard, francoise.lecaignard@inserm.fr
"""



###############################################################################
import numpy as np             # convention d'import
import os
import sys      # path to my own functions
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Python/EEG')
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/BIDS/mne-bids')

from mne.io import read_raw_fif
from mne import Epochs, pick_types, read_events, read_epochs
from mne import get_config, set_config
type(get_config('MNE_BROWSE_RAW_SIZE'))
set_config('MNE_BROWSE_RAW_SIZE', '20.0,10.0')

#from fun_NN_template import  ...
from fun_eeg_misc import  print_step_label,import_epochs

################################################################################
# Input Parameters 
################################################################################

# Paths ----------------------------
dir_mne = '/sps/cermep/cermep/experiments/DCM/ContextOdd/Ana_MNE'



################################################################################
# Define Constant Variables
################################################################################
# Band-pass (Hz)
lf, hf = 1, 40


# prefixe of  raw files
pref_raw = 'preproc'
# prefix of raw filtered raw = output file
pref_raw_filt = f'preproc_{lf}-{hf}Hz'


# prefix raw epoch file (from step04)
pref_epo_early =  'preproc_early-epo'

# prefix filtered epoch
pref_epo_out = 'preproc_early_{}-{}Hz-epo'.format(lf, hf)

### events ###
event_id_codd={'var_c/c4/c-/p0/std': 14121, 'var_c/c4/c-/p0/dev': 14122,'var_c/c4/c-/p0/std_oth': 14123,\
              'var_c/c5/c-/p0/std': 15121, 'var_c/c5/c-/p0/dev': 15122,'var_c/c5/c-/p0/std_oth': 15123,\
              'var_c/c6/c-/p0/std': 16121, 'var_c/c6/c-/p0/dev': 16122,'var_c/c6/c-/p0/std_oth': 16123,\
              'var_c/c2/c+/p0/std': 12221, 'var_c/c2/c+/p0/dev': 12222,'var_c/c2/c+/p0/std_oth': 12223,\
              'var_c/c3/c+/p0/std': 13221, 'var_c/c3/c+/p0/dev': 13222,'var_c/c3/c+/p0/std_oth': 13223,\
              'var_c/c4/c+/p0/std': 14221, 'var_c/c4/c+/p0/dev': 14222,'var_c/c4/c+/p0/std_oth': 14223,\
              'var_c/c6/c+/p0/std': 16221, 'var_c/c6/c+/p0/dev': 16222,'var_c/c6/c+/p0/std_oth': 16223,\
              'var_c/c7/c+/p0/std': 17221, 'var_c/c7/c+/p0/dev': 17222,'var_c/c7/c+/p0/std_oth': 17223,\
              'var_c/c8/c+/p0/std': 18221, 'var_c/c8/c+/p0/dev': 18222,'var_c/c8/c+/p0/std_oth': 18223,\
              'var_p/c2/c-/p+/std': 22111, 'var_p/c2/c-/p+/dev': 22112,'var_p/c2/c-/p+/std_oth': 22113,\
              'var_p/c3/c-/p+/std': 23111, 'var_p/c3/c-/p+/dev': 23112,'var_p/c3/c-/p+/std_oth': 23113,\
              'var_p/c4/c-/p+/std': 24111, 'var_p/c4/c-/p+/dev': 24112,'var_p/c4/c-/p+/std_oth': 24113,\
              'var_p/c4/c-/p0/std': 24121, 'var_p/c4/c-/p0/dev': 24122,'var_p/c4/c-/p0/std_oth': 24123,\
              'var_p/c5/c-/p0/std': 25121, 'var_p/c5/c-/p0/dev': 25122,'var_p/c5/c-/p0/std_oth': 25123,\
              'var_p/c6/c-/p0/std': 26121, 'var_p/c6/c-/p0/dev': 26122,'var_p/c6/c-/p0/std_oth': 26123,\
              'var_p/c6/c-/p-/std': 26131, 'var_p/c6/c-/p-/dev': 26132,'var_p/c6/c-/p-/std_oth': 26133,\
              'var_p/c7/c-/p-/std': 27131, 'var_p/c7/c-/p-/dev': 27132,'var_p/c7/c-/p-/std_oth': 27133,\
              'var_p/c8/c-/p-/std': 28131, 'var_p/c8/c-/p-/dev': 28132,'var_p/c8/c-/p-/std_oth': 28133                  }

### bad sensors ###
# exclude eeg: this dictionary from step04 epoching- Do Not Modify Here !
# It is dedicated for epoching rejection, which won't be applied here
# Bad sensors will not be dropped in the resulting epochs
# This is because they could be either dropped or intrepolated => this should be handled in a separate process

exclude_eeg = { 'sub-00': [],	 'sub-01': [],	'sub-02':['CP5', 'P4'],	 'sub-03': [],	'sub-04': ['PO10'],\
             'sub-05': [],	 'sub-06': [],	'sub-07':[],	 'sub-08': [],	'sub-09': [],\
             'sub-10': [],    'sub-11': ['PO9'],   'sub-12':[],  'sub-13': [],    'sub-14': [],\
             'sub-15': [],    'sub-16': [],   'sub-17':[],  'sub-18': [],    'sub-19': [],\
             'sub-20': [],    'sub-21': [],   'sub-22':['T7'],	 'sub-23': ['CP1'],    'sub-24': [],\
             'sub-25': ['T8', 'FC2', 'Oz'],    'sub-26': [],   'sub-27':[],	 'sub-28': [],    'sub-29': [],\
             'sub-30': [],    'sub-31': [],   'sub-32':[],	 'sub-33': [],    'sub-34': [],\
             'sub-35': []	}
# epoch size
tmin, tmax = -0.2, 0.41

# Notch frequency array (Hz)
power_freq_array = np.arange(50, 201, 50)


# Plot raw data
plot=False

# Save epochs
save=True
################################################################################
# Let's start!
################################################################################

# subj_id_list -----------------------
# test on one subject
subj_id_list = np.arange(0,36) # sub-00
# applyto all
#subj_id_list = np.arange(29,30) # 0 to 35, to include sub-00 to sub-35


#
for i_su in subj_id_list:
    subj='sub-{:02}'.format(i_su) 
    dir_imne = os.path.join(dir_mne, subj, 'preproc') #individual mne path

    #### ---- Load raw data, unfilter if not achieved yet
    f_raw_filt = os.path.join(dir_imne, f'{subj}.{pref_raw_filt}.raw.fif') 
    
    if os.path.isfile(f_raw_filt):
        raw_filt = read_raw_fif(f_raw_filt, preload=True)
    else:
        f_raw = os.path.join(dir_imne, f'{subj}.{pref_raw}.raw.fif') 
        raw = read_raw_fif(f_raw, preload=True)
       
        print_step_label('Notch - Filtering {}.....'.format(subj))    
        raw_filt =   raw.notch_filter(power_freq_array, filter_length='auto', phase='zero')
        print_step_label('BandPass - Filtering {}.....'.format(subj))    
        raw_filt.filter(lf, hf,  l_trans_bandwidth='auto',filter_length='auto', phase='zero', fir_window='hann', fir_design='firwin')
        f_raw_filt = os.path.join(dir_imne, '{}.preproc_{}-{}.raw.fif'.format(subj, lf, hf)) 
        print_step_label('Save raw {}.....'.format(subj))
        raw_filt.save(fname=f_raw_filt,overwrite=True)
    
    #### ----  Load raw epochs
    f_epoch_raw = os.path.join(dir_imne, '{}.{}.fif'.format(subj, pref_epo_early))
    epochs_raw = read_epochs(f_epoch_raw)    
    
    
    #### ---- Compute filtered epochs with raw-epoch rejection
    picks = pick_types(raw_filt.info, eeg=True, stim=True,exclude=())
    reject=None
    exclude_eeg_ind = exclude_eeg[subj]
    print_step_label('Epoching---  {}.....(No Rejection)'.format(subj))  
    
    if exclude_eeg_ind:
        raw_filt.info['bads']=exclude_eeg_ind
        print('excluded sensors for rejection: {}'.format(exclude_eeg_ind))
    
    
    epochs_filt = import_epochs(subj, raw_filt, epochs_raw, picks,  event_id_codd, tmin, tmax, plot=plot)
    
    #### ----  save
    if save:
        f_epoch_filt=os.path.join(dir_imne, '{}.{}.fif'.format(subj, pref_epo_out))
        epochs_filt.save(f_epoch_filt)    



################################################################################
# Attempts to filter epochs directly (without filtering raw data)
################################################################################

#    #notch_filter is not available for epochs => we use a stopband +-4Hz
#    
#    # TROUBLE WITH FREQs BELOW: ValueError: Stop bands are not sufficiently separated.
#    # To be fixed (filter raw data and then re-do the epoching with same bad rejection?)
#    
#    # lf > hf - this is fine !
#    #50 Hz
#    hf_notch, lf_notch = 46, 54
#    epochs_filt =   epochs_raw.filter(lf_notch, hf_notch,  l_trans_bandwidth='auto',filter_length='auto', phase='zero', fir_window='hann', fir_design='firwin')
#    # 100 Hz
#    hf_notch, lf_notch = 96, 104
#    epochs_filt =   epochs_filt.filter(lf_notch, hf_notch,  l_trans_bandwidth='auto',filter_length='auto', phase='zero', fir_window='hann', fir_design='firwin')
#    # 150 Hz
#    hf_notch, lf_notch = 146, 154
#    epochs_filt =   epochs_filt.filter(lf_notch, hf_notch,  l_trans_bandwidth='auto',filter_length='auto', phase='zero', fir_window='hann', fir_design='firwin')
#    # 200 Hz
#    hf_notch, lf_notch = 196, 204
#    epochs_filt =   epochs_filt.filter(lf_notch, hf_notch,  l_trans_bandwidth='auto',filter_length='auto', phase='zero', fir_window='hann', fir_design='firwin')
    
    
    #bandpass filter
#    epochs_filt =   epochs_raw.filter(lf, hf,  l_trans_bandwidth='auto',filter_length='auto', phase='zero', fir_window='hann', fir_design='firwin')
#     Filter length of 1551 samples (3.102 sec) selected
#    /sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Python/EEG/step_05_COdd_eeg_filter.py:98: RuntimeWarning: filter_length (1551) is longer than the signal (306), distortion is likely. Reduce filter length or filter a longer signal.
