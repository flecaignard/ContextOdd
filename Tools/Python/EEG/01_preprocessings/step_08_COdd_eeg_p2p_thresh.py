#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
===============================================================================
Step 08 - Artifact rejection : Peak-to-Peak rejection 
===============================================================================

Working with -200 +410 ms early-rejection and ica-corrected  epochs, 1-40 Hz bandpass, (created in step07, *.preproc_early_1-40Hz_ica-epo.fif)
Peak-to-peak (p2p) rejection (typical preprocessings)

Outputs  ======> /preproc/sub-xx.clean_1-40Hz-epo.fif <=> cleaned epochs in 1-40 Hz (ica, early and autoreject dropping)

ContextOdd EEG -  May 2nd 2019 -  we select this approach (and no AR)

Created on Mon Feb 11 17:57:17 2019

@author: Françoise Lecaignard, francoise.lecaignard@inserm.fr
"""



################################################################################

###############################################################################
import numpy as np             # convention d'import
import os
import sys      # path to my own functions
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Python/EEG')
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Python/EEG/01_preprocessings')
import math
import matplotlib.pyplot as plt # from the MNE examples
import pickle
import tqdm # progress bars

from mne.io import read_raw_fif
from mne import Epochs, pick_types, read_events, read_epochs, pick_events
from mne import get_config, set_config
type(get_config('MNE_BROWSE_RAW_SIZE'))
set_config('MNE_BROWSE_RAW_SIZE', '20.0,10.0')
from mne.preprocessing import read_ica
from mne.viz import plot_evoked_topo

from fun_util_eeg import  print_step_label, count_events, peak_to_peak_raw
from fun_plot_eeg import  plot_epochs_drop
from fun_plot_autoreject import  plot_autoreject_threshold_hist,plot_autoreject_labels
from fun_08_autoreject import codd_eeg_sample_ar_training_epochs,run_autoreject
from fun_util_epoch import get_dropped_epochs_idx, import_epochs
################################################################################
# Input Parameters 
################################################################################

# Paths ----------------------------
dir_mne = '/sps/cermep/cermep/experiments/DCM/ContextOdd/Ana_MNE'


### ----------------------------input Filenames----------------------------

# Input --- Pref epoch file (early rejection, ica correction) - from step 07
pref_epo_in = 'preproc_early_1-40Hz_ica'

# events file to get total number of auditory stimuli (renamed and downsampled events)
pref_eve_orig = 'preproc.5d'

### ----------------------------bad sensors ###----------------------------
# These sensors are excluded from ICA fit, but will not be dropped in the resulting epochs
# we use drops from step_05 PSD inspection

# exclude eeg:  Do Not Modify Here !
psd_bad_sensors = { 'sub-00': ['T7', 'TP9'],	 'sub-01': [],	'sub-02':[],	 'sub-03': [],	'sub-04': ['T7', 'T8'],\
             'sub-05': [],	 'sub-06': [],	'sub-07':[],	 'sub-08': [],	'sub-09': ['T7'],\
             'sub-10': [],    'sub-11': ['PO9'],   'sub-12':[],  'sub-13': [],    'sub-14': [],\
             'sub-15': ['T8'],    'sub-16': [],   'sub-17':[],  'sub-18': [],    'sub-19': ['T7', 'T8', 'TP10'],\
             'sub-20': [],    'sub-21': [],   'sub-22':[],	 'sub-23': [],    'sub-24': [],\
             'sub-25': ['Oz'],    'sub-26': [],   'sub-27':[],	 'sub-28': [],    'sub-29': [],\
             'sub-30': [],    'sub-31': [],   'sub-32':[],	 'sub-33': [],    'sub-34': [],\
             'sub-35': []	}


psd_and_p2p_bad_sensors = { 'sub-00': ['T7', 'TP9'],	 'sub-01': [],	'sub-02':['TP10', 'CP5', 'P4'],	 'sub-03': ['Oz'],	'sub-04': ['T7', 'T8'],\
             'sub-05': [],	 'sub-06': [],	'sub-07':[],	 'sub-08': ['O1', 'O2'],	'sub-09': ['T7', 'T8'],\
             'sub-10': [],    'sub-11': ['PO9'],   'sub-12':[],  'sub-13': ['T7', 'PO10'],    'sub-14': ['P3'],\
             'sub-15': ['T8'],    'sub-16': ['Oz'],   'sub-17':['PO9'],  'sub-18': [],    'sub-19': ['T7', 'T8', 'TP10'],\
             'sub-20': [],    'sub-21': [],   'sub-22':['P3'],	 'sub-23': ['CP1'],    'sub-24': [],\
             'sub-25': ['Oz', 'T8', 'TP9'],    'sub-26': [],   'sub-27':[],	 'sub-28': [],    'sub-29': ['CP5'],\
             'sub-30': ['T8', 'PO10'],    'sub-31': [],   'sub-32':['Oz'],	 'sub-33': ['T8'],    'sub-34': ['T8', 'T7'],\
             'sub-35': []	}

exclude_eeg = psd_and_p2p_bad_sensors



### ----------------------------Peak-2-Peak (p2p) specs----------------------------
# individual peak-2-peak amplitude threshold , in V
threshold_eeg = { 'sub-00': 90e-6,	 'sub-01': 90e-6,	'sub-02':100e-6,	 'sub-03': 100e-6,	'sub-04': 100e-6,\
         'sub-05': 95e-6,	 'sub-06': 100e-6,	'sub-07':95e-6,	 'sub-08': 90e-6,	'sub-09': 90e-6,\
         'sub-10': 90e-6,    'sub-11': 90e-6,   'sub-12':85e-6,  'sub-13': 95e-6,    'sub-14': 90e-6,\
         'sub-15': 90e-6,    'sub-16': 100e-6,   'sub-17':80e-6,  'sub-18': 95e-6,    'sub-19': 80e-6,\
         'sub-20': 95e-6,    'sub-21': 85e-6,   'sub-22':100e-6,	 'sub-23': 95e-6,    'sub-24': 70e-6,\
         'sub-25': 85e-6,    'sub-26': 90e-6,   'sub-27':90e-6,	 'sub-28': 90e-6,    'sub-29': 95e-6,\
         'sub-30': 95e-6,    'sub-31': 90e-6,   'sub-32':90e-6,	 'sub-33': 85e-6,    'sub-34': 95e-6,\
         'sub-35': 90e-6	}

# Threshold can be adjusted within sensors
## 
#sensorwise_threshold_adjust = { 'sub-00': [],	 'sub-01': [],	'sub-02':[],	 'sub-03': [],	'sub-04': [],\
#         'sub-05': {'Cz':100e-6, 'CP1': 100e-6, 'CP2':100e-6},	 'sub-06': [],	'sub-07':[],	 'sub-08': [],	'sub-09': [],\
#         'sub-10': [],    'sub-11': [],   'sub-12':[],  'sub-13': [],    'sub-14': [],\
#         'sub-15': [],    'sub-16': [],   'sub-17':[],  'sub-18': [],    'sub-19': [],\
#         'sub-20': [],    'sub-21': [],   'sub-22':[],	 'sub-23': [],    'sub-24': [],\
#         'sub-25': [],    'sub-26': [],   'sub-27':[],	 'sub-28': [],    'sub-29': [],\
#         'sub-30': [],    'sub-31': [],   'sub-32':[],	 'sub-33': [],    'sub-34': [],\
#         'sub-35': []	}

### ----------------------------output Filenames----------------------------


#prefix for pickle file containing bad_epochs = boolean vector ; summary of AutoReject and Peak-to-Peak
pref_rej_out = f'bad_epochs_summary'

## Pref epoch file = selected epochs after ealry rejection (step04) and current autoreject
# data are ica-correcyed 
pref_epo_out = 'clean_1-40Hz'

#prefix for good/bad events (pickle file, to plot raw data with good and bad events)
pref_goodbad_events = 'good_bad_events'

# Plot epochs
plot_raw=False
plot_epochs=False


################################################################################
# Let's start!
################################################################################

# subj_id_list -----------------------
# test on one subject
subj_id_list = np.arange(35,36) # sub-00
# applyto all
#subj_id_list = np.arange(1,36) # 0 to 35, to include sub-00 to sub-35

#


for i_su in subj_id_list:
    
    subj='sub-{:02}'.format(i_su) 
    dir_imne = os.path.join(dir_mne, subj, 'preproc') #individual mne path
    
    sens_exclude = exclude_eeg[subj]
    

    
    # 1) we take  epochs with early rejection and ica-correction (setp07))
    ################################################################################
    f_epochs_in=os.path.join(dir_imne, '{}.{}-epo.fif'.format(subj, pref_epo_in))
    epochs_in = read_epochs(f_epochs_in) 
    events_early=epochs_in.events #  does not refer to original events (nr = 6068), but to the resulting ones after jump-like noise rejection (step 04)
    
    picks = pick_types(epochs_in.info, meg=False, eeg=True, stim=False,  eog=False, exclude=sens_exclude)

            
    # we open events from acquisition (renamed, downsampled) to get the original nr of events
    f_events = os.path.join(dir_imne, '{}.{}-eve.fif'.format(subj, pref_eve_orig)) 
    events=read_events(f_events)
    nr_epo_acq = len(events)


    
#     # 2) peak-to peak rejection on remaining epochs
#    ################################################################################
    
    picks_p2p = picks
    nr_epo_early = len(events_early)
    nr_sens = len(picks_p2p) # wrt picks !!!
    th = threshold_eeg[subj] 
    

    bad_epochs_p2p = np.zeros(nr_epo_early, dtype=bool) # all epochs to False
    
    A = epochs_in[:].get_data() # dim0=epochs, dim1 = sensors, dim2=samples
#    A = epochs_in[0].get_data() # dim0=epochs, dim1 = sensors, dim2=samples
    As = A[:,picks_p2p,:]
    mi = np.amin(As, axis = 2) # dim0 = epochs, dim1 = sensors (minimum per sensor for each epoch)
    ma = np.amax(As, axis = 2) # dim0 = epochs, dim1 = sensors (minimum per sensor for each epoch)
    p2p = np.abs(ma -mi)
    h = np.where(p2p > th) # h = tuple of two elements (vectors of same size , n_reject) ;h[0] = where in dim0 (what epochs), h[1] = where in dim1 (what sensors)
    epo_rej_p2p = np.unique(h[0]) # indices of rejected epochs
    bad_epochs_p2p[epo_rej_p2p] = True #  
    contrib = dict()
    
    
    for i_sens, sens  in enumerate(epochs_in.ch_names):
        if i_sens in picks:
            i = np.where(picks_p2p==i_sens)[0]
            count = len(np.where(h[1] == i)[0]) # we count the nr of times where sensor i is found in h[1], whatever the epochs
            pc = count *100 / nr_epo_early 
            contrib[sens]= f'{np.round(pc)} pc '
        else:
            contrib[sens]='................set as bad'
     
    print('Peak-to-Peak Rejection (pc relative to surviving early epochs')
    for i_key in contrib:
        print(f'-- {i_key} ------ >  {contrib[i_key]}')       
            
        
    bad_epochs = bad_epochs_p2p 
    nr_drop = sum(bad_epochs)
    nr_drop_early = nr_epo_acq - nr_epo_early
    nr_drop_tot = nr_drop_early +  nr_drop
    
    print(f'=============> Rejection Summary (relative to acquisition, 6068)')   
    print(f'=============> Early rejection              : dropped {nr_drop_early} epochs <=> {np.round(100*nr_drop_early/nr_epo_acq)} pc')
    print(f'=============> Peak-to-Peak                 : dropped {nr_drop} epochs <=> {np.round(100*nr_drop/nr_epo_acq)} pc')
    print(f'=============> TOTAL                        : dropped {nr_drop_tot} epochs <=> {np.round(100*nr_drop_tot/nr_epo_acq)}pc')

    f_rej_out = os.path.join(dir_imne, '{}.{}.pkl'.format(subj, pref_rej_out)) 
    with  open(f_rej_out, 'wb') as output:
        pickle.dump((bad_epochs,  th), output, pickle.HIGHEST_PROTOCOL)

    # 3) Save epochs
    ################################################################################
    # Early rejection and p2p led to cleaned epochs, available dfor subsequent analysis
    # We save them, in the 1-40 Hz bandwidth
    # For further use with other continuous data (other filters etc), just run import_epochs function in fun_eeg_misc to import these epoch selection onto raw continuous data
    # Output : ========> sub-XX.clean_1-40Hz-epo.fif file, 

    print(f'--- From early rejection to cleaned epochs ...')   
    epochs_out = epochs_in.copy()
    epochs_out.drop(bad_epochs)
    f_epoch_out=os.path.join(dir_imne, '{}.{}-epo.fif'.format(subj, pref_epo_out))
    epochs_out.save(f_epoch_out) 

    # 7) Save save good/bad epochs in an event file (*.pkl)
    ################################################################################
    # homegenity between approaches = p2p / AR-p2p
    bad_epochs_ar = np.zeros(nr_epo_early, dtype=bool) # all epochs to False
    rej_epochs_ar = epochs_in.copy()
    rej_epochs_ar.drop(~bad_epochs_ar, verbose = False)
    rej_epochs_p2p = epochs_in.copy()
    rej_epochs_p2p.drop(~bad_epochs_p2p,  verbose = False)
    
     
    c_ar=0
    c_p2p=0
    c_good=0
    c_early=0
    events_goodbad = events.copy()
    
    for idx, lat in enumerate(events[:,0]):
         if lat not in epochs_in.events[:,0]:
                events_goodbad[idx,2] = 100 # means that events rejected in early phase 
                c_early+=1
         else:
             if lat in rej_epochs_ar.events[:,0]:# means that events rejected in AR 
                events_goodbad[idx,2] = 200
                c_ar+=1
             else:
                if lat in rej_epochs_p2p.events[:,0]:# means that events rejected in p2p 
                    events_goodbad[idx,2] = 300
                    c_p2p+=1
                else:
                    events_goodbad[idx,2] = 1
                    c_good+=1
      
                
                

                
#    events[:10,:]
#    epochs_p2p.events[:10,:]
#    epochs_ar.events[:10,:]   
#    epochs_in.events[:10,:]
    event_good_id = {'good':1,'bad_early': 100,  'bad_ar': 200, 'bad_p2p': 300}
    print(f'Pickle File: {c_early} bad_early epochs // {c_ar} bad_ar epochs // {c_p2p} bad_p2p epochs // {c_good} good epochs')
    print('------------------------ save good / bad events ---------------------------')
    f_goodbad_events = os.path.join(dir_imne, '{}.{}.pkl'.format(subj, pref_goodbad_events)) 
    nr_drop_ar = 0
    nr_drop_p2p = nr_drop
    with  open(f_goodbad_events, 'wb') as output:
               pickle.dump((events_goodbad, sens_exclude, nr_drop_tot, nr_drop_p2p, nr_drop_ar, bad_epochs,event_good_id, contrib, th), output, pickle.HIGHEST_PROTOCOL)
               
               
    
#import pickle
#for i_su in subj_id_list:
#    
#    subj='sub-{:02}'.format(i_su) 
#    dir_imne = os.path.join(dir_mne, subj, 'preproc')  
#    f_rej_out = os.path.join(dir_imne, '{}.{}.pkl'.format(subj, pref_rej_out)) 
#    with  open(f_rej_out, 'rb') as input:
#            a = pickle.load(input)
#    
#    events = a[0]
#    exclude_eeg_ind = a[1]
#    nr_drop_tot = a[2]
#    nr_drop_p2p = a[3]
#    nr_drop_ar = a[4]
#    print(subj, exclude_eeg_ind,nr_drop_ar, nr_drop_p2p, nr_drop_tot )