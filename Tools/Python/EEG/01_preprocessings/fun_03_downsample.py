#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
===============================================
FUNCTIONS for CONTEXTODD EEG - STEP 03 analysis
===============================================
=> 03: downsampling raw data and events, from 1000 Hz to 500 Hz


Created on Tue Feb 12 13:43:16 2019

@author: Francoise Lecaignard

"""


#############################################################################################
## Import packages          -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
# import argparse               # add input arguments

import numpy as np             # convention d'import
import os                       # path.join for file names
import matplotlib.pyplot as plt # from the MNE examples
import warnings

print(__doc__)

from mne import get_config, set_config
type(get_config('MNE_BROWSE_RAW_SIZE'))
set_config('MNE_BROWSE_RAW_SIZE', '20.0,10.0')

from mne import write_events, read_events
from mne.io import read_raw_fif




import sys      # path to my own functions
sys.path.append('/Users/francoise/Documents/Projets/ContextOdd/ContextOdd_Git/Tools/Python/EEG')


warnings.filterwarnings("ignore",category=DeprecationWarning)



from fun_eeg_misc import print_step_label


#####################################################################################################################
#############################################     FUNCTIONS      ####################################################
#####################################################################################################################

#####################################################################################################################
#####################################################################################################################
#####################################################################################################################
## Step 3: Downsampling to 500 Hz (1000/2)  : data AND events             -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-
#####################################################################################################################
#####################################################################################################################
#####################################################################################################################

def codd_eeg_downsample_raw_events(dir_mne, subj, new_freq, plot=True ):
    # downsample raw  events (codes 1, 2)
    # (1000 -> 500 Hz)
    dir_imne = os.path.join(dir_mne, subj, 'preproc') #individual mne path
    
    f_raw = os.path.join(dir_imne, '{}.raw.fif'.format(subj))
    f_eve = os.path.join(dir_imne, '{}.5d-eve.fif'.format(subj))
    
    f_new_raw = os.path.join(dir_imne, '{}.preproc.raw.fif'.format(subj))
    f_new_eve = os.path.join(dir_imne, '{}.preproc.5d-eve.fif'.format(subj))

    raw = read_raw_fif(f_raw, preload=True)
    events =read_events(f_eve)


    print_step_label('Downsampling  {}.....'.format(subj))    
    new_raw, new_events = raw.resample(new_freq, npad='auto', events=events)
    
     
    print_step_label('Save {}.....'.format(subj))
    new_raw.save(fname=f_new_raw,overwrite=True)
    write_events(f_new_eve, new_events)
   
    print_step_label('Done {}.....'.format(subj))
    
    if plot:
        raw = read_raw_fif(f_new_raw, preload=True)
        events =read_events(f_new_eve)
        scal = dict(eeg=10e-5)
        raw.plot(events=events,   n_channels=new_raw.info['nchan'],remove_dc = True,  duration=2, highpass = None, scalings=scal) #, show_options=False)

    

#############################################################################################
## -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.    TEST and DEBUG     --.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
#if __name__== '__main__':
    

   
    