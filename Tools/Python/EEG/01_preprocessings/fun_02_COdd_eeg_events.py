#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
===============================================
FUNCTIONS for CONTEXTODD EEG - STEP 2 analysis
===============================================
=> 02: Event recoding


Created on Tue Feb 12 13:43:16 2019

@author: Francoise Lecaignard

"""


#############################################################################################
## Import packages          -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
# import argparse               # add input arguments

import numpy as np             # convention d'import
import os                       # path.join for file names
import matplotlib.pyplot as plt # from the MNE examples
import shutil as sh
import warnings
import csv

print(__doc__)

from mne import get_config, set_config
type(get_config('MNE_BROWSE_RAW_SIZE'))
set_config('MNE_BROWSE_RAW_SIZE', '20.0,10.0')


from mne import write_events, read_events
from mne.viz import plot_events
import sys      # path to my own functions
sys.path.append('/Users/francoise/Documents/Projets/ContextOdd/ContextOdd_Git/Tools/Python/EEG')


warnings.filterwarnings("ignore",category=DeprecationWarning)



# from fun_eeg_misc import ...


#####################################################################################################################
#############################################     FUNCTIONS      ####################################################
#####################################################################################################################

#####################################################################################################################
#####################################################################################################################
#####################################################################################################################
## Step2 - Recode events using a 5-digit format  (output = *.5d.raw-eve.fif) -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#####################################################################################################################
#####################################################################################################################
#####################################################################################################################
# Each subject has  received 4 sessions, recorded in the same file (continous acquisition)
# Session order was balanced across subjects 
# We define below:
# --------- pred_sess_id = N in 1:4 ,  which is the session that should have been delivered in position N, and associated to Matlab text file sub-XX_ContextOdd_Code_BlocN.txt
# ----------acq_sess_id = N' in 1:4 , which is the real session that subject has received (and possibly differentr from N under manual errors during acquisition)

# it turns out that finally, everything went well regarding this, but we keep this dissociation just in case...
# so that N = N' always


# some subjects received sequences from other subjects
# we thus consider sub_id as their name
# and subj_id_design as the name of whom sequences belong to (in most cases subj_id = sub_id_design)


# Matlab mapping files: rename eSuXX into sub-XX :-) 
#for i in np.arange(0,36):
#    print('mv eSu{:02} sub-{:02}'.format(i,i))
#
#for i in np.arange(17,36):
#    for ext in ['Code_Bloc1', 'Code_Bloc2', 'Code_Bloc3', 'Code_Bloc4']:        
#        f_ori= '/sps/cermep/cermep/experiments/DCM/ContextOdd/StimSeq_BKP/eSu{:02}/eSu{:02}_ContextOdd_{}.txt'.format(i,i, ext)
#        f_tgt= '/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Experimental_Design/StimSeq/sub-{:02}/sub-{:02}_ContextOdd_{}.txt'.format(i,i, ext)
#        sh.copyfile(f_ori, f_tgt)
#        print(f_ori, f_tgt)
#    for ext in [  'Expe', 'Sequence', 'SequenceReverse']:        
#        f_ori= '/sps/cermep/cermep/experiments/DCM/ContextOdd/StimSeq_BKP/eSu{:02}/eSu{:02}_ContextOdd_{}.mat'.format(i,i, ext)
#        f_tgt= '/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Experimental_Design/StimSeq/sub-{:02}/sub-{:02}_ContextOdd_{}.mat'.format(i,i, ext)
#        sh.copyfile(f_ori, f_tgt)
#        print(f_ori, f_tgt)        
#    for ext in [ 'EventMrk']:
#        
#        f_ori= '/sps/cermep/cermep/experiments/DCM/ContextOdd/StimSeq_BKP/ePilote1/eSu{:02}_ContextOdd_{}.mat'.format(i, ext)
#        f_tgt= '/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Experimental_Design/StimSeq/sub-{:02}/sub-{:02}_ContextOdd_{}.mat'.format(i,i, ext)
#        sh.copyfile(f_ori, f_tgt)
#        print(f_ori, f_tgt)
        
        


           
def codd_eeg_rename_events_loop(dir_eventcodes,subj_id_design,pred_sess_id, events, events_new,ind_e_session, ind_s_session ):
    # rename events within each session (subfunction to codd_eeg_rename_events)
     
    acq_sess_id = pred_sess_id 
    print('                -                       ')
    f_code = os.path.join(dir_eventcodes,subj_id_design, '{}_ContextOdd_Code_Bloc{}.txt'.format(subj_id_design,pred_sess_id))
    print('{},  session{}----->  {}'.format(subj_id_design, acq_sess_id,  '{}_ContextOdd_Code_Bloc{}.txt'.format(subj_id_design,pred_sess_id) ))
    with open(f_code, 'r') as f:
        reader = csv.reader(f,delimiter = '\t')
        nr_row = sum(1 for row in reader) # once read, object reader no longer exists (csv.reader stuff)
        print('Matlab Design, session #{}-----> {} events found '.format(acq_sess_id, nr_row))
    with open(f_code, 'r') as f:
        reader = csv.reader(f,delimiter = '\t')
    
        if((ind_e_session - ind_s_session- 1) == nr_row):
            print('youpi, number of events in this session matches raw-eve.fif')
        else:
            print('Problem with session {}:not the good number of events'.format(acq_sess_id+1))
    
        cnt=1        
        for row in reader: #object reader=csv.reader()
            #print('session{},stim #{}, {}/{} - codesession, acq: {}, {} '.format(acq_sess_id+1,cnt,ind_s[acq_sess_id]+cnt+1, events.shape[0], row[0], events[ind_s[acq_sess_id]+cnt,2] ))
            if(float(row[0]) == events[ind_s_session+cnt,2]):
                events_new[ind_s_session+cnt,2] = row[1]
                cnt = cnt+1
            else:
                print('session {}: Mismatch btw codes  in session.txt and raw-eve.fif -> abandon'.format(acq_sess_id+1) ) 
                print('session{},stim #{}, {}/{} - codesession, acq: {}, {} '.format(acq_sess_id+1,cnt,ind_s_session+cnt+1, events.shape[0], row[0], events[ind_s_session+cnt,2] ))
                break 
            
def codd_eeg_rename_events(dir_eventcodes, dir_mne,  subj_id, subj_id_design,nr_event, session_order = np.array([1, 2, 3, 4 ]),  plot = True):
    # from a continuous file made of 4 sessions (codes: 16, 1, 2, 32 for start, std, dev and stop, resp.)
    # rename every std and dev of each session according to a 5-digit code (see notes about this)
    # for each session, refers to a text file with two columns (1st= {1 and 2 } original, 2nd= {5-digit codes}  originally created with Matlab
    # subj_id = eSu00 for instance
    # subj_id_design = subj_id unless a participant received sequences of another (mistaken manipulation of Presentation *.sce files)
    dir_imne = os.path.join(dir_mne, subj_id, 'preproc') #individual mne path
    code_start = 16
    code_end = 32
    f_events = os.path.join(dir_imne,  '{}.raw-eve.fif'.format(subj_id))
    events =read_events(f_events)
    events_new = np.copy(events)
    
    ind_s = np.where(events[:,2]==code_start)[0] # session boundaries within the continuous recording
    ind_e = np.where(events[:,2]==code_end)[0] # session boundaries within the continuous recording
    for acq_sess_id in range(len(ind_s)) :
        print(acq_sess_id)
        print('Raw-eve data, session #{}-----> {} events in between [start; end] (expected {})'.format(acq_sess_id+1, ind_e[acq_sess_id] - ind_s[acq_sess_id]- 1, nr_event/4 -2) )# should be equal to nr_evt 

    for acq_sess_id in range(len(session_order)):
        ind_e_session = ind_e[acq_sess_id]
        ind_s_session = ind_s[acq_sess_id]
        codd_eeg_rename_events_loop(dir_eventcodes,subj_id_design,session_order[acq_sess_id],events,  events_new,ind_e_session, ind_s_session )

            
    print('                -                       ')
    print('Writing new event file...')
    f_events_new = os.path.join(dir_imne,  '{}.5d-eve.fif'.format(subj_id))
    write_events(f_events_new,events_new)
    
    ### PLOT
    if plot:
        plot_events(events_new,  show = False)
        plt.title(' {} : renamed events'.format(subj_id))
        plt.show()
    

    
    
def codd_eeg_rename_events_eSu04(dir_eventcodes, dir_mne,nr_event,  session_order= np.array([1, 2, 3, 4 ]) , plot = True):
    # from a continuous file made of 4 sessions (codes: 16, 1, 2, 32 for start, std, dev and stop, resp.)
    # rename every std and dev of each session according to a 5-digit code (see notes about this)
    # for each session, refers to a text file with two columns (1st= {1 and 2 } original, 2nd= {5-digit codes}  originally created with Matlab
    # subj_id = eSu00 for instance
    # subj_id_design = subj_id unless a participant received sequences of another (mistaken manipulation of Presentation *.sce files)
    subj_id = 'sub-04'
    dir_imne = os.path.join(dir_mne, subj_id, 'preproc') #individual mne path
    code_start = 16
    code_end = 32
    f_events = os.path.join(dir_imne,  '{}.raw-eve.fif'.format(subj_id))
    events =read_events(f_events)
    events_new = np.copy(events)
    
    ind_s = np.where(events[:,2]==code_start)[0] # session boundaries within the continuous recording
    ind_e = np.where(events[:,2]==code_end)[0] # session boundaries within the continuous recording
    for acq_sess_id in range(len(ind_s)) :
        print(acq_sess_id)
        print('Raw-eve data, session #{}-----> {} events in between [start; end] (expected {})'.format(acq_sess_id+1, ind_e[acq_sess_id] - ind_s[acq_sess_id]- 1, nr_event/4 -2) )# should be equal to nr_evt 

    ### session 1; normal
    acq_sess_id = 0
    ind_e_session = ind_e[acq_sess_id]
    ind_s_session = ind_s[acq_sess_id]
    codd_eeg_rename_events_loop(dir_eventcodes,subj_id,session_order[acq_sess_id], events, events_new, ind_e_session, ind_s_session )

    ### session 2 => account for missing start, and missing first events
    acq_sess_id = 1
    pred_sess_id = acq_sess_id
    f_code = os.path.join(dir_eventcodes,subj_id, '{}_ContextOdd_Code_Bloc{}.txt'.format(subj_id, pred_sess_id+1))
    print('                -                       ')
    print('{}, session{}-----> {}'.format(subj_id, acq_sess_id+1,  f_code ))
    with open(f_code, 'r') as f:
        reader = csv.reader(f,delimiter = '\t')
        nr_row = sum(1 for row in reader) # once read, object reader no longer exists (csv.reader stuff)
        
    nr_ev2 = ind_e[1] - ind_e[0] - 1 # 1507 => we missed 9 events: start and 8 sounds
    offset = nr_row - nr_ev2
    with open(f_code, 'r') as f:
        reader = csv.reader(f,delimiter = '\t')
        cnt=1  
        dummy = 0      
        for row in reader: #object reader=csv.reader()- how to skip the first 7 rows???
            if(dummy==offset):
                # print('stim #{}, {}/{} - codesession, acq: {}, {} '.format(cnt,ind_e[acq_sess_id-1]+cnt+1, events.shape[0], row[0], events[ind_e[acq_sess_id-1]+cnt,2] ))
                if(float(row[0]) == events[ind_e[acq_sess_id-1]+cnt,2]):
                    events_new[ind_e[acq_sess_id-1]+cnt,2] = row[1]
                    cnt = cnt+1
                else:
                    print('session {}: Mismatch btw codes  in session.txt and raw-eve.fif -> abandon'.format(acq_sess_id+1) ) 
                    break
            else:
                dummy = dummy+1
    ### session 3 and 4 : use ind_e normally, and ind_s[acq_sess_id-1] instaed of acq_sess_id
    for acq_sess_id in [2, 3]:
        ind_e_session = ind_e[acq_sess_id]
        ind_s_session = ind_s[acq_sess_id-1]
        codd_eeg_rename_events_loop(dir_eventcodes,subj_id,session_order[acq_sess_id], events, events_new,ind_e_session, ind_s_session )

    ### store eveything
    print('                -                       ')
    print('Writing new event file...')
    f_events_new = os.path.join(dir_imne,  '{}.5d-eve.fif'.format(subj_id))
    write_events(f_events_new,events_new)
    
    ### PLOT
    if plot:
        plot_events(events_new,  show = False)
        plt.title(' {} : renamed events'.format(subj_id))
        plt.show()


def codd_eeg_rename_events_eSu16(dir_eventcodes, dir_mne, nr_event, session_order= np.array([1, 2, 3, 4 ]) ,  plot = True):
    # from a continuous file made of 4 sessions (codes: 16, 1, 2, 32 for start, std, dev and stop, resp.)
    # rename every std and dev of each session according to a 5-digit code (see notes about this)
    # for each session, refers to a text file with two columns (1st= {1 and 2 } original, 2nd= {5-digit codes}  originally created with Matlab
    # subj_id = eSu00 for instance
    # subj_id_design = subj_id unless a participant received sequences of another (mistaken manipulation of Presentation *.sce files)
    subj_id = 'sub-16'
    dir_imne = os.path.join(dir_mne, subj_id, 'preproc') #individual mne path
    code_start = 16
    code_end = 32
    f_events = os.path.join(dir_imne,  '{}.raw-eve.fif'.format(subj_id))
    events =read_events(f_events)
    events_new = np.copy(events)
    
    ind_s = np.where(events[:,2]==code_start)[0] # session boundaries within the continuous recording
    ind_e = np.where(events[:,2]==code_end)[0] # session boundaries within the continuous recording
    ind_s=np.delete(ind_s,0) #### issue #1: faux-depart

    ### session 1; normal
    acq_sess_id = 0
    ind_e_session = ind_e[acq_sess_id]
    ind_s_session = ind_s[acq_sess_id]
    codd_eeg_rename_events_loop(dir_eventcodes,subj_id,session_order[acq_sess_id], events, events_new, ind_e_session, ind_s_session )

    ### fix issue #2, session2
    acq_sess_id = 1
    pred_sess_id = acq_sess_id
    f_code = os.path.join(dir_eventcodes,subj_id, '{}_ContextOdd_Code_Bloc{}.txt'.format(subj_id, pred_sess_id+1))
    print('{}-----> session {}:{}'.format(subj_id, acq_sess_id+1,  f_code ))
    with open(f_code, 'r') as f:
        reader = csv.reader(f,delimiter = '\t')
        nr_row = sum(1 for row in reader) # once read, object reader no longer exists (csv.reader stuff)
        print('Nr of events found in session.txt: {}'.format(nr_row))
    nr_bad =  ind_e[acq_sess_id] - ind_s[acq_sess_id]- 1 - nr_row # manually
    cnt=1+nr_bad    
    with open(f_code, 'r') as f:
        reader = csv.reader(f,delimiter = '\t')
        for row in reader: #object reader=csv.reader()
            if(float(row[0]) == events[ind_s[acq_sess_id]+cnt,2]):
                events_new[ind_s[acq_sess_id]+cnt,2] = row[1]
                cnt = cnt+1
            else:
                print('session {}: Mismatch btw codes  in session.txt and raw-eve.fif -> abandon'.format(acq_sess_id+1) ) 
                break

    ### session 3 and 4 : normal
    for acq_sess_id in [2, 3]:
        ind_e_session = ind_e[acq_sess_id]
        ind_s_session = ind_s[acq_sess_id]
        codd_eeg_rename_events_loop(dir_eventcodes,subj_id,session_order[acq_sess_id], events, events_new,ind_e_session, ind_s_session )

    ### store eveything
    print('                -                       ')
    print('Writing new event file...')
    f_events_new = os.path.join(dir_imne,  '{}.5d-eve.fif'.format(subj_id))
    write_events(f_events_new,events_new)
    
    ### PLOT
    if plot:
        plot_events(events_new,  show = False)
        plt.title(' {} : renamed events'.format(subj_id))
        plt.show()

def codd_eeg_rename_events_eSu19(dir_eventcodes, dir_mne, nr_event, session_order= np.array([1, 2, 3, 4 ]) ,  plot = True):
    #faux depart, and received sounds of eSu18
    subj_id = 'sub-19'
    dir_imne = os.path.join(dir_mne, subj_id, 'preproc') #individual mne path
    subj_id_design = 'sub-18'
    code_start = 16
    code_end = 32
    f_events = os.path.join(dir_imne,  '{}.raw-eve.fif'.format(subj_id))
    events =read_events(f_events)
    events_new = np.copy(events)
    
    ind_s = np.where(events[:,2]==code_start)[0] # session boundaries within the continuous recording
    ind_e = np.where(events[:,2]==code_end)[0] # session boundaries within the continuous recording
    ind_s=np.delete(ind_s,0) #### issue #1: faux-depart

    for acq_sess_id in range(len(session_order)):
        ind_e_session = ind_e[acq_sess_id]
        ind_s_session = ind_s[acq_sess_id]
        print(subj_id)
        print('   ')
        codd_eeg_rename_events_loop(dir_eventcodes,subj_id_design,session_order[acq_sess_id],events,  events_new,ind_e_session, ind_s_session )

            
    print('                -                       ')
    print('Writing new event file...')
    f_events_new = os.path.join(dir_imne,  '{}.5d-eve.fif'.format(subj_id))
    write_events(f_events_new,events_new)
    
    ### PLOT
    if plot:
        plot_events(events_new,  show = False)
        plt.title(' {} : renamed events'.format(subj_id))
        plt.show()
    

#############################################################################################
## -.-.-.-.-.-.-.-   RECODE EVENTS FOR ERP ana_erp_bloc1_bloc2 analysis --.-.-.-.-.-.-.-.-.-.
#############################################################################################

def codd_eeg_rename_events_bloc1_bloc2(subj, f_events_5d, f_events_6d, plot=False):
    # each condition (var_p, var_c) was delivered twice (bloc1, bloc2)
    # from renamed event file , coded with 5-digit code: we add a 6th digit to differenciate first bloc and second one (in each condition var_c, and var_p)
    # example : stimulus 16123 will be equal to 116123 if in bloc1, and 216123 if bloc2
    #
    # Three atypical subjects
    # sub-04 : start code of bloc2 is missing
    # sub-16 : 5 blocs, bloc1 is irrelevant; bloc2: first sounds are irrelevant
    # sub-19: 5 blocs; bloc1 is irrelevant
    #
    # inputs
    #----------- subj_id : id of the suject
    #----------- f_events_5d : name of the original 5-digit file, downsampled
    #-----------
    
    
    
    
    code_start = 16
    code_end = 32
    events =read_events(f_events_5d)
        
    ind_s = np.where(events[:,2]==code_start)[0] # session boundaries within the continuous recording
    ind_e = np.where(events[:,2]==code_end)[0] # session boundaries within the continuous recording
    
    #----------screen events and check the four blocs
    stim_last = []
    stop=0
    if len(ind_e) == 4: # four expected blocs
        for bloc_id, bloc_sample in enumerate(ind_e) :
            stim_last.append(events[bloc_sample-1,2])
            if stim_last[bloc_id] not in [10003,20003]:
                stop=1
            print(f'{subj} : bloc#{bloc_id+1}: stim_last = {stim_last[bloc_id]}')
    else:
        stop=1
    
    #------------- recode
    digit6_dict = {'var_p_b1':100000, 'var_p_b2':200000,'var_c_b1':100000,'var_c_b2':200000}
    
    if stop==1:
        print('ABORTED - Not the expected number of blocs, or expected stimulus codes')
    else:# we assume four blocs var_p; var_c
        events_new =events.copy()        
        sort_bloc=dict()
        cpt_c = 0
        cpt_p = 0
        for bloc_id, bloc_ind_e in enumerate(ind_e) :
            if (stim_last[bloc_id] == 10003 and cpt_c==0):
                sort_bloc[bloc_id] = 'var_c_b1'
                cpt_c = 1
            
            elif (stim_last[bloc_id] == 10003 and cpt_c==1):
                sort_bloc[bloc_id] = 'var_c_b2'
                cpt_c = 2
                
            
            elif (stim_last[bloc_id] == 20003 and cpt_p==0):
                sort_bloc[bloc_id] = 'var_p_b1'
                cpt_p = 1
             
            elif (stim_last[bloc_id] == 20003 and cpt_p==1):
                sort_bloc[bloc_id] = 'var_p_b2'
                cpt_p = 2
            
            code_min = stim_last[bloc_id] 
            code_max = stim_last[bloc_id] + 9997 #10003->20000 or 20003->30000

            if bloc_id ==0:
                ind_stim = np.intersect1d(np.where( events[:,2] > code_min)[0], np.where( events[:,2] < code_max)[0])
                ind_stim = np.intersect1d(ind_stim, np.where( events[:,0] < events[bloc_ind_e,0] )[0])
            else:
                ind_stim = np.intersect1d(np.where( events[:,2] > code_min)[0], np.where( events[:,2] < code_max)[0])
                ind_stim = np.intersect1d(ind_stim, np.where( events[:,0] < events[bloc_ind_e,0] )[0])
                ind_stim = np.intersect1d(ind_stim, np.where( events[:,0] > events[ind_e[bloc_id-1],0] )[0])

            print(len(ind_stim))
            events_new[ind_stim,2]+=digit6_dict[sort_bloc[bloc_id]]
            
        

            
    print('                -                       ')
    print('Writing new event file...')
    write_events(f_events_6d,events_new)
    
#    ### PLOT
    if plot:
        plot_events(events_new,  show = False) 
        plt.title(' {} '.format(subj))
        plt.show()
#############################################################################################
## -.-.-.-.-.-.-.-   RECODE EVENTS FOR ERP ana_erp_deb_fin analysis --.-.-.-.-.-.-.-.-.-.
#############################################################################################

def codd_eeg_rename_events_deb_fin(subj, f_events_6d, f_events_7d, plot=False):
    # each condition (var_p, var_c) was delivered twice (bloc1, bloc2)
    # from renamed event file , coded with 5-digit code: we add a 6th digit to differenciate first bloc and second one (in each condition var_c, and var_p)
    # example : stimulus 16123 will be equal to 116123 if in bloc1, and 216123 if bloc2
    # here we add a 7th digit to code start / end of withina bloc (begin = 1st half, end = second half)
    #
    # Three atypical subjects
    # sub-04 : start code of bloc2 is missing
    # sub-16 : 5 blocs, bloc1 is irrelevant; bloc2: first sounds are irrelevant
    # sub-19: 5 blocs; bloc1 is irrelevant
    #
    # inputs
    #----------- subj_id : id of the suject
    #----------- f_events_5d : name of the original 5-digit file, downsampled
    #-----------
    
    
    
    
    code_start = 16
    code_end = 32
    events =read_events(f_events_6d)
    
        
    ind_e = np.where(events[:,2]==code_end)[0] # session boundaries within the continuous recording
    
    #----------screen events and check the four blocs
    stim_last = []
    stop=0
    if len(ind_e) == 4: # four expected blocs
        for bloc_id, bloc_sample in enumerate(ind_e) :
            stim_last.append(events[bloc_sample-1,2])
            if stim_last[bloc_id] not in [10003,20003]:
                stop=1
            print(f'{subj} : bloc#{bloc_id+1}: stim_last = {stim_last[bloc_id]}')
    else:
        stop=1
    
    #------------- recode
    
    if stop==1:
        print('ABORTED - Not the expected number of blocs, or expected stimulus codes')
    else:# we assume four blocs var_p; var_c
        events_new =events.copy()        
  
        for bloc_id, bloc_ind_e in enumerate(ind_e) :
            
            code_min = stim_last[bloc_id] 
            code_max = stim_last[bloc_id] + 9997 #10003->20000 or 20003->30000

            if bloc_id ==0:
                ind_stim = np.intersect1d(np.where( events[:,2] > code_min)[0],  np.where( events[:,0] < events[bloc_ind_e,0] )[0])
            else:
                ind_stim = np.intersect1d(np.where( events[:,2] > code_min)[0],np.where( events[:,0] < events[bloc_ind_e,0] )[0])
                ind_stim = np.intersect1d(ind_stim, np.where( events[:,0] > events[ind_e[bloc_id-1],0] )[0])
                
            nr_stim = len(ind_stim)
            print(nr_stim)
            ind_stim_deb = ind_stim.copy()
            ind_stim_fin = ind_stim.copy()
                
            if nr_stim % 2 == 0: # remainder of division / 2; if 0 then odd
                ind_stim_deb = ind_stim[np.arange(0, nr_stim/2, dtype=int)]
                ind_stim_fin = ind_stim[np.arange(nr_stim/2, nr_stim,dtype=int)]
            else:
                ind_stim_deb = ind_stim[np.arange(0, round(nr_stim/2)-1, dtype=int)]
                ind_stim_fin = ind_stim[np.arange(round(nr_stim/2), nr_stim, dtype=int)]

            events_new[ind_stim_deb,2]+=1000000
            events_new[ind_stim_fin,2]+=2000000
            
        

            
    print('                -                       ')
    print('Writing new event file...')
    write_events(f_events_7d,events_new)
    
#    ### PLOT
    if plot:
        plot_events(events_new,  show = False) 
        plt.title(' {} '.format(subj))
        plt.show()    
#############################################################################################
## -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.    TEST and DEBUG     --.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
if __name__== '__main__':
    

    
    ## DEBUG Step 2 ########################################################################
    dir_mne = '/sps/cermep/cermep/experiments/DCM/ContextOdd/Ana_MNE'
    dir_eventcodes='/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Experimental_Design/StimSeq' # path to the mapping matlab text files
    nr_event = 6068
    subj_id = 'sub-00'
    subj_id_design = 'sub-00'
    codd_eeg_rename_events(dir_eventcodes, dir_mne,  subj_id, subj_id_design, nr_event, plot=True)
##    
##    subj_id = 'eSu04'
##    codd_eeg_rename_events_eSu04(dir_eventcodes, dir_mne, session_order , nr_event, plot = True)
##    subj_id = 'eSu16'
##    codd_eeg_rename_events_eSu16(dir_eventcodes, dir_mne, session_order , nr_event, plot = True)
#    subj_id = 'eSu18'
#    codd_eeg_rename_events_eSu19(dir_eventcodes, dir_mne, session_order , nr_event, plot = True)
    
#    subj_id, event_fname = 'eSu11', 'preproc_renamed'
#    events=read_events(os.path.join(dir_imne, '{}.{}-eve.fif'.format(subj_id, event_fname)))
#    plot_events(events)
    # each session = 1512 relevant sounds + 3 extra sounds at the end, + start + end = 1517 events
    
    