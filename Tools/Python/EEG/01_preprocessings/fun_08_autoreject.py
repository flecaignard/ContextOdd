#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
===============================================
FUNCTIONS for CONTEXTODD EEG - STEP 08 analysis
===============================================
=> 08: drop bad epochs, and identify bad sensors using MNE autoreject


Created on Tue Feb 12 13:43:16 2019

@author: Francoise Lecaignard

"""


#############################################################################################
## Import packages          -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
# import argparse               # add input arguments

import numpy as np             # convention d'import
import os                       # path.join for file names
import matplotlib.pyplot as plt # from the MNE examples
import warnings
import pickle

print(__doc__)

from mne import get_config, set_config
type(get_config('MNE_BROWSE_RAW_SIZE'))
set_config('MNE_BROWSE_RAW_SIZE', '20.0,10.0')

import sys      # path to my own functions
sys.path.append('/Users/francoise/Documents/Projets/ContextOdd/ContextOdd_Git/Tools/Python/EEG')
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/toolboxes/autoreject')

from autoreject import compute_thresholds, get_rejection_threshold
from autoreject import set_matplotlib_defaults  # noqa
set_matplotlib_defaults(plt)
from autoreject import AutoReject

from mne.viz import plot_evoked_topo

warnings.filterwarnings("ignore",category=DeprecationWarning)

from fun_plot_autoreject import  plot_autoreject_threshold_hist,plot_autoreject_labels
from fun_util_eeg import  count_events


# from fun_eeg_misc import ...


#####################################################################################################################
#############################################     FUNCTIONS      ####################################################
#####################################################################################################################
def codd_eeg_sample_ar_training_epochs(epochs, n_samples, f_ar_trainingdata):
    # the aim is to randmoly select representative epochs for AutoReject training
    # arbitrary indexes could not be applied in all participants because of  first epoching in step04 
    # we save the sampled epochs in a pickle file, to be re-used if necessary
    # let's try 120 epochs,  1/3 in std, 1/3 in dev , and 1/3 in std-oth
    # and 1/2 in condition var_c, 1/2 in cond var_p
    # sampling is saved to be re-used 
    #
    # First attempts with circa 50 epochs led to very badly informed p2p amplitude threshes (too small)
    # 
    #
    # epochs: mne Epochs object
    # n_samples : int, number of epochs to be sampled
    # f_ar_trainingdata: output pickle filename (*.pkl)
    #
    #
    cond_list = ('var_c/std','var_c/std_oth', 'var_c/dev',
                 'var_p/std','var_p/std_oth', 'var_p/dev' )
    n_per_cond = round(n_samples/len(cond_list))
    sel_ar = list()
    sample_ar = dict()
    for idx, cond in enumerate(cond_list):
        rand = np.random.choice(len(epochs[cond]),n_per_cond, replace = False )
        sel = sorted(epochs[cond].selection[rand].tolist())
        sel_ar.append(sel) # indices of epochs relative to epochs and not epochs[cond]
        sample_ar[cond]=sel
    
    flat_sel_ar = [item for sublist in sel_ar for item in sublist] # whaaat?
    epochs_ar = epochs[flat_sel_ar].copy()
    
    with  open(f_ar_trainingdata, 'wb') as output:
        pickle.dump((epochs_ar, sample_ar), output, pickle.HIGHEST_PROTOCOL)
    
    return epochs_ar, sample_ar
        
#############################################################################################
##  o*o*o*o*o*o*o*o*o*o*o*o*o*o* Run  AutoReject classification   *o*o*o*o*o*o*o*o*o*o*o*o*o*
#############################################################################################

 
def run_autoreject(subj, epochs, picks_ar,sens_exclude,  learn_epochs, event_id, erp_list,  mod='eeg', kappa=None, plot_learn=False, plot_erp=False):
    # the aim is to fit ar on a subset of epochs (learn_epoch
    # then apply to entire epochs (epochs): plots to see how many bad sensors we have, what rejection rate?
    # then compute ERPs in erp_list to see how it goes
    # kappa in AutoReject (or consensus) is the  minimum percentage of bad channels to declare an epoch as bad
    # kappa can be inferred in ar.fit(learn_epochs) or fixed; in this case input kappa = percentage
    
    if kappa:
        ar = AutoReject(picks=picks_ar, random_state=42, n_jobs=1, n_interpolate=[0], consensus = [kappa],  verbose='tqdm') #)
    else:
        ar = AutoReject(picks=picks_ar, random_state=42, n_jobs=1, n_interpolate=[0],  consensus = np.linspace(0,1.0, 21), verbose='tqdm') #)

    ar.fit(learn_epochs)
    
    
    # see how fit is? bad sensors ?
    # labels matrix in reject_log is such that an epoch is not set to bad across all sensors if bad > kappa (this is the case in bad_epochs)
    # labels = f(threshes) (possibly f(threshes, rho) ? but here rho=0)
    # whereas bad_epochs = f(threshes, kappa)
    reject_log = ar.get_reject_log(learn_epochs)
#    title='{} (yellow=bad)'.format(subj)
#    plot_autoreject_labels(reject_log, title)

    if plot_learn:
            scalings = dict(eeg=40e-5)
            reject_log.plot_epochs(learn_epochs, scalings=scalings)

    #apply to all epochs
    reject_log_all = ar.get_reject_log(epochs)
    # Compute guiltiness of each sensor:
    labels = reject_log_all.labels.transpose() # Nsens * Nepochs
    Th = ar.threshes_
    count = labels.sum(axis=1)*100/labels.shape[1]
    
    for idx, name in enumerate(reject_log_all.ch_names):
        if count[idx]>=0:
            print('{}: Th = {} uV =====> {} % '.format(name, round( Th[name]*1e6), round( count[idx])))
        else:
            print('{}: set as bad'.format(name))
    
    #summarize rejection
    rej = sum(reject_log_all.bad_epochs)
    ok = len(reject_log_all.bad_epochs) -rej
    pc = round(100 *rej/len(reject_log_all.bad_epochs))
    print('{} : kappa = {} % =======> {} ok, {} bad <=> {} % rej'.format(subj, round(100* ar.consensus_[mod]), ok, rej, pc))
    
    #plot ERPs
    if plot_erp:
        # drop bad epochs 
        epochs_clean = epochs.copy()
        epochs_clean.drop(reject_log_all.bad_epochs)
        # filter
        epochs_clean_test= epochs_clean.copy().filter(2,20)
        evokeds = [epochs_clean_test[name].average().apply_baseline((-0.2, 0)) for name in erp_list]
        id_to_be_counted = list()
        for i_ev, name in enumerate(erp_list):
            evokeds[i_ev].comment = '{}, {} %'.format(name, round(100* ar.consensus_[mod]))
            id_to_be_counted.append(event_id[name])
        plot_evoked_topo(evokeds, ylim=dict(eeg=[-5,5]))
        count_events(epochs_clean_test.events, id_to_be_counted, events_ref=epochs.events)

    return ar, reject_log_all, pc
#############################################################################################
##  o*o*o*o*o*o*o*o*o*o*o*o*o*o* explore AutoReject classification   *o*o*o*o*o*o*o*o*o*o*o*o*o*
#############################################################################################

 
def launch_and_see_autoreject(subj, epochs, picks_ar,sens_exclude,  learn_epochs, event_id, erp_list, maxflag=30, mod='eeg', kappa=None, plot_learn=False, plot_erp=False):
    # the aim is to fit ar on a subset of epochs (learn_epoch
    # then apply to entire epochs (epochs): plots to see how many bad sensors we have, what rejection rate?
    # then compute ERPs in erp_list to see how it goes
    # kappa in AutoReject (or consensus) is the  minimum percentage of bad channels to declare an epoch as bad
    # kappa can be inferred in ar.fit(learn_epochs) or fixed; in this case input kappa = percentage
    
    if kappa:
        ar = AutoReject(picks=picks_ar, random_state=42, n_jobs=1, n_interpolate=[0], consensus = [kappa],  verbose='tqdm') #)
    else:
        ar = AutoReject(picks=picks_ar, random_state=42, n_jobs=1, n_interpolate=[0],  consensus = np.linspace(0,1.0, 21), verbose='tqdm') #)

    ar.fit(learn_epochs)
    
    
    # see how fit is? bad sensors ?
    # labels matrix in reject_log is such that an epoch is not set to bad across all sensors if bad > kappa (this is the case in bad_epochs)
    # labels = f(threshes) (possibly f(threshes, rho) ? but here rho=0)
    # whereas bad_epochs = f(threshes, kappa)
    reject_log = ar.get_reject_log(learn_epochs)
#    title='{} (yellow=bad)'.format(subj)
#    plot_autoreject_labels(reject_log, title)

    if plot_learn:
            scalings = dict(eeg=40e-5)
            reject_log.plot_epochs(learn_epochs, scalings=scalings)

    #apply to all epochs
    reject_log_all = ar.get_reject_log(epochs)
    # Compute guiltiness of each sensor:
    labels = reject_log_all.labels.transpose() # Nsens * Nepochs
    Th = ar.threshes_
    count = labels.sum(axis=1)*100/labels.shape[1]
    c=0
    flag_stop=True
    for idx, name in enumerate(reject_log_all.ch_names):
        if count[idx]>=0:
            if count[idx]>= maxflag:
                print('{}: Th = {} uV =====> {} % ============> ABOVE THRESH.'.format(name, round( Th[name]*1e6), round( count[idx])))
                sens_exclude.append(name)
                flag_stop=False
            else:
                print('{}: Th = {} uV =====> {} % '.format(name, round( Th[name]*1e6), round( count[idx])))
            c=c+1
        else:
            print('{}: set as bad'.format(name))
    
    #summarize rejection
    rej = sum(reject_log_all.bad_epochs)
    ok = len(reject_log_all.bad_epochs) -rej
    pc = round(100 *rej/len(reject_log_all.bad_epochs))
    print('{} : kappa = {} % =======> {} ok, {} bad <=> {} % rej'.format(subj, round(100* ar.consensus_[mod]), ok, rej, pc))
    
    #plot ERPs
    if plot_erp:
        # drop bad epochs 
        epochs_clean = epochs.copy()
        epochs_clean.drop(reject_log_all.bad_epochs)
        # filter
        epochs_clean_test= epochs_clean.copy().filter(2,20)
        evokeds = [epochs_clean_test[name].average().apply_baseline((-0.2, 0)) for name in erp_list]
        id_to_be_counted = list()
        for i_ev, name in enumerate(erp_list):
            evokeds[i_ev].comment = '{}, {} %'.format(name, round(100* ar.consensus_[mod]))
            id_to_be_counted.append(event_id[name])
        plot_evoked_topo(evokeds, ylim=dict(eeg=[-5,5]))
        count_events(epochs_clean_test.events, id_to_be_counted, events_ref=epochs.events)

    return ar, reject_log_all, sens_exclude, pc, flag_stop
    
#############################################################################################
## -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.    TEST and DEBUG     --.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
if __name__== '__main__':
    codd_eeg_sample_ar_training_epochs(epochs, f_ar_trainingdata)

   
    