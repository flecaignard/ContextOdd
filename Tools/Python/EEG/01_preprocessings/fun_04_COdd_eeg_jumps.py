#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
===============================================
FUNCTIONS for CONTEXTODD EEG - STEP 04 analysis
===============================================
=> 04: remove jumps in continuous data


Created on Tue Feb 12 13:43:16 2019

@author: Francoise Lecaignard

"""


#############################################################################################
## Import packages          -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
# import argparse               # add input arguments

import numpy as np             # convention d'import
import os                       # path.join for file names
import matplotlib.pyplot as plt # from the MNE examples
import warnings

print(__doc__)

from mne import get_config, set_config
type(get_config('MNE_BROWSE_RAW_SIZE'))
set_config('MNE_BROWSE_RAW_SIZE', '20.0,10.0')


from fun_eeg_misc import raw_jump_detect
from mne.io import read_raw_fif
from mne import pick_types


import sys      # path to my own functions
sys.path.append('/Users/francoise/Documents/Projets/ContextOdd/ContextOdd_Git/Tools/Python/EEG')


warnings.filterwarnings("ignore",category=DeprecationWarning)




#####################################################################################################################
#############################################     FUNCTIONS      ####################################################
#####################################################################################################################

def codd_eeg_rm_jumps(dir_mne, subj,jump_thresh, jump_dur, jump_tw, lf=1, hf=40):
#    dir_mne: path to MNE analysis
#    subj: subject code : sub-00
#    jump_thresh= threshol in volts
#    jump_dur: jump duration in seconds
#    jump_tw:  bad segment time-window (in sec). Ex tw=2 means that we remove 2 seconds around each jumps (using MNE annotations)
#    lf: high-pass
#    hf: low-pass in Hertz
    
    
#    jump_thresh = 250e-6
#    jump_dur = 40e-3
#    jump_tw = 2 
    
    dir_imne = os.path.join(dir_mne, subj, 'preproc') #individual mne path
    f_raw = os.path.join(dir_imne, '{}.preproc.raw.fif'.format(subj))
    
    raw = read_raw_fif(f_raw, preload=True)
    
    raw.filter(lf, hf)
    picks = pick_types(raw.info, eeg=True, stim=False,exclude=())
    f_jump_out = os.path.join(dir_imne, '{}.{}-annot.fif'.format(subj, 'preproc'))
    f_raw_out = os.path.join(dir_imne, '{}.{}.raw.fif'.format(subj, 'preproc_annot'))
    raw_jump_detect(raw, picks, jump_thresh, jump_dur, jump_tw,save=True, f_jump_out =f_jump_out,f_raw_out=f_raw_out)

    

#############################################################################################
## -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.    TEST and DEBUG     --.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
if __name__== '__main__':
    
   
    dir_mne = '/sps/cermep/cermep/experiments/DCM/ContextOdd/Ana_MNE'
    subj = 'sub-29'
    jump_thresh = 300e-6
    jump_dur = 60e-3
    jump_tw = 2 
    codd_eeg_rm_jumps(dir_mne, subj,jump_thresh, jump_dur, jump_tw, lf=None, hf=None)


   
    