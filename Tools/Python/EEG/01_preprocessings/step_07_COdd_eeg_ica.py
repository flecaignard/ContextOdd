#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
===============================================
Step 07 - ICA correction
===============================================

1) Compute ICA components on 1-40 Hz epochs (-200 +410 ms)
2) Identify blink components and when possible saccade component
3) Remove EOG components to derive cleaned epochs, check output is ok

=====>output = preproc/sub-xx_preproc_1-40Hz-ica.fif 
=====>         preproc/sub-xx_preproc_1-40Hz_ica.raw.fif (corrected continuous data)
=====>         preproc/sub-xx_preproc_early_1-40Hz_ica-epo.fif (corrected epochs with early rejection)


Created on Mon Feb 28th

@author: Françoise Lecaignard, francoise.lecaignard@inserm.fr
"""



###############################################################################
import numpy as np             # convention d'import
import os
import sys      # path to my own functions
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Python/EEG')
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/BIDS/mne-bids')
import matplotlib.pyplot as plt # from the MNE examples
from mne.io import read_raw_fif
from mne import Epochs, pick_types, read_events, read_epochs
from mne import get_config, set_config
type(get_config('MNE_BROWSE_RAW_SIZE'))
set_config('MNE_BROWSE_RAW_SIZE', '20.0,10.0')
from mne.preprocessing import ICA, read_ica, find_eog_events
from mne.viz import  plot_ica_components, plot_ica_overlay
#from fun_NN_template import  ...
from fun_util_eeg import  print_step_label,ica_view_correction, ica_plot_blink_evoked
from fun_util_epoch import import_epochs


################################################################################
# Input Parameters 
################################################################################

### ---------------------------- Paths ----------------------------
dir_mne = '/sps/cermep/cermep/experiments/DCM/ContextOdd/Ana_MNE'

### ----------------------------Filenames----------------------------
# prefix for ica files
pref_ica = 'preproc_1-40Hz' #preproc/sub-xx_preproc_1-40Hz-ica.fif 

# Pref epoch file
pref_epo = 'preproc_early_1-40Hz'

# Pref epoch file
pref_epo_out = 'preproc_early_1-40Hz_ica' # output file : epochs with early rejection and ica  correction

# Pref raw file (downsampled, filtered , but before correction)
pref_raw = 'preproc_1-40Hz'  #preproc/sub-xx_preproc_1-40Hz.raw.fif 
# Pref raw file for output  (downsampled, filtered , and ica-corrected)
pref_raw_out = 'preproc_1-40Hz_ica'  #preproc/sub-xx_preproc_1-40Hz_ica.raw.fif 

### ----------------------------bad sensors ###----------------------------
# These sensors are excluded from ICA fit, but will not be dropped in the resulting epochs
# Initial version: we rejected bad sensors from step04 epoching on unfiltered data 
# But it turns out that notch + filter led to acceptable sensors (ex: sub-02, P4)
# So we use drops from step_05 PSD inspection

# exclude eeg:  Do Not Modify Here !
psd_bad_sensors = { 'sub-00': ['T7', 'TP9'],	 'sub-01': [],	'sub-02':[],	 'sub-03': [],	'sub-04': ['T7', 'T8'],\
             'sub-05': [],	 'sub-06': [],	'sub-07':[],	 'sub-08': [],	'sub-09': ['T7'],\
             'sub-10': [],    'sub-11': ['PO9'],   'sub-12':[],  'sub-13': [],    'sub-14': [],\
             'sub-15': ['T8'],    'sub-16': [],   'sub-17':[],  'sub-18': [],    'sub-19': ['T7', 'T8', 'TP10'],\
             'sub-20': [],    'sub-21': [],   'sub-22':[],	 'sub-23': [],    'sub-24': [],\
             'sub-25': ['Oz'],    'sub-26': [],   'sub-27':[],	 'sub-28': [],    'sub-29': [],\
             'sub-30': [],    'sub-31': [],   'sub-32':[],	 'sub-33': [],    'sub-34': [],\
             'sub-35': []	}
exclude_eeg = psd_bad_sensors

### ----------------------------ICA specs----------------------------
# number of ica components
ica_n_comp = 0.99

# threshold for peak-to-peak amplitude rejection
reject = dict(eeg=300e-6) # as measured (manually) with a peak-to-peak analysis in run_MISC

# decimate data for speed
decim = 2

### ----------------------------Epochs specs----------------------------

# epoch size
tmin, tmax = -0.2, 0.41

### ----------------------------Plot OPtions----------------------------
# Plot corrected data
plot_corr = False
# Plot blink ERP
blink_erp = False



################################################################################
# Let's start! -  1_Compute Components
################################################################################
# Here, we fit ICA for all participants- there is no plot of results
# Plots and components selection is performed just below (section: 2_Plot)

# subj_id_list -----------------------
# test on one subject
subj_id_list = np.arange(28,29) # sub-00
# applyto all
#subj_id_list = np.arange(5,36) # 0 to 35, to include sub-00 to sub-35


#
#for i_su in subj_id_list:
#    subj='sub-{:02}'.format(i_su) 
#    dir_imne = os.path.join(dir_mne, subj, 'preproc') #individual mne path
#    
#   
#    # Load epochs
#    f_epoch_filt = os.path.join(dir_imne, '{}.{}-epo.fif'.format(subj, pref_epo))
#    epochs = read_epochs(f_epoch_filt)   
#    exclude_eeg_ind = exclude_eeg[subj]
#    if exclude_eeg_ind:
#        epochs.info['bads']=exclude_eeg_ind
#        print('excluded sensors for ica: {}'.format(exclude_eeg_ind))
#
#    
#    # Fit ICA
#    print_step_label('Fit ica---  {}.....'.format(subj)) 
#    ica = ICA(n_components=ica_n_comp, method='fastica')
#    ica.fit(epochs, reject=reject, decim=decim)
#    
#    print('  Fit %d components (explaining at least %0.1f%% of the variance)' % (ica.n_components_, 100 * ica_n_comp))
#    
#    
#    # Save (this is a temporary save, since we don't reject components at this step)
#    
#    f_ica = os.path.join(dir_imne, '{}.{}_temp-ica.fif'.format(subj, pref_ica))
#    ica.save(f_ica)





#################################################################################
## 2_Select Components
#################################################################################
#Here, we plot components and select EOG ones
# Selected components should be reported in the dictionary ica_selection (see BELOW, in this section)

#plt.close('all')
#i_su=34
#subj='sub-{:02}'.format(i_su) 
#dir_imne = os.path.join(dir_mne, subj, 'preproc') #individual mne path
#f_ica = os.path.join(dir_imne, '{}.{}_temp-ica.fif'.format(subj, pref_ica))
#ica=read_ica(f_ica)
#ica.plot_components(title=subj)
##Load raw (plot epochs is really long)
#f_raw = os.path.join(dir_imne, '{}.{}.raw.fif'.format(subj, pref_raw)) 
#raw = read_raw_fif(f_raw, preload=True)
#ica.plot_sources(raw, title=subj)

#Resulting ICA selection (dictionary to fill in, after the current step2 (ICA selection))
# In most subjects, two components could be found: blk + sacc
# for some of them, "peaky-reference" could be found (sub-22), but we don't include them so far
# let's see how it goes on ERPs

ica_selection = { 'sub-00': [0,3],	 'sub-01': [1,4],	'sub-02':[0],	 'sub-03': [2],	'sub-04': [1,4],\
             'sub-05': [0],	 'sub-06': [0],	'sub-07':[0,3],	 'sub-08': [1,3],	'sub-09': [0,8],\
             'sub-10': [0,3],    'sub-11': [0,3],   'sub-12':[1,4],  'sub-13': [1,3],    'sub-14': [1,6],\
             'sub-15': [1,5],    'sub-16': [0,3],   'sub-17':[0,5],  'sub-18': [2,9],    'sub-19': [1,4],\
             'sub-20': [1,2],    'sub-21': [1,4],   'sub-22':[1,3],	 'sub-23': [0,3],    'sub-24': [1,2],\
             'sub-25': [2,4],    'sub-26': [0,2],   'sub-27':[1,5],	 'sub-28': [0,1,3],    'sub-29': [1,3],\
             'sub-30': [0,2],    'sub-31': [1,4],   'sub-32':[0,3],	 'sub-33': [0,3],    'sub-34': [0,1,3],\
             'sub-35': [1,6]	}

## --------------- Comments on results ---------------
# Subject with "reference noise component" (not selected, but if ERPs are really bad, we should consider this option)
### ---- every subject presents such a component (c0 or c1 typically)
# for some subjects,  heart can be seen (sub-21 (0,5), sub-26(1), sub-27(0) )



#################################################################################
## 3_Save
#################################################################################
# Here, we apply ICA correction on continuous 1-40 Hz data, based on the components declared in ica_selection dictionary
# Is output data fine? We control this by plotting the continuous data pre/post ica, as well as we compute blink ERPs

# applyto all


subj_id_list = np.arange(34,35) # 0 to 35, to include sub-00 to sub-35
#subj_id_list = np.array([8])

for i_su in subj_id_list:
    
    subj='sub-{:02}'.format(i_su) 
    dir_imne = os.path.join(dir_mne, subj, 'preproc') #individual mne path
    print('youpi')
    f_ica = os.path.join(dir_imne, '{}.{}_temp-ica.fif'.format(subj, pref_ica))
    ica=read_ica(f_ica)
    ica.exclude=ica_selection[subj]
    f_ica = os.path.join(dir_imne, '{}.{}-ica.fif'.format(subj, pref_ica))
    print(f'{subj} --- Save ICA with selected components: {ica.exclude}')
    ica.save(f_ica)
    
    
    # apply to continuous data
    print(f'{subj} --- Save ica-corrected continuous raw data ')
    f_raw = os.path.join(dir_imne, '{}.{}.raw.fif'.format(subj, pref_raw)) 
    raw = read_raw_fif(f_raw, preload=True)
    f_raw_out = os.path.join(dir_imne, f'{subj}.{pref_raw_out}.raw.fif') # raw continuous data: downsampled,  filtered   and ica-corrected
    raw_out = raw.copy()
    ica.apply(raw_out)
    raw_out.save(fname=f_raw_out,overwrite=True)
     
    print(f'{subj} --- Save ica-corrected epochs ....')
    f_epochs_in =os.path.join(dir_imne, '{}.{}-epo.fif'.format(subj, pref_epo))
    f_epoch_out=os.path.join(dir_imne, '{}.{}-epo.fif'.format(subj, pref_epo_out))
    epochs_in = read_epochs(f_epochs_in) 
#    epochs_out = epochs_in.copy()
#    ica.apply(epochs_out)
    epochs_out = import_epochs(subj, raw_out, epochs_in,  epochs_in.picks,  epochs_in.event_id, tmin, tmax,  plot=False)
    epochs_out.save(f_epoch_out) 
    print(f'{subj} --- DONE')
   

    if plot_corr:
#        ica_view_correction(raw, ica, scalings = dict(eeg=10e-5),plot_eeg=[],   view_start=0, view_stop=0)
        scalings = dict(eeg=10e-5)
        raw.plot(n_channels=raw.info['nchan'],remove_dc = True,  duration=30, highpass = None, scalings=scalings)
        raw_out.plot(n_channels=raw.info['nchan'],remove_dc = True,  duration=30, highpass = None, scalings=scalings)

    if blink_erp:
        ica_plot_blink_evoked(subj, raw, ica, plot_raw=False, blink_id=888,thresh=110e-6, lf=1, hf=10)

 

   
## --------------- Comments on results ---------------
### Quality of correction: Based on blink -ERPs: we assume that a good correction would give flat traces on blink-evoked responses
#   Good subjects
### ---- 00,01,02,05,06,07,09,10,11,12,13,16,17,19,22,23,25,26,27,28,29,31,33 
#   Intermediate
### ---- 15,18,20,22,24,32,34,35 (n=8)
#   Bad
### ----  03,04,08,14,21,30 (n=6)



#################################################################################
## Grenier
#################################################################################
# previous report of ICA - not to be used anymore (delete asap)
#ica_selection = { 'sub-00': [0,3],	 'sub-01': [1,7],	'sub-02':[0],	 'sub-03': [2],	'sub-04': [1,7],\
#             'sub-05': [0,4],	 'sub-06': [0,6],	'sub-07':[0,3],	 'sub-08': [1,3],	'sub-09': [0,9],\
#             'sub-10': [0,3],    'sub-11': [0,3],   'sub-12':[1,4],  'sub-13': [1,3],    'sub-14': [1,6],\
#             'sub-15': [1,4],    'sub-16': [0,3],   'sub-17':[0,5],  'sub-18': [2,10],    'sub-19': [1,4],\
#             'sub-20': [1,2],    'sub-21': [1,3],   'sub-22':[1,4],	 'sub-23': [0,3],    'sub-24': [1,2],\
#             'sub-25': [1,3],    'sub-26': [0,2],   'sub-27':[1,7],	 'sub-28': [0,3],    'sub-29': [3],\
#             'sub-30': [0,2],    'sub-31': [1,5],   'sub-32':[0,3],	 'sub-33': [0,3],    'sub-34': [0,3],\
#             'sub-35': [1,6]	}
