"""
===============================================
Import ContextOdd raw EEG data into MNE
===============================================

This script converts  raw (*.vhdr, BrainAmp) into MNE 
Outputs = *.raw.fif, *.raw-eve.fif

Raw data may have been formatted as Bids prior this step (not mandatory but advised)

"""

# Authors: Françoise Lecaignard
# Created on Tue 05 Feb 2019

###############################################################################
import numpy as np             # convention d'import
import os
import sys      # path to my own functions
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Python/EEG')
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/BIDS/mne-bids')

import mne_bids 
from mne_bids.utils import print_dir_tree
from mne.channels import read_layout, Montage, read_montage


from fun_01_COdd_eeg_import import  codd_eeg_import_raw_all

################################################################################
# Input Parameters 
################################################################################

# Paths ----------------------------
# Bids version of paths
dir_raw = '/sps/cermep/cermep/experiments/DCM/ContextOdd/Raw_Data/EEG_BIDS'
dir_mne = '/sps/cermep/cermep/experiments/DCM/ContextOdd/Ana_MNE'


# subj_id_list -----------------------
# test on one subject
subj_id_list = np.arange(24,25) # sub-00
# applyto all
#subj_id_list = np.arange(0,36) # 0 to 35, to include sub-00 to sub-35


################################################################################
# Define Constant Variables
################################################################################

# Acquisition name (bids) should be  sub-03_run-1 in most cases, but not all.
subj_run = { 'sub-00': 1,	 'sub-01': 1,	'sub-02':1,	 'sub-03': 2,	'sub-04': 4,\
             'sub-05': 1,	 'sub-06': 1,	'sub-07':1,	 'sub-08': 1,	'sub-09': 1,\
             'sub-10': 1,    'sub-11': 1,   'sub-12':1,  'sub-13': 1,    'sub-14': 1,\
             'sub-15': 1,    'sub-16': 1,   'sub-17':1,  'sub-18': 1,    'sub-19': 1,\
             'sub-20': 1,    'sub-21': 1,   'sub-22':1,	 'sub-23': 1,    'sub-24': 1,\
             'sub-25': 1,    'sub-26': 1,   'sub-27':1,	 'sub-28': 1,    'sub-29': 1,\
             'sub-30': 1,    'sub-31': 1,   'sub-32':1,	 'sub-33': 1,    'sub-34': 1,\
             'sub-35': 1	}


montage = read_montage('standard_1020')
    
# Total number of events 
nr_evt = 6068 # 4 sessions of 1517 evts (start - 1515 sounds - end)

# Plot raw data
plot=True
################################################################################
# Let's start!
################################################################################

#
# For each subject, preprocessings datafile  are in dir_mne/sub-XX/preproc/.
for i_su in subj_id_list:
    subj='sub-{:02}'.format(i_su) 
    run_id = subj_run[subj]
    print(subj)
    # create folders
    subj_path = os.path.join(dir_mne,subj, 'preproc')
    if not os.path.exists(subj_path):
        os.makedirs(subj_path)
    
    # Input raw filename  (BIDS format)
    f_raw_in = os.path.join(dir_raw, subj, 'eeg', '{}_run-{}_eeg.vhdr'.format(subj, run_id))
    
    # Output filenames
    f_raw_out = os.path.join(subj_path, '{}.raw.fif'.format(subj))
    f_eve_out = os.path.join(subj_path, '{}.raw-eve.fif'.format(subj))
    
    # Import data and events => creates *.raw.fif, *.raw-eve.fif
    codd_eeg_import_raw_all(subj, f_raw_in, f_raw_out, f_eve_out, montage,  nr_evt, plot=plot )
    print_dir_tree(subj_path)
    
    
 ################################################################################
# TEMP - This code was used while waiting for BIDS issues to be fixed
 # to recover the name of the raw eeg input file
 # Not necessary anymore
################################################################################
#    dir_raw = '/sps/cermep/cermep/experiments/DCM/ContextOdd/Raw_Data/EEG'
#    dir_mne = '/sps/cermep/cermep/experiments/DCM/ContextOdd/Ana_MNE'
#    # ----------execute the code in temp_waintingBids.py to recover datecode and namecode => this gives subj_list, subj_runs_fname, subj_datecode in the workspace
#    #-----------then
#    inv_subj_list = {v: k for k, v in subj_list.items()}
#    subj_code = inv_subj_list[i_su]
#    runlist  = subj_runs_fname[subj_code][0]
#    f_raw_in = os.path.join(dir_raw, subj_datecode[subj_code],  '{}{}.vhdr'.format(runlist[0:-6], run_id))

   