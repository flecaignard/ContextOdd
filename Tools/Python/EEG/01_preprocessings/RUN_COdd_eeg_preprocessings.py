#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
=====================================================================================
CONTEXTODD EEG - PREPROCESSINGS ANALYSIS HISTORY

Preprocessing EEG data rests on several steps described below
It starts with raw data in the acquisition system format,
And ends with filtered, ica-corrected epochs where bad events have been dropped and bad sensors identified (not dropped)

Run each step one after the other, following the commented instructions
Precisely, for each step, open the corresponding file (step_XX_*.py) and run it
======================================================================================
Created on Tue Feb 05 2019

@author: Francoise Lecaignard
"""


#############################################################################################
## Import packages          -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################

import warnings
import sys      # path to my own functions
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Python/EEG')


warnings.filterwarnings("ignore",category=DeprecationWarning)
print(__doc__)

#from step_00_COdd_eeg_convert_to_bids import step_00_COdd_eeg_convert_to_bids
#import step_01_COdd_eeg_import_into_mne
#import step_02_COdd_eeg_recode_events
#import step_03_COdd_eeg_downsample
#import step_04_COdd_eeg_epochs_raw

############################################################################################
## Paths for the present analysis.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################

# EEG data - path to the acquisition data (raw data before BIDS )
dir_raw_acq='/sps/cermep/cermep/experiments/DCM/ContextOdd/Raw_Data/EEG'
# EEG data -  path to the BIDS-acquisition data (raw data after BIDS - Step00 )
dir_raw_bids='/sps/cermep/cermep/experiments/DCM/ContextOdd/Raw_Data/EEG_BIDS'
# EEG data -  path to the data, analyzed with MNE  ( onwards Step01 )
dir_mne='/sps/cermep/cermep/experiments/DCM/ContextOdd/Ana_MNE'
# STIM data - path to the Matlab folder containing stimulus sequences (before BIDS )
dir_stim_acq='/sps/cermep/cermep/experiments/DCM/ContextOdd/StimSeq'


#############################################################################################
## STEP00 - BIDS .-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
# This steps converts raw data in native format into Bids
# Original raw data are stored in: /sps/cermep/cermep/experiments/DCM/ContextOdd/Raw_Data/EEG
# Bids-raw data (native format, *.vhdr) are created in /sps/cermep/cermep/experiments/DCM/ContextOdd/Raw_Data/EEG_BIDS

step_00_COdd_eeg_convert_to_bids 

# To date, bids-conversion is incomplete (bug to be fixed by the MNE team)

#############################################################################################
## STEP01 -  IMPORT INTO MNE.-.-.(Outputs = *.raw.fif, *.raw-eve.fif) .-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
# This steps imports raw data  into MNE
# Raw data are stored in: /sps/cermep/cermep/experiments/DCM/ContextOdd/Raw_Data/EEG_BIDS
# MNE data are created in /sps/cermep/cermep/experiments/DCM/ContextOdd/Ana_MNE
# Output files: *.raw.fif, *.raw-eve.fif

# (Because of the bids issue, data were converted from native instead of Bids)

step_01_COdd_eeg_import_into_mne

#############################################################################################
## STEP02 -  RECODE EVENTS .-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
# Original event codings rests on 4 event types: Start:16, Standard:1, Deviant:2, End:32
# These are stored in *.raw-eve.fif
# Here we adopt a five-digit coding to account for all conditions
# Output = *.5d.raw-eve.fif

# we have 4 "non-regular" subjects: 4,16,19,31 : fixed

step_02_COdd_eeg_recode_events

#############################################################################################
## STEP03 -  DOWNSAMPLE .-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
# Reduce data by downsampling from 1000Hz to 500Z. Events occurence are adjjusted.
# OUtput files: *.preproc.raw.fif, *.preproc.5d-eve.fif

step_03_COdd_eeg_downsample

#############################################################################################
## STEP04 - EPOCHS (with "early rejection"   .-.-.-.-.-.-.-.-.
#############################################################################################
# Epochs of -200 + 410 ms (continuous) with "early" rejection (rough)
# Peak-to-peak amplitude rejection (500uV) on unfiltered data to remove non-physiological noise (EEG system), and very strong muscle artifacts
# Unfiltered data : this is to avoid filtering on jumps that would really damage data
# This step also enables the identification of bad sensors
# Output: data are epoched, but stay unfiltered 
# Creates :  sub-XX/preproc/sub-XX.preproc_early-epo.fif
step_04_COdd_eeg_epochs_raw
# to fill in:
early_bad_sensors = { 'sub-00': [],	 'sub-01': [],	'sub-02':['CP5', 'P4'],	 'sub-03': [],	'sub-04': ['PO10'],\
             'sub-05': [],	 'sub-06': [],	'sub-07':[],	 'sub-08': [],	'sub-09': [],\
             'sub-10': [],    'sub-11': ['PO9'],   'sub-12':[],  'sub-13': [],    'sub-14': [],\
             'sub-15': [],    'sub-16': [],   'sub-17':[],  'sub-18': [],    'sub-19': [],\
             'sub-20': [],    'sub-21': [],   'sub-22':['T7'],	 'sub-23': ['CP1'],    'sub-24': [],\
             'sub-25': ['T8', 'FC2', 'Oz'],    'sub-26': [],   'sub-27':[],	 'sub-28': [],    'sub-29': [],\
             'sub-30': [],    'sub-31': [],   'sub-32':[],	 'sub-33': [],    'sub-34': [],\
             'sub-35': []	}


#############################################################################################
## STEP05_1 - FILTERING and EPOCHING    .-.-.-.-.-.-.-.-.
#############################################################################################
# We don't filter epochs directly (unadvised by MNE developers)
# Raw data are filtered : notch (powerline) and bandpass 1-40 Hz
# Epochs are then created, using the step04 trial rejection (*.preproc_early-epo.fif)
# Bad sensors are reported but not dropped nor interpolated (further steps below)
#
# Creates :  sub-XX/sub-XX.preproc_1-40Hz.raw.fif (continuous filtered data)
#            sub-XX/sub-XX.preproc_early_1-40Hz-epo.fif (filtered epochs, with early rejection inherited from step04)


step_05_COdd_eeg_filter

#############################################################################################
## STEP06 -POWER SPECTRAL DENSITY    .-.-.-.-.-.-.-.-.
#############################################################################################
# we plot the power spectral density of data (using continuous filtered data,sub-XX.preproc_1-40Hz.raw.fif  )
# within each session (to avoid accounting for noisy data in between two sessions)
# the aim is to identify bad sensors, and prevent them to corrupt ica
step_06_COdd_eeg_psd
# additional inspection with : scr_plot_raw

# to fill in:
psd_bad_sensors = { 'sub-00': ['T7', 'TP9'],	 'sub-01': [],	'sub-02':[],	 'sub-03': [],	'sub-04': ['T7', 'T8'],\
             'sub-05': [],	 'sub-06': [],	'sub-07':[],	 'sub-08': [],	'sub-09': ['T7'],\
             'sub-10': [],    'sub-11': ['PO9'],   'sub-12':[],  'sub-13': [],    'sub-14': [],\
             'sub-15': ['T8'],    'sub-16': [],   'sub-17':[],  'sub-18': [],    'sub-19': ['T7', 'T8', 'TP10'],\
             'sub-20': [],    'sub-21': [],   'sub-22':[],	 'sub-23': [],    'sub-24': [],\
             'sub-25': ['Oz'],    'sub-26': [],   'sub-27':[],	 'sub-28': [],    'sub-29': [],\
             'sub-30': [],    'sub-31': [],   'sub-32':[],	 'sub-33': [],    'sub-34': [],\
             'sub-35': []	}

# peaky sensors : non-physiological artefacts, mostly seen at occipital sensors
# ex= sub-25 electrode Oz (use scr_plot_raw to see this)
# we don't seek peaky sensors here: if some are really contaminated (leading to sensor drop) then these peaks affect PSD and 
# have been detected using PSD (ex: sub-25: Oz)
# if only few peaks, confined in bad segments, then we include the corresponding sensors in  the ICA

#############################################################################################
## STEP06 -ICA on EPOCHS    .-.-.-.-.-.-.-.-.
#############################################################################################
#ICA is performed on 1-40Hz good epochs (good = surviving early rejection in step04 )
# Bad sensors from PSD inspection (step 06) are excluded for ICA fit
# output :  sub-XX.preproc_1-40Hz_temp-ica.fif : output of ICA fit 
#           sub-XX.preproc_1-40Hz-ica.fif  : output of ICA fit with bad components identified
#           preproc/sub-xx_preproc_1-40Hz_ica.raw.fif  : corrected raw file
step_07_COdd_eeg_ica



#############################################################################################
## STEP08 -Reject of bad 1-40 Hz epochs     .-.-.-.-.-.-.-.-.
#############################################################################################
# Here, several approaches have been considered
#   typical peak-to-peak (p2p) rejection, AutoReject (AR), AR followed by p2p
#   finally, we keep the typical p2p rejection (AR varies a lot depending on the subset of learning epochs, and what sensors are excluded => possibly due to our choice for non-interpolation)
#
# Below: notes  regarding AR
#         We use the AutoReject toolbox (without signal interpolation)
#         Without interpolation enabled, Autoreject (AR) relies on thresholds (per channel) and a consensus (kappa) = the minimum nr of bad sensors for a particular
#         =>>> An epoch should be above threshold (p2p) over at least kappa sensors to be declared as bad
#         Basically, AR model is fitted on 300 random epochs (equally distributed across main conditions)
#         and then applied on all epochs
#        
#         It turns out that this approach works really good for the majority of artifacts, but is conservative for single peaky noise (possibly not physio.)
#         This is due to 1) kappa (an artifact present at one isolated sensor can not trigger rejection of the corresponding epoch alone) 
#                        combined with 
#                        2) we don't interpolate bad data
#         This issue is not accounted for in MNE  because MNE promotes interpolation (and we don't) 
#         To fix this, we add (after AR) an extra-rejection step, based on 100 uV threshold- 
#         Note that kappa and threshold are not estimated together: tests with kappa fixed to 1 sensor did not impact thresholds (we expected an increase but values stay the same)
#
# There are two possible scripts 
#       step_08_COdd_eeg_autoreject_and_thresh.py : launch AR prior to p2p
#       step_08_COdd_eeg_p2p_thresh.py  : typical p2p ===>selected in the present analysis
#
#
# output :   sub-XX.AutoReject_training_epochs300.pkl containing sampled epoched (--------- > if AR)
#            sub-XX.AutoReject_output300.pkl containing the reject_log object (including bad epochs)  (--------- > if AR)
#            sub-XX.bad_epochs_AR_and_p2p.pkl  pickle file containing bad_epochs (boolean vector) total, AR and p2p
#            sub-XX.good_bad_events.pkl pickle file to plot raw data with good and dropped events (and additional summarizing info regarding rejection)
#            sub-XX.clean_1-40Hz-epo.fif : cleaned epochs (ica + rejection) ready for ERPs!!!


# if p2p (selected):
step_08_COdd_eeg_p2p_thresh.py 
# if AR and p2p:
# step_08_COdd_eeg_autoreject_and_thresh.py 

# to vizualize rejection outputs (are there remaining artefacts?)
# this is also a way to control that peaky artifacts are dropped

scr_plot_raw_good_bad_events

# want to understand what happens at a particular latency?
scr_util_raw_p2p_at_at_latency


## we also try to detect peaky sensors: sharp artifacts, mostly affecting occipital sensors
#scr_plot_raw
## to fill in:
#peaky_sensors = { 'sub-00': [],	 'sub-01': [],	'sub-02':[],	 'sub-03': [],	'sub-04': [],\
#             'sub-05': [],	 'sub-06': [],	'sub-07':[],	 'sub-08': [],	'sub-09': [],\
#             'sub-10': [],    'sub-11': [],   'sub-12':[],  'sub-13': [],    'sub-14': [],\
#             'sub-15': [],    'sub-16': [],   'sub-17':[],  'sub-18': [],    'sub-19': [],\
#             'sub-20': [],    'sub-21': [],   'sub-22':[],	 'sub-23': [],    'sub-24': [],\
#             'sub-25': [],    'sub-26': [],   'sub-27':[],	 'sub-28': [],    'sub-29': [],\
#             'sub-30': [],    'sub-31': [],   'sub-32':[],	 'sub-33': [],    'sub-34': [],\
#             'sub-35': []	}

#############################################################################################
## STEP08 -This is it !!! we summarize everything     .-.-.-.-.-.-.-.-.
#############################################################################################
# This step summarizes the total number of events, accepted , dropped
step_09_COdd_eeg_summarize_preproc.py



#############################################################################################
## STEP09 -We compute 2-20 Hz epochs for the ERP analysis     .-.-.-.-.-.-.-.-.
#############################################################################################
# Creates :  sub-XX/erp_2-20Hz/sub-XX.erp_2-20Hz-epo.fif
#
#step_09_COdd_eeg_erp_epochs_2-20Hz.py
## =====> outputs : in erp_2-20Hz folder, sub-xx.clean_2-20Hz.raw.fif
##                                        sub-xx.clean_2-20Hz-epo.fif
#
## alternative to compute long epochs, overlapping multiple stimuli
## "double" erps
#step_09_bis_COdd_eeg_erp_double_epochs_2-20Hz.py
## =====> outputs : in erp_2-20Hz folder, sub-xx.double_erp_clean_2-20Hz-epo.fif
#
