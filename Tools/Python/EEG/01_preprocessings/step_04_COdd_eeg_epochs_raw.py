#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
===============================================
Step 04 - Epoching on raw data
===============================================
We do the epoching on raw continuous unfiltered data
The aim here is to reject strong artifacts that could compromise filtering
Like squid jumps in MEG, on other non-physiologocal noise in EEG
Data is unfiltered, and we use a large threshold- It will suppress very strong muscle episodes that would have been rejected afterwards
This step also enables to detect bad sensors
We refer to this rejection as "early rejection" by contrast to step_07 which will rest on finer peak-to-peak threshold on ica-corrected data
OUtput files : preproc/sub-xx.preproc_early-epo.fif


Created on Mon Feb 12 17:57:17 2019

@author: Françoise Lecaignard, francoise.lecaignard@inserm.fr
"""

# Because viewing the four-session raw data is very slow, we plot the epochs only in a few subjects to make sure
# that threshold is fine, we reject jumps and  jump-like noise, while keeping the signal that can be cleaned with filtering
# Individual reports of dropped percentahe should be noted below. 

# Epochs have been applied: no detrend, no baseline 

# Examples of jump-like noises in EEG
# sub-29 at latency 3627.02 -> 3627.08: it lasts 60 ms , and p2p>450 uV
# sub-05, t=2408.36
 
###############################################################################
import numpy as np             # convention d'import
import os
import sys      # path to my own functions
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Python/EEG')
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/BIDS/mne-bids')
import matplotlib.pyplot as plt # from the MNE examples

from mne.io import read_raw_fif
from mne import Epochs, pick_types, read_events
from mne import get_config, set_config
type(get_config('MNE_BROWSE_RAW_SIZE'))
set_config('MNE_BROWSE_RAW_SIZE', '20.0,10.0')



from fun_eeg_misc import  print_step_label, epoch_rej_and_plot,peak_to_peak_raw

################################################################################
# Input Parameters 
################################################################################

# Paths ----------------------------
dir_mne = '/sps/cermep/cermep/experiments/DCM/ContextOdd/Ana_MNE'



################################################################################
# Define Constant Variables
################################################################################

# prefix output *-epo.fif file
pref_epo_out = 'preproc_early-epo'
# events for epochs
#Stimulus coding (five-digit format)
event_id_codd={'var_c/c4/c-/p0/std': 14121, 'var_c/c4/c-/p0/dev': 14122,'var_c/c4/c-/p0/std_oth': 14123,\
              'var_c/c5/c-/p0/std': 15121, 'var_c/c5/c-/p0/dev': 15122,'var_c/c5/c-/p0/std_oth': 15123,\
              'var_c/c6/c-/p0/std': 16121, 'var_c/c6/c-/p0/dev': 16122,'var_c/c6/c-/p0/std_oth': 16123,\
              'var_c/c2/c+/p0/std': 12221, 'var_c/c2/c+/p0/dev': 12222,'var_c/c2/c+/p0/std_oth': 12223,\
              'var_c/c3/c+/p0/std': 13221, 'var_c/c3/c+/p0/dev': 13222,'var_c/c3/c+/p0/std_oth': 13223,\
              'var_c/c4/c+/p0/std': 14221, 'var_c/c4/c+/p0/dev': 14222,'var_c/c4/c+/p0/std_oth': 14223,\
              'var_c/c6/c+/p0/std': 16221, 'var_c/c6/c+/p0/dev': 16222,'var_c/c6/c+/p0/std_oth': 16223,\
              'var_c/c7/c+/p0/std': 17221, 'var_c/c7/c+/p0/dev': 17222,'var_c/c7/c+/p0/std_oth': 17223,\
              'var_c/c8/c+/p0/std': 18221, 'var_c/c8/c+/p0/dev': 18222,'var_c/c8/c+/p0/std_oth': 18223,\
              'var_p/c2/c-/p+/std': 22111, 'var_p/c2/c-/p+/dev': 22112,'var_p/c2/c-/p+/std_oth': 22113,\
              'var_p/c3/c-/p+/std': 23111, 'var_p/c3/c-/p+/dev': 23112,'var_p/c3/c-/p+/std_oth': 23113,\
              'var_p/c4/c-/p+/std': 24111, 'var_p/c4/c-/p+/dev': 24112,'var_p/c4/c-/p+/std_oth': 24113,\
              'var_p/c4/c-/p0/std': 24121, 'var_p/c4/c-/p0/dev': 24122,'var_p/c4/c-/p0/std_oth': 24123,\
              'var_p/c5/c-/p0/std': 25121, 'var_p/c5/c-/p0/dev': 25122,'var_p/c5/c-/p0/std_oth': 25123,\
              'var_p/c6/c-/p0/std': 26121, 'var_p/c6/c-/p0/dev': 26122,'var_p/c6/c-/p0/std_oth': 26123,\
              'var_p/c6/c-/p-/std': 26131, 'var_p/c6/c-/p-/dev': 26132,'var_p/c6/c-/p-/std_oth': 26133,\
              'var_p/c7/c-/p-/std': 27131, 'var_p/c7/c-/p-/dev': 27132,'var_p/c7/c-/p-/std_oth': 27133,\
              'var_p/c8/c-/p-/std': 28131, 'var_p/c8/c-/p-/dev': 28132,'var_p/c8/c-/p-/std_oth': 28133                  }



eve_name = 'preproc.5d-eve'

# epoch size
tmin, tmax = -0.2, 0.41

# filter epochs
#lf, hf = 1, 40 #Hz
lf, hf = None, None #Hz

# epoch rejection on threshold (peak-2-peak)
rej_thresh = 500e-6 #uV

# exclude eeg
# for each subject, a subset of sensors can be specified to do the rejection on all sensors but these one (because they are bad)
exclude_eeg = { 'sub-00': [],	 'sub-01': [],	'sub-02':['CP5', 'P4'],	 'sub-03': [],	'sub-04': ['PO10'],\
             'sub-05': [],	 'sub-06': [],	'sub-07':[],	 'sub-08': [],	'sub-09': [],\
             'sub-10': [],    'sub-11': ['PO9'],   'sub-12':[],  'sub-13': [],    'sub-14': [],\
             'sub-15': [],    'sub-16': [],   'sub-17':[],  'sub-18': [],    'sub-19': [],\
             'sub-20': [],    'sub-21': [],   'sub-22':['T7'],	 'sub-23': ['CP1'],    'sub-24': [],\
             'sub-25': ['T8', 'FC2', 'Oz'],    'sub-26': [],   'sub-27':[],	 'sub-28': [],    'sub-29': [],\
             'sub-30': [],    'sub-31': [],   'sub-32':[],	 'sub-33': [],    'sub-34': [],\
             'sub-35': []	}

# Warning: if there are more than 2 excluded sensors, then plot_epochs fails (for some reason in RejectLog that I don't understand)

# Several subjects show weird , and tight peaks , mostly occipital, in Oz and others
# these peaks are removed with thresh-epoching
# sub-28, sub-22 , sub-16: peaks over several sensors
# for particpants where it happens a lot, we set Oz as bad: sub-25, 

################################################################################
# Individual Reports
################################################################################
# Number of dropped epoched for each subject (out of 6048) 
dropped_epochs = { 'sub-00': 21,	 'sub-01': 76,	'sub-02':412,	 'sub-03': 162,	'sub-04': 291,\
             'sub-05': 39,	 'sub-06': 303,	'sub-07':156,	 'sub-08': 52,	'sub-09': 50,\
             'sub-10': 6,    'sub-11': 50,   'sub-12':6,  'sub-13': 344,    'sub-14': 166,\
             'sub-15': 31,    'sub-16': 509,   'sub-17':8,  'sub-18': 82,    'sub-19': 58,\
             'sub-20': 45,    'sub-21': 95,   'sub-22':593,	 'sub-23': 382,    'sub-24': 6,\
             'sub-25': 52,    'sub-26': 35,   'sub-27':87,	 'sub-28': 431,    'sub-29': 165,\
             'sub-30': 229,    'sub-31': 304,   'sub-32':390,	 'sub-33': 155,    'sub-34': 361,\
             'sub-35': 212	}

# Plot epochs
plot_raw=False
plot_epochs=False
verbose=False
################################################################################
# Let's start!
################################################################################

# subj_id_list -----------------------
# test on one subject
subj_id_list = np.arange(0,36) # sub-00
# applyto all
#subj_id_list = np.arange(1,36) # 0 to 35, to include sub-00 to sub-35


#
for i_su in subj_id_list:
    
    subj='sub-{:02}'.format(i_su) 
    dir_imne = os.path.join(dir_mne, subj, 'preproc') #individual mne path
    
    #load data and events
    f_raw = os.path.join(dir_imne, '{}.preproc.raw.fif'.format(subj)) 
    f_events = os.path.join(dir_imne, '{}.{}.fif'.format(subj, eve_name)) 
    raw = read_raw_fif(f_raw, preload=False) 
    events=read_events(f_events)
    
    if plot_raw:
        scal = dict(eeg=10e-5)
        raw.plot( n_channels=raw.info['nchan'],remove_dc = True,  duration=30, highpass = None, scalings=scal)
    
    picks = pick_types(raw.info, eeg=True, stim=True,exclude=())
    reject=dict(eeg=rej_thresh)
    exclude_eeg_ind = exclude_eeg[subj]
    print_step_label('Epoching---  {}.....({} uV)'.format(subj, rej_thresh*1e6))  
    
    if exclude_eeg_ind:
        raw.info['bads']=exclude_eeg_ind
        print('excluded sensors for rejection: {}'.format(exclude_eeg_ind))
    
    thresh_dict = dict(eeg=rej_thresh)
    epochs_rej = epoch_rej_and_plot(subj, raw, events, picks, thresh_dict, event_id_codd, tmin, tmax, plot=plot_epochs, verbose=verbose)
    
    # save
    #f_epoch_out=os.path.join(dir_mne, '{}.preproc_{}-{}Hz-epo.fif'.format(subj, lf, hf))
    f_epoch_out=os.path.join(dir_imne, '{}.{}.fif'.format(subj, pref_epo_out))
    epochs_rej.save(f_epoch_out)    


#################################################################################
# Tests
################################################################################
    

## test on raw data, a peak-to-peak value at a particular latency
#lat = 1296.15 # sec
#pre_lat = -0.1
#post_lat = 0.1
#peak_to_peak_raw(raw, lat, pre_lat, post_lat, picks, thresh=None, plot=False)
#

##plot raw data
#scal = dict(eeg=10e-5)
#raw.plot( n_channels=raw.info['nchan'],remove_dc = True,  duration=30, highpass = None, scalings=scal)
