#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
===============================================
FUNCTIONS for CONTEXTODD EEG - STEP 1 analysis
===============================================
=> 01: Import into MNE

@author: Francoise Lecaignard
"""


#############################################################################################
## Import packages          -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
# import argparse               # add input arguments

import numpy as np             # convention d'import
import os                       # path.join for file names
import matplotlib.pyplot as plt # from the MNE examples
import warnings

from mne import find_events, events_from_annotations, write_events, read_events
from mne.io import read_raw_brainvision, read_raw_fif
from mne.viz import plot_events

print(__doc__)

from mne import get_config, set_config
type(get_config('MNE_BROWSE_RAW_SIZE'))
set_config('MNE_BROWSE_RAW_SIZE', '20.0,10.0')




import sys      # path to my own functions
sys.path.append('/Users/francoise/Documents/Projets/ContextOdd/ContextOdd_Git/Tools/Python/EEG')


warnings.filterwarnings("ignore",category=DeprecationWarning)



#from fun_eeg_misc import print_step_label, peak_to_peak_raw, plot_good_and_bad_epochs, count_events
#from fun_eeg_misc import  epoch_rejection_adaptive_threshold, epoch_rejection_explore_threshold



#####################################################################################################################
#############################################     FUNCTIONS      ####################################################
#####################################################################################################################


def codd_eeg_import_raw_all(subj, f_raw_in, f_raw_out, f_eve_out, montage,  nr_evt, plot=True ):
    
    # Import raw data: creates *.raw.fif
    codd_eeg_import_raw_data(f_raw_in, f_raw_out, f_eve_out,  montage ) 
    
    # Import raw events: creates *.raw-eve.fif
    raw = read_raw_fif(f_raw_out, preload=True)
    events = codd_eeg_import_raw_event(raw, f_eve_out)
    # Control nr_evt is fine
    if len(events) != nr_evt:
        print('--------------> TROUBLE WITH {}: {} events found instead of {}\n'.format(subj, len(events), nr_evt))
    else:
        print('--------------> YOUPI WITH {}\n'.format(subj))
    ### PLOT
    if plot:
        codd_eeg_import_view(f_raw_out, f_eve_out, subj)

 


def codd_eeg_import_raw_data(f_raw_in, f_raw_out, f_eve_out,  montage ):
    
    if os.path.isfile(f_raw_in) :
        print('Found raw data file: ok')
    else:
        print('Raw data file not found- abandon', f_raw_in)
        return
    if os.path.isfile(f_raw_out):
        print('MNE raw file already exists- abandon', f_raw_out)
        return
    else:
        raw = read_raw_brainvision(f_raw_in, preload=True )
        raw.set_montage(montage, set_dig=True, verbose=None)
        raw.save(f_raw_out, overwrite=True)
        print('-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-')
        print('Save Raw Data : Done')
        return raw



def codd_eeg_import_raw_event(raw, f_eve_out):
    # extract events from raw.fif 
    events = find_events(raw, shortest_event=2, stim_channel='STI 014', output='onset')
    #events, event_id = events_from_annotations(raw, event_id=event_id) #, regexp=None, use_rounding=True, chunk_duration=None, verbose=None)
    write_events(f_eve_out,events)
    print('Save Raw Events : Done')
    return events
   
def codd_eeg_import_view(f_raw, f_eve, subj):
    raw = read_raw_fif(f_raw, preload=True)
    events =read_events(f_eve)
    dict_color_event = {16: 'red', 1: 'black', 2: 'magenta', 32: 'red'}
    scal = dict(eeg=10e-5)
    # plot sensors (top view)
    raw.plot_sensors(show_names=True)
    # plot eeg data with events
    raw.plot(events=events,  event_color = dict_color_event, n_channels=raw.info['nchan'],remove_dc = True,  duration=30, highpass = None, scalings=scal) #, show_options=False)
    # plot events
    event_id_raw = {'std': 1, 'dev': 2, 'start': 16, 'stop': 32}
    color_id_raw = {1: 'blue', 2: 'green', 16: 'red', 32: 'c'}
    plot_events(events, color=color_id_raw, event_id=event_id_raw)
    plt.title(' {} : raw events'.format(subj))
    plt.show()


#def codd_eeg_view_raw(dir_mne, subj_id, pref,highpass = None, lowpass = None ):
#    # pref : subj_id.pref.raw.fif
#    # defaut is no filter
#    f_raw = os.path.join(dir_mne, '{}.{}.raw.fif'.format(subj_id, pref))
#    raw = read_raw_fif(f_raw, preload=False)
#    scal = dict(eeg=10e-5)
#    raw.plot(n_channels=raw.info['nchan'],remove_dc = True,  duration=30, highpass = highpass, lowpass = lowpass,  scalings=scal) #, show_options=False)
#    plt.show()




#############################################################################################
## -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.    TEST and DEBUG     --.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
if __name__== '__main__':
    
    #############################################################################################
    ## Init Paths              -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
    #############################################################################################
    
    # mac_fl
    #dir_mne = '/Users/francoise/Documents/Projets/ContextOdd/Data/EEG/MNE_DevData/MNE_Data'
    #dir_raw_bids = '/Users/francoise/Documents/Projets/ContextOdd/ContextOdd_Git/Experimental_Design/StimSeq' # path to the StimSeq folder
    #dir_eventcodes = '/Users/francoise/Documents/Projets/ContextOdd/ContextOdd_Git/Experimental_Design/StimSeq' # path to the StimSeq folder
    #dir_ref = '/Users/francoise/Documents/Projets/ContextOdd/ContextOdd_Git/Reference_Files'
    
    # in2p3
    dir_mne='/sps/inter/crnl/users/pduret/ContextOdd/ContextOdd_Data/eMNE'
    dir_raw_bids = '/sps/inter/crnl/users/pduret/ContextOdd...'
    dir_eventcodes='/sps/inter/crnl/users/pduret/ContextOdd/ContextOdd_Git/Experimental_Design/StimSeq' # path to the StimSeq folder
    dir_ref='/sps/inter/crnl/users/pduret/ContextOdd/ContextOdd_Git/Reference_Files'
    

    #############################################################################################
    ## Init Variables              -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
    #############################################################################################

    # Apply to all  ---------------------------------------------------------------------------
#    for subj_code, subj_id in subj_list.items():
#        codd_eeg_import_raw_all(dir_raw_bids, dir_mne, subjlist_file, subj_code, nr_evt, plot=True )
        
    ## DEBUG Step 1 ########################################################################
#    subj_code='EDDAR'
#    montage = read_montage('standard_1020')
#    nr_evt = 6068 # should be the case : 4 sessions of 1517 evts (start - 1515 sounds - end)
#    codd_eeg_import_raw_all(dir_raw_bids, dir_mne, montage, subjlist_file, subj_code, nr_evt, plot=True )
#    
