#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
===============================================
Step 09 - Summarize preprocessings info
===============================================

Preprocessings have been achieved in step00 to step08
We end up with 

    - *.preproc_early_1-40Hz-epo.fif      => 1-40 Hz epochs (-200 +410 ms), with early rejection (big jump-like artifacts)
    - *.preproc_early_1-40Hz_ica-epo.fif  => 1-40 Hz epochs (-200 +410 ms), with early rejection and ICA correction
    - *.clean_1-40Hz-epo.fif              => 1-40 Hz epochs (-200 +410 ms), with early and p2p rejection and ICA correction => ready for ERPs!!!

    - *.bad_epochs_summary.pkl              => bad epochs vector (relative to early rejection)
    - *.good_bad_events.pkl                 => summary of dropped epochs and threshes specs

Here, we summarize the global rejection (% of ok and dropped epochs), as well as recap the bad sensors identified in early rejection (step04) 
We also plot typical ERPs on either 1-40 Hz epochs, or 2-20 Hz (for the latter, step09 should be processed first !)

Output =========> NONE

Created on Mon Mar 11 2019

@author: Françoise Lecaignard, francoise.lecaignard@inserm.fr
"""


###############################################################################
import numpy as np             # convention d'import
import os                       # path.join for file names
import matplotlib.pyplot as plt # from the MNE examples
import warnings
import pickle
import sys
# path to my own functions
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Python/EEG')
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/BIDS/mne-bids')
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/toolboxes/autoreject')


from autoreject import compute_thresholds, get_rejection_threshold
from autoreject import set_matplotlib_defaults  # noqa
set_matplotlib_defaults(plt)
from autoreject import AutoReject

from mne import Epochs, pick_types, read_events, read_epochs
from mne import get_config, set_config
type(get_config('MNE_BROWSE_RAW_SIZE'))
set_config('MNE_BROWSE_RAW_SIZE', '20.0,10.0')
from mne.preprocessing import read_ica
from mne.viz import plot_evoked_topo
#from mne.viz import plot

#from fun_NN_template import  ...
################################################################################
# Input Parameters 
################################################################################

# Paths ----------------------------
dir_mne = '/sps/cermep/cermep/experiments/DCM/ContextOdd/Ana_MNE'

# ----------- continuous data folder
folder = 'preproc'
# -----------   events file (pickle file)
eve_name = 'good_bad_events'



################################################################################
# Let's start!
################################################################################

# subj_id_list -----------------------
# test on one subject
subj_id_list = np.arange(0,36) # sub-00
# applyto all
#subj_id_list = np.arange(1,36) # 0 to 35, to include sub-00 to sub-35


#
for i_su in subj_id_list:
    
    subj='sub-{:02}'.format(i_su) 
    dir_imne_preproc = os.path.join(dir_mne, subj, 'preproc') #individual mne path
    dir_imne_raw = os.path.join(dir_mne, subj, folder) #individual mne path
    
   
    #### 2 ----- load good/bad events

    f_events_pkl = os.path.join(dir_imne_raw, f'{subj}.{eve_name}.pkl') ### created in step_08 summarize
    with  open(f_events_pkl, 'rb') as input:
        a = pickle.load(input)
    events_ok = a[0]
    sens_exclude = a[1]
    nr_drop = a[2]
    nr_drop_p2p = a[3]
    nr_drop_ar = a[4]
    bad_epochs = a[5]
    event_good_id = a[6]
    dict_contrib = a[7]
    thresh = a[8]


    #### 4 ----- print useful info
    nr_epo = 6068
    print(f'-------------------------{subj}--------------------------------------------')
    print(f'=============> AutoReject         : dropped {nr_drop_ar} epochs <=> {np.round(100*nr_drop_ar/nr_epo)} pc')
    print(f'=============> P2P (post AR)      : dropped {nr_drop_p2p} epochs <=> {np.round(100*nr_drop_p2p/nr_epo)} pc')
    print(f'=============> TOTAL              : dropped {nr_drop} epochs <=> {np.round(100*nr_drop/nr_epo)} pc')
    print(f'threshold = {thresh*1e6} uV')
    

   




    
    