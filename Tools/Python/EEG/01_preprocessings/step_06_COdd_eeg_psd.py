#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
==================================================================================
Step 06 - Before ICA, we control power spectral density
==================================================================================

We plot for each session the PSD for each subject
The aim is to quickly check that data , sensors  looks fine across subjects 
Output Files =======> NONE
But bad sensors should be reported
@author: Françoise Lecaignard, francoise.lecaignard@inserm.fr
"""



###############################################################################
import numpy as np             # convention d'import
import os
import matplotlib.pyplot as plt # from the MNE examples
import sys      # path to my own functions
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Python/EEG')
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Python/EEG/01_preprocessings')

from mne.io import read_raw_fif
from mne import Epochs, pick_types, read_events, read_epochs
from mne import get_config, set_config
type(get_config('MNE_BROWSE_RAW_SIZE'))
set_config('MNE_BROWSE_RAW_SIZE', '20.0,10.0')
from mne.viz import  tight_layout
#from fun_NN_template import  ...
from fun_util_eeg import  print_step_label,import_epochs

################################################################################
# Input Parameters 
################################################################################

# Paths ----------------------------
dir_mne = '/sps/cermep/cermep/experiments/DCM/ContextOdd/Ana_MNE'

# ----------- data bandwidth
lf, hf = 1,40
# ----------- continuous data folder
folder = 'preproc'
# ----------- prefix  of  continuous data 
pref_raw = f'preproc_{lf}-{hf}Hz'


# -----------  orig events file (without any rejection), downsampled
eve_name = 'preproc.5d-eve'
# -----------  SOA in seconds (to separate sessions from continuous recordings)
soa=0.61 # 610ms


# -----------  PSD bandwidth
psd_min, psd_max = 1, 45



################################################################################
# Let's start!
################################################################################

# subj_id_list -----------------------
# test on one subject
subj_id_list = np.arange(35,36) # sub-00
# applyto all
#subj_id_list = np.arange(29,30) # 0 to 35, to include sub-00 to sub-35

per_session = True
#
for i_su in subj_id_list:
    subj='sub-{:02}'.format(i_su) 
    dir_imne_preproc = os.path.join(dir_mne, subj, 'preproc') #individual mne path
    dir_imne_raw = os.path.join(dir_mne, subj, folder) #individual mne path
    
    #### 1 ----- Create clean continuous 1-40 Hz (*.clean_1-40Hz.raw.fif)
    f_raw = os.path.join(dir_imne_preproc, f'{subj}.{pref_raw}.raw.fif') #  continuous data
    raw = read_raw_fif(f_raw, preload=True)

    
    #### 2 ----  Recover latencies of start-end events to process PSD per sessions
    
    fs = raw.info['sfreq']
    
    f_events = os.path.join(dir_imne_preproc,  '{}.{}.fif'.format(subj,eve_name ))
    orig_events =read_events(f_events) # downsampled
    lat_all = orig_events[:,0] / fs
    
    if per_session:
        tmin_all=np.array([])
        tmax_all=np.array([])
        
        
        diff_lat = lat_all[1:] - lat_all[:-1] # diff btw successive events
        jump_ind = np.where(diff_lat>= 10*soa)[0]
        
        tmin_all=np.append(tmin_all, lat_all[0])
        for idx  in jump_ind:
            tmax_all = np.append(tmax_all, lat_all[idx-1])
            tmin_all = np.append(tmin_all, lat_all[idx])
        tmax_all = np.append(tmax_all, lat_all[-1])
        
        #### 3 ----  Plot PSD per sessions
        for i_sess in np.arange(len(tmax_all)):
            tmin = tmin_all[i_sess]
            tmax = tmax_all[i_sess]
            print(f'Session {i_sess} : [{tmin} , {tmax}] sec')
            raw.plot_psd_topo(tmin=tmin, tmax=tmax, fmin=psd_min, fmax=psd_max, n_fft=2048)
            
    else: # Plot PSD for all sessions              
        tmin=lat_all[0]
        tmax = lat_all[-1]
        print(f'All Sessions : [{tmin} , {tmax}] sec')
        raw.plot_psd_topo(tmin=tmin, tmax=tmax, fmin=0, fmax=45, n_fft=2048)
        
    

