#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import os
import os.path as op
import re
from collections import defaultdict

import shutil as sh


import sys      # path to my own functions
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Python/EEG')
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/BIDS/mne-bids')


from mne_bids.utils import print_dir_tree

from fun_00_COdd_eeg import  codd_eeg_bids

################################################################################
# Get input parameters 
################################################################################

# Paths     ----------------
dir_raw_acq = '/sps/cermep/cermep/experiments/DCM/ContextOdd/Raw_Data/EEG'
dir_raw_bids = '/sps/cermep/cermep/experiments/DCM/ContextOdd/Raw_Data/EEG_BIDS'

# Subj_List ----------------
subj_list = {'NAVMA': 0,	 'POSRI': 1,	 'BOULA':2,	 'BREME': 3,	   'VENAN':	4,\
             'BIGOP': 5,	 'LANJU': 6,	 'LOQJE':7,	 'PICJO': 8,	   'EDDAR':	9,\
             'ADOBE': 10,'THIMA': 11,'BOUAU':12, 'MASLO': 13,  'ANTRA':14,\
             'DURMA': 15,'BERAM': 16,'ALLMA':17, 'VIAMA': 18,  'BARTH':19,\
             'COULE': 20,'CHMMI': 21,'MIGAN':22,	 'PEGAR': 23,  'MERAR':24,\
             'SANLU': 25,'ESTYO': 26,'FERMA':27,	 'ADANI': 28,  'COTCL':29,\
             'ANDNI': 30,'SOUMA': 31,'KOCPI':32,	 'TALCA': 33,  'MERAM':34,\
             'LEDGA': 35	}


subj_datecode = dict()  #  EEG folder of the form 20180503_ANTRA
subj_runs_id = defaultdict(list) # ids of run(s) for each subject
subj_runs_fname = defaultdict(list) # e.g.: ANTRA__0001.vhdr


folder_list = os.listdir(dir_raw_acq)

# identify participant folders (one per subject), among all folders in dir_raw_acq
# BrainAmp folder are formatted: e.g.: 20180524_SOUMA
for folder in folder_list: 
    eegfolder = re.match( r'[0-9]{8}_(.*)', folder) 
    if eegfolder:       #obey BrainAmp format
        #recover datecode and subj_code
        subj_datecode[eegfolder[1]] = (f'{eegfolder[0]}')

# Identify run(s) for each subject in subj_list

for subj_code, subj_id in subj_list.items():
    print(subj_code)    
    print(subj_list[subj_code],subj_datecode[subj_code] ) 
    
    file_list = os.listdir(op.join(dir_raw_acq, subj_datecode[subj_code]))
    for file in file_list:
        eegfile = re.match( r'(.*)\.vhdr', file) 
        if eegfile:       #one or many vhdr files
            subj_runs_id[subj_code].append(file[-6]) 
            subj_runs_fname[subj_code].append(file) 

    
