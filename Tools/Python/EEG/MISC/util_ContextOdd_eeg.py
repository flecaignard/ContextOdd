#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
===============================================
CONTEXTODD EEG - UTIL SCRIPTS
===============================================


@author: Francoise Lecaignard
Created on Thu Feb 14 17:09:27 2019

"""
#############################################################################################
## Import packages          -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
import numpy as np             # convention d'import
import os                       # path.join for file names
import matplotlib.pyplot as plt # from the MNE examples
import warnings

print(__doc__)

import mne
from mne import get_config, set_config
type(get_config('MNE_BROWSE_RAW_SIZE'))
set_config('MNE_BROWSE_RAW_SIZE', '20.0,10.0')


import sys      # path to my own functions
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Python/EEG')


warnings.filterwarnings("ignore",category=DeprecationWarning)

from fun_eeg_misc import peak_to_peak_raw
from mne.io import read_raw_fif
from mne import pick_types



 epoch = mne.Epochs(raw, events, events_id, tmin, tmax, proj=True,
                       baseline=None,
                       preload=False, reject=None, decim=4)
    epochs.append(epoch)

    # Same `dev_head_t` for all runs so that we can concatenate them.
    epoch.info['dev_head_t'] = epochs[0].info['dev_head_t']


epochs = mne.epochs.concatenate_epochs(epochs)

#############################################################################################
## Get a peak-to-peak value within an interval on raw data   -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
dir_mne = '/sps/cermep/cermep/experiments/DCM/ContextOdd/Ana_MNE'
subj = 'sub-29'
dir_imne = os.path.join(dir_mne, subj, 'preproc') #individual mne path
f_raw = os.path.join(dir_imne, '{}.preproc.raw.fif'.format(subj))

raw = read_raw_fif(f_raw, preload=True)
#raw.filter(1,40)
picks = pick_types(raw.info, eeg=True, stim=False,exclude=())

lat = 3627.05 #3618.65 # #3627 # seconds
pre_lat = -0.03 # sec
post_lat = 0.03
peak_to_peak_raw(raw, lat, pre_lat, post_lat, picks, thresh=None, plot=True)