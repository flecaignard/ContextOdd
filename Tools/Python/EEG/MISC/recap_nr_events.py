#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Recap the number of events for each conditions, as designed in the experimental setup
(This is not data-dependant)
Created on Fri Mar 15 09:45:30 2019

@author: lecaigna
"""
import numpy as np             # convention d'import
import os                       # path.join for file names
import matplotlib.pyplot as plt # from the MNE examples
import sys
# path to my own functions

from mne import Epochs, pick_types, read_events, read_epochs
from mne import get_config, set_config
type(get_config('MNE_BROWSE_RAW_SIZE'))
set_config('MNE_BROWSE_RAW_SIZE', '20.0,10.0')
from mne.preprocessing import read_ica
from mne.viz import plot_evoked_topo
#from mne.viz import plot

#from fun_NN_template import  ...
from fun_eeg_misc import  count_events


################################################################################
# Input Parameters 
################################################################################

dir_mne = '/sps/cermep/cermep/experiments/DCM/ContextOdd/Ana_MNE'
eve_name = 'preproc.5d-eve'
i_su = 0
subj='sub-{:02}'.format(i_su) 
dir_imne = os.path.join(dir_mne, subj, 'preproc') #individual mne path

# events for epochs
#Stimulus coding (five-digit format)
event_id_codd={'var_c/c4/c-/p0/std': 14121, 'var_c/c4/c-/p0/dev': 14122,'var_c/c4/c-/p0/std_oth': 14123,\
              'var_c/c5/c-/p0/std': 15121, 'var_c/c5/c-/p0/dev': 15122,'var_c/c5/c-/p0/std_oth': 15123,\
              'var_c/c6/c-/p0/std': 16121, 'var_c/c6/c-/p0/dev': 16122,'var_c/c6/c-/p0/std_oth': 16123,\
              'var_c/c2/c+/p0/std': 12221, 'var_c/c2/c+/p0/dev': 12222,'var_c/c2/c+/p0/std_oth': 12223,\
              'var_c/c3/c+/p0/std': 13221, 'var_c/c3/c+/p0/dev': 13222,'var_c/c3/c+/p0/std_oth': 13223,\
              'var_c/c4/c+/p0/std': 14221, 'var_c/c4/c+/p0/dev': 14222,'var_c/c4/c+/p0/std_oth': 14223,\
              'var_c/c6/c+/p0/std': 16221, 'var_c/c6/c+/p0/dev': 16222,'var_c/c6/c+/p0/std_oth': 16223,\
              'var_c/c7/c+/p0/std': 17221, 'var_c/c7/c+/p0/dev': 17222,'var_c/c7/c+/p0/std_oth': 17223,\
              'var_c/c8/c+/p0/std': 18221, 'var_c/c8/c+/p0/dev': 18222,'var_c/c8/c+/p0/std_oth': 18223,\
              'var_p/c2/c-/p+/std': 22111, 'var_p/c2/c-/p+/dev': 22112,'var_p/c2/c-/p+/std_oth': 22113,\
              'var_p/c3/c-/p+/std': 23111, 'var_p/c3/c-/p+/dev': 23112,'var_p/c3/c-/p+/std_oth': 23113,\
              'var_p/c4/c-/p+/std': 24111, 'var_p/c4/c-/p+/dev': 24112,'var_p/c4/c-/p+/std_oth': 24113,\
              'var_p/c4/c-/p0/std': 24121, 'var_p/c4/c-/p0/dev': 24122,'var_p/c4/c-/p0/std_oth': 24123,\
              'var_p/c5/c-/p0/std': 25121, 'var_p/c5/c-/p0/dev': 25122,'var_p/c5/c-/p0/std_oth': 25123,\
              'var_p/c6/c-/p0/std': 26121, 'var_p/c6/c-/p0/dev': 26122,'var_p/c6/c-/p0/std_oth': 26123,\
              'var_p/c6/c-/p-/std': 26131, 'var_p/c6/c-/p-/dev': 26132,'var_p/c6/c-/p-/std_oth': 26133,\
              'var_p/c7/c-/p-/std': 27131, 'var_p/c7/c-/p-/dev': 27132,'var_p/c7/c-/p-/std_oth': 27133,\
              'var_p/c8/c-/p-/std': 28131, 'var_p/c8/c-/p-/dev': 28132,'var_p/c8/c-/p-/std_oth': 28133                  }

f_events = os.path.join(dir_imne, '{}.{}.fif'.format(subj, eve_name)) 
events=read_events(f_events)


################################################################################
# All events separately (nr = 54)
################################################################################
 
event_nr=dict()
for name, code in event_id_codd.items():
    print(name)
    nr_events = len(np.where(events[:,2]==code )[0])
    event_nr[name]=nr_events

for name, nr in event_nr.items():
    print(name, nr)
    
    
################################################################################
# Conditions var_p and var_c
################################################################################
    
event_nr=dict()
combine_event_id  = {'var_c':10000, 'var_p':20000} # we don(t care about codes, it's just a way to count events
for name, new_code in combine_event_id.items():
    print(name)
    new_events = events.copy()
    old_event_codes =np.array([event_id_codd[key] for key in event_id_codd if name in key.split('/')])
    print(old_event_codes)
    # find the ones to replace
    inds = np.any(new_events[:, 2][:, np.newaxis] ==   old_event_codes[np.newaxis, :], axis=1)
    # replace the event numbers in the events list
    new_events[inds, 2] = new_code
    nr_events = len(np.where(new_events[:,2]==new_code )[0])
    event_nr[name]=nr_events
for name, nr in event_nr.items():
    print(name, nr)

################################################################################
# Std, Dev in Conditions var_p and var_c
################################################################################
    
event_nr=dict()
combine_event_id  = {'var_c/std':10001,'var_c/dev':10002, 'var_p/std':20001, 'var_p/dev':20002}
for name, new_code in combine_event_id.items():
    print(name)
    new_events = events.copy()
    old_event_codes = []
    for subname in name.split('/'):
        old_event_codes.append([event_id_codd[key] for key in event_id_codd if subname in key.split('/')])
    
    red = old_event_codes[0]
    for i in np.arange(len(old_event_codes)):
        red = np.intersect1d(red, old_event_codes[i])
    old_event_codes = red
    print(old_event_codes)
    # find the ones to replace
    inds = np.any(new_events[:, 2][:, np.newaxis] ==   old_event_codes[np.newaxis, :], axis=1)
    # replace the event numbers in the events list
    new_events[inds, 2] = new_code
    nr_events = len(np.where(new_events[:,2]==new_code )[0])
    event_nr[name]=nr_events
for name, nr in event_nr.items():
    print(name, nr)


################################################################################
# Std, Dev in chunks c2->c8 in Conditions var_p and var_c
################################################################################
    
event_nr=dict()
combine_event_id  = {'var_c/std/c2':1,'var_c/std/c3':1,'var_c/std/c4':1,'var_c/std/c5':1, 'var_c/std/c6':1, 'var_c/std/c7':1, 'var_c/std/c8':1}
combine_event_id  = {'var_p/std/c2':1,'var_p/std/c3':1,'var_p/std/c4':1,'var_p/std/c5':1, 'var_p/std/c6':1, 'var_p/std/c7':1, 'var_p/std/c8':1}
print('------------------------------------------------')
for name, new_code in combine_event_id.items():
    print(name)
    new_events = events.copy()
    old_event_codes = []
    for subname in name.split('/'):
        old_event_codes.append([event_id_codd[key] for key in event_id_codd if subname in key.split('/')])
    
    red = old_event_codes[0]
    for i in np.arange(len(old_event_codes)):
        red = np.intersect1d(red, old_event_codes[i])
    old_event_codes = red
    print(old_event_codes)
    # find the ones to replace
    inds = np.any(new_events[:, 2][:, np.newaxis] ==   old_event_codes[np.newaxis, :], axis=1)
    # replace the event numbers in the events list
    new_events[inds, 2] = new_code
    nr_events = len(np.where(new_events[:,2]==new_code )[0])
    event_nr[name]=nr_events
    print('------------------------------------------------')
for name, nr in event_nr.items():
    print(name, nr)

################################################################################
# Std, Dev in chunks c4, c5, c6 in Conditions  var_c
################################################################################
    
event_nr=dict()
combine_event_id  = {'var_c/std/c4/c-':1,'var_c/std/c4/c+':1, 'var_c/std/c5/c-':1,'var_c/std/c5/c+':1, 'var_c/std/c6/c-':1,'var_c/std/c6/c+':1}
print('------------------------------------------------')
for name, new_code in combine_event_id.items():
    print(name)
    new_events = events.copy()
    old_event_codes = []
    for subname in name.split('/'):
        old_event_codes.append([event_id_codd[key] for key in event_id_codd if subname in key.split('/')])
    
    red = old_event_codes[0]
    for i in np.arange(len(old_event_codes)):
        red = np.intersect1d(red, old_event_codes[i])
    old_event_codes = red
    print(old_event_codes)
    # find the ones to replace
    inds = np.any(new_events[:, 2][:, np.newaxis] ==   old_event_codes[np.newaxis, :], axis=1)
    # replace the event numbers in the events list
    new_events[inds, 2] = new_code
    nr_events = len(np.where(new_events[:,2]==new_code )[0])
    event_nr[name]=nr_events
    print('------------------------------------------------')
for name, nr in event_nr.items():
    print(name, nr)


################################################################################
# Std, Dev in  Conditions  var_p
################################################################################
    
event_nr=dict()
combine_event_id  = {'var_p/std/c4/p-':1,'var_p/std/c4/p0':1,'var_p/std/c4/p+':1,}
combine_event_id  = {'var_p/std/c5/p-':1,'var_p/std/c5/p0':1,'var_p/std/c5/p+':1,}
combine_event_id  = {'var_p/std/c6/p-':1,'var_p/std/c6/p0':1,'var_p/std/c6/p+':1,}
combine_event_id  = {'var_p/std/c8/p-':1,'var_p/std/c8/p0':1,'var_p/std/c8/p+':1,}
print('------------------------------------------------')
for name, new_code in combine_event_id.items():
    print(name)
    new_events = events.copy()
    old_event_codes = []
    for subname in name.split('/'):
        old_event_codes.append([event_id_codd[key] for key in event_id_codd if subname in key.split('/')])
    
    red = old_event_codes[0]
    for i in np.arange(len(old_event_codes)):
        red = np.intersect1d(red, old_event_codes[i])
    old_event_codes = red
    print(old_event_codes)
    # find the ones to replace
    inds = np.any(new_events[:, 2][:, np.newaxis] ==   old_event_codes[np.newaxis, :], axis=1)
    # replace the event numbers in the events list
    new_events[inds, 2] = new_code
    nr_events = len(np.where(new_events[:,2]==new_code )[0])
    event_nr[name]=nr_events
    print('------------------------------------------------')
for name, nr in event_nr.items():
    print(name, nr)
