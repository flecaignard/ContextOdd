#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
===============================================
Step 04 - Remove jumps
===============================================

Remove jumps (high and rapid peak-to-peak variation)
These could corrupt filtering afterwards (mostly in MEG)
Such artifcats are of non-physiological origin (EEG system)
Output: annotation files where bad segments have been defined

Created on Mon Feb 11 17:57:17 2019

@author: Françoise Lecaignard, francoise.lecaignard@inserm.fr
"""



###############################################################################
import numpy as np             # convention d'import
import sys      # path to my own functions
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Python/EEG')
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/BIDS/mne-bids')



from fun_XX_COdd_eeg_jumps import  codd_eeg_rm_jumps

################################################################################
# Input Parameters 
################################################################################

# Paths ----------------------------
dir_mne = '/sps/cermep/cermep/experiments/DCM/ContextOdd/Ana_MNE'



################################################################################
# Define Constant Variables
################################################################################
# In EEG, these jumps are hard to detect because not too rapid wrt muscles.
# A typical example : sub-29 at latency 3627.02 -> 3627.08: it lasts 60 ms , and p2p>450 uV
# but at 3618.65, there is a muscle artifcat that is more rapid
# So the following threshold is such that it also removes muscle, but this is fine because we don' want these data
#
# other example: sub-05, t=2408.36

jump_thresh = 450e-6  #    jump_thresh= threshol in volts
jump_dur = 60e-3      #    jump_dur: jump duration in seconds
jump_tw = 2           #    jump_tw:  bad segment time-window (in sec). Ex tw=2 means that we remove 2 seconds around each jumps (using MNE annotations)

# filter data to detect jumps (?)
lf, hf =None, None


# Plot raw data
plot=True
################################################################################
# Let's start!
################################################################################

# subj_id_list -----------------------
# test on one subject
subj_id_list = np.arange(29,30) # sub-00
# applyto all
#subj_id_list = np.arange(1,6) # 0 to 35, to include sub-00 to sub-35


#
for i_su in subj_id_list:
    subj='sub-{:02}'.format(i_su) 
    codd_eeg_rm_jumps(dir_mne, subj,jump_thresh, jump_dur, jump_tw, plot=plot, lf=lf, hf=hf)
    