#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 12 16:47:12 2019

@author: lecaigna
"""


import os                       # path.join for file names
from shutil import move
import numpy as np             # convention d'import

# Paths ----------------------------
dir_mne = '/sps/cermep/cermep/experiments/DCM/ContextOdd/Ana_MNE'


subj_id_list = np.arange(0,36) # sub-00
# applyto all
#subj_id_list = np.arange(1,36) # 0 to 35, to include sub-00 to sub-35
pref_in = 'preproc_bad_sensors_noiter.pkl'
pref_out = 'preproc_bad_sensors.pkl'

#
for i_su in subj_id_list:
    
    subj='sub-{:02}'.format(i_su) 
    dir_imne = os.path.join(dir_mne, subj, 'preproc') #individual mne path
    f_orig = os.path.join(dir_imne, f'{subj}.{pref_in}')
    f_tgt = os.path.join(dir_imne, f'{subj}.{pref_out}')
    move(f_orig, f_tgt)
  
    
 