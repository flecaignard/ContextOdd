#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
==================================================================================
UTIL SCRIPT - we look at continuous data with good/bad events
==================================================================================

We plot continuous data on either all sensors or a subset
and we add good events (green) and bad ones (red) (whatever the code)
The aim is to quickly control  rejection outputs  

Output =======> NONE
@author: Françoise Lecaignard, francoise.lecaignard@inserm.fr
"""



###############################################################################
import numpy as np             # convention d'import
import os
import matplotlib.pyplot as plt # from the MNE examples
import sys      # path to my own functions
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/Python/EEG')
sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/ContextOdd_Git/Tools/BIDS/mne-bids')

from mne.io import read_raw_fif
from mne import Epochs, pick_types, read_events, read_epochs
from mne import get_config, set_config
type(get_config('MNE_BROWSE_RAW_SIZE'))
set_config('MNE_BROWSE_RAW_SIZE', '20.0,10.0')
from mne.viz import  tight_layout
#from fun_NN_template import  ...

import pickle
################################################################################
# Input Parameters 
################################################################################

# Paths ----------------------------
dir_mne = '/sps/cermep/cermep/experiments/DCM/ContextOdd/Ana_MNE'

# ----------- continuous data folder
folder = 'preproc'
# ----------- prefix  of  continuous data 
pref_raw = f'preproc_1-40Hz_ica'


# -----------   events file (pickle file)
eve_name = 'good_bad_events'

# -----------   plot options
#event_good_id = {'good':1,'bad_early': 100,  'bad_ar': 200, 'bad_p2p': 300}
dict_color_event = {1: 'green', 100: 'blue', 200: 'magenta', 300: 'red'}
scal = dict(eeg=10e-5)

# subset of electrodes
subset = []
#subset = ['Oz', 'TP9', 'TP10', 'PO10' ,'PO9', 'O1', 'O2']

################################################################################
# Let's start!
################################################################################

# subj_id_list -----------------------
subj_id_list = np.arange(35,36) # sub-00


for i_su in subj_id_list:
    subj='sub-{:02}'.format(i_su) 
    dir_imne_preproc = os.path.join(dir_mne, subj, 'preproc') #individual mne path
    dir_imne_raw = os.path.join(dir_mne, subj, folder) #individual mne path
    
    #### 1 ----- load continuous data
    f_raw = os.path.join(dir_imne_preproc, f'{subj}.{pref_raw}.raw.fif') #  continuous data
    raw = read_raw_fif(f_raw, preload=True)
    
    #### 2 ----- load good/bad events

    f_events_pkl = os.path.join(dir_imne_raw, f'{subj}.{eve_name}.pkl') ### created in step_08 summarize
    with  open(f_events_pkl, 'rb') as input:
        a = pickle.load(input)
    events_ok = a[0]
    sens_exclude = a[1]
    nr_drop = a[2]
    nr_drop_p2p = a[3]
    nr_drop_ar = a[4]
    bad_epochs = a[5]
    event_good_id = a[6]
    dict_contrib = a[7]
    thresh = a[8]
    for i_key in dict_contrib:
        print(i_key, dict_contrib[i_key])       



    #### 3 ----- plot
    if not subset:
        raw.plot(events=events_ok,  event_color = dict_color_event,   n_channels=raw.info['nchan'],remove_dc = True,  duration=30, highpass = None, scalings=scal) #, show_options=False)
    else:
        rawmini = raw.pick_channels(subset)
        rawmini.plot(events=events_ok,  event_color = dict_color_event,n_channels=rawmini.info['nchan'],remove_dc = True,  duration=60, highpass = None, scalings=dict(eeg=10e-4)) #, show_options=False)
#    else:
#        raw.plot(events=events,  event_color = dict_color_event,   n_channels=raw.info['nchan'],remove_dc = True,  duration=30, highpass = None, scalings=scal) #, show_options=False)
#    
    plt.show()
    
    #### 4 ----- print useful info
    nr_epo = 6068
    print(f'=============> AutoReject         : dropped {nr_drop_ar} epochs <=> {np.round(100*nr_drop_ar/nr_epo)} pc')
    print(f'=============> P2P (post AR)      : dropped {nr_drop_p2p} epochs <=> {np.round(100*nr_drop_p2p/nr_epo)} pc')
    print(f'=============> TOTAL              : dropped {nr_drop} epochs <=> {np.round(100*nr_drop/nr_epo)} pc')
    print(f'threshold = {thresh} uV')
    
    # want to plot kappa?
#    sys.path.append('/sps/cermep/cermep/experiments/DCM/ContextOdd/toolboxes/autoreject')
#    from autoreject import AutoReject
#    pref_ar_out = f'AutoReject_output300'
#    f_ar_out = os.path.join(dir_imne_raw, '{}.{}.pkl'.format(subj, pref_ar_out)) 
#    with  open(f_ar_out, 'rb') as input:
#        a = pickle.load(input)
#    ar = a[0]  
#    kappa = ar.consensus_['eeg']
#    print(f'kappa = {kappa*100} pc')
    




         
