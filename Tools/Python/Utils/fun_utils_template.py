#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
===============================================
FUNCTIONS for CONTEXTODD UTILS - STEP NN analysis
===============================================
=> NN: what it is about (short)


Created on Tue Feb 12 13:43:16 2019

@author:

"""


#############################################################################################
## Import packages          -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
# import argparse               # add input arguments

import numpy as np             # convention d'import
import os                       # path.join for file names
import matplotlib.pyplot as plt # from the MNE examples
import warnings

print(__doc__)

from mne import get_config, set_config
type(get_config('MNE_BROWSE_RAW_SIZE'))
set_config('MNE_BROWSE_RAW_SIZE', '20.0,10.0')

warnings.filterwarnings("ignore",category=DeprecationWarning)

#####################################################################################################################
#############################################     FUNCTION     ####################################################
#####################################################################################################################
 