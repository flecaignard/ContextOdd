#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
===============================================
FUNCTION for CONTEXTODD ANALYSIS - Create a list of your subjects and corresponding runs.
===============================================

Created on Wed Mar 27 13:43:16 2019

@author: Pauline Duret

"""


#############################################################################################
## Import packages          -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
import os                       # path.join for file names
import warnings
from collections import defaultdict

print(__doc__)

from mne import get_config, set_config
type(get_config('MNE_BROWSE_RAW_SIZE'))
set_config('MNE_BROWSE_RAW_SIZE', '20.0,10.0')

warnings.filterwarnings("ignore",category=DeprecationWarning)

#####################################################################################################################
#############################################     FUNCTION     ####################################################
#####################################################################################################################
 
def create_subj_list(dir_raw_bids):
    subj_id_list = defaultdict(list)
    with open(os.path.join(dir_raw_bids,'participants.tsv')) as subj_file:
        for line in subj_file:
            runs = []
            subj = line.split()[0]
            if 'sub' in subj:
                for run in [1,2,3,4]:
                    with open(os.path.join(dir_raw_bids, f'{subj}', f'{subj}_scans.tsv')) as run_file:
                        if f'run-{run}' in run_file.read():
                            runs.append(run)
                        else:
                            print( f'WARNING: no run {run} for subject {subj}')
            subj_id_list[subj] = runs
        print( f'You have {len(subj_id_list)} subjects in your group.')
    return subj_id_list