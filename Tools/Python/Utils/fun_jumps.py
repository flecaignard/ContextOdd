import numpy as np             
from mne.io import read_raw_fif
from mne import Annotations
from statistics import mean

           
#############################################################################################
##  o*o*o*o*o*o*o*o*o*o*o*o*o*o*         jump! jump!              *o*o*o*o*o*o*o*o*o*o*o*o*o*
#############################################################################################
def raw_jump_detect(raw, picks, jump_thresh, jump_dur, jump_tw, remove_jumps = False,
                    plot = True, save=False, f_jump_out = None,f_raw_out=None):
    # detection of signal jumps (EEG, MEG) on raw data
    # each jump is assigned an event and a time interval so that events in this interval do not enter subsequent analyses (using mne Annotations)
    #
    # inputs:
    #   raw =  [mne-raw data]
    #   picks = [mne-picks]
    #   jump_thresh = [float], peak-to-peak threshold amplitude in Volt or Tesla
    #   jump_dur = [float], minimum duration for jump occurence to trigger a detection
    #   jump_tw = duration of the time window (centered at jump onset latency), in sec
    #   f_jump_out = name of file to save annontations
    #   f_raw_out = raw file with annotations
    #   output:
    #       
    #
    # versions: 1) M. Maby, FLecaignard - 12-06-18 (inspired from eegdeljump!)
    #           2) P. Duret. 25/03/19
    
    # define jump duration in samples (instead of seconds)
    sfreq = raw.info['sfreq']
    jump_dur_samp = int(jump_dur*sfreq)
    jump_tw_samp =  int(jump_tw*sfreq)
    
    oversampled_jump_dur = 4 * jump_dur_samp # we consider as one all jumps occuring in 4*jump_duration.If jump rise = 25 ms, all jumps detected within 100 ms will be collapsed
    #oversampled_jump_dur = 1 # set to 1 sec
    m_data, times = raw[:,:]
    jumps = [] # expressed in samples
    
    for i_sens, sens in enumerate(picks):
        
        if not raw.ch_names[sens] in raw.info['bads']:
            data_temp = m_data[sens, :]
            diff_data = data_temp[:-jump_dur_samp] - data_temp[jump_dur_samp:]
            jump_ind = np.where(np.abs(diff_data)>= jump_thresh)[0]
            #jump_lat = times[jump_ind]
#            print(jump_lat)
            if jump_ind.size > 0:
                # remove too close jumps if any
                diff_jump = jump_ind[:-1] - jump_ind[1:] 
                over_ind = np.where(abs(diff_jump)<= oversampled_jump_dur)[0]
                #jump_lat = np.delete(jump_lat, over_ind)
                jump_ind = np.delete(jump_ind, over_ind)
                if remove_jumps:
                    for ind in jump_ind:
                        start, stop = ind - 1/2*jump_tw_samp , ind + 1/2*jump_tw_samp
                        jump_mean = mean (data_temp[start], data_temp[stop])
                        data_temp[start:stop] = np.repeat(jump_mean, jump_tw_samp)[0]
       
                for jump in jump_ind:
                    print(f'Channel {raw.ch_names[sens]}: jump found at latency {jump/sfreq} sec')
                    # jumps.append(jmp-jump_tw/2)
                    jumps.append(jump - jump_tw_samp/2)
        
    #remove several occurence of the same jumps (across channels)
    jumps = np.array(jumps)
    jumps = np.sort(jumps)
    diff_jump = jumps[:-1] - jumps[1:] 
    over_ind = np.where(np.abs(diff_jump)<= 1.0)[0] # 1 sec
    jumps = np.delete(jumps, over_ind)
    print(jumps)
    
    if save:
        if jumps.size ==0:
            onset = [0.0] # dummy jump at t=0.0 sec
            duration = np.repeat(0.05, len(onset))
        else:
            onset = jumps 
            duration = np.repeat(jump_tw, len(onset))
        
        
        description = ['bad_jump']*len(onset) # labels starting with bad* will be excluded from epoching
        annotations = Annotations(onset, duration, description)
        annotations.save(f_jump_out)
        raw.set_annotations(annotations)
        raw.save(f_raw_out, overwrite=True)
        print(raw.annotations)
        
#    print(jumps.size)
    if plot:
        scal = dict(mag=10e-12)
        raw = read_raw_fif(f_raw_out, preload=False)
        raw.plot( n_channels=raw.info['nchan'],remove_dc = True, start=130 ,duration=30, highpass = None, scalings=scal) #, show_options=False)
    
