function [] = ContextOdd_sort_events_fromElanPos(SeqDir, SubjName, PosFile_Acq, EvMrk_Bloc, PosFile_Out)

% PosFile_Acq = Elan pos file created from raw BrainAmp data (made of 1,2, 16, 32 and 225 codes)
% EvMrk_Bloc = cell array composed of Nbloc column vectors, each made of Nt
% values, with the digit notation, created by 
% PosFile_Out = Elan pos file created from raw BrainAmp data (made of 1,2, 16, 32 and 225 codes)

%% load StimSeq files
SubjDir = [ SeqDir '/' SubjName ];
A = load( [ SubjDir '/' SubjName '_ContextOdd_Sequence.mat']) ; % gives S1, S2, T1, T2, Con1, Con2 (see CreateNewDesignSeq_FL.m in SeqPresentation )
R = load( [ SubjDir '/' SubjName '_ContextOdd_SequenceReverse.mat']) ; % gives S1, S2, T1, T2, Con1, Con2 (see SeqPresentation files)
exp  =load([ SeqDir '/' SubjName '/' SubjName '_ContextOdd_Expe.mat']);

%% Load pos file
pos=load(PosFile_Acq);
lat=pos(:,1);
code=pos(:,2);
rej=pos(:,3);

lat_new = lat;
code_new = code;
rej_new = rej;

%% Check bloc number
Nbloc = numel(EvMrk_Bloc);
h_bloc = find(code == 16);
if numel(h_bloc)~=Nbloc
    disp('Not the good number of blocs found in Input sequence');
    return
end


%% sort
% we will use a 5-digit code
% d1-d2-d3-d4-d5
% d1 = condition: 1/2
% d2 = chunk: 2/../8
% d3 = r: 1/2 (r-,r+) <=> chunks 4:6 vs. [2:4 6:8]
% d4 = p:1/2/3 (p-,p,p+) <=> 2:4 vs. 4:6 vs. 6:8
% d5 = tone category: standard preceding a deviant=1, deviant=2

for i_b=1:Nbloc
    
    digit = [  ];
    [Cond, Tone] = ContextOdd_find_cond(exp.Expe(i_b), exp.Reverse(i_b), A, R);
    
    
    
    try
        C = code(h_bloc(i_b)+1 : h_bloc(i_b+1)-2); % supp code 32 preceding 16 
       
    catch
        C = code(h_bloc(i_b)+1 : end-1);
        
    end
    if numel(C)~=size(Cond,1)
        fprintf('Mismatch of nr of events in bloc %d \n', i_b);
        return
    end
    
    
    nr_ev = size(Cond,1);
    %digit d1 = cond 1/2
    digit(1:nr_ev,1)=exp.Expe(i_b);
    
    
    % digit d2 = chunk: 2/../8
    % digit d3 = r: 1/2 (r-,r+) <=> chunks 4:6 vs. [2:4 6:8]
    % digit d4 = p:1/2/3 (p-,p,p+) <=> 2:4 vs. 4:6 vs. 6:8
    digit(:,2:4) = Cond;
    
    
    % digit d5: sort standard preceding a deviant (1) and deviant (2),
    % other (3)
    digit(:,5)=3; % all standards but preceding a deviant
    h_dev = find(C==2);
    digit(h_dev,5)=2;
    
    h_std=h_dev-1;
    h=find(C(h_std)>1);
    if ~isempty(h)
        fprintf('Trouble with std preceding a deviant in bloc %d \n', i_b);
        return
    end
    digit(h_std,5)=1;
    
    
    % finally:
    
    Cnew = digit(:,1).*10000 + digit(:,2).*1000 + digit(:,3).*100 + digit(:,4).*10 + digit(:,5); 
    do = find(Cnew==13222);
    so = find(Cnew==13221);
    [length(do) length(so) ]
    if EvMrk_Bloc{i_b} ~= Cnew
        fprintf('Trouble with StimSeq and BrainAmp/ElanPos compatibility in bloc %d \n', i_b);
        return
    else
      
        try
            code_new(h_bloc(i_b)+1 : h_bloc(i_b+1)-2) = Cnew;
            
        catch
            code_new(h_bloc(i_b)+1 : end-1) = Cnew;
            
        end
    end
end



fid=fopen(PosFile_Out,'w');

for i=1:size(lat,1)
    fprintf(fid,'%d %d %d \n',lat(i), code_new(i), rej(i));
end

fclose(fid);

[codemrk nbmrk nbmrkok pcok]=ContextOdd_pos_compte_mrk(PosFile_Out);
[a b ]=sort(codemrk);
codemrk=codemrk(b);
N=length(codemrk);nbmrk=nbmrk(b); nbmrkok=nbmrkok(b);  pcok=pcok(b);
for i=1:N
    disp(['code  ' num2str(codemrk(i)) ' :  ' num2str(nbmrk(i)) '   ' num2str(nbmrkok(i)) '   ' num2str(pcok(i)) ]);
end




end


