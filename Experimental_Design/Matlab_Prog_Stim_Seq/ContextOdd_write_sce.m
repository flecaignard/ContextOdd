function ContextOdd_write_sce(flag_reverse, seqcode, file_out)

% Input
% flag_reverse=     1 if no reverse (f_std = 500 Hz)
%                   2 if reverse (f_std = 550 Hz)
% seqcode =        vector of std/dev codes
% file_out =        *.sce file to be created



ISI=540;
label={'std' 'dev'};

wavefile={'F0_500_RF5.wav'  'F0_550_RF5.wav' };

switch flag_reverse
    case 1
        std=1; Freq_s = 500;
        dev=2; Freq_d = 550;
    case 2
        std=2; Freq_s = 550;
        dev=1; Freq_d= 500;
end

att_std=0.2 % 60 dB ;
att_dev=att_std;


ch='TEMPLATE "ContextOdd.tem"';


fid=fopen(file_out,'w');
fprintf(fid,'scenario = "ContextOdd";\n');
fprintf(fid,'no_logfile = false;\n');
fprintf(fid,'pulse_width = 3;\n');
fprintf(fid,'write_codes = true;\n');
fprintf(fid,'\n');
fprintf(fid,'\n');
% fprintf(fid,'#Corresponding sce file  = %s \n',file_out );
% fprintf(fid,'\n');
% fprintf(fid,'\n');
fprintf(fid,'#-------------------------------------------------------\n');
fprintf(fid,'begin;\n');
fprintf(fid,'#-----------------hello!--------------------------------\n');
fprintf(fid,'sound { wavefile { filename = "%s"; }; attenuation=%f ;} std;\n',wavefile{std},att_std );
fprintf(fid,'# binaural standard,  duree 70ms, F= %dHz;\n', Freq_s);
fprintf(fid,'sound { wavefile { filename = "%s"; }; attenuation=%f ;} dev;\n',wavefile{dev} ,att_dev);
fprintf(fid,'# binaural dev. duree 70ms, F= %dHz;\n', Freq_d);
fprintf(fid,'\n');
fprintf(fid,'\n');
fprintf(fid,'#-------------------------------------------------------\n');
fprintf(fid,'\n');
fprintf(fid,'\n');
fprintf(fid,'trial{\n');
fprintf(fid,' trial_duration=5000;\n');
fprintf(fid,'       stimulus_event{\n');
fprintf(fid,'       nothing{};\n');
fprintf(fid,'      time=0;\n');
fprintf(fid,'      port_code=16;\n');
fprintf(fid,'      code="start";\n');
fprintf(fid,'      };\n');
fprintf(fid,'};\n');
fprintf(fid,'\n');
fprintf(fid,'\n');
fprintf(fid,'%s\n',ch);
fprintf(fid,'{\n');
fprintf(fid,'codeS label ISI ;\n');
for j=1:size(seqcode,1)%Nstim
    fprintf(fid,'%d %s %d;\n',seqcode(j),label{seqcode(j)} , ISI);
end
fprintf(fid,'};\n');
fprintf(fid,'\n');
fprintf(fid,'\n');
fprintf(fid,'trial{\n');
fprintf(fid,' trial_duration=stimuli_length;\n');
fprintf(fid,'      stimulus_event{\n');
fprintf(fid,'      nothing{};\n');
fprintf(fid,'      time=5000;\n');
fprintf(fid,'      port_code=32;\n');
fprintf(fid,'      code="end";\n');
fprintf(fid,'      };\n');
fprintf(fid,'};\n');
fprintf(fid,'\n');
fprintf(fid,'\n');
fclose(fid);


