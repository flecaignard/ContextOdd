function [ EvMrk ] = ContextOdd_sort_events_fromStimSeq(SeqDir, SubjName, Nbloc)

% nargin = InputSeq which is either a vector of 16-0-1 codes from the acquisition
% or build up here from A and R matrices



%% Load sequence info.
SubjDir = [ SeqDir '/' SubjName ];
A = load( [ SubjDir '/' SubjName '_ContextOdd_Sequence.mat']) ; % gives S1, S2, T1, T2, Con1, Con2 (see CreateNewDesignSeq_FL.m in SeqPresentation )
R = load( [ SubjDir '/' SubjName '_ContextOdd_SequenceReverse.mat']) ; % gives S1, S2, T1, T2, Con1, Con2 (see SeqPresentation files)
exp  =load([ SeqDir '/' SubjName '/' SubjName '_ContextOdd_Expe.mat']);



%% sort
% we will use a 5-digit code
% d1-d2-d3-d4-d5
% d1 = condition: 1/2
% d2 = chunk: 2/../8
% d3 = r: 1/2 (r-,r+) <=> chunks 4:6 vs. [2:4 6:8]
% d4 = p:1/2/3 (p-,p,p+) <=> 2:4 vs. 4:6 vs. 6:8
% d5 = tone category: standard preceding a deviant=1, deviant=2

for i_b=1:Nbloc
    
    digit = [  ];
    [Cond, Tone] = ContextOdd_find_cond(exp.Expe(i_b), exp.Reverse(i_b), A, R);
    C = Tone;
    
    
    nr_ev = size(Cond,1);
    %digit d1 = cond 1/2
    digit(1:nr_ev,1)=exp.Expe(i_b);
    
    
    % digit d2 = chunk: 2/../8
    % digit d3 = r: 1/2 (r-,r+) <=> chunks 4:6 vs. [2:4 6:8]
    % digit d4 = p:1/2/3 (p-,p,p+) <=> 2:4 vs. 4:6 vs. 6:8
    digit(:,2:4) = Cond;
    
    
    % digit d5: sort standard preceding a deviant (1) and deviant (2),
    % other (3)
    digit(:,5)=3; % all standards but preceding a deviant
    h_dev = find(C==2);
    digit(h_dev,5)=2;
    
    h_std=h_dev-1;
    h=find(C(h_std)>1);
    if ~isempty(h)
        fprintf('Trouble with std preceding a deviant in bloc %d \n', i_b);
        return
    end
    digit(h_std,5)=1;
    
    
    % finally:
    EvMrk{i_b}  = digit(:,1).*10000 + digit(:,2).*1000 + digit(:,3).*100 + digit(:,4).*10 + digit(:,5); 
    
    
    
    





end


end


