%____-------------------------------------------------------------____%
%____-------------------------------------------------------------____%
%
% Make Presentation files
%
%____-------------------------------------------------------------____%
%____-------------------------------------------------------------____%

% feb. 2018
% April 2018 : Adjust to Pauline's workspace at IN2P3
% October 2019 : adjust path to my workspace at IN2P3 , create new subjects
% for additional EEG (30 subjects to be done by February 2020)

%% PATHS
curr_place = '/sps/inter/crnl/users/pduret';
curr_place = '/sps/cermep/cermep/experiments/DCM';

dirlm = [ curr_place '/ContextOdd/ContextOdd_Git/Experimental_Design/Matlab_Prog_Stim_Seq'];
dirseq = [ curr_place '/ContextOdd/ContextOdd_Git/Experimental_Design/StimSeq'];
addpath(dirlm)
addpath(dirseq)

%% SEQUENCES - GENERAL REMARKS
% 
% Two conditins : var_r and var_p, repeated twice because of the reverse
% issue (association tone category std/dev and tone frequency low/high)
% sequence = 1512 sounds, 7 alternation (or cycles)  (r+ / r- ) or (p+ / p / p- )
% 
% ''carre latin'' to balance conditions across subjects
% 2 conditions : var_r = 1, var_p = 2 
% and 2 reverse : 1 = std-low, and 2 = std-high
% Four sequences per subjects in {11,12,21,22} where decimal <=> condition,
% and unit <=> reverse
%
% We consider the 6 possibilities for sequence order for Conditions
% (1=var_r, 2=var_p)
%     A:    1 2 1 2
%     B:    2 1 2 1
%     C:    1 2 2 1
%     D:    2 1 1 2
%     E:    1 1 2 2
%     F:    2 2 1 1
%
% For each sequence order, we have 4 possibilities of reversing
% This is because once first sequence is fixed (say std-high), then the
% second replication of this condition (in position 2,3 or 4) is fixed too.
% Example for sequence order 1 2 1 2 we have the four possibilities
% high low low high / high high low low / low high low high / low low high high
% we formalize this as follows: (h,l) means high for the first occurence of
% the firts condition, low for the first occurence of the second condition
% so that (h,l) in A gives: high low low high <=> 12 21 11 22
%   a:  (l,l)
%   b: (l,h)
%   c:  (h,l)
%   d:  (h,h)
%
% {A,B,C,D,E,F} *(a,b,c,d) <=>  We thus have 6*4 = 24 different sequences order
%
% How do we distribute them among subject? This is to manage the following
% issue: if the first half of the group should be dropped out for any reason, then we should still
% have our halfed-group balanced
% 


%% CREATE SEQUENCES - First Phase of EEG acquisition - 2018
% (second phase, below)



% 4th april 2018:  2 pilotes so far  ePilote1 and ePilote2 (ePilote1 was also  run in MEG)
% we go for circa 40 subjects in EEG, we create 48 sequences


% SubjName = 'Pilote1';
N = 48 ; % because the carre latin stuff creates 24 possibilities
for i=1:48
    if i <10
        SubjList{i} = ['eSu0' num2str(i)] ;
    else
            SubjList{i} = ['eSu' num2str(i)] ;
    end
end
save([ dirseq '/Listname_24Su_EEG.mat'], 'SubjList')

% create subj directory
for i_s = 1:numel(SubjList)
    
    SubjName = SubjList{i_s};
    try
        system(['mkdir  ' dirseq '/' SubjName ]);
    end
    % create equences
    file_out = [ dirseq '/' SubjName '/' SubjName '_ContextOdd_Sequence.mat'];
    [S1 , S2, T1 , T2,Con1 , Con2] = CreateNewDesignSeq_FL(file_out);
    % reverse, retour : std = high freq
    file_out = [ dirseq '/' SubjName '/' SubjName '_ContextOdd_SequenceReverse.mat'];
    [S1 , S2, T1 , T2,Con1 , Con2] = CreateNewDesignSeq_FL(file_out);
    clear S1 S2 T1 T2 Con1 Con2

end

    
load([ dirseq '/Listname_24Su_EEG.mat'])
SubjName = SubjList{34};
A = load( [ dirseq '/' SubjName '/' SubjName '_ContextOdd_Sequence.mat']) ; % gives S1, S2, T1, T2, Con1, Con2 (see CreateNewDesignSeq_FL.m in SeqPresentation )
R = load( [ dirseq '/' SubjName '/' SubjName '_ContextOdd_SequenceReverse.mat']) ; % gives S1, S2, T1, T2, Con1, Con2 (see SeqPresentation files)


figure;
subplot(2,1,1);
plot(A.T1, 'ro');
subplot(2,1,2);
plot(A.Con1(:,1), 'k-+'); hold on
plot(A.Con1(:,2), 'r*-'); hold on
plot(A.Con1(:,3), 'm-'); hold on
title('k: chunk size,    r: (r-, r+),   m: (p-, p, p+)')

figure;
subplot(2,1,1);
plot(A.T2, 'ro');
subplot(2,1,2);
plot(A.Con2(:,1), 'k-+'); hold on
plot(A.Con2(:,2), 'r*-'); hold on
plot(A.Con2(:,3), 'm-'); hold on
title('k: chunk size,    r: (r-, r+),   m: (p-, p, p+)')

%% Create the ''carre latin'' to balance conditions across subjects
% 2 conditions : var_r = 1, var_p = 2 
% and 2 reverse : 1 = std-low, and 2 = std-high
% the code below provides 24 subjects
%
% we aim to include 40-42 ...so we need (1 + 2/3)  cycle of 24
% 

List_Cond = [1 2 1 2; ...
    2 1 2 1 ; ...
    1 2 2 1 ; ...
    2 1 1 2 ; ...
    1 1 2 2 ; ...
    2 2 1 1 ; ...
    ]

List_Rev = List_Cond;

Bloc_Cond ={};
Bloc_Rev = {};
img_bloc = [ ];
for i_c = 1:size(List_Cond,1)
    for i_r = 1:size(List_Rev,1)
        C = List_Cond(i_c, :);
        R = List_Rev(i_r,:) ;
        newb=C.*10 + R;
        if isequal(length(unique(newb)), 4)
        
            img_bloc = [img_bloc; newb];
            Bloc_Cond{end+1} =C;
            Bloc_Rev{end+1} = R;
        end
    end
end
 figure; imagesc(img_bloc)
 
 
 
 rafine = [1 2 5 6 3 4 7 8 9 10 13 14 11 12 15 16 17 18 21 22 19 20 23 24 ];
 raf_img_bloc = img_bloc(rafine',:);
 figure; imagesc(raf_img_bloc)
 teimg_bloc = [ ];
 raf_Bloc_Cond ={};
raf_Bloc_Rev = {};
 for i=1:numel(Bloc_Cond)
    raf_Bloc_Cond{i}=Bloc_Cond{rafine(i)};
    raf_Bloc_Rev{i}=Bloc_Rev{rafine(i)};
    teimg_bloc = [teimg_bloc; raf_Bloc_Cond{i}.*10 + raf_Bloc_Rev{i}];
 end
 
 
% backup of the 24-subj cycle
save([ dirseq '/StimSeq/Order_Bloc_24Su.mat', 'raf_Bloc_Cond', 'raf_Bloc_Rev', 'raf_img_bloc')
 
 
%% Write scenario files 

dirseq='/sps/inter/crnl/users/pduret/ContextOdd/StimSeq';

su = load('/sps/inter/crnl/users/pduret/ContextOdd/StimSeq/Listname_24Su_EEG.mat'); % gives 'SubjList')
co = load('/sps/inter/crnl/users/pduret/ContextOdd/StimSeq/Order_Bloc_24Su.mat'); % gives 'raf_Bloc_Cond', 'raf_Bloc_Rev', 'raf_img_bloc'

figure;  imagesc(co.raf_img_bloc)
Nmax = numel(co.raf_Bloc_Cond)


% Su01 to Su24 : use the 24-cycle
for i_s = 1:24 % numel(su.SubjList)
    SubjName = su.SubjList{i_s};
    Expe = co.raf_Bloc_Cond{i_s}; % 1<=> r varies, 2 <=> p varies
    Reverse = co.raf_Bloc_Rev{i_s}; %
    ContextOdd_write_sce_all(SubjName, Expe, Reverse)
end

% Su25 to Su48 : 
% we could re-use the same cycle, but we plan to record  max 44 subjects
% so we create a bias since the last third of the cycle rests on A-A-B-B or
% B_B-A-A series
% to avoid this, we reorder : 

newo = [1 2 3 4 9 10 11 12 17 18 19 20 5 6 7 8 13 14 15 16 21 22 23 24 ];
newo_img_bloc = co.raf_img_bloc(newo',:);
figure; imagesc(newo_img_bloc)
for i_s = 1:24 % 25:  numel(su.SubjList)
    SubjName = su.SubjList{24 + i_s};
    Expe = co.raf_Bloc_Cond{newo(i_s)}; % 1<=> r varies, 2 <=> p varies
    Reverse = co.raf_Bloc_Rev{newo(i_s)}; %
    ContextOdd_write_sce_all(SubjName, Expe, Reverse)
end


% verif


f = load([ dirseq '/eSu09/eSu09_ContextOdd_Expe.mat']);

% SubjName = 'Pilote1';
% 
% SubjName = 'ePilote2';
% Expe = [1 2 1 2]; % 1<=> r varies, 2 <=> p varies
% Reverse = [1 2 2 1]; % warning: Pilote 1 was mistaken!


%% Sort events
% code 16 separates blocs

% say we have a vector of 16-0-1 codes from the acquisition
% we will use a 5-digit code
% d1-d2-d3-d4-d5
% d1 = condition: 1/2
% d2 = chunk: 2/../8
% d3 = r: 1/2 (r-,r+) <=> chunks 4:6 vs. [2:4 6:8]
% d4 = p:1/2/3 (p-,p,p+) <=> 2:4 vs. 4:6 vs. 6:8
% d5 = tone category: standard preceding a deviant=1, deviant=2

Nbloc = 4;

dirseq='/sps/inter/crnl/users/pduret/ContextOdd/StimSeq';
su = load('/sps/inter/crnl/users/pduret/ContextOdd/StimSeq/Listname_24Su_EEG.mat'); % gives 'SubjList')

for i_su=1:numel(su.SubjList)
    SubjName = su.SubjList{i_su}
    [ EvMrk_Bloc ] = ContextOdd_sort_events(dirseq, SubjName, Nbloc) % recode directly from the matlab sound sequence
    File_EvMrk = [ SubjDir '/' SubjName '_ContextOdd_EventMrk.mat'];
    save(File_EvMrk, 'EvMrk_Bloc' ) ; 
end


% alt: using BrainAmp mark file
% InputSeq = from pos file or brainamp mrk ?
% [ EvMrk_Bloc ] = ContextOdd_sort_events(dirseq, SubjName, Nbloc, InputSeq) % recode  from the acquisition code sequence (continuous, with 16-start codes)

a = load(File_EvMrk)


%% CREATE SEQUENCES - SECOND PHASE of EEG acquisition - 2019-2020
% 
% First, remember the carre latin
cd(dirseq);
b = load('Order_Bloc_24Su.mat');
% raf_Bloc_Cond: {1×24 cell}
% raf_Bloc_Rev: {1×24 cell}
% raf_img_bloc: [24×4 double]

figure; imagesc(b.raf_img_bloc)

% go
sub_ind=36:65;
nr_sub=30;
for i=1:nr_sub
    ind=sub_ind(i);
    SubjList{i} = ['sub-' num2str(ind)] ;
  
end
save([ dirseq '/Listname_eSu36-eSu65_EEG.mat'], 'SubjList')

% create subj directory and the four sequences
for i_s = 1:numel(SubjList)
    
    SubjName = SubjList{i_s};
    try
        system(['mkdir  ' dirseq '/' SubjName ]);
    end
    % create equences
    file_out = [ dirseq '/' SubjName '/' SubjName '_ContextOdd_Sequence.mat'];
    [S1 , S2, T1 , T2,Con1 , Con2] = CreateNewDesignSeq_FL(file_out);
    % reverse, retour : std = high freq
    file_out = [ dirseq '/' SubjName '/' SubjName '_ContextOdd_SequenceReverse.mat'];
    [S1 , S2, T1 , T2,Con1 , Con2] = CreateNewDesignSeq_FL(file_out);
    clear S1 S2 T1 T2 Con1 Con2

end

% assign bloc order to each sequence, following the scheme described in
% ParticipantList.xls (MACbook pro FL)
assign_sub = 36:65; 
assign_24cycle_id = [5 6 7 8 13 14 15 16 20 21 22 23 24 14 19 17 4 2 3 9 11 10 12 21 23 22 24 5 8 14];
for i=1:numel(assign_sub)
    disp(['sub-' num2str(assign_sub(i)) ' :  ' num2str(assign_24cycle_id(i))]);
end


% carre latin - 24 cycle: 24 combinations of order to deliver the four
% sequences
A = load([ dirseq '/Order_Bloc_24Su.mat']); % , 'raf_Bloc_Cond', 'raf_Bloc_Rev', 'raf_img_bloc')
% How to control this is ok?
% Remember the code :
%     A:    1 2 1 2
%     B:    2 1 2 1
%     C:    1 2 2 1
%     D:    2 1 1 2
%     E:    1 1 2 2
%     F:    2 2 1 1
%   a:  (l,l) (low means 1 means std-low)
%   b: (l,h)
%   c:  (h,l)
%   d:  (h,h)
% Take one combination: #18 => A.raf_img_bloc(18,:) = 12    11    22    21
% Take the decimal (or equivalently A.raf_Bloc_Cond{18}) => 1     1     2 2
% => combi E
% Take the unit (or equivalently A.raf_Bloc_Rev{18}) => 2     1     2     1
% <=> high, high <=> d 
% So combi 18 is equal to Ed
% In the ParticipantList.xls file, search for which subject receives #18
% and check that it is Ed (This is a bad example as #18 is not delivered in
% phase 2!!!)- 
%

% Write scenario files 

nr_sub = 30;

for i_s = 1:nr_sub 
    SubjName = ['sub-' num2str(assign_sub(i_s ))];
    i_combi = assign_24cycle_id(i_s );
    combi = A.raf_img_bloc(i_combi,:);
    disp([SubjName ' ' num2str(i_combi) ' : ' num2str(combi(1)) ' ' num2str(combi(2)) ' ' num2str(combi(3)) ' ' num2str(combi(4)) ' ' ]);
    Expe = A.raf_Bloc_Cond{i_combi}; % 1<=> r varies, 2 <=> p varies
    Reverse = A.raf_Bloc_Rev{i_combi}; %
    ContextOdd_write_sce_all(dirseq,SubjName, Expe, Reverse)
end

