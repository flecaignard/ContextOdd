function [S1 , S2, T1, T2, Con1, Con2] = CreateNewDesignSeq_FL(varargin)

% Passive oddball auditory paradigm made of 2 conditions presented
% in seperated blocs. We use a deviance in sound frequency.
%
% One bloc type manipulates probabilistic uncertainty (1), while the
% second bloc type manipulates environmental uncertainty (2)
%
% (1): the deviance probability does not change but the variaiblity in
% the number of standard tones preceding a deviant increases
%
% (2): the deviance probability does change over time but the variability
% in the number of standard tones preceding a deviant remains stable
%
% Outputs:
%       S1, S2 = Chunk sequence for (1) and (2)
%       T1, T2 = Conversion of S1 and S2 into Tone sequences (1/0)
%       SCon1, SCon2 = 3-col matrix, with col1 = chunk size, col2 coding for condition 'r'
%       (variaiblity in chunk size), and col3  coding for condition 'p' (deviant probability)
%
% Output file: optional
%
% F. Lecaignard and J. Mattout - 09 / 12 / 2017


SOA  = 610; % ms


S1 = [];
S2 = [];
T1 = [];
T2 = [];
SCon1 = []; % vector of 'chunk length'
SCon2 = [];
Con1 = [];  % vector of 'tone length'
Con2 = [];

transiMin = 2; % minimum chnuk size btw conditiosn to avoid ambiguity, and keep as many events  as possible
% con_r in  {1, 2} <=> {r-, r+} 
% con_p in  {1, 2, 3} <=> {p-, pn, p+} 

%% Cycle types
C1 = 4:6;           % <=> r-, pn
C2 = [2:4 6:8];     % <=> r+, pn
C3 = 2:4;           % <=> r-, p-
C4 = 6:8;           % <=> r-, p+

%% Bloc type (1)
n1   = length(C1);
n2   = length(C2);
Nc1  = 6; % # consecutive similar cycles
Nc2  = 3;
Nalt = 7; % ~15 minutes
con_p = 2; 
S1 = [];
for k = 1:Nalt
    for i = 1:Nc1
        con_r = 1;
        
        if isempty(S1)
            I = randperm(n1);
            S1 = [S1 C1(I)];
            SCon1 = [SCon1; ones(n1,1).*con_r ones(n1,1).*con_p  ];
        else
            if SCon1(end,1) ~= con_r % transition r+ => r-
                out = 1;
                while out
                    I = randperm(n1);
                    if abs(C1(I(1)) - S1(end)) >=transiMin
                        out = 0;
                        S1 = [S1 C1(I)];
                        SCon1 = [SCon1; ones(n1,1).*con_r ones(n1,1).*con_p ];
                    end
                end
            else
                
                out = 1;
                while out
                    I = randperm(n1);
                    if C1(I(1)) ~= S1(end)
                        out = 0;
                        S1 = [S1 C1(I)];
                        SCon1 = [SCon1; ones(n1,1).*con_r ones(n1,1).*con_p ];
                    end
                end
            end
        end
        
    end
    for j = 1:Nc2
        con_r = 2;
        
        if SCon1(end,1) ~= con_r % transition r- => r+
            out = 1;
            while out
                I = randperm(n2);
                h4 = find(I == 3); h6 = find(I==4); % we want chunk4 and chunk6 not to be follwoed one to each other               
                if (abs(C2(I(1)) - S1(end)) >= transiMin ) && (abs(h4-h6)>=2)
                    out = 0;
                    S1 = [S1 C2(I)];
                    SCon1 = [SCon1; ones(n2,1).*con_r ones(n2,1).*con_p ];
                end
            end
        else
            out = 1;
            while out
                I = randperm(n2);
                h4 = find(I == 3); h6 = find(I==4);
                if (C2(I(1)) ~= S1(end)) && (abs(h4-h6)>=2)
                    out = 0;
                    S1 = [S1 C2(I)];
                    SCon1 = [SCon1; ones(n2,1).*con_r ones(n2,1).*con_p ];
                end
            end
        end
        
        
    end
end

Nstim = sum(S1) + length(S1);
BlocDur = Nstim*SOA/(1000*60);

disp(['This blocs made of ' num2str(Nalt) ' alternations and will last ' num2str(BlocDur) ' minutes']);
disp(['Nombre de sons total ' num2str(Nstim)]);

figure;
set(gcf,'color','white');
subplot(2,2,1);
hist(S1,2:8);
title('Bloc type 1: dist. of chunk size');
subplot(2,2,2);
plot(1:length(S1),S1,'or','MarkerSize',10,'MarkerFaceColor','r');
ylim([0 10]);
title('Bloc type 1: chunk seq.');


%% Bloc type (2)
n = n1;
Nc = Nc2;
%Nalt = 5;
con_r = 1;  % r-
S2 = [];
for k = 1:Nalt
    for i = 1:2*Nc
        con_p = 2; 
        if isempty(S2)
            I = randperm(n);
            S2 = [S2 C1(I)];
            SCon2 = [SCon2 ; ones(n,1).*con_r ones(n,1).*con_p ];
            [size(S2,1) size(SCon2,2)  ]
        else
            if SCon2(end,2) ~= con_p % transition p* => p2
                out = 1;
                while out
                    I = randperm(n);
                    if abs(C1(I(1))-S2(end)) >=transiMin
                        out = 0;
                        S2 = [S2 C1(I)];
                        SCon2 = [SCon2 ; ones(n,1).*con_r ones(n,1).*con_p ];
                        
                    end
                end
            else
                out = 1;
                while out
                    I = randperm(n);
                    if C1(I(1)) ~= S2(end)
                        out = 0;
                        S2 = [S2 C1(I)];
                        SCon2 = [SCon2 ; ones(n,1).*con_r ones(n,1).*con_p ];
                        
                    end
                end
            end
        end
        
    end
    for j = 1:Nc
        con_p = 1 ;
        if SCon2(end,2) ~= con_p % transition p* => p1 
            out = 1;
            while out
                I = randperm(n);
                if abs(C3(I(1)) - S2(end)) >=transiMin
                    out = 0;
                    S2 = [S2 C3(I)];
                    SCon2 = [SCon2 ; ones(n,1).*con_r ones(n,1).*con_p  ];
                end
            end
        else
            out = 1;
            while out
                I = randperm(n);
                if C3(I(1)) ~= S2(end)
                    out = 0;
                    S2 = [S2 C3(I)];
                    SCon2 = [SCon2 ; ones(n,1).*con_r ones(n,1).*con_p  ];
                end
            end
        end
        
    end
    for j = 1:Nc
        con_p = 3 ;
        if SCon2(end,2) ~= con_p % transition p* => p3 
            out = 1;
            while out
                I = randperm(n);
                if abs(C4(I(1)) - S2(end)) >= transiMin
                    
                    out = 0;
                    S2 = [S2 C4(I)];
                    SCon2 = [SCon2 ;ones(n,1).*con_r ones(n,1).*con_p  ];
                end
            end
        else
            out = 1;
            while out
                I = randperm(n);
                if C4(I(1)) ~= S2(end)
                    
                    out = 0;
                    S2 = [S2 C4(I)];
                    SCon2 = [SCon2 ;ones(n,1).*con_r ones(n,1).*con_p  ];
                end
            end
        end
        
    end
end

Nstim = sum(S2) + length(S2);
BlocDur = Nstim*SOA/(1000*60);

disp(['This blocs made of ' num2str(Nalt) ' alternations and will last ' num2str(BlocDur) ' minutes']);
disp(['Nombre de sons total ' num2str(Nstim)]);

subplot(2,2,3);
hist(S2,2:8);
title('Bloc type 2: dist. of chunk size');
subplot(2,2,4);
plot(1:length(S2),S2,'og','MarkerSize',10,'MarkerFaceColor','g');
ylim([0 10]);
title('Bloc type 2: chunk seq.');


%% Move Chunk seq. into Tone sequences

[T1 , T2, Con1, Con2] = CreateNewDesignSeq_Move2Tone(S1, S2, SCon1, SCon2);
clear SCon1 SCon2;

%% Save
if nargin == 1
    OutputFile = varargin{1};
    save(OutputFile, 'S1', 'S2','T1', 'T2',  'Con1', 'Con2');
end

end % end function



function [T1 , T2, C1, C2] = CreateNewDesignSeq_Move2Tone(S1, S2, SCon1, SCon2)

% S1, S2 are two sequences of chunk size created with CreateNewDesignSeq.m
% Conversion into oddball sequences
% 3 additionnal standards are added at the end to spare last events
%
% FL, feb 2018


SeqC = S1;
SCon = SCon1;
T = [];C = [];
for i = 1:numel(SeqC)
    try
    
    T =[T;ones(SeqC(i),1);2];
    % val = [ones(SeqC(i) +1 ,1)* SCon(i,1)  ones(SeqC(i) +1, 1) .* SCon(i,2)];
    C =[C;[ones(SeqC(i) +1 ,1).* SeqC(i) ones(SeqC(i) +1 ,1).* SCon(i,1)  ones(SeqC(i) +1, 1) .* SCon(i,2)] ];
    catch
        u=2;
    end
end
T1 = [T; 1;1;1];
C1 = [C; 0 0 0; 0 0 0 ; 0 0 0];


SeqC = S2;
SCon = SCon2;
T = [];C = [];
for i = 1:numel(SeqC)
    
    T =[T;ones(SeqC(i),1);2];
    C =[C;[ones(SeqC(i) +1 ,1).* SeqC(i) ones(SeqC(i) +1 ,1)* SCon(i,1)  ones(SeqC(i) +1, 1) .* SCon(i,2)] ];
    
end
T2 = [T; 1;1;1];
C2 = [C;0  0 0; 0 0 0 ;0  0 0];


end
