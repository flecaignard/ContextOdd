function ContextOdd_write_sce_all(dirseq, SubjName, Expe, Reverse)

%dirseq='/sps/inter/crnl/users/pduret/ContextOdd/StimSeq';

A = load( [ dirseq '/' SubjName '/' SubjName '_ContextOdd_Sequence.mat']) ; % gives S1, S2, T1, T2, Con1, Con2 (see CreateNewDesignSeq_FL.m in SeqPresentation )
R = load( [ dirseq '/' SubjName '/' SubjName '_ContextOdd_SequenceReverse.mat']) ; % gives S1, S2, T1, T2, Con1, Con2 (see SeqPresentation files)

for i_b = 1:numel(Expe)
    
    flag_reverse = Reverse(i_b);
    
    switch Expe(i_b)
        case 1
            switch flag_reverse
                case 1
                    seqcode = A.T1;
                case 2
                    seqcode = R.T1;
            end
            
        case 2
            
            switch flag_reverse
                case 1
                    seqcode = A.T2;
                case 2
                    seqcode = R.T2;
            end
    end
    
    
    file_out = [ dirseq '/' SubjName '/' SubjName '_ContextOdd_bloc' num2str(i_b) '.sce'];
    ContextOdd_write_sce(flag_reverse, seqcode, file_out)
    
end

file_out = [ dirseq '/' SubjName '/' SubjName '_ContextOdd_Expe.mat'];
save(file_out, 'Expe', 'Reverse');
