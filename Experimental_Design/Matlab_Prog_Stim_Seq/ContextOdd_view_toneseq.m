function [  ] = ContextOdd_view_toneseq(A,R,exp)


% A = no reverse, with  fields S1, S2, T1, T2, Con1, Con2 (see CreateNewDesignSeq_FL.m in SeqPresentation )
% B = reverse
% exp = information about bloc conditions, exp.Reverse, exp.Expe with
% 1=r varies, and 2= p varies

figure;
for i_b = 1:numel(exp.Expe)
    flag_reverse = exp.Reverse(i_b);
    subplot(2,2,i_b);
     switch exp.Expe(i_b)
        case 1
            switch flag_reverse
                case 1
                    Code = A.Con1;
                case 2
                    Code = R.Con1;
            end
            
        case 2
            
            switch flag_reverse
                case 1
                    Code = A.Con2;
                case 2
                    Code = R.Con2;
            end
     end
     

     plot(Code(:,1), 'k-'); hold on
     plot(Code(:,2), 'r*-'); hold on
     plot(Code(:,3), 'm+-'); hold on
     title([ 'bloc ' num2str(i_b) ', reverse = ' num2str(flag_reverse) ', condition ' num2str(exp.Expe(i_b)) ] );
     
     
end


