function [  Cond, Tone ]= ContextOdd_find_cond(expe_code, reverse_code, A, R)

switch expe_code
    case 1
        switch reverse_code
            case 1
                Cond = A.Con1;
                Tone = A.T1;
            case 2
                Cond = R.Con1;
                Tone = R.T1;
        end
        
    case 2
        
        switch reverse_code
            case 1
                Cond = A.Con2;
                Tone = A.T2;
            case 2
                Cond = R.Con2;
                Tone = R.T2;
        end
end

end
