function [T1 , T2] = CreateNewDesignSeq_Move2Tone(S1, S2)

% S1, S2 are two sequences of chunk size created with CreateNewDesignSeq.m
% Conversion into oddball sequences
% 3 additionnal standards are added at the end to spare last events
% 
% FL, feb 2018


SeqC = S1;
T = [];
for i = 1:numel(SeqC)
  
    T =[T;ones(SeqC(i),1);2];
    
end  
T1 = [T; 1;1;1]; 


SeqC = S2;
T = [];
for i = 1:numel(SeqC)
  
    T =[T;ones(SeqC(i),1);2];
    
end  
T2 = [T; 1;1;1]; 



