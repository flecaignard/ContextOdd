function [ DataAvg ] = ContextOdd_contrast(SeqDir, SubjName, Data_Bloc, Reject_Bloc, EvCode_Bloc, ContrastName, varargin)


%% Inputs
% - SeqDir: path to sequence mat files created by CreateNewDesignSeq_FL.m in SeqPresentation )
% - SubjName: dubject name
% - Data_Bloc: cell of data matrix (one cell per bloc)
% - Reject_Bloc: cell of vector of rejected/accepted trials (one per bloc)
% - EvCode_Bloc: cell of event code vector across trials (one per bloc)
% - ContrastName: to date's available contrasts
%             % 'local_r_46'  :   contrast MMN(r+) vs. MMN(r-) on chunk 4 and 6  (predictability effect)
%             % 'local_p_all' :   contrast MMN(p+) vs. MMN(p) vs. MMN(p-) using every chunk , (2-3-4) vs. (4-5-6) vs. (6-7-8)
%             % 'local_p_46'  :   contrast MMN(p high) vs. MMN(p low) on chunks 4 and 6 
%             % 'global_rp_46':   interaction contrast the contextual effect (var_p vs. var_r) on chunks 4 and 6: local_r_46 vs. local_p_46
% 
% - varargin
%           - Pref_OutFile: /mypath/Subj_2-2Hz

%% event code  in EvCode
% d1-d2-d3-d4-d5
% d1 = condition: 1/2
% d2 = chunk: 2/../8
% d3 = r: 1/2 (r-,r+) <=> chunks 4:6 vs. [2:4 6:8]
% d4 = p:1/2/3 (p-,p,p+) <=> 2:4 vs. 4:6 vs. 6:8
% d5 = tone category: standard preceding a deviant=1, deviant=2

%% 
if nargin > 6
    SaveFlag = 1;
    
else
    SaveFlag = 0;
end


%% load expe info
SubjDir = [ SeqDir '/' SubjName ];
A = load( [ SubjDir '/' SubjName '_ContextOdd_Sequence.mat']) ; % gives S1, S2, T1, T2, Con1, Con2 (see CreateNewDesignSeq_FL.m in SeqPresentation )
R = load( [ SubjDir '/' SubjName '_ContextOdd_SequenceReverse.mat']) ; % gives S1, S2, T1, T2, Con1, Con2 (see SeqPresentation files)
exp  =load([ SeqDir '/' SubjName '/' SubjName '_ContextOdd_Expe.mat']);

switch ContrastName
    
    case 'local_r_46'
        % within condition 'var_r' (1), contrast MMN(chunk 4 and 6) across
        % r+ (r1) and r- (r2)
        
        DataAvg_Label = {'varr_r1.std.4' 'varr_r1.dev.4' ... % std and dev in 4-chunk in r- in condition var_r
            'varr_r1.std.6' 'varr_r1.dev.6' ...
            'varr_r2.std.4' 'varr_r2.dev.4' ...
            'varr_r2.std.6' 'varr_r2.dev.6' ...
            'varr_r1.std.46' 'varr_r1.dev.46' ... % std_avg(4,6) and dev_avg(4,6) in r+
            'varr_r2.std.46' 'varr_r2.dev.46' ...
            'varr_r1.mmn.46' 'varr_r2.mmn.46' ...
            'varr_r2VSr1.std.46'  'varr_r2VSr1.dev.46' ...
            'varr_r2VSr1.mmn.46' ...
            };
        
        EvList = [14121 14122 ... % std and dev in 4-chunk in r- in condition var_r with p=2
            16121 16122 ...
            14221 14222 ...
            16221 16222 ...
            ];
      

        DataAvg = [];
        for i_e =1:numel(EvList)
            DataE = [];
            for i_b=1:numel(Data_Bloc)
                h_e = find(EvCode_Bloc{i_b} == EvList(i_e));
                if ~isempty(h_e)
                    h_ok = find(Reject_Bloc{i_b}(h_e)==0);
                    DataE = [DataE;Data_Bloc{i_b}(h_e(h_ok),:) ];
                end
            end
            DataAvg(i_e,:) =  mean(DataE,1); % ep for each sound type, per condition           
            
        end
        
        % pool 4-6 for standards in r-
        DataAvg(9,:)=mean(DataAvg([1 3]',:),1);
        % pool 4-6 for deviants in r-
        DataAvg(10,:)=mean(DataAvg([2 4]',:),1);
        % pool 4-6 for standards in r+
        DataAvg(11,:)=mean(DataAvg([5 7]',:),1);
        % pool 4-6 for deviants in r+
        DataAvg(12,:)=mean(DataAvg([6 8]',:),1);
        % 'varr_r1.mmn.46'
        DataAvg(13,:)=DataAvg(10,:) - DataAvg(9,:);
        % 'varr_r2.mmn.46'
        DataAvg(14,:)=DataAvg(12,:) - DataAvg(11,:);
        
        % 'varr_r2vsr1.std.46'
        DataAvg(15,:)=DataAvg(11,:) - DataAvg(9,:);
        % 'varr_r2vsr1.dev.46'
        DataAvg(16,:)=DataAvg(12,:) - DataAvg(10,:);
        % 'varr_r2vsr1.mmn.46'
        DataAvg(17,:)=DataAvg(14,:) - DataAvg(13,:);
        
        
       
     case 'local_p_all'
        % within condition 'var_p' (2), contrast MMN across p-(2-3-4),
        % p(4-5-6) and p+(6-7-8)
        
        DataAvg_Label = {'varp_p1.std.2' 'varp_p1.dev.2' ... % std and dev in 2-chunk in p- in condition var_p
            'varp_p1.std.3' 'varp_p1.dev.3' ...
            'varp_p1.std.4' 'varp_p1.dev.4' ...
            'varp_p2.std.4' 'varp_p2.dev.4' ...
            'varp_p2.std.5' 'varp_p2.dev.5' ...
            'varp_p2.std.6' 'varp_p2.dev.6' ...
            'varp_p3.std.6' 'varp_p3.dev.6' ...
            'varp_p3.std.7' 'varp_p3.dev.7' ...
            'varp_p3.std.8' 'varp_p3.dev.8' ...
            'varp_p1.std.234' 'varp_p1.dev.234' ...
            'varp_p2.std.456' 'varp_p2.dev.456' ...
            'varp_p3.std.678' 'varp_p3.dev.678' ...
            'varp_p1.mmn.234' ...
            'varp_p2.mmn.456' ...
            'varp_p3.mmn.678'...
            };
        
        EvList = [22111 22112 ... % std and dev in 2-chunk in r- in condition var_p with p=1=p-
           23111 23112 ...
           24111 24112 ...
           24121 24122 ... % std and dev in 4-chunk in r- in condition var_p with p=2=pn
           25121 25122 ...
           26121 26122 ...
           26131 26132 ... % std and dev in 6-chunk in r- in condition var_p with p=3=p+
           27131 27132 ...
           28131 28132 ...
            ];
        
        DataAvg = [];
        for i_e =1:numel(EvList)
            DataE = [];
            for i_b=1:numel(Data_Bloc)
                h_e = find(EvCode_Bloc{i_b} == EvList(i_e));
                if ~isempty(h_e)
                    h_ok = find(Reject_Bloc{i_b}(h_e)==0);
                    DataE = [DataE;Data_Bloc{i_b}(h_e(h_ok),:) ];
                end
            end
            DataAvg(i_e,:) =  mean(DataE,1); % ep for each sound type, per condition
            
        end
        
        % 'varp_p1.std.234'
        DataAvg(19,:)=mean(DataAvg([1:2:5]',:),1);
        % 'varp_p1.dev.234'
        DataAvg(20,:)=mean(DataAvg([2:2:6]',:),1);
        % 'varp_p2.std.456'
        DataAvg(21,:)=mean(DataAvg([7:2:11]',:),1);
        % 'varp_p2.dev.456'
        DataAvg(22,:)=mean(DataAvg([8:2:12]',:),1);
        % 'varp_p3.std.678'
        DataAvg(23,:)=mean(DataAvg([13:2:17]',:),1);
        % 'varp_p3.dev.678'
        DataAvg(24,:)=mean(DataAvg([14:2:18]',:),1);
        % 'varp_p1.mmn.234'
        DataAvg(25,:)=DataAvg(20,:) - DataAvg(19,:);
        % 'varp_p2.mmn.456'
        DataAvg(26,:)=DataAvg(22,:) - DataAvg(21,:);
        % 'varp_p3.mmn.678'
        DataAvg(27,:)=DataAvg(24,:) - DataAvg(23,:);
        
        
        
    case 'local_p_46'
        % within condition 'var_p' (2), contrast MMN(p high) vs. MMN(p low) vs. MMN(p-)on chunks 4 and 6, MMN(high,6) + MMN(high,4) - MMN(low,6) -MMN(low,4)
        
        DataAvg_Label = { 'varp_p1.std.4' 'varp_p1.dev.4' ...
            'varp_p2.std.4' 'varp_p2.dev.4' ...
            'varp_p2.std.6' 'varp_p2.dev.6' ...
            'varp_p3.std.6' 'varp_p3.dev.6' ...
            'varp_plow.std.46' 'varp_plow.dev.46' 'varp_plow.mmn.46'...
            'varp_phigh.std.46' 'varp_phigh.dev.46' 'varp_phigh.mmn.46'...
            'varp_phighVSplow.std.46' 'varp_phighVSplow.dev.46' 'varp_phighVSplow.mmn.46' ...
            };
        
        EvList = [ 24111 24112 ...
            24121 24122 ... % std and dev in 4-chunk in r- in condition var_p with p=2=pn
            26121 26122 ...
            26131 26132 ... % std and dev in 6-chunk in r- in condition var_p with p=3=p+
              ];
          
          DataAvg = [];
          for i_e =1:numel(EvList)
              DataE = [];
              for i_b=1:numel(Data_Bloc)
                  h_e = find(EvCode_Bloc{i_b} == EvList(i_e));
                  if ~isempty(h_e)
                      h_ok = find(Reject_Bloc{i_b}(h_e)==0);
                      DataE = [DataE;Data_Bloc{i_b}(h_e(h_ok),:) ];
                  end
              end
              DataAvg(i_e,:) =  mean(DataE,1); % ep for each sound type, per condition
              
          end
        % 'varp_plow.std.46'
        DataAvg(9,:)=mean(DataAvg([1 5]',:),1);
        % 'varp_plow.dev.46'
        DataAvg(10,:)=mean(DataAvg([2 6]',:),1);
        % 'varp_plow.mmn.46'
        DataAvg(11,:) = DataAvg(10,:) - DataAvg(9,:);
        % 'varp_phigh.std.46'
        DataAvg(12,:)=mean(DataAvg([3 7]',:),1);
        % 'varp_phigh.dev.46'
        DataAvg(13,:)=mean(DataAvg([4 8]',:),1);
        % 'varp_phigh.mmn.46'
        DataAvg(14,:) = DataAvg(13,:) - DataAvg(12,:);
        % 'varp_phighVSplow.std.46' 
        DataAvg(15,:) = DataAvg(12,:) - DataAvg(9,:);
        % 'varp_phighVSplow.dev.46'
        DataAvg(16,:) = DataAvg(13,:) - DataAvg(10,:);
        % 'varp_phighVSplow.mmn.46'
        DataAvg(17,:) = DataAvg(14,:) - DataAvg(11,:);
        
    case 'global_rp_46' %    contrast the contextual effect (var_p vs. var_r) on chunks 4 and 6: local_r_46 vs. local_p_46
        
        DataAvg_Label = {'varr_r1.std.4' 'varr_r1.dev.4' ... % std and dev in 4-chunk in r- in condition var_r
            'varr_r1.std.6' 'varr_r1.dev.6' ...
            'varr_r2.std.4' 'varr_r2.dev.4' ...
            'varr_r2.std.6' 'varr_r2.dev.6' ...
            'varp_p1.std.4' 'varp_p1.dev.4' ...  % std and dev in 4-chunk in p- in condition var_p
            'varp_p2.std.4' 'varp_p2.dev.4' ...
            'varp_p2.std.6' 'varp_p2.dev.6' ...
            'varp_p3.std.6' 'varp_p3.dev.6' ...
            'varr_r1.std.46' 'varr_r1.dev.46' ... % std_avg(4,6) and dev_avg(4,6) in r+
            'varr_r2.std.46' 'varr_r2.dev.46' ...
            'varr_r1.mmn.46' 'varr_r2.mmn.46' ...
            'varp_plow.std.46' 'varp_plow.dev.46' ...
            'varp_phigh.std.46' 'varp_phigh.dev.46' ...
            'varp_plow.mmn.46'  'varp_phigh.mmn.46'...
            'varrVSvarp_highVSlow.std.46' ...
            'varrVSvarp_highVSlow.dev.46' ...
            'varrVSvarp_highVSlow.mmn.46' ...
            };
        
        EvList = [14121 14122 ... % std and dev in 4-chunk in r- in condition var_r with p=2
            16121 16122 ...
            14221 14222 ...
            16221 16222 ...
            24111 24112 ...% std and dev in 4-chunk in r- in condition var_p with p=2=pn
            24121 24122 ...
            26121 26122 ...
            26131 26132 ...
            ];
        
        DataAvg = [];
        for i_e =1:numel(EvList)
            DataE = [];
            for i_b=1:numel(Data_Bloc)
                h_e = find(EvCode_Bloc{i_b} == EvList(i_e));
                if ~isempty(h_e)
                    h_ok = find(Reject_Bloc{i_b}(h_e)==0);
                    DataE = [DataE;Data_Bloc{i_b}(h_e(h_ok),:) ];
                end
            end
            DataAvg(i_e,:) =  mean(DataE,1); % ep for each sound type, per condition
            
        end
        % 'varr_r1.std.46' 
        DataAvg(17,:)=mean(DataAvg([1 3]',:),1);
        % 'varr_r1.dev.46' 
        DataAvg(18,:)=mean(DataAvg([2 4]',:),1);
        % 'varr_r2.std.46'
       DataAvg(19,:)=mean(DataAvg([5 7]',:),1);
        % 'varr_r2.dev.46' ...
        DataAvg(20,:)=mean(DataAvg([6 8]',:),1);
        % 'varr_r1.mmn.46' 
        DataAvg(21,:) = DataAvg(18,:) - DataAvg(17,:);
        % 'varr_r2.mmn.46' ...
        DataAvg(22,:) = DataAvg(20,:) - DataAvg(19,:);
        % 'varp_plow.std.46'
        DataAvg(23,:)=mean(DataAvg([9 13]',:),1);
        % 'varp_plow.dev.46' ...
        DataAvg(24,:)=mean(DataAvg([10 14]',:),1);
        % 'varp_phigh.std.46'
        DataAvg(25,:)=mean(DataAvg([11 15]',:),1);
        % 'varp_phigh.dev.46'
        DataAvg(26,:)=mean(DataAvg([12 16]',:),1);
        % 'varp_plow.mmn.46'
        DataAvg(27,:) = DataAvg(24,:) - DataAvg(23,:);
        % 'varp_phigh.mmn.46'...
        DataAvg(28,:) = DataAvg(26,:) - DataAvg(25,:);
        % 'varrVSvarp_highVSlow.std.46'
        DataAvg(29,:) = (DataAvg(19,:)  - DataAvg(17,:) ) - (DataAvg(25,:) - DataAvg(23,:));
        % 'varrVSvarp_highVSlow.dev.46' ...
        DataAvg(30,:) = (DataAvg(20,:)  - DataAvg(18,:) ) - (DataAvg(26,:) - DataAvg(24,:));
        % 'varrVSvarp_highVSlow.mmn.46' ...
        DataAvg(31,:) = (DataAvg(22,:)  - DataAvg(21,:) ) - (DataAvg(28,:) - DataAvg(27,:));
        
        
    otherwise
        fprintf('contrast not found \n');
        return       
        
        
end

%% Ep files
if SaveFlag
    
    EP = varargin{7};
    Pref_OutFile = EP.PrefOutFile;
    Fech = EP.Fech;
    Nsens = EP.Nsens;
    
    FileInit_ep = EP.Init_ep;
    
    [HEADER1,  HEADER2, DATA, CHANNELS] = ep2mat(FileInit_ep);
    elecdat_ind_REF = HEADER2.v_Elec ; %
    Nprestim = HEADER2.s_Nb_Sample_PreStim;
    hdr1_eventcode=123;
    hdr2_nbeventaver=123;
    
    
    
    
    
    % reshape DataAvg columns into sensor epochs

   
    
     
    
   
    for i_f = 1:numel(DataAvg_Label)
        FileName = [Pref_OutFile '_'  DataAvg_Label{i_f} '.p'];
        if ~exist(FileName, 'file')
            % mat2ep
            mat2ep (FileName, hdr1_eventcode, Fech, Nprestim, elecdat_ind_REF, hdr2_nbeventaver,DataAvg{i_f});
            
        else
            
            fprintf('file %s already exists and will NOT be  oevrwritten\n', FileName);
            
        end
    end
        

end

