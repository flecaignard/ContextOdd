function [fx] = f_BinomTone_v1(x,P,u,in) %(x,P,u,~)

% Computes VB update rules for hidden states sufficient statistics
%
% [fx] = f_BinomWithForgetting(x,P,u,in)
%
% This is the one-step Markovian update rule for the posterior sufficient
% statistics in an oddball paradigm (see Ostwald et al. 2012)
%
% IN:
%   - x: states
%   - P: the perceptual model parameter vector, ie. P = [tau]
%   - u: the trial input
%   - in: options set in options.inF
%
% OUT:
%   - fx: the updated posterior sufficient statistics (having observed u).

%% Model parameters P
% P = tau; % forgetting parameter. The larger, the smaller forgetting
%
%% Model states x
% mu = probability to have a deviant (1)
% Ut = current obs
% p( Ut = 1) = mu
% p( Ut = 0) = 1-mu

% x = parameters of the beta distribution on mu
% x(1) = number of observed deviants  (given the size of memeory, defined by tau)
% x(2) = number of observed stds  
%
%
% First trial starts with:
% x(1) = 1  - Apr
% x(2) = 1  -  Bpr
% x(3) = 1  - Apo
% x(4) = 1  -  Bpo

%% Model parameters
tau  =P(1); 

%% Model states
Apr=x(3);
Bpr=x(4);
% Apo=x(1);
% Bpo=x(2);
% 

%% Forgetting weight
fw = exp(-1/tau);
%fw=1;%
%% Up-dating the sufficient statistics of the state posterior distribution
Apo = u + fw*Apr;
Bpo = (1-u) + fw*Bpr;

%% Conditional posteriors on states
fx(1) = Apr;
fx(2) = Bpr;
fx(3) = Apo;
fx(4) = Bpo;
