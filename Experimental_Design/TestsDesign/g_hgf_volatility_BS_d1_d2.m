function [gx] = g_hgf_volatility_BS_d1_d2(x,P,~,~)


gx=zeros(3,1);

h0 = P(1);
h1 = P(2);

mu1_po = x(1);
mu2_pr  = x(7);     
sigma2_pr  = exp(x(8));
mu2_po  = x(2);     
sigma2_po  = exp(x(3));
expmu3 = x(12);

% Bayesian surprise on x2 distribution

KLD = 0.5*log(sigma2_po/sigma2_pr) + 0.5*(mu2_po^2 + mu2_pr^2 - 2*mu2_po*mu2_pr + sigma2_pr)/sigma2_po - 0.5;
gx(1) = h0 + h1*KLD;

%% First -level error (delta1 in Mathys 2011, unweighted)

d1 = mu2_pr; % mu1_po - sgm(mu2_po);
gx(2) = d1;


%% Second -level error (delta2 in Mathys 2011, unweighted)

d2 = (sigma2_po + power((mu1_po - mu2_pr),2)) / (sigma2_pr + expmu3);

gx(3) = d2;


