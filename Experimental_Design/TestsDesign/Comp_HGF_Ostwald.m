function Comp_HGF_Ostwald(U)

flag_plot_in = 0;
HGFinit = [0 0 0 0 0]'; % [mu1_0 mu2_0 log(sa2_0) mu3_0 log(sa3_0)], 

Prob = 1 ; % useless

TH = {[1.4 -2.2 0.5]; ...%triplet [ka om th]
    [0.5 -2 0.5]; ...
    [1 -3 0.7]; ...
    }
TH = {[3 -8 0.5]; ...%triplet [ka om th]
    [3 -6 0.5]; ...
    [3 -4 0.5]; ...
    }


figure;
set(gcf,'color','white');

subplot(2,3,1);
HGFpar = TH{1 }; 
try, clear x; end
[Y,x] = TestingHGF_ModelInversion_PlotLR(Prob, U, HGFpar, HGFinit,flag_plot_in);
plot(sgm(x(2,:)),'-r'); hold on;
plot(U,'.g');
title(['[ ka om th ] = ' num2str(HGFpar(1)) ' ' num2str(HGFpar(2)) '  ' num2str(HGFpar(3))]) ;
    

subplot(2,3,2);
HGFpar = TH{2}; 
try, clear x; end
[Y,x] = TestingHGF_ModelInversion_PlotLR(Prob, U, HGFpar, HGFinit,flag_plot_in);
plot(sgm(x(2,:)),'-r');hold on;
plot(U,'.g');
title(['[ ka om th ] = ' num2str(HGFpar(1)) ' ' num2str(HGFpar(2)) '  ' num2str(HGFpar(3))]) ;


subplot(2,3,3);
HGFpar = TH{3}; 
try, clear x; end
[Y,x] = TestingHGF_ModelInversion_PlotLR(Prob, U, HGFpar, HGFinit,flag_plot_in);
plot(sgm(x(2,:)),'-r');hold on;
plot(U,'.g');
title(['[ ka om th ] = ' num2str(HGFpar(1)) ' ' num2str(HGFpar(2)) '  ' num2str(HGFpar(3))]) ;



subplot(2,3,4);
tau = 20;
try, clear x; end
[Y,x] = TestingOstwald_ModelInversion(Prob, U, tau,flag_plot_in);
mu = x(3,:) ./ (x(3,:) + x(4,:));
plot(mu,'-r');hold on;
plot(U,'.g');
title(['tau = ' num2str(tau)]);
    
subplot(2,3,5);
tau = 10;
try, clear x; end
[Y,x] = TestingOstwald_ModelInversion(Prob, U, tau,flag_plot_in);
mu = x(3,:) ./ (x(3,:) + x(4,:));
plot(mu,'-r');hold on;
plot(U,'.g');
title(['tau = ' num2str(tau)]);


subplot(2,3,6);
tau = 5;
try, clear x; end
[Y,x] = TestingOstwald_ModelInversion(Prob, U, tau,flag_plot_in);
mu = x(3,:) ./ (x(3,:) + x(4,:));
plot(mu,'-r');hold on;
plot(U,'.g');
title(['tau = ' num2str(tau)]);

   
