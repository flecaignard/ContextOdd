function [gx] = g_hgf_volatility(x,P,~,~)

h0 = P(1);
h1 = P(2);

Mpr  = x(7);     
Vpr  = exp(x(8));
Mpo  = x(2);     
Vpo  = exp(x(3));


KLD = 0.5*log(Vpo/Vpr) + 0.5*(Mpo^2 + Mpr^2 - 2*Mpo*Mpr + Vpr)/Vpo - 0.5;

gx = h0 + h1*KLD;
%gx = KLD;
