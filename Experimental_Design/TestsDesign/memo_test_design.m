%____-------------------------------------------------------------____%
%____-------------------------------------------------------------____%
%
% Tests HGF for our new paradigm
%
%____-------------------------------------------------------------____%
%____-------------------------------------------------------------____%

% jan. 2018

dirvba='/sps/cermep/cermep/experiments/DCM/VBA/VBA-toolbox-master';
dirlm='/sps/cermep/cermep/experiments/DCM/manip2/tests_design';

addpath(genpath(dirvba))
addpath(dirlm)


%% Familiarisation HGF, using Mathys sequence in Mathys et al., 2011 (Figure 5)

% sequence of Mathys 2011, Figure 5
Prob = [0.5*ones(1,100) 0.8*ones(1,25)  0.2*ones(1,25) 0.8*ones(1,25)  0.2*ones(1,25) 0.8*ones(1,25)  0.2*ones(1,25) 0.5*ones(1,75)];
nt = length(Prob);
Umat= zeros(1,nt);
for k = 1:nt
    Umat(k) = binornd(1,Prob(k));
end
U = Umat;
save([dirlm '/SeqMathys2011.mat'], 'U', 'Prob'); clear U;




load([dirlm '/SeqMathys2011.mat']); 
Umat = U;
% ref, figure5
HGFpar = [1.4 -2.2 0.5] ; %triplet [ka om th]
HGFinit = [0 0 0 0 0]'; % [mu1_0 mu2_0 log(sa2_0) mu3_0 log(sa3_0)], 
TestingHGF_ModelInversion_PlotLR(Prob, Umat, HGFpar, HGFinit);


% parameters from Mathys 2014
HGFpar = [1 -3 0.7] ; %triplet [ka om th]
HGFinit = [0 0 0 0 0]'; % [mu1_0 mu2_0 log(sa2_0) mu3_0 log(sa3_0)], 
TestingHGF_ModelInversion_PlotLR(Prob, Umat, HGFpar, HGFinit);

% diff Mathys2011 vs. Mathys2014
% in 2011, LR2 increases after the volatility episode while it decreases  in 2014
% due to omega only
% hence, the lower om, the more flexible LR2 (goes back to stability when
% volatility episode is over)

%% see how LR evolves between 5-1-5-1 .... and (2:8) chnunks
U1 = repmat([0 0 0 0 0 1] , 1, 100);


%% Create sound sequence
% S1 <=> p constant, SI varies
% S2 <=> p varies, SI stable

[S1 , S2] = CreateNewDesignSeq;
U1 = Chunk2Stims(S1);
U2 = Chunk2Stims(S2);
[S3 , S4] = CreateNewDesignSeq; % reverse manually in code
U3 = Chunk2Stims(S3);
% U4 = Chunk2Stims(S4);
% test with U3 = U1 inverted : low-hig-low becomes hgh-low-high ...




%% Simulate an HGF
% [Y1, x1] = TestingHGF_ModelInversion_FL(S1);
% [Y2, x2] = TestingHGF_ModelInversion_FL(S2);


% HGFpar = [1.4 -2.2 0.5] ; %triplet [ka om th]
HGFpar = [1  0  0.5] ;  %  dans les choux tres vite: LR2, BS
HGFpar = [1  -2 0.5] ;  %  ok au d?but, puis choux
HGFpar = [1 -3 0.7] ; % dans les choux plus tard,


HGFpar = [0.5 -2 0.5] ;  %  marche pas mal


Prob = U1.*0 + 0.17;
HGFinit = [0 0 0 0 0]'; % [mu1_0 mu2_0 log(sa2_0) mu3_0 log(sa3_0)], 
TestingHGF_ModelInversion_PlotLR(Prob, U1, HGFpar, HGFinit);

TestingHGF_ModelInversion_PlotLR(Prob, U2, HGFpar, HGFinit);



%% Simulate Otswald model (with tau forgetting)
% aim = see the influence of tau on mu learning, and compare with HGF mu2
% => temporal integration of the HGF (under specific ka,om, th)

tau=10;
flag_plot = 1;
[Y,x] = TestingOstwald_ModelInversion(Prob, U1, tau, flag_plot)
[Y,x] = TestingOstwald_ModelInversion(Prob, U2, tau)


% compare trajectories of mu= prob deviant estimates
Comp_HGF_Ostwald(U1)
Comp_HGF_Ostwald(U2)
Comp_HGF_Ostwald(U3)



%% Sequences for Maeva, March 1st
[S1 , S2] = CreateNewDesignSeq()




