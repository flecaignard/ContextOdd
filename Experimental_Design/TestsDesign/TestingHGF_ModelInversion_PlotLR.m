function [Y,x] = TestingHGF_ModelInversion_PlotLR(Prob, U, HGFpar, HGFinit, flag_plot)


% HGFpar = triplet [ka om th]
% HGFinit = [mu1_0 mu2_0 sa2_0 mu3_0 sa3_0]

%% Simulating the data with a 3-level HGF

% nr trials
nt = length(U);

fname   = @f_VBvolatile0_FL;
gname   = @g_hgf_volatility_BS_d1_d2;
%gname   = @g_VBvolatile0;


%%  state init ---------
% 10 states, 1-5 <=> posteriors, 6-10 <=> priors
% mu1 = x(1);
% mu2 = x(2);
% sa2 = x(3);
% mu3 = x(4);
% sa3 = x(5);

% debug FL: add x(11) = LR at the third level (for display issue)

x0 = repmat(HGFinit,2,1);
x0 = [x0;0; 0 ]; % x11 and x12 (see f_VBvolatile0_FL.m )

%%  evol par --------- P = theta  = [ka;om;th]
inF.kaub = 1;
inF.thub = 1;
inF.rf = 1;
% we want P = [ka;om;th] = HGFpar , with theta as input in  simulateNLSS such
% that:
% ka = inF.lev2 * sgm(theta(1), inF.kaub);
% th = sgm(theta(3), inF.thub);
% om = theta(2);

theta(1) = 0;
inF.lev2 = 2 * HGFpar(1); %  0.1; % 3rd level (volatility learning)

theta(2) = HGFpar(2);

if HGFpar(3)==0
    theta(3) = 0;
else
    a = HGFpar(3) ;
    theta(3) = log( a / (1-a));
end
% check-in
KA = inF.lev2 * sgm(theta(1), inF.kaub);
TH = sgm(theta(3), inF.thub);
OM = theta(2); 
disp(['want: [ ka om th ] = ' num2str(HGFpar(1)) ' ' num2str(HGFpar(2)) '  ' num2str(HGFpar(3)) ]);
disp(['get : [ ka om th ] = ' num2str(KA) ' ' num2str(OM) '  ' num2str(TH) ', with theta = [ ' num2str(theta(1)) ' ' num2str(theta(2)) ' '  num2str(theta(3)) ' ], and inF.lev2 = ' num2str(inF.lev2)  ]);

%%  obs par ---------
phi = [0;1]; % inverse temperature & bias
inG.respmod = 'taylor';

options.binomial = 0;
options.inF = inF;
options.inG = inG;
options.skipf = zeros(1,nt);
options.skipf(1) = 1;



%% simul
[Y,x,~,~,~,~] = simulateNLSS(nt,fname,gname,theta,phi,U,Inf,Inf,options,x0);


%%  Display as in Mathys 2011

if flag_plot
    figure; set(gcf,'color','white');
    K=3;
    subplot(K,1,1)
    plot(x(4,:),'r-'); hold on
    % plot(exp(x(11,:)),'b-');
    ylim([-1 3]);
    title(['[ ka om th ] = ' num2str(KA) ' ' num2str(OM) '  ' num2str(TH) ]);
    ylabel('mu3');
    
    subplot(K,1,2);
    plot(x(2,:),'-r'); hold on
    % plot(exp(x(3,:)),'b-');
    ylim([-4 4]);
    title(['C.I. [ mu1 mu2 s2 mu3 s3 ] = ' num2str(HGFinit(1)) ' ' num2str(HGFinit(2)) ' '  num2str(exp(HGFinit(3))) ' ' num2str(HGFinit(4)) ' ' num2str(exp(HGFinit(5))) ' ']);
    ylabel('mu2');
    
    
    subplot(K,1,3);
    plot(Prob,'k-');
    hold on
    plot(sgm(x(2,:)),'-r');
    plot(U,'.g');
    plot(Y(1,:), '.b');
    ylabel('sgm(mu2)');
    ylim([-0.2 1.2]);
    
    
    figure; set(gcf,'color','white');
    K2 = 4;
    subplot(K2,1, 1 )
    plot(exp(x(11,:)),'m-'); ylabel(['LR3' ]);
    subplot(K2,1, 2 )
    plot(Y(3,:), '.b');ylabel(['delta2' ]);
    subplot(K2,1, 3 )
    plot(exp(x(3,:)),'m-'); ylabel(['LR2' ]);
    subplot(K2,1, 4 )
    plot(Y(2,:), '.b');ylabel(['delta1' ]);
end


