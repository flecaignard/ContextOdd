function Stim = Chunk2Stims(S)

Nc = length(S);
Ns = sum(S) + Nc;

Stim = zeros(1,Ns);
Chunk = zeros(1,Ns);
CS = cumsum(S);

% Stim(CS) = 1;

for nr_S = 1:numel(S)
    Stim(CS(nr_S) + nr_S) = 1;
    try
        Chunk(CS(nr_S-1) + nr_S:CS(nr_S) + nr_S) = S(nr_S);
    end
    
end



% plot chunk / stim
figure;
set(gcf,'color','white');
plot(Stim, 'b.'); hold on
plot(Chunk, 'k-*');

