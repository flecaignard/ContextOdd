function [Y,x] = TestingOstwald_ModelInversion(Prob, U, tau, flag_plot)

%% Inputs
% Prob: vector of deviant probability value across trials
% U = binary sequence (1/0) of dev and std , resp.
% tau = forgetting parameter as defined in Ostwald et al., 2012
% flag_plot: for display purposes
%
%
%% Outputs
% Y = vector of Bayesian surprise across trials
% x = hidden state, standard and deviants counts


%% Prep data

nt = length(U);% nr of  trials


%%  Evol 
fname = @f_BinomTone_v1;
 % First trial starts with:
 % x(1) = 1  - Apr
 % x(2) = 1  -  Bpr
 % x(3) = 1  - Apo
 % x(4) = 1  -  Bpo
 x0 = [1 1 1 1]'; % Initial state values
 theta = tau; % [log(tau)];
 inF = [];
 
 %%  Obs 
 gname = @g_Learning_BS_Beta_v1;
 phi = [0; 1]; % Linear regression parameter h0 (constant) and h1 (1st order)
 inG.respmod = 'taylor';

 %% VBA options
 
options.binomial = 0; % ??
options.inF = inF;
options.inG = inG;
options.skipf = zeros(1,nt);
options.skipf(1) = 1;

%% simul
[Y,x,~,~,~,~] = simulateNLSS(nt,fname,gname,theta,phi,U,Inf,Inf,options,x0);



%%  Display 
if iscell(flag_plot) % new version
    if isempty(flag_plot)
        % do nothing
    else
        figure; set(gcf,'color','white');
        mu = x(3,:) ./ (x(3,:) + x(4,:));
       
        subplot(2,1,1);
        plot(Prob,'k-');
        hold on
        plot(U,'.g');
        if ismember('mu', flag_plot)
            plot(mu,'-r');
        end
        plot(Prob,'k-');
        title(['tau = ' num2str(tau)])
        
        subplot(2,1,2);      
        plot(U,'.g'); hold on
        if ismember('BS', flag_plot)
            plot(Y,'bo');
        end
        title(['tau = ' num2str(tau)])
    end
    
    
else
    
    if flag_plot
        mu = x(3,:) ./ (x(3,:) + x(4,:));
        figure; set(gcf,'color','white');
        subplot(2,1,1);
        plot(Prob,'k-');
        hold on
        plot(mu,'-r');
        plot(U,'.g');
        title(['tau = ' num2str(tau)]);
        
        
        subplot(2,1,2);
        plot(Y,  '.b');
        ylabel('mu');
        % ylim([-0.2 1.2]);
    end
    
end




