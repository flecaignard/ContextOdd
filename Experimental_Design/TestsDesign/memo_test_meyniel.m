%____-------------------------------------------------------------____%
%____-------------------------------------------------------------____%
%
% Tests Meyniel 
%
%____-------------------------------------------------------------____%
%____-------------------------------------------------------------____%

% feb. 2018

dirmey='/sps/cermep/cermep/experiments/DCM/manip2/Meyniel_MinimalTransitionProbsModel-master';
dirmat='/sps/cermep/cermep/experiments/DCM/manip2/tests_design';

addpath(genpath(dirmey))
addpath(dirmat)


% test ToyExample HMM, TransitionProba
[ s, gen_p1, gen_p1g2, gen_p2g1 ] = GenRandSeq([150, 100, 50], [1/3, 2/3; 1/3 1/2; 1/2, 1/2]);
figure;
plot(s, 'k+');
hold on
plot(gen_p1, 'g-');
plot(gen_p1g2, 'r.-');
plot(gen_p2g1, 'm.-');



% oddball seq: we want p1g2 = 1 and p1 = 0.8
% this gives p2g1 = (1/p1) -1;
p1g2 = 1;
p1 = 5/6;
p =[p1g2 1/p1-1  ]; % [p(1|2)  p(2|1)]
[ s, gen_p1, gen_p1g2, gen_p2g1 ] = GenRandSeq(100, p); % test oddball
figure;
plot(s, 'k+-');
hold on
plot(gen_p1, 'g-');
plot(gen_p1g2, 'r.-');
plot(gen_p2g1, 'm.-');

length(find(s==2))


