function [Y,x] = TestingHGF_ModelInversion_FL(S)


%% Simulating the experimental design
% (i.e. the dynamics of the (hidden) probability of getting a face or a
% house)
if isempty(S)
    Prob = [0.8*ones(1,40) 0.2*ones(1,30) 0.5*ones(1,25) 0.7*ones(1,40) 0.4*ones(1,30) 0.8*ones(1,25) 0.4*ones(1,30) 0.7*ones(1,25) 0.5*ones(1,40) 0.2*ones(1,35)];
    nt = length(Prob);
    U  = zeros(1,nt);
    for k = 1:nt
        U(k) = binornd(1,Prob(k));
    end
else
    U = Chunk2Stims(S);
    nt = length(U);
end


%% Simulating the data with a 3-level HGF
fname   = @f_VBvolatile0;
gname   = @g_hgf_volatility;
%gname   = @g_VBvolatile0;

theta = [0;-2;0];
phi = [0;1]; % inverse temperature & bias
x0 = repmat([0;0;0;0;0],2,1);
inF.lev2 = 0; %  0.1; % 3rd level (volatility learning)
inF.kaub = 1;
inF.thub = 1;
inF.rf = 1;
inG.respmod = 'taylor';
options.binomial = 0;
options.inF = inF;
options.inG = inG;
options.skipf = zeros(1,nt);
options.skipf(1) = 1;




[Y,x,~,~,~,~] = simulateNLSS(nt,fname,gname,theta,phi,U,Inf,Inf,options,x0);
Z = Y;
% Z(Z==1) = 1 + 0.1;
% Z(Z==0) = -0.1;


% 




KA = inF.lev2 * sgm(theta(1), inF.kaub);
TH = sgm(theta(3), inF.thub);
OM = theta(2); 
% Display
figure; set(gcf,'color','white');
subplot(1,2,1);
% plot(sgm(x(2,:)),'-k');
ylim([-0.2 1.2]);
hold on
% plot(U,'.r');
% plot(Z,'.b');
% plot(x(4,:),'m-');
% plot(x(3,:),'g-');
plot(x(5,:),'y-');
title(['[ ka om th ] = ' num2str(KA) ' ' num2str(OM) '  ' num2str(TH) ]);
subplot(1,2,2);
plot(1:length(S),S,'or','MarkerSize',10,'MarkerFaceColor','r');
ylim([0 10]);
title('chunk seq.');



%% Parameter estimation

% Priors



% Model inversion