function [gx] = g_Learning_BS_Beta_v1(x,P,~,~)

%
% IN:
%   - x: none
%   - P: the response model parameter vector
%   - u: the current input to the observer
%   - in: further quantities handed to the function
%
% OUT:
%   - gx: the predicted physiological output
%
% disp([ '--------------------------------Obs =' num2str(u)  ])
% for i=1:numel(x)
%     
%     disp(num2str(x(i)));
% end

Apr=x(1);
Bpr=x(2);
Apo=x(3);
Bpo=x(4);

Spr = Apr + Bpr;
Spo = Apo + Bpo;

BS = log(gamma(Spr)/gamma(Spo)) ...
    + log(gamma(Apo)/gamma(Apr)) + log(gamma(Bpo)/gamma(Bpr)) ...
    + (Apr-Apo)*(psi(Apr)-psi(Spr)) ...
    + (Bpr-Bpo)*(psi(Bpr)-psi(Spr));

% disp([ 'Obs = ' num2str(u)  '   A =' num2str(Apo)  '   B =' num2str(Bpo)  '   BS =' num2str(BS)   ]);  


h0 = P(1); % 1st regression parameter (constant)
h1 = P(2); % 2nd regression parameter (1st order)

gx = h0 + h1*BS;

