function [S1 , S2] = CreateNewDesignSeq()

% Passive oddball auditory paradigm made of 2 conditions presented
% in seperated blocs. We use a deviance in sound frequency.
%
% One bloc type manipulates probabilistic uncertainty (1), while the
% second bloc type manipulates environmental uncertainty (2)
%
% (1): the deviance probability does not change but the variaiblity in
% the number of standard tones preceding a deviant increases
%
% (2): the deviance probability does change over time but the variability
% in the number of standard tones preceding a deviant remains stable
%
% F. Lecaignard and J. Mattout - 09 / 12 / 2017

SOA  = 610; % ms

%% Cycle types
C1 = 4:6;
C2 = [2:4 6:8];
C3 = 2:4;
C4 = 6:8;

%% Bloc type (1)
n1   = length(C1);
n2   = length(C2);
Nc1  = 6; % # consecutive similar cycles
Nc2  = 3;
Nalt = 7; % ~15 minutes

S1 = [];
for k = 1:Nalt
    for i = 1:Nc1
        if isempty(S1)
            I = randperm(n1);
            S1 = [S1 C1(I)];
        else
            out = 1;
            while out
                I = randperm(n1);
                if C1(I(1)) ~= S1(end)
                    out = 0;
                    S1 = [S1 C1(I)];
                end
            end
        end
    end
    for j = 1:Nc2
        out = 1;
        while out
            I = randperm(n2);
            if C2(I(1)) ~= S1(end)
                out = 0;
                S1 = [S1 C2(I)];
            end
        end
    end
end

Nstim = sum(S1) + length(S1);
BlocDur = Nstim*SOA/(1000*60);

disp(['This blocs made of ' num2str(Nalt) ' alternations and will last ' num2str(BlocDur) ' minutes']);
disp(['Nombre de sons total ' num2str(Nstim)]);

figure;
set(gcf,'color','white');
subplot(2,2,1);
hist(S1,2:8);
title('Bloc type 1: dist. of chunk size');
subplot(2,2,2);
plot(1:length(S1),S1,'or','MarkerSize',10,'MarkerFaceColor','r');
ylim([0 10]);
title('Bloc type 1: chunk seq.');


%% Bloc type (2)
n = n1;
Nc = Nc2;
%Nalt = 5;

S2 = [];
for k = 1:Nalt
    for i = 1:2*Nc
        if isempty(S2)
            I = randperm(n);
            S2 = [S2 C1(I)];
        else
            out = 1;
            while out
                I = randperm(n);
                if C1(I(1)) ~= S2(end)
                    out = 0;
                    S2 = [S2 C1(I)];
                end
            end
        end
    end
    for j = 1:Nc
        out = 1;
        while out
            I = randperm(n);
            if C3(I(1)) ~= S2(end)
                out = 0;
                S2 = [S2 C3(I)];
            end
        end
    end
    for j = 1:Nc
        out = 1;
        while out
            I = randperm(n);
            if C4(I(1)) ~= S2(end)
                out = 0;
                S2 = [S2 C4(I)];
            end
        end
    end
end

Nstim = sum(S2) + length(S2);
BlocDur = Nstim*SOA/(1000*60);

disp(['This blocs made of ' num2str(Nalt) ' alternations and will last ' num2str(BlocDur) ' minutes']);
disp(['Nombre de sons total ' num2str(Nstim)]);

subplot(2,2,3);
hist(S2,2:8);
title('Bloc type 2: dist. of chunk size');
subplot(2,2,4);
plot(1:length(S2),S2,'og','MarkerSize',10,'MarkerFaceColor','g');
ylim([0 10]);
title('Bloc type 2: chunk seq.');



