function  [LR] =  HGF_computeLR3(x, ka, om)

% computes LR at the third level of the HGF, from Mathys 2011 equations  (Eq. 30)
% x = nr_states*nr_trials



for i=1:size(x,1)
    w2 = exp(ka*x(4,i)+om)/(x(3,i)+exp(ka*x(4,i)+om));
    r2 = (exp(ka*x(4,i)+om)-x(3,i))/(exp(ka*x(4,i)+om)+x(3,i));
    pe2 = (fx(3)+(fx(2)-x(2,i))^2)/(exp(ka*x(4,i)+om)+x(3,i)) -1;
    fx(5) = 1/(pi3h + .5*ka^2*w2*(w2+r2*pe2));
    if fx(5) <= 0
        LR(i) = NaN;
    else
        LR(i) .5*ka*w2*fx(5);
    end
end

